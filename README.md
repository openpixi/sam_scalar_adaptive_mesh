# Scalar adaptive mesh
The scalar adaptive mesh (sam) project aims to implement simple scalar field toy models
on a multi-resolution lattice, which results in "predetermined" adaptive mesh sampling.

### Dependencies

The dependencies are listed in the `setup.cfg` file.

The `mrlattice` package with target `mrlattice[jupyter] >= 0.3` is required.
It can be installed following the instructions in:  
https://gitlab.com/openpixi/mrlattice

### How-To

Running the `src/sam/examples` requires that the `src/` folder is in the `PYTHONPATH`.
Use
```bash
$ export PYTHONPATH="/path/to/src:$PYTHONPATH"
$ python /path/to/src/sam/examples/scripts/{name_of_example}.py
```
