"""Test properties and behavior of ScalarField objects."""

import unittest
from itertools import cycle
from unittest import mock

from mrlattice.lattice.generate import LatticeArray

from sam.sim import ScalarField
from sam.solvers import Solver
from sam.solvers.refinement import BorderTypes

# overwrites test prefix in sam.solvers.refinement.borders debug types
mock.patch.TEST_PREFIX = "test_"


@mock.patch.object(ScalarField, "evolve")
class TestScalarFieldIterator(unittest.TestCase):
    """Test for correct iteration through instances and slices."""

    def assert_called_only_with(
        self, called_mock: mock.MagicMock, call_list: list[mock._Call]
    ):
        """Helper for exactly comparing mock.call_list with given."""
        self.assertListEqual(called_mock.mock_calls, call_list)

    def setUp(self):
        self.la_mock = mock.MagicMock(spec_set=LatticeArray)
        self.a_mock = mock.MagicMock()
        self.dt_mock = mock.MagicMock()
        self.solver_mock = mock.MagicMock(spec_set=Solver)
        self.border_types_mock = mock.MagicMock(spec_set=BorderTypes)
        self.cusps_mock = mock.MagicMock()
        self.dunder_init_mock = mock.MagicMock(return_value=None)
        with mock.patch.object(ScalarField, "__init__", new=self.dunder_init_mock):
            # this will create an empty ScalarField instance
            self.field = ScalarField(
                self.la_mock,
                self.a_mock,
                self.dt_mock,
                self.solver_mock,
                self.border_types_mock,
                self.cusps_mock,
            )
        # set all attrs required for iteration manually
        self.field.next_iter_res = cycle(((1,),))
        self.field.lat_array = mock.MagicMock()
        self.field.lat_array.ts_resolution_list.return_value = (1,)
        self.MAX_T = 42
        self.field.max_t = self.MAX_T
        self.field.t = 0

    def test_invalid(self, _):
        """Test invalid argument to getitem."""
        with self.assertRaises(TypeError):
            _ = self.field[None]

    def test_idx(self, mock_evolve: mock.MagicMock):
        """Test indexing with integers."""

        with self.assertRaises(IndexError, msg="negative indices"):
            _ = self.field[-1]

        with self.assertRaises(IndexError, msg="idx >= max t"):
            _ = self.field[self.MAX_T]

        call_cnt = self.MAX_T // 2
        with self.subTest(msg=f"indexing at {call_cnt}"):
            t = self.field[call_cnt]
            self.assertEqual(t, call_cnt)
            self.assert_called_only_with(mock_evolve, [mock.call()] * call_cnt)
            mock_evolve.reset_mock()

            # repeated same idx is allowed
            t = self.field[call_cnt]
            self.assertEqual(t, call_cnt)
            self.assert_called_only_with(mock_evolve, [])
            mock_evolve.reset_mock()

        with self.assertRaises(IndexError, msg="idx below current t"):
            _ = self.field[call_cnt - 1]

        call_cnt_incr = min(3, call_cnt)
        with self.subTest(msg=f"inrementing by {call_cnt_incr}"):
            t = self.field[call_cnt + call_cnt_incr]
            self.assertEqual(t, call_cnt + call_cnt_incr)
            # now only the additional incr calls, because of mock_reset
            self.assert_called_only_with(mock_evolve, [mock.call()] * call_cnt_incr)

    def test_slice_errors(self, mock_evolve: mock.MagicMock):
        """Test indexing with slices."""

        with self.assertRaises(IndexError, msg="negative start"):
            *_, _ = self.field[-1:]

        with self.assertRaises(IndexError, msg="negative stop"):
            *_, _ = self.field[:-1]

        with self.subTest(msg="start => stop"):
            ts = list(self.field[self.MAX_T :])
            self.assertListEqual(ts, [])
            ts = list(self.field[self.MAX_T + 1 :])
            self.assertListEqual(ts, [])

        with self.assertRaises(IndexError, msg="stop > max t"):
            _ = self.field[: self.MAX_T + 1]

        with self.assertRaises(IndexError, msg="negative stepping"):
            *_, _ = self.field[::-1]

        with self.subTest(msg="stepping too large"):
            s = self.field.t
            ts = list(self.field[s :: self.MAX_T])
            self.assertListEqual(ts, [s])

        mock_evolve.assert_not_called()

    def test_slice_step_1(self, mock_evolve: mock.MagicMock):
        """Test slicing with stepping of 1."""

        res = list(self.field[:])
        self.assertListEqual(res, list(range(1, self.MAX_T, 1)), msg="returned steps")
        self.assert_called_only_with(mock_evolve, [mock.call()] * (self.MAX_T - 1))

    def test_slice_step_2(self, mock_evolve: mock.MagicMock):
        """Test slicing with stepping of 1."""

        res = list(self.field[::2])
        self.assertListEqual(res, list(range(1, self.MAX_T, 2)), msg="returned steps")
        self.assert_called_only_with(mock_evolve, [mock.call()] * (self.MAX_T - 1))

    def test_slice_step_5(self, mock_evolve: mock.MagicMock):
        """Test slicing with stepping of 1."""

        res = list(self.field[::5])
        self.assertListEqual(res, list(range(1, self.MAX_T, 5)), msg="returned steps")
        self.assert_called_only_with(mock_evolve, [mock.call()] * (self.MAX_T - 1))

    def test_slice_start(self, mock_evolve: mock.MagicMock):
        """Test slicing with start > current t."""
        start = self.MAX_T // 2
        res = list(self.field[start:])
        self.assertListEqual(res, list(range(start, self.MAX_T, 1)))
        self.assert_called_only_with(mock_evolve, [mock.call()] * (self.MAX_T - 1))

    def test_slice_end(self, mock_evolve: mock.MagicMock):
        """Test slicing with end < max t."""
        end = self.MAX_T // 2
        res = list(self.field[:end])
        self.assertListEqual(res, list(range(1, end, 1)))
        self.assert_called_only_with(mock_evolve, [mock.call()] * (end - 1))

    def test_bare_iterator(self, mock_evolve: mock.MagicMock):
        """Test bare iteration through all elements."""

        res = list(self.field)
        self.assertListEqual(res, list(range(1, self.MAX_T, 1)), msg="returned steps")
        self.assert_called_only_with(mock_evolve, [mock.call()] * (self.MAX_T - 1))

        res = list(self.field)
        self.assertListEqual(res, [], "depleted iterator")

    def test_bare_after_slice(self, mock_evolve: mock.MagicMock):
        """Test bare iterator after slicing earlier."""
        stop = self.MAX_T // 2
        *_, _ = self.field[:stop]
        res = list(self.field)
        self.assertListEqual(res, list(range(stop, self.MAX_T, 1)))
        self.assert_called_only_with(mock_evolve, [mock.call()] * (self.MAX_T - 1))

    def test_bare_after_slice_manual(self, mock_evolve: mock.MagicMock):
        """Test bare iterator after slicing and manual next() calls."""
        stop = self.MAX_T // 2
        *_, _ = self.field[:stop]
        n = 2
        for _ in range(n):
            _ = next(self.field)
        res = list(self.field)
        self.assertListEqual(res, list(range(stop + n, self.MAX_T, 1)))
        self.assert_called_only_with(mock_evolve, [mock.call()] * (self.MAX_T - 1))

    def test_slice_after_bare(self, _):
        """Iterating old slice reference after depletion."""
        old_iter = self.field[:]
        _ = list(self.field)
        with self.assertRaises(IndexError):
            _ = next(old_iter)


if __name__ == "__main__":
    unittest.main()
