"""Tests for `sam.solvers.refinement.border_base.py`."""

import unittest

from sam.solvers.refinement.border_base import TempData


class TestTempData(unittest.TestCase):
    """Test implementation of `TempData`."""

    # pylint: disable=protected-access

    def setUp(self):
        self.t0_idx = 3
        self.t0 = (list(range(5)), self.t0_idx)
        self.t1_idx = 2
        self.t1 = (list(chr(i) for i in range(97, 103)), self.t1_idx)
        self.ts_elems = (self.t0, self.t1)
        self.ts = TempData(values=self.ts_elems)

    def test_init(self):
        """Test initialization."""

        with self.subTest(args="sequence"):
            ts = TempData(values=(self.t0, self.t1))
            self.assertSequenceEqual(ts._slice, self.ts._slice)

        with self.subTest(args="positional"):
            ts = TempData(self.t0, self.t1)
            self.assertSequenceEqual(ts._slice, self.ts._slice)

        with self.subTest(args="sequence and positional"):
            ts = TempData(self.t0, values=(self.t0, self.t1))
            self.ts.insert(0, self.t0)
            self.assertSequenceEqual(ts._slice, self.ts._slice)

    def test_getitem(self):
        """Test item retrieval."""

        with self.subTest(key="int"):
            self.assertEqual(self.ts[0], self.t0[0][self.t0_idx])
            self.assertEqual(self.ts[1], self.t1[0][self.t1_idx])

        with self.subTest(key="negative int"):
            self.assertEqual(self.ts[-1], self.t1[0][self.t1_idx])
            self.assertEqual(self.ts[-2], self.t0[0][self.t0_idx])

        with self.subTest(key="slice"):
            self.assertEqual(
                self.ts[:], (self.t0[0][self.t0_idx], self.t1[0][self.t1_idx])
            )
            self.assertEqual(self.ts[2:], tuple())

        with self.subTest(key="invalid"):
            with self.assertRaises(IndexError):
                _ = self.ts[3]

            with self.assertRaises(TypeError):
                _ = self.ts["a"]

    def test_setitem(self):
        """Test item assignment."""

        with self.subTest(key="int"):
            comp = list(self.t0[0])
            comp[self.t0_idx] = None
            self.ts[0] = None
            self.assertIs(self.ts[0], None)
            self.assertListEqual(self.ts._slice[0][0], comp)

        with self.subTest(key="invalid"):
            with self.assertRaises(TypeError):
                self.ts[:] = None

            with self.assertRaises(TypeError):
                self.ts["a"] = None

    def test_delitem(self):
        """Test item deletion."""

        item = self.ts[0]
        self.assertIn(item, self.ts)
        self.assertIn(self.t0, self.ts._slice)
        del self.ts[0]
        self.assertNotIn(item, self.ts)
        self.assertNotIn(self.t0, self.ts._slice)

    def test_len(self):
        """Test `len(s)`."""
        self.assertEqual(len(self.ts), len(self.ts_elems))

    def test_insert(self):
        """Test item insertion `s.insert()`."""
        item = self.ts[0]
        del self.ts[0]
        self.assertNotIn(item, self.ts)
        self.ts.insert(1, self.t0)
        self.assertIn(item, self.ts)
        self.assertEqual(self.ts.index(item), 1)


if __name__ == "__main__":
    unittest.main()
