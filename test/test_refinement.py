"""Test setup to verify `sam.solvers.refinement` module setup."""

import unittest

from sam.solvers.refinement import BorderTypes


class TestBorderTypesDict(unittest.TestCase):
    """Test custom `BorderTypes` frozen dict.

    Goes through all documented behavior of dicts for BorderTypes.
    """

    # pylint: disable=protected-access,E1137,E1101,E1138

    def setUp(self):
        self.keys = tuple(BorderTypes.KEYS)
        self.init_dict = dict((k, i) for i, k in enumerate(self.keys))
        self.valid = self.keys[0]
        self.validkw = {self.valid: self.valid}
        self.bt = BorderTypes(self.init_dict)

    def test_init(self):
        """Test all initializer signatures."""
        # pylint: disable=too-many-statements

        with self.subTest(args="no"):
            with self.assertRaises(KeyError):
                bt = BorderTypes()

        with self.subTest(args="kwargs"):
            with self.subTest(kwargs="valid"):
                bt = BorderTypes(**self.init_dict)
                self.assertDictEqual(bt._dict, self.init_dict)
            with self.subTest(kwargs="invalid"):
                with self.assertRaises(KeyError):
                    bt = BorderTypes(invalid=None)

        with self.subTest(args="mapping"):
            with self.subTest(keys="valid"):
                bt = BorderTypes(self.init_dict)
                self.assertDictEqual(bt._dict, self.init_dict)
            with self.subTest(keys="invalid"):
                with self.assertRaises(KeyError):
                    bt = BorderTypes({"invalid": None})

        with self.subTest(args="iterable"):
            with self.subTest(keys="valid"):
                bt = BorderTypes((k, v) for k, v in self.init_dict.items())
                self.assertDictEqual(bt._dict, self.init_dict)
            with self.subTest(keys="invalid"):
                with self.assertRaises(KeyError):
                    bt = BorderTypes((k, k) for k in range(4))

        with self.subTest(args="mapping and kwargs"):
            with self.subTest(keys="valid"):
                bt = BorderTypes(self.init_dict, **self.validkw)
                control_bt = self.init_dict.copy()
                control_bt[self.valid] = self.valid
                self.assertDictEqual(bt._dict, control_bt)
            with self.subTest(keys="invalid"):
                with self.assertRaises(KeyError):
                    bt = BorderTypes({"invalid": None}, **self.validkw)
                with self.assertRaises(KeyError):
                    bt = BorderTypes(self.init_dict, invalid=self.valid)
                with self.assertRaises(KeyError):
                    bt = BorderTypes({"invalid1": None}, invalid2=self.valid)

        with self.subTest(args="iterable and kwargs"):
            with self.subTest(keys="valid"):
                bt = BorderTypes(
                    ((k, v) for k, v in self.init_dict.items()), **self.validkw
                )
                control_bt = self.init_dict.copy()
                control_bt[self.valid] = self.valid
                self.assertDictEqual(bt._dict, control_bt)
            with self.subTest(keys="invalid"):
                with self.assertRaises(KeyError):
                    bt = BorderTypes(((k, k) for k in range(4)), SCTF=self.valid)
                with self.assertRaises(KeyError):
                    bt = BorderTypes(
                        ((k, v) for k, v in self.init_dict.items()), invalid=self.valid
                    )
                with self.assertRaises(KeyError):
                    bt = BorderTypes(((k, k) for k in range(4)), invalid=self.valid)

        with self.subTest(args="fromkeys classmethod"):
            with self.subTest(keys="valid"):
                bt = BorderTypes.fromkeys(self.init_dict.keys(), None)
                control_bt = dict.fromkeys(self.init_dict.keys(), None)
                self.assertDictEqual(bt._dict, control_bt)
            with self.subTest(keys="invalid"):
                with self.assertRaises(KeyError):
                    bt = BorderTypes.fromkeys(range(4), None)

    def test_iter(self):
        """Test using `iter(d)`."""
        self.assertListEqual(list(iter(self.bt)), list(iter(self.init_dict)))

    def test_reversed(self):
        """Test using `reversed(d)`."""
        with self.assertRaises(TypeError):
            _ = reversed(self.bt)

    def test_list(self):
        """Test using `list(d)`."""
        self.assertListEqual(list(self.bt), list(self.init_dict))

    def test_len(self):
        """Test using `len(d)`."""
        self.assertEqual(len(self.bt), len(self.init_dict))

    def test_key_access(self):
        """Test key access `d[key]` for (in)valid keys."""

        with self.subTest(msg="valid keys"):
            for k in self.init_dict:
                self.assertEqual(self.bt[k], self.init_dict[k])

        with self.subTest(msg="invalid key"):
            with self.assertRaises(KeyError):
                _ = self.bt["invalid"]

    def test_value_assignment(self):
        """Test assigning to (in)valid keys."""

        with self.subTest(msg="valid keys"):
            with self.assertRaises(TypeError):
                self.bt[self.valid] = None

        with self.subTest(msg="invalid key"):
            with self.assertRaises(TypeError):
                self.bt["invalid"] = None

    def test_key_membership(self):
        """Test `key in d` for (in)valid keys."""

        with self.subTest(msg="valid key"):
            self.assertTrue(self.valid in self.bt)
            self.assertFalse(self.valid not in self.bt)

        with self.subTest(msg="invalid key"):
            self.assertFalse("invalid" in self.bt)
            self.assertTrue("invalid" not in self.bt)

    def test_setdefault(self):
        """Test using `d.setdefault()` with (in)valid keys."""

        with self.subTest(msg="valid key"):
            with self.assertRaises(AttributeError):
                self.bt.setdefault(self.valid)

        with self.subTest(msg="invalid key"):
            with self.assertRaises(AttributeError):
                self.bt.setdefault("invalid")

    def test_clear(self):
        """Test using `d.clear()`."""

        with self.assertRaises(AttributeError):
            self.bt.clear()

    def test_copy(self):
        """Test using `d.copy()`."""
        self.assertDictEqual(self.bt.copy()._dict, self.bt._dict)
        self.assertIsInstance(self.bt, BorderTypes)

    def test_get(self):
        """Test using `d.get()` for (in)valid keys."""

        with self.subTest(msg="valid keys"):
            for k in self.init_dict:
                self.assertEqual(self.bt.get(k), self.init_dict.get(k))

        with self.subTest(msg="invalid key"):
            self.assertIsNone(self.bt.get("invalid"))

    def test_items(self):
        """Test using `d.items()`."""
        self.assertListEqual(list(self.bt.items()), list(self.init_dict.items()))

    def test_keys(self):
        """Test using `d.keys()`."""
        self.assertListEqual(list(self.bt.keys()), list(self.init_dict.keys()))

    def test_values(self):
        """Test using `d.values()`."""
        self.assertListEqual(list(self.bt.values()), list(self.init_dict.values()))

    def test_item_removal(self):
        """Test usage of `del` or any pop method for (in)valid keys."""

        with self.subTest(msg="del"):
            with self.assertRaises(TypeError):
                del self.bt[self.valid]

        with self.subTest(msg="pop"):
            with self.subTest(pop="valid key"):
                with self.assertRaises(AttributeError):
                    self.bt.pop(self.valid)
            with self.subTest(pop="invalid key"):
                with self.assertRaises(AttributeError):
                    self.bt.pop("invalid key")
            with self.subTest(pop="invalid with default"):
                with self.assertRaises(AttributeError):
                    self.bt.pop("invalid key", None)

        with self.subTest(msg="popitem"):
            with self.assertRaises(AttributeError):
                self.bt.popitem()

    def test_update(self):
        """Test using `d.update()` for (in)valid keys."""

        with self.assertRaises(AttributeError):
            self.bt.update(SCTF=None)
        with self.assertRaises(AttributeError):
            self.bt.update(invalid=None)


if __name__ == "__main__":
    unittest.main()
