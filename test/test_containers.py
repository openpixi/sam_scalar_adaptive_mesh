"""Tests for custom mapping container types.

Some tested types depend on others and a proper testing suite should
patch out these dependencies. However, as all components are part of the
same module, having tests for composite types depend on (and check) the
correct implementation of their dependencies is passable. This will make
interpreting failed tests harder when treating them independently.
"""

import unittest
from copy import deepcopy
from itertools import cycle

import numpy as np

from mrlattice.lattice.generate import RegionSlice, RegionTuple

from sam.containers import (
    NONDARR,
    Data,
    RegData,
    ResMap,
    ResRegData,
    ResRegMap,
    ResRegRegionTuple,
)

# pylint: disable=protected-access


class TestResMap(unittest.TestCase):
    """Test `ResMap` implementation."""

    def setUp(self, ref_iter=None):
        # pylint: disable=arguments-differ

        if ref_iter is None:
            self.REF_ITER = tuple((2 * i, f"{i:2b}") for i in range(4))
        else:
            self.REF_ITER = ref_iter

        self.REF = dict(self.REF_ITER)
        self.map = ResMap(self.REF)

    def test_init(self):
        """Test all initializer signatures."""

        with self.subTest(msg="mapping"):
            res_map = ResMap(self.REF)
            self.assertDictEqual(res_map._dict, self.REF)

        with self.subTest(msg="iterable"):
            res_map = ResMap(self.REF_ITER)
            self.assertDictEqual(res_map._dict, self.REF)

    def test_fromkeys(self):
        """Test using `fromkeys`."""
        REF = dict.fromkeys(self.REF, 42)
        res_map = ResMap.fromkeys(self.REF, 42)
        self.assertDictEqual(res_map._dict, REF)

    def test_iter(self):
        """Test iteration over elements."""
        self.assertListEqual(list(self.map), list(self.REF))

    def test_len(self):
        """Test calling `len`."""
        self.assertEqual(len(self.map), len(self.REF))

    def test_getitem(self):
        """Test indexing."""
        for key in self.REF:
            self.assertEqual(self.map[key], self.REF[key])

    def test_setitem(self):
        """Test item assignment."""
        for key in self.REF:
            self.REF[key] = f"{key:2d}"
            self.map[key] = f"{key:2d}"
            self.assertDictEqual(self.map._dict, self.REF)

    def test_delitem(self):
        """Test item deletion."""
        for key in tuple(self.REF):
            d = self.map[key]
            del self.REF[key]
            del self.map[key]
            self.assertDictEqual(self.map._dict, self.REF)
            self.assertNotIn(key, self.map)
            self.assertNotIn(d, self.map.values())


class TestResRegMap(TestResMap):
    """Test `ResRegMap` implementation."""

    def setUp(self, ref_iter=None):
        if ref_iter is None:
            ref_iter = tuple(
                (2 * i, [f"{k + 4*i:4b}" for k in range(4)]) for i in range(4)
            )

        super().setUp(ref_iter)
        self.map = ResRegMap(deepcopy(self.REF))

    def test_init(self):
        with self.subTest(msg="init"):
            super().test_init()

        with self.subTest(msg="validate fixture"):
            for res, val in self.REF.items():
                self.assertIsNot(self.map[res], val)

    def test_getitem(self):
        with self.subTest(msg="int"):
            super().test_getitem()

        with self.subTest(msg="int, int"):
            for res in self.REF:
                for reg, val in enumerate(self.REF[res]):
                    self.assertEqual(self.map[res, reg], val)

        with self.subTest(msg="int, slice"):
            for res in self.REF:
                val = self.map[res, 1::2]
                self.assertListEqual(val, self.REF[res][1::2])

    def test_setitem(self):
        with self.subTest(msg="int"):
            super().test_setitem()

        self.setUp()
        with self.subTest(msg="int, int"):
            for res in self.REF:
                for reg in range(len(self.REF[res])):
                    self.REF[res][reg] = f"{res:2d}"
                    self.map[res, reg] = f"{res:2d}"
                    self.assertDictEqual(self.map._dict, self.REF)

        self.setUp()
        with self.subTest(msg="int, slice"):
            for res in self.REF:
                self.REF[res][1::2] = ["XXX", "YYY"]
                self.map[res, 1::2] = ["XXX", "YYY"]
                self.assertDictEqual(self.map._dict, self.REF)

    def test_delitem(self):
        with self.subTest(msg="int"):
            super().test_delitem()

        self.setUp()
        with self.subTest(msg="int, int"):
            for res in self.REF:
                reg_cycle = cycle((0, -1))
                for _ in range(len(self.REF[res])):
                    reg = next(reg_cycle)
                    d = self.map[res, reg]
                    del self.REF[res][reg]
                    del self.map[res, reg]
                    self.assertDictEqual(self.map._dict, self.REF)
                    self.assertNotIn(d, self.map[res])

        self.setUp()
        with self.subTest(msg="int, slice"):
            for res in self.REF:
                del self.REF[res][1::2]
                del self.map[res, 1::2]
                self.assertDictEqual(self.map._dict, self.REF)


class TestResRegRegionTuple(unittest.TestCase):
    """Test `ResRegRegionTuple` added features."""

    def test_get_res_regions(self):
        """Test instance creation with classmethod and `RegionSlice`."""
        res_list = [2 ** i for i in range(0, 6, 2)]
        num_list = [2, 4, 7]
        reg_slice = RegionSlice(RegionTuple(a, n) for a in res_list for n in num_list)
        data = ResRegRegionTuple.from_region_slice(res_list, reg_slice)
        self.assertListEqual(list(data.keys()), res_list)
        count = ResMap((k, int()) for k in res_list)
        for reg in reg_slice:
            n = count[reg.a]
            count[reg.a] += 1
            self.assertIs(data[reg.a, n], reg)

        for reg_list in data.values():
            a = reg_list[0].a
            for reg in reg_list:
                self.assertEqual(reg.a, a)

            self.assertEqual(len(reg_list), count[a])


class TestData(unittest.TestCase):
    """Test `Data` implementation."""

    def setUp(self):
        self.arr_size = (10,)
        self.data_arr = np.empty(self.arr_size, dtype=np.float_)
        self.empty_arr = np.empty(self.arr_size, dtype=np.bool_)
        self.idx_arr = np.arange(self.arr_size[0], dtype=np.intp)
        self.rng = np.random.default_rng()
        self.rng.shuffle(self.idx_arr)

        self.data = Data(self.data_arr, self.empty_arr, self.idx_arr)

    def test_init(self):
        """Test correct initialization."""
        # be lazy, use self.setUp() result
        self.assertIs(self.data.arr, self.data_arr)
        self.assertIs(self.data.empty, self.empty_arr)
        self.assertIs(self.data.idx, self.idx_arr)

    def test_getitem_int(self):
        """Test correct indexing with int."""
        for i in range(self.arr_size[0]):
            self.assertEqual(self.data[i], self.data_arr[self.idx_arr[i]])

    def test_getitem_slice_any(self):
        """Test if indexing with slice or ndarray raises exception."""
        with self.assertRaises(NotImplementedError, msg="slice"):
            _ = self.data[:]

        with self.assertRaises(NotImplementedError, msg="ndarray"):
            _ = self.data[self.idx_arr[1::3]]

    def test_setitem_int(self):
        """Test setting items with int index."""
        target_arr = self.rng.random(self.arr_size, dtype=np.float_)
        for i in range(self.arr_size[0]):
            self.data[i] = target_arr[self.idx_arr[i]]

        self.assertTrue(np.array_equal(self.data.arr, target_arr))

    def test_setitem_slice(self):
        """Test setting items with slice index."""
        target_arr = self.rng.random(self.arr_size, dtype=np.float_)
        i_max = self.data.idx.shape[0] // 2
        self.data[:i_max] = target_arr[self.idx_arr[:i_max]]
        self.data[i_max:] = target_arr[self.idx_arr[i_max:]]
        self.assertTrue(np.array_equal(self.data.arr, target_arr))

        self.setUp()
        self.data[:] = target_arr[self.idx_arr[:]]
        self.assertTrue(np.array_equal(self.data.arr, target_arr))

        with self.assertRaises(ValueError, msg="shape mismatch"):
            target_arr.shape = (2, 5)
            self.data[:] = target_arr

    def test_setitem_any(self):
        """Test setting items with ndarray index."""
        target_arr = self.rng.random(self.arr_size, dtype=np.float_)
        index_arr = np.argsort(self.data.idx)
        self.data[index_arr] = target_arr
        self.assertTrue(np.array_equal(self.data.arr, target_arr))


class TestRegData(unittest.TestCase):
    """Test `RegData` implementation."""

    def setUp(self):
        self.iter_size = 5
        self.arr_size = (10,)
        self.data_arr = []
        self.empty_arr = []
        self.idx_arr = []
        for _ in range(self.iter_size):
            self.data_arr.append(np.empty(self.arr_size, dtype=np.float_))
            self.empty_arr.append(np.empty(self.arr_size, dtype=np.bool_))
            self.idx_arr.append(np.arange(self.arr_size[0], dtype=np.intp))
            self.rng = np.random.default_rng()
            self.rng.shuffle(self.idx_arr[-1])

        self.data = RegData(
            Data(data, empty, idx)
            for data, empty, idx in zip(self.data_arr, self.empty_arr, self.idx_arr)
        )

    def test_init(self):
        """Test initialization."""

        with self.subTest(msg="zero elements"):
            reg_data = RegData([])
            self.assertListEqual(reg_data._data, [])

        with self.subTest(msg="one element"):
            elem_data = [Data(self.data_arr[0], self.empty_arr[0], self.idx_arr[0])]
            reg_data = RegData(elem_data)
            self.assertListEqual(reg_data._data, elem_data)

        with self.subTest(msg="many elements"):
            elem_data = [
                Data(data, empty, idx)
                for data, empty, idx in zip(self.data_arr, self.empty_arr, self.idx_arr)
            ]
            reg_data = RegData(elem_data)
            self.assertListEqual(reg_data._data, elem_data)

    def test_getitem_int(self):
        """Test 1D int index."""
        for a in range(self.iter_size):
            data = self.data[a]
            self.assertIs(data.arr, self.data_arr[a])
            self.assertIs(data.empty, self.empty_arr[a])
            self.assertIs(data.idx, self.idx_arr[a])

    def test_getitem_slice(self):
        """Test 1D slice index."""
        reg_data = self.data[: self.iter_size // 2]
        self.assertIsInstance(reg_data, RegData)
        self.assertEqual(len(reg_data), self.iter_size // 2)

        for a, data in enumerate(reg_data):
            self.assertIs(data.arr, self.data_arr[a])
            self.assertIs(data.empty, self.empty_arr[a])
            self.assertIs(data.idx, self.idx_arr[a])

    def test_getitem_int_int(self):
        """Test 2D int, int index."""
        for a in range(self.iter_size):
            for i in range(self.arr_size[0]):
                self.assertEqual(self.data[a, i], self.data_arr[a][self.idx_arr[a][i]])

    def test_getitem_int_slice(self):
        """Test 2D int, slice index, raises from Data."""
        with self.assertRaises(NotImplementedError, msg="slice"):
            _ = self.data[0, :]

    def test_getitem_int_any(self):
        """Test 2D int, ndarray index, raises from Data."""
        with self.assertRaises(NotImplementedError, msg="ndarray"):
            _ = self.data[0, self.idx_arr[1::3]]

    def test_getitem_slice_any(self):
        """Test 2D slice, int index raises TypeError."""
        with self.assertRaises(TypeError):
            _ = self.data[:, 0]

    def test_setitem_int(self):
        """Test 1D int index assignment."""
        for n in range(self.iter_size):
            data = Data(
                self.rng.random(self.arr_size, dtype=np.float_),
                np.empty(self.arr_size, dtype=np.bool_),
                np.empty(self.arr_size, dtype=np.intp),
            )
            self.data[n] = data
            self.assertIs(self.data[n], data)

    def test_setitem_slice(self):
        """Test 1D slice index assignment."""
        num = 3
        data_list = [
            Data(
                self.rng.random(self.arr_size, dtype=np.float_),
                np.empty(self.arr_size, dtype=np.bool_),
                np.empty(self.arr_size, dtype=np.intp),
            )
            for _ in range(num)
        ]
        self.data[:num] = data_list
        for n in range(num):
            self.assertIs(self.data[n], data_list[n])

    def test_setitem_int_int(self):
        """Test 2D int, int index assignment."""
        target_arr = self.rng.random(self.arr_size, dtype=np.float_)
        for n in range(self.iter_size):
            for i in range(self.arr_size[0]):
                self.data[n, i] = target_arr[self.idx_arr[n][i]]

            self.assertTrue(np.array_equal(self.data[n].arr, target_arr))

    def test_setitem_int_slice(self):
        """Test 2D int, slice index assignment."""
        target_arr = self.rng.random(self.arr_size, dtype=np.float_)
        for n in range(self.iter_size):
            self.data[n, :] = target_arr[self.idx_arr[n][:]]
            self.assertTrue(np.array_equal(self.data[n].arr, target_arr))

    def test_setitem_int_any(self):
        """Test 2D int, ndarray index assignment."""
        target_arr = self.rng.random(self.arr_size, dtype=np.float_)
        for n in range(self.iter_size):
            index_arr = np.argsort(self.data[n].idx)
            self.data[n, index_arr] = target_arr
            self.assertTrue(np.array_equal(self.data[n].arr, target_arr))

    def test_setitem_slice_any(self):
        """Test 2D slice, any index assignment raises TypeError."""
        with self.assertRaises(TypeError):
            self.data[:, 0] = 1

    def test_delitem_int(self):
        """Test 1D int index item deletion."""
        for _ in range(self.iter_size):
            data = self.data[0]
            del self.data[0]
            self.assertNotIn(data, self.data)

    def test_delitem_slice(self):
        """Test 1D slice index item deletion."""
        data = self.data[0 : self.iter_size // 2]
        assert len(data) != 0
        for d in data:
            self.assertIn(d, self.data)
        del self.data[0 : self.iter_size // 2]
        for d in data:
            self.assertNotIn(d, self.data)

    def test_delitem_tuple(self):
        """Test ND tuple index raises TypeError."""
        with self.assertRaises(TypeError):
            del self.data[0, 0]

    def test_len(self):
        """Test calling `len()`."""
        self.assertEqual(len(self.data), self.iter_size)

    def test_insert(self):
        """Test inserting new `Data` object."""
        data = Data(
            self.rng.random(self.arr_size, dtype=np.float_),
            np.empty(self.arr_size, dtype=np.bool_),
            np.empty(self.arr_size, dtype=np.intp),
        )
        ins_idx = 3
        self.data.insert(ins_idx, data)
        self.assertEqual(len(self.data), self.iter_size + 1)
        self.assertIn(data, self.data)
        self.assertEqual(self.data.index(data), ins_idx)


class TestResRegData(unittest.TestCase):
    """Test `ResRegData` implementation."""

    # pylint: disable=too-many-public-methods

    def assert_data_is(self, d: Data, data, empty, idx):
        """Helper to test `Data` instance attributes equality."""
        self.assertIs(d.arr, data)
        self.assertIs(d.empty, empty)
        self.assertIs(d.idx, idx)

    def setUp(self):
        self.res = [2 ** i for i in range(5)]
        self.arr_size = (10,)
        self.data_res_map = ResMap(
            (i, np.empty(self.arr_size, dtype=np.float_)) for i in self.res
        )
        self.empty_res_map = ResMap(
            (i, np.empty(self.arr_size, dtype=np.bool_)) for i in self.res
        )
        tmp_idx_dict = {}
        self.num_regs = 2
        assert self.arr_size[0] % self.num_regs == 0
        for i in self.res:
            self.rng = np.random.default_rng()
            idx_arr = np.arange(self.arr_size[0], dtype=np.intp)
            self.rng.shuffle(idx_arr)
            idx_arr = idx_arr.reshape(
                (self.num_regs, self.arr_size[0] // self.num_regs)
            )
            tmp_idx_dict[i] = [idx_arr[0, :], idx_arr[1, :]]

        self.idx_res_reg_map = ResRegMap(tmp_idx_dict)

        self.data = ResRegData(
            self.data_res_map, self.empty_res_map, self.idx_res_reg_map
        )

    def test_init(self):
        """Test initialization."""
        with self.subTest(msg="no elements"):
            data = ResRegData(ResMap([]), ResMap([]), ResRegMap([]))
            self.assertDictEqual(data._dict, {})

        with self.subTest(msg="one element"):
            data = ResRegData(
                ResMap({1: self.data_res_map[1]}),
                ResMap({1: self.empty_res_map[1]}),
                ResRegMap({1: self.idx_res_reg_map[1]}),
            )
            self.assertListEqual(list(data.keys()), [1])
            rd = data[1]
            self.assertEqual(len(rd), self.num_regs)  # two regions per res
            for i in range(self.num_regs):
                self.assert_data_is(
                    rd[i],
                    self.data_res_map[1],
                    self.empty_res_map[1],
                    self.idx_res_reg_map[1][i],
                )

        with self.subTest(msg="selected no index arrays"):
            idx_changed = ResRegMap(self.idx_res_reg_map)
            del idx_changed[1, :]
            data = ResRegData(self.data_res_map, self.empty_res_map, idx_changed)
            self.assertNotIn(1, data)
            self.assertListEqual(self.res[1:], list(data))

        with self.subTest(msg="many elements"):
            # be lazy, use from self.setUp()
            self.assertListEqual(list(self.data.keys()), self.res)
            for r, rd in data.items():
                self.assertEqual(len(rd), self.num_regs)  # two regions per res
                for i in range(self.num_regs):
                    self.assert_data_is(
                        rd[i],
                        self.data_res_map[r],
                        self.empty_res_map[r],
                        self.idx_res_reg_map[r][i],
                    )

    def test_getitem_int(self):
        """Test 1D int index."""
        for r in self.data.keys():
            rd = self.data[r]
            t = (r,)
            self.assertIs(self.data[t], rd)
            self.assertIsInstance(rd, RegData)
            self.assertEqual(len(rd), self.num_regs)  # two regions per res
            for i in range(self.num_regs):
                self.assert_data_is(
                    rd[i],
                    self.data_res_map[r],
                    self.empty_res_map[r],
                    self.idx_res_reg_map[r][i],
                )

    def test_getitem_int_int(self):
        """Test 2D int, int, index."""
        for r in self.data.keys():
            for i in range(self.num_regs):
                d = self.data[r, i]
                self.assertIsInstance(d, Data)
                self.assert_data_is(
                    d,
                    self.data_res_map[r],
                    self.empty_res_map[r],
                    self.idx_res_reg_map[r][i],
                )

    def test_getitem_int_slice(self):
        """Test 2D int, slice index."""
        for r in self.data.keys():
            rd = self.data[r, : self.num_regs - 1]
            self.assertIsInstance(rd, RegData)
            self.assertEqual(len(rd), self.num_regs - 1)  # slice has less
            for i in range(self.num_regs - 1):
                self.assert_data_is(
                    rd[i],
                    self.data_res_map[r],
                    self.empty_res_map[r],
                    self.idx_res_reg_map[r][i],
                )

    def test_getitem_int_int_int(self):
        """Test 3D int, int, int index."""
        for r in self.data.keys():
            for i in range(self.num_regs):
                for k in range(self.arr_size[0] // self.num_regs):
                    self.assertEqual(
                        self.data[r, i, k],
                        self.data_res_map[r][self.idx_res_reg_map[r][i][k]],
                    )

    def test_getitem_int_int_slice(self):
        """Test 3D int, int, slice index raises Error."""
        with self.assertRaises(NotImplementedError, msg="slice"):
            _ = self.data[1, 0, :]

    def test_getitem_int_int_any(self):
        """Test 3D int, int, ndarray index raises Error."""
        with self.assertRaises(NotImplementedError, msg="ndarray"):
            _ = self.data[1, 0, self.idx_res_reg_map[1][0][1::3]]

    def test_getitem_int_slice_any(self):
        """Test 3D int, slice, any index raises Error."""
        with self.assertRaises(TypeError):
            _ = self.data[1, :, 0]

    def test_getitem_any_any_any_any(self):
        """Test 4D any, any, any, any index raises Error."""
        with self.assertRaises(ValueError):
            _ = self.data[1, 1, 1, 1]

    def test_setitem_int(self):
        """Test 1D int index assignment."""
        reg_data = self.data[1]
        for r in self.res:
            self.data[r] = reg_data
            self.assertIn(reg_data, self.data.values())
            self.assertIs(self.data[r], reg_data)
            self.assertIsInstance(self.data[r], RegData)

    def test_setitem_int_comma(self):
        """Test 1D int index assignment."""
        reg_data = self.data[1]
        for r in self.res:
            t = (r,)
            self.data[t] = reg_data
            self.assertIn(reg_data, self.data.values())
            self.assertIs(self.data[r], reg_data)
            self.assertIsInstance(self.data[r], RegData)

    def test_setitem_int_int(self):
        """Test 2D int, int index assignment."""
        data = self.data[1, -1]
        for r in self.res:
            # this loop is revered for .index to work
            for i in range(self.num_regs - 1, 0, -1):
                self.data[r, i] = data
                self.assertIn(data, self.data[r])
                self.assertEqual(self.data[r].index(data), i)
                self.assertIs(self.data[r, i], data)
                self.assertIsInstance(self.data[r, i], Data)

    def test_setitem_int_slice(self):
        """Test 2D int, slice index assignment."""
        reg_data = [self.data[1, 0]] * 2
        for r in self.res:
            self.data[r, :] = reg_data
            for i in range(self.num_regs):
                self.assertIs(self.data[r, i], reg_data[i])
                self.assertIsInstance(self.data[r, i], Data)

    def test_setitem_int_int_int(self):
        """Test 3D int, int, int index assignment."""
        target_arr = self.rng.random(self.arr_size, dtype=np.float_)
        for r in self.res:
            for i in range(self.num_regs):
                for k in range(self.arr_size[0] // self.num_regs):
                    self.data[r, i, k] = target_arr[self.idx_res_reg_map[r, i][k]]
            # right now all regions with same res share one data array
            self.assertTrue(np.array_equal(self.data[r, 0].arr, target_arr))

    def test_setitem_int_int_slice(self):
        """Test 3D int, int, slice index assignment."""
        target_arr = self.rng.random(self.arr_size, dtype=np.float_)
        for r in self.res:
            for i in range(self.num_regs):
                self.data[r, i, :] = target_arr[self.idx_res_reg_map[r, i]]
            # right now all regions with same res share one data array
            self.assertTrue(np.array_equal(self.data[r, 0].arr, target_arr))

    def test_setitem_int_int_any(self):
        """Test 3D int, int, ndarray index assignment."""
        target_arr = self.rng.random(self.arr_size, dtype=np.float_)
        for r in self.res:
            for i in range(self.num_regs):
                d = self.data[r, i]
                index_arr = np.argsort(d.idx)
                self.data[r, i, index_arr] = target_arr[
                    self.idx_res_reg_map[r, i][index_arr]
                ]
            # right now all regions with same res share one data array
            self.assertTrue(np.array_equal(self.data[r, 0].arr, target_arr))

    def test_setitem_int_slice_any(self):
        """Test 3D int, slice, any raises TypeError."""
        with self.assertRaises(TypeError):
            self.data[1, :, 0] = 0

    def test_setitem_any_any_any_any(self):
        """Test 4D any, any, any, any index assignment raises Error."""
        with self.assertRaises(ValueError):
            self.data[1, 1, 1, 1] = 1

    def test_delitem_int(self):
        """Test 1D int index deletion."""
        for r in self.res:
            d = self.data[r]
            del self.data[r]
            self.assertNotIn(r, self.data)
            self.assertNotIn(d, self.data.values())

    def test_delitem_int_int(self):
        """Test 2D int, int index deletion."""
        for r in self.res:
            for _ in range(self.num_regs - 1):
                d = self.data[r, 0]
                del self.data[r, 0]
                self.assertNotIn(d, self.data[r])

            del self.data[r, 0]
            self.assertNotIn(r, self.data)

    def test_delitem_int_slice(self):
        """Test 2D int, slice index deletion."""
        for r in self.res:
            d = self.data[r, -1]
            del self.data[r, :-1]
            self.assertListEqual(self.data[r]._data, [d])
            del self.data[r, :]
            self.assertNotIn(r, self.data)

    def test_delitem_any_any_any(self):
        """Test 3D index deletion raises ValueError."""
        with self.assertRaises(ValueError):
            del self.data[1, 0, 0]

    def test_iter(self):
        """Test calling `iter()`."""
        self.assertListEqual(list(self.data), self.res)

    def test_len(self):
        """Test calling `len()`."""
        self.assertEqual(len(self.data), len(self.res))


class TestNONDARRSingletons(unittest.TestCase):
    """Test `NONDARR` singleton implementation."""

    def setUp(self):
        NONDARR.cache_clear()

    def tearDown(self):
        NONDARR.cache_clear()

    def test_singleton(self):
        """Test returned instances are singleton."""
        for dtype in filter(lambda t: isinstance(t, str), np.sctypeDict):
            a = NONDARR(np.dtype(dtype))
            b = NONDARR(np.dtype(dtype))
            self.assertIs(a, b)


if __name__ == "__main__":
    unittest.main()
