#! /usr/bin/env python3

"""Script for diagonal border lattice testing.
Pulses placed at the border 4/2 on init.
"""

import os
from multiprocessing import Pool

from mrlattice.lattice.generate import LatticeArray
from mrlattice.lattice.configure import LatticeArrayScheme

from sam.solvers.refinement import BORDER_TYPES
from sam.init import single_gauss_pulse, single_caret_pulse

from worker import worker, return_product


# # # Parameters

PROCESSES = 8

# path to save output data; NOTE: linux path seperators used!
FILE = os.path.splitext(os.path.basename(__file__))[0]
SAVE_PATH = f"/tmp/{os.path.basename(os.path.dirname(__file__))}/{FILE}/"
if not os.path.exists(SAVE_PATH):
    os.makedirs(SAVE_PATH)

# save sim results as png or pickle lattice plots (huge file size!)
PICKLE_SIM = False

# save lattice plots (requires lots of RAM)
LATTICE_PLOTS = False

# sim config

DIR = 1  # pulse direction
A = 1.0  # t/x ratio
PPOS = 148  # position of pulse
MAX_SCALE = 5  # 2^x for scaling lattice

# configure initial pulse shapes
PULSES = {
    "gauss_cut_high": (
        single_gauss_pulse,
        {"mu": PPOS, "sigma": 8, "cutoff": 0.005, "amplitude": 1},
    ),
    "gauss_cut_low": (
        single_gauss_pulse,
        {"mu": PPOS, "sigma": 8, "cutoff": 0.001, "amplitude": 1},
    ),
    "caret": (
        single_caret_pulse,
        {"center": PPOS, "width": 16, "amplitude": 0.1},
    ),
}

# init scheme configs:
SCHEMES = {
    "ctr": "centered",
    "avg": "averaged",
}


# lattice configs:
def dftcr(scale):
    lat1 = LatticeArray(
        scheme=LatticeArrayScheme.FIXED_NUMBER_SCHEME,
        coarse_steps=2,
        finest_width=8 * scale,
        borders_width=51 * scale,
        buffer=37 * scale,
        repeat=10 * scale,
    )
    lat1.generate_array()
    return lat1


def dftcl(scale):
    lat1 = dftcr(scale)
    lat1.spatial_mirror()
    return lat1


def dctfr(scale):
    lat1 = LatticeArray(
        scheme=LatticeArrayScheme.FIXED_NUMBER_SCHEME,
        coarse_steps=2,
        finest_width=8 * scale,
        borders_width=69 * scale,
        buffer=28 * scale,
        repeat=10 * scale,
    )
    lat1.generate_array()
    lat1.spatial_mirror()
    lat1.temporal_mirror()
    return lat1


def dctfl(scale):
    lat = dctfr(scale)
    lat.spatial_mirror()
    return lat


LATTICES = {
    "dftcr": dftcr,
    "dftcl": dftcl,
    "dctfr": dctfr,
    "dctfl": dctfl,
}


if __name__ == "__main__":

    with Pool(PROCESSES, maxtasksperchild=1) as p:
        print(
            *p.starmap(
                worker,
                return_product(
                    SCHEMES,
                    PULSES,
                    LATTICES,
                    (MAX_SCALE,),
                    (A,),
                    (DIR,),
                    (BORDER_TYPES,),
                    (SAVE_PATH,),
                    (FILE,),
                    (LATTICE_PLOTS,),
                    (PICKLE_SIM,),
                ),
            ),
            sep="\n",
        )
