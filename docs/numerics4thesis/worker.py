"""Module for the worker loop of the numerics' setups."""

import gc
import pickle
import shutil
import os
from itertools import product
from typing import Callable, Iterable, Literal, TypeVar

import numpy as np

from mrlattice.lattice.generate import LatticeArray

from sam.solvers.refinement import BorderTypes
from sam.plotting import LatticePlot, GhostPlot, plt
from sam.sim import ScalarField
from sam.solvers.leapfrog import EvolveLeapfrog

from evaluation_ipynb.evaluate import DataNames, diff_vals, x_vals, timeslice_vals

_T = TypeVar("_T", bound=np.ndarray)


def return_product(schemes, pulses, lattices, *iterables):
    """Return iterator over all products of the configurations."""
    return product(
        schemes.items(),
        pulses.items(),
        lattices.items(),
        *iterables,
    )


def _prep_subpath(save_path, *args):
    subpath = save_path + "_".join(args)
    if os.path.exists(subpath):
        # clear any existing data
        shutil.rmtree(subpath)
    os.makedirs(subpath)
    os.makedirs(f"{subpath}/callbacks")

    return subpath


def worker(
    scheme: tuple[str, str],
    pulse: tuple[str, tuple[Callable, dict[str, float]]],
    lattice: tuple[str, Callable[[float], LatticeArray]],
    max_scale: int,
    A: float,
    DIR: Literal[1, -1],
    border: BorderTypes | tuple[str, BorderTypes],
    save_path: str,
    file: str,
    lattice_plots: bool = False,
    pickle_sim: bool = False,
    loop_callbacks: Iterable[Callable[[ScalarField, _T | None], _T]] = tuple(),
):
    """Run all scales for a setup config."""

    init_scheme_name, init_scheme = scheme
    pulse_name, (pulse_callable, pulse_args) = pulse
    lat_name, lat_callable = lattice
    if isinstance(border, tuple):
        border_name, border_types = border
        save_subpath = _prep_subpath(
            save_path, border_name, lat_name, init_scheme_name, pulse_name
        )
    else:
        border_types = border
        save_subpath = _prep_subpath(save_path, lat_name, init_scheme_name, pulse_name)

    pulse_profile = pulse_callable(**pulse_args)

    print("starting worker for", save_subpath, flush=True)
    message = f"{save_subpath}:\t"

    for scale in [int(2**i) for i in range(max_scale + 1)]:

        gc.collect()

        message += f"{scale}..."

        lat = lat_callable(scale)

        if lattice_plots:
            plot = LatticePlot(lat.array)
            ghost = GhostPlot(lat.array)

        sim = ScalarField(
            lat,
            (A,),
            1 / scale,
            EvolveLeapfrog(),
            border_types,
            lambda *x: None,
        )
        # initialize pulse
        getattr(sim, "init_cell_" + init_scheme)(pulse_profile, direction=DIR)

        # init plot's first time slice
        if lattice_plots:
            plot.update_field_vals(
                sim.t,
                sim.dd_now,
                sim.array[sim.t],
                sim.res_list,
                sim.res_list,
                sim.reg_by_res_now,
                sim.p_now,
            )
            for res in sim.res_list:
                ghost.update_field_vals(sim.t, sim.border_by_res[res * 2])

        # init callbacks
        clb_data = [clb(sim, None) for clb in loop_callbacks]

        # save starting profile
        d0 = DataNames(
            type=DataNames.DATA_TYPE,
            name=file,
            a=A,
            scale=scale,
            init=init_scheme,
            pulse=pulse_callable,
            params=list(pulse_args.values()),
            time=sim.t,
        )
        start_vals, start_norm = timeslice_vals(sim.array[sim.t], sim)
        np.savez_compressed(
            f"{save_subpath}/{d0}",
            start_x=x_vals(sim.array[sim.t], sim),
            start_vals=start_vals,
            start_norm=start_norm,
        )

        # time evolution
        for t in sim:
            if lattice_plots:
                plot.update_field_vals(
                    sim.t,
                    sim.dd_now,
                    sim.array[t],
                    sim.res_list,
                    sim.res_list,
                    sim.reg_by_res_now,
                    sim.p_now,
                )
                for res in sim.res_list:
                    ghost.update_field_vals(sim.t, sim.border_by_res[res * 2])

            for clb, data in zip(loop_callbacks, clb_data):
                clb(sim, data)

        # save end profile
        # save exact end profile and diff to sim
        x_vals_end = x_vals(sim.array[sim.t], sim)
        # diff values array for each region
        exact_x, exact_vals, diff = diff_vals(sim, x_vals_end, pulse_profile, DIR)

        d3 = DataNames(
            type=DataNames.DATA_TYPE,
            name=file,
            a=A,
            scale=scale,
            init=init_scheme,
            pulse=pulse_callable,
            params=list(pulse_args.values()),
            time=sim.t,
        )
        end_vals, end_norm = timeslice_vals(sim.array[sim.t], sim)
        np.savez_compressed(
            f"{save_subpath}/{d3}",
            end_x=x_vals_end,
            end_vals=end_vals,
            end_norm=end_norm,
            end_exact_x=np.concatenate(exact_x),
            end_exact_vals=np.concatenate(exact_vals),
            diff_vals=np.concatenate(diff),
        )

        # save callback results
        d6 = DataNames(
            type=DataNames.DATA_TYPE,
            name=f"{file}_callbacks",
            a=A,
            scale=scale,
            init=init_scheme,
            pulse=pulse_callable,
            params=list(pulse_args.values()),
            time=sim.t,
        )
        np.savez_compressed(
            f"{save_subpath}/callbacks/{d6}",
            **{clb.__name__: data for clb, data in zip(loop_callbacks, clb_data)},
        )

        if lattice_plots:
            # save plot data
            plot.prep_plot()
            plot.fig.suptitle(f"Values on scale 1/{scale}")
            d1 = DataNames(
                type=DataNames.PLOT_TYPE,
                name=file,
                a=A,
                scale=scale,
                init=init_scheme,
                pulse=pulse_callable,
                params=list(pulse_args.values()),
                time=sim.t,
            )
            if pickle_sim:
                with open(f"{save_subpath}/{d1}.pickle", "wb") as f:
                    pickle.dump(plot.fig, f)
            else:
                plot.fig.savefig(f"{save_subpath}/{d1}.png", dpi=300)

            plot.fig.clf()
            plt.close(plot.fig)

            # save ghost plot data
            ghost.prep_plot()
            ghost.fig.suptitle(f"Ghost cells on scale 1/{scale}")
            d2 = DataNames(
                type=DataNames.GHOST_TYPE,
                name=file,
                a=A,
                scale=scale,
                init=init_scheme,
                pulse=pulse_callable,
                params=list(pulse_args.values()),
                time=sim.t,
            )
            if pickle_sim:
                with open(f"{save_subpath}/{d2}.pickle", "wb") as f:
                    pickle.dump(ghost.fig, f)
            else:
                ghost.fig.savefig(f"{save_subpath}/{d2}.png", dpi=300)

            ghost.fig.clf()
            plt.close(ghost.fig)

            del plot, ghost

        del sim, lat

    message += " Done"
    return message
