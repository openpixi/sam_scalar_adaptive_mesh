"""Rerun tests and compare to archive."""

import gc
from glob import iglob
from multiprocessing import Process
from os import path
import os
import shutil
import sys

import numpy as np

from evaluation_ipynb.evaluate import DataNames

# used for subprocess imports
TMP_PATH = "/tmp/compare_archive"
if not path.exists(TMP_PATH):
    os.makedirs(TMP_PATH)

sys.path.insert(0, TMP_PATH)

ARCHIVE_PATH = "/tmp/old_data"
DATA_PATH = "/tmp/data"
if not path.exists(DATA_PATH):
    os.makedirs(DATA_PATH)

TESTS_PATH = path.basename(path.dirname(__file__))
# TODO: FIXME: update the setups!
TESTS = [
    "C_dispersion",
    "homogen",
    "SCTF_SFTC_deltapulses",
    "SCTF_SFTC",
    "double_straight",
    "stripes_straight",
    "DCTF_DFTC_deltapulses",
    "DCTF_DFTC",
    "double_diag",
    "stripes_diag",
]

test_results = dict.fromkeys(TESTS, False)
errors = dict.fromkeys(TESTS, "\n")


for test in TESTS:

    print(f"\n\n{test}:")

    shutil.copyfile(
        f"{path.abspath(path.curdir)}/{TESTS_PATH}/{test}.py", f"{TMP_PATH}/{test}.py"
    )

    all_files = False
    while not all_files:

        gc.collect()

        for archive_file in iglob(f"{ARCHIVE_PATH}/{test}/**/*.npz"):
            print(DataNames.parse(archive_file))

            dirname, filename = path.split(archive_file)
            data_pre_path = path.relpath(dirname, f"{ARCHIVE_PATH}/{test}")

            new_file = f"{DATA_PATH}/{test}/{data_pre_path}/{filename}"
            if not path.exists(new_file):
                p = Process(target=__import__, args=(test,))
                p.start()
                p.join()
                p.close()
                break

            archive = np.load(archive_file)
            new = np.load(new_file)

            for a, n in zip(archive.values(), new.values()):
                if not np.allclose(a, n):
                    errors[test] += (
                        f"allclose:\n{archive_file}\n{new_file}\n"
                        f"{np.sort(np.abs(a-n))[-6:]}\n"
                    )
                # if not np.array_equal(a, n):
                #     errors[test] += (
                #         f"equal:\n{archive_file}\n{new_file}\n"
                #         f"{np.sort(np.abs(a-n))[-6:]}\n"
                #     )

            print("==> finished")

        else:
            all_files = True
            if errors[test] == "\n":  # sentinel value is newline
                test_results[test] = True


print("#" * 60, end="\n\n")
print(
    "\n".join(
        f"{k.ljust(25)}==> {'passed' if v else 'failed !!' + errors[k]}"
        for k, v in test_results.items()
    )
)
print("#" * 60, end="\n\n")
print(
    "\n".join(
        f"{k.ljust(25)}==> {'passed' if v else 'failed !!'}"
        for k, v in test_results.items()
    )
)
