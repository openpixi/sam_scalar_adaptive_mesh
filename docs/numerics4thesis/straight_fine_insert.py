#! /usr/bin/env python3

"""Script for testing insert regions with straight borders.

RES=1 | RES=2 | RES=1
RES=2 | RES=4 | RES=2
"""

import os
from multiprocessing import Pool

from mrlattice.lattice.generate import HomogeneousLatticeArray

from sam.solvers.refinement import BORDER_TYPES1, BORDER_TYPES3, BORDER_TYPES5
from sam.init import single_gauss_pulse, single_caret_pulse

from worker import worker, return_product


# # # Parameters

PROCESSES = 8

# path to save output data; NOTE: linux path seperators used!
FILE = os.path.splitext(os.path.basename(__file__))[0]
SAVE_PATH = f"/tmp/{os.path.basename(os.path.dirname(__file__))}/{FILE}/"
if not os.path.exists(SAVE_PATH):
    os.makedirs(SAVE_PATH)

# save sim results as png or pickle lattice plots (huge file size!)
PICKLE_SIM = False

# save lattice plots (requires lots of RAM)
LATTICE_PLOTS = False


# sim config

DIR = 1  # pulse direction
A = 1.0  # t/x ratio
PPOS = 42  # position of pulse
MAX_SCALE = 5  # 2^x for scaling lattice

# configure initial pulse shapes
PULSES = {
    "gauss_cut_high": (
        single_gauss_pulse,
        {"mu": PPOS, "sigma": 8, "cutoff": 0.005, "amplitude": 1},
    ),
    "gauss_cut_low": (
        single_gauss_pulse,
        {"mu": PPOS, "sigma": 8, "cutoff": 0.001, "amplitude": 1},
    ),
    "caret": (
        single_caret_pulse,
        {"center": PPOS, "width": 16, "amplitude": 0.1},
    ),
}

# init scheme configs:
SCHEMES = {
    "ctr": "centered",
    "avg": "averaged",
}

BORDERS = {
    "V1": BORDER_TYPES1,
    "V3": BORDER_TYPES3,
    "V5": BORDER_TYPES5,
}


# lattice configs:
def res2_1(scale):
    stripes = 1
    kernel = 16 * scale
    s_width = 32 * scale
    repeat = int((s_width + 2 * kernel) * stripes + 5 * s_width - PPOS * scale * 2)

    S = HomogeneousLatticeArray(coarse_steps=0, buffer=s_width, repeat=repeat)
    S.generate_array()
    K = HomogeneousLatticeArray(coarse_steps=1, buffer=kernel, repeat=repeat // 2)
    K.generate_array()
    left = HomogeneousLatticeArray(
        coarse_steps=1, buffer=3 * kernel, repeat=repeat // 2
    )
    left.generate_array()
    for _ in range(stripes):
        left.spatial_extend(S)
        left.spatial_extend(K)
    left.spatial_extend(K)
    left.spatial_extend(K)
    return left


def res4_2(scale):
    stripes = 1
    kernel = 8 * scale
    s_width = 16 * scale
    repeat = int((s_width + 2 * kernel) * stripes + 5 * s_width - PPOS * scale)

    S = HomogeneousLatticeArray(coarse_steps=1, buffer=s_width, repeat=repeat)
    S.generate_array()
    K = HomogeneousLatticeArray(coarse_steps=2, buffer=kernel, repeat=repeat // 2)
    K.generate_array()
    left = HomogeneousLatticeArray(
        coarse_steps=2, buffer=3 * kernel, repeat=repeat // 2
    )
    left.generate_array()
    for _ in range(stripes):
        left.spatial_extend(S)
        left.spatial_extend(K)
    left.spatial_extend(K)
    left.spatial_extend(K)
    return left


LATTICES = {
    "res2_1": res2_1,
    "res4_2": res4_2,
}


if __name__ == "__main__":

    with Pool(PROCESSES, maxtasksperchild=1) as p:
        print(
            *p.starmap(
                worker,
                return_product(
                    SCHEMES,
                    PULSES,
                    LATTICES,
                    (MAX_SCALE,),
                    (A,),
                    (DIR,),
                    BORDERS.items(),
                    (SAVE_PATH,),
                    (FILE,),
                    (LATTICE_PLOTS,),
                    (PICKLE_SIM,),
                ),
            ),
            sep="\n",
        )
