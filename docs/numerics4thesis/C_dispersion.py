#! /usr/bin/env python3

"""Script for homogeneous lattice dispersion testing.
"""

import os
from multiprocessing import Pool

import numpy as np

from mrlattice.lattice.generate import HomogeneousLatticeArray

from sam.sim import ScalarField
from sam.solvers.refinement import BORDER_TYPES
from sam.init import single_gauss_pulse

from worker import worker, return_product


# # # Parameters

PROCESSES = 8

# path to save output data; NOTE: linux path seperators used!
FILE = os.path.splitext(os.path.basename(__file__))[0]
SAVE_PATH = f"/tmp/{os.path.basename(os.path.dirname(__file__))}/{FILE}/"
if not os.path.exists(SAVE_PATH):
    os.makedirs(SAVE_PATH)

# save sim results as png or pickle lattice plots (huge file size!)
PICKLE_SIM = False

# save lattice plots (requires lots of RAM)
LATTICE_PLOTS = False


# sim config

DIR = 1  # pulse direction
A = 32 / 33  # t/x ratio
MAX_SCALE = 5  # 2^x for scaling lattice

# configure initial pulse shapes
PULSES = {
    "gauss_cut_high": (
        single_gauss_pulse,
        {"mu": 50 / A + (1 / A - DIR) / 2, "sigma": 3, "cutoff": 0.005, "amplitude": 1},
    ),
    "gauss_cut_low": (
        single_gauss_pulse,
        {"mu": 50 / A + (1 / A - DIR) / 2, "sigma": 3, "cutoff": 0.001, "amplitude": 1},
    ),
}

# init scheme configs:
SCHEMES = {"ctr": "centered"}


# lattice configs:
def res1(scale):
    lat = HomogeneousLatticeArray(0, 1000 * scale, 950 * scale)
    lat.generate_array()
    return lat


LATTICES = {"res1": res1}


def timeslice_norm(sim: ScalarField, vals: np.ndarray = None):
    """Calcualte norm of time slice."""
    if vals is None:
        # prepare memory for all values
        vals = np.zeros(sim.max_t)

    dd_now = sim.dd_now
    r_c = dict(zip(sim.res_list, [0] * len(sim.res_list)))
    for region in sim.array[sim.t]:
        # for 1+1D copy is faster than looping through with indices
        d_now = dd_now[region.a, r_c[region.a]]
        data = d_now.arr[d_now.idx[: region.n]]
        vals[sim.t] += np.sum(data * region.a)
        r_c[region.a] += 1

    return vals


if __name__ == "__main__":

    with Pool(PROCESSES, maxtasksperchild=1) as p:
        print(
            *p.starmap(
                worker,
                return_product(
                    SCHEMES,
                    PULSES,
                    LATTICES,
                    (MAX_SCALE,),
                    (A,),
                    (DIR,),
                    (BORDER_TYPES,),
                    (SAVE_PATH,),
                    (FILE,),
                    (LATTICE_PLOTS,),
                    (PICKLE_SIM,),
                    ((timeslice_norm,),),
                ),
            ),
            sep="\n",
        )
