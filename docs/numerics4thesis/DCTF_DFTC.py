#! /usr/bin/env python3

"""Script for diagonal border lattice testing.
"""

import os
from multiprocessing import Pool

from mrlattice.lattice.generate import LatticeArray
from mrlattice.lattice.configure import LatticeArrayScheme

from sam.solvers.refinement import BORDER_TYPES
from sam.init import single_gauss_pulse, single_caret_pulse

from worker import worker, return_product


# # # Parameters

PROCESSES = 8

# path to save output data; NOTE: linux path seperators used!
FILE = os.path.splitext(os.path.basename(__file__))[0]
SAVE_PATH = f"/tmp/{os.path.basename(os.path.dirname(__file__))}/{FILE}/"
if not os.path.exists(SAVE_PATH):
    os.makedirs(SAVE_PATH)

# save sim results as png or pickle lattice plots (huge file size!)
PICKLE_SIM = False

# save lattice plots (requires lots of RAM)
LATTICE_PLOTS = False

# sim config

DIR = 1  # pulse direction
A = 1.0  # t/x ratio
PPOS = 42  # position of pulse
MAX_SCALE = 5  # 2^x for scaling lattice

# configure initial pulse shapes
PULSES = {
    "gauss_cut_high": (
        single_gauss_pulse,
        {"mu": PPOS, "sigma": 8, "cutoff": 0.005, "amplitude": 1},
    ),
    "gauss_cut_low": (
        single_gauss_pulse,
        {"mu": PPOS, "sigma": 8, "cutoff": 0.001, "amplitude": 1},
    ),
    "caret": (
        single_caret_pulse,
        {"center": PPOS, "width": 16, "amplitude": 0.1},
    ),
}

# init scheme configs:
SCHEMES = {
    "ctr": "centered",
    "avg": "averaged",
}


# lattice configs:
def res2_1(scale):
    lat1 = LatticeArray(
        scheme=LatticeArrayScheme.FIXED_NUMBER_SCHEME,
        coarse_steps=1,
        finest_width=48 * scale,
        buffer=24 * scale,
        repeat=41 * scale,
    )
    lat1.generate_array()
    lat1.temporal_mirror()
    return lat1


def res1_2(scale):
    lat1 = res2_1(scale)
    lat1.temporal_mirror()
    lat1.spatial_mirror()
    return lat1


def res4_2(scale):
    # diag coarse to fine with res 4, 2, 1
    lat1 = LatticeArray(
        scheme=LatticeArrayScheme.FIXED_NUMBER_SCHEME,
        coarse_steps=2,
        finest_width=34 * scale,
        buffer=10 * scale,
        repeat=41 * scale,
    )
    lat1.borders_width.increment(20 * scale)
    lat1.generate_array()
    lat1.temporal_mirror()
    return lat1


def res2_4(scale):
    lat = res4_2(scale)
    lat.temporal_mirror()
    lat.spatial_mirror()
    return lat


LATTICES = {
    "res2_1": res2_1,
    "res1_2": res1_2,
    "res4_2": res4_2,
    "res2_4": res2_4,
}


if __name__ == "__main__":

    with Pool(PROCESSES, maxtasksperchild=1) as p:
        print(
            *p.starmap(
                worker,
                return_product(
                    SCHEMES,
                    PULSES,
                    LATTICES,
                    (MAX_SCALE,),
                    (A,),
                    (DIR,),
                    (BORDER_TYPES,),
                    (SAVE_PATH,),
                    (FILE,),
                    (LATTICE_PLOTS,),
                    (PICKLE_SIM,),
                ),
            ),
            sep="\n",
        )
