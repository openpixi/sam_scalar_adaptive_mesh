#! /usr/bin/env python3

"""Script for homogeneous lattice testing.
"""

import os
from multiprocessing import Pool

from mrlattice.lattice.generate import HomogeneousLatticeArray

from sam.solvers.refinement import BORDER_TYPES
from sam.init import single_gauss_pulse, single_caret_pulse, single_step_pulse

from worker import worker, return_product


# # # Parameters

PROCESSES = 8

# path to save output data; NOTE: linux path seperators used!
FILE = os.path.splitext(os.path.basename(__file__))[0]
SAVE_PATH = f"/tmp/{os.path.basename(os.path.dirname(__file__))}/{FILE}/"
if not os.path.exists(SAVE_PATH):
    os.makedirs(SAVE_PATH)

# save sim results as png or pickle lattice plots (huge file size!)
PICKLE_SIM = False

# save lattice plots (requires lots of RAM)
LATTICE_PLOTS = False


# sim config

DIR = 1  # pulse direction
A = 1.0  # t/x ratio
XWIDTH = 160  # spatial size of lattices
PPOS = 42  # position of pulses
MAX_SCALE = 5  # 2^x for scaling lattice

# configure initial pulse shapes
PULSES = {
    "gauss_cut_high": (
        single_gauss_pulse,
        {"mu": PPOS, "sigma": 8, "cutoff": 0.005, "amplitude": 1},
    ),
    "gauss_cut_low": (
        single_gauss_pulse,
        {"mu": PPOS, "sigma": 8, "cutoff": 0.001, "amplitude": 1},
    ),
    "caret": (
        single_caret_pulse,
        {"center": PPOS, "width": 16, "amplitude": 0.1},
    ),
    "step": (
        single_step_pulse,
        {"center": PPOS, "width": 1 / 2**MAX_SCALE, "amplitude": 0.1},
    ),
}

# init scheme configs:
SCHEMES = {
    "ctr": "centered",
    "avg": "averaged",
}


def res1(scale):
    lat = HomogeneousLatticeArray(0, XWIDTH * scale, XWIDTH // 2 * scale)
    lat.generate_array()
    return lat


def res4(scale):
    lat = HomogeneousLatticeArray(2, XWIDTH // 4 * scale, XWIDTH // 8 * scale)
    lat.generate_array()
    return lat


# lattice configs:
LATTICES = {
    "res1": res1,
    "res4": res4,
}

if __name__ == "__main__":

    with Pool(PROCESSES, maxtasksperchild=1) as p:
        print(
            *p.starmap(
                worker,
                return_product(
                    SCHEMES,
                    PULSES,
                    LATTICES,
                    (MAX_SCALE,),
                    (A,),
                    (DIR,),
                    (BORDER_TYPES,),
                    (SAVE_PATH,),
                    (FILE,),
                    (LATTICE_PLOTS,),
                    (PICKLE_SIM,),
                ),
            ),
            sep="\n",
        )
