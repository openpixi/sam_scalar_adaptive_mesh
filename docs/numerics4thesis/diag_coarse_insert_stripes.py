#! /usr/bin/env python3

"""Script for testing stripes of insert regions with diag borders.

RES=1 \ RES=2 \ RES=1 \ RES=2 \ RES=1 \ RES=2 \ RES=1
"""

import os
from multiprocessing import Pool

from mrlattice.lattice.generate import (
    LatticeArray,
    HomogeneousLatticeArray,
    PulseLatticeArray,
)
from mrlattice.lattice.configure import LatticeArrayScheme

from sam.solvers.refinement import BORDER_TYPES
from sam.init import single_gauss_pulse, single_caret_pulse

from worker import worker, return_product


# # # Parameters

PROCESSES = 8

# path to save output data; NOTE: linux path seperators used!
FILE = os.path.splitext(os.path.basename(__file__))[0]
SAVE_PATH = f"/tmp/{os.path.basename(os.path.dirname(__file__))}/{FILE}/"
if not os.path.exists(SAVE_PATH):
    os.makedirs(SAVE_PATH)

# save sim results as png or pickle lattice plots (huge file size!)
PICKLE_SIM = False

# save lattice plots (requires lots of RAM)
LATTICE_PLOTS = False


# sim config

DIR = 1  # pulse direction
A = 1.0  # t/x ratio
MAX_SCALE = 5  # 2^x for scaling lattice

# configure initial pulse shapes
PULSES = {
    "gauss_cut_high": (
        single_gauss_pulse,
        {"mu": 90, "sigma": 8, "cutoff": 0.005, "amplitude": 1},
    ),
    "gauss_cut_low": (
        single_gauss_pulse,
        {"mu": 90, "sigma": 8, "cutoff": 0.001, "amplitude": 1},
    ),
    "caret": (
        single_caret_pulse,
        {"center": 90, "width": 16, "amplitude": 0.1},
    ),
}

# init scheme configs:
SCHEMES = {
    "ctr": "centered",
    "avg": "averaged",
}


# lattice configs:
def res2_1(scale):
    stripes = 2  # actual number is +1
    kernel = 30 * scale
    s_width = 14 * scale
    buffer = 2
    repeat = s_width - buffer - 1

    P = PulseLatticeArray(
        scheme=LatticeArrayScheme.FIXED_NUMBER_SCHEME,
        coarse_steps=1,
        finest_width=kernel,
        repeat=repeat,
        buffer=buffer,
    )
    P.generate_array()
    P.temporal_mirror()

    HH = HomogeneousLatticeArray(coarse_steps=0, repeat=repeat * 2, buffer=repeat * 2)
    HH.generate_array()
    D = LatticeArray(
        scheme=LatticeArrayScheme.FIXED_NUMBER_SCHEME, repeat=repeat, buffer=buffer
    )
    D.finest_width.increment()
    D.generate_array()
    DD = D.copy()
    D.temporal_mirror()
    DD.spatial_mirror()

    r_stripes = stripes * 3
    bottom = HH.copy()
    for _ in range(r_stripes):
        bottom.spatial_extend(HH)
    bottom.spatial_extend(DD)
    for _ in range(stripes):
        bottom.spatial_extend(P)
    bottom.spatial_extend(D)
    bottom.spatial_extend(HH)

    for r in range(1, r_stripes + 1):
        top = HH.copy()
        for _ in range(r_stripes - r):
            top.spatial_extend(HH)
        top.spatial_extend(DD)
        for _ in range(stripes):
            top.spatial_extend(P)
        top.spatial_extend(D)
        for _ in range(r + 1):
            top.spatial_extend(HH)

        bottom.temporal_extend(top)

    return bottom


LATTICES = {
    "res2_1": res2_1,
}


if __name__ == "__main__":

    with Pool(PROCESSES, maxtasksperchild=1) as p:
        print(
            *p.starmap(
                worker,
                return_product(
                    SCHEMES,
                    PULSES,
                    LATTICES,
                    (MAX_SCALE,),
                    (A,),
                    (DIR,),
                    (BORDER_TYPES,),
                    (SAVE_PATH,),
                    (FILE,),
                    (LATTICE_PLOTS,),
                    (PICKLE_SIM,),
                ),
            ),
            sep="\n",
        )
