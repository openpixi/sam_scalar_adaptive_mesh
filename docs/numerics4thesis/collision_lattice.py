#! /usr/bin/env python3

"""Script for collision lattice testing.
Pulses placed in mesh kernel and also crossing to first coarsest.
"""
import gc
import os
from multiprocessing import Pool

import numpy as np
from matplotlib import pyplot as plt

from mrlattice.lattice.generate import SuperimposePulseLatticeArray
from mrlattice.lattice.configure import LatticeArrayScheme

from sam.init import single_gauss_pulse, single_step_pulse
from sam.plotting import LatticePlot, GhostPlot
from sam.sim import ScalarField
from sam.solvers.refinement import BORDER_TYPES5, CUSP_TYPES2
from sam.solvers.leapfrog import EvolveLeapfrog

from evaluation_ipynb.evaluate import DataNames, diff_vals, x_vals, timeslice_vals
from worker import return_product, _prep_subpath


# # # Parameters

PROCESSES = 8

# path to save output data; NOTE: linux path seperators used!
FILE = os.path.splitext(os.path.basename(__file__))[0]
SAVE_PATH = f"/tmp/{os.path.basename(os.path.dirname(__file__))}/{FILE}/"
if not os.path.exists(SAVE_PATH):
    os.makedirs(SAVE_PATH)


# save lattice plots (requires lots of RAM)
LATTICE_PLOTS = False

# refinement scheme config
BORDERS = {
    "CCTFV1": BORDER_TYPES5,
    "CCTFV2": CUSP_TYPES2,
}

# sim config

A = 1.0  # t/x ratio
PPOS_L = 61  # position of right moving pulse placed left
PPOS_R = 123  # position of left moving pulse placed right
MAX_SCALE = 5  # 2^x for scaling lattice


PULSES = {
    "gauss_cut_high": (
        single_gauss_pulse,
        {"mu": PPOS_L, "sigma": 8, "cutoff": 0.005, "amplitude": 1},
        {"mu": PPOS_R, "sigma": 8, "cutoff": 0.005, "amplitude": 1},
    ),
    "gauss_cut_low": (
        single_gauss_pulse,
        {"mu": PPOS_L, "sigma": 8, "cutoff": 0.001, "amplitude": 1},
        {"mu": PPOS_R, "sigma": 8, "cutoff": 0.001, "amplitude": 1},
    ),
    "control_step": (
        single_step_pulse,
        {"center": PPOS_L, "width": 1},
        {"center": PPOS_R, "width": 1},
    ),
}

# init scheme configs:
SCHEMES = {
    "ctr": "centered",
    "avg": "averaged",
}


# lattice configs:
def coll_lat(scale):
    # arg table for lattice properties
    # 2   8  10  10  16
    # 2  16  20  18  32
    # 2  32  39  42  64
    # 2  64  78  82 128
    # 2 128 156 162 256
    # 2 256 312 322 512
    lat = SuperimposePulseLatticeArray(
        coarse_steps=2,
        borders_width=8 * scale,
        buffer=10 * scale,
        scheme=LatticeArrayScheme.FIXED_NUMBER_SCHEME,
    )
    while lat.finest_width.value < 10 * scale:
        lat.finest_width.increment()

    if scale == 2:
        # special fix for as this is 18, not 20
        lat.finest_width.decrement()

    lat.repeat.value = 16 * scale

    if scale > 2:
        # special fix, again
        lat.buffer.decrement(scale // 4)

    lat.generate_array()
    return lat


LATTICES = {"coll_lat": coll_lat}


def worker(
    scheme,
    pulse,
    lattice,
    max_scale,
    a,
    border,
    save_path,
    file,
):
    """Run all scales for a setup config.

    Adapted from evaluation_ipynb.evaluate.worker to deal with two
    pulses for the initial conditions.
    Section of code are mostly identical.
    """

    init_scheme_name, init_scheme = scheme
    pulse_name, (pulse_callable, pulse_args_l, pulse_args_r) = pulse
    lat_name, lat_callable = lattice
    if isinstance(border, tuple):
        border_name, border_types = border
        save_subpath = _prep_subpath(
            save_path, border_name, lat_name, init_scheme_name, pulse_name
        )
    else:
        border_types = border
        save_subpath = _prep_subpath(save_path, lat_name, init_scheme_name, pulse_name)

    pulse_profile_l = pulse_callable(**pulse_args_l)
    pulse_profile_r = pulse_callable(**pulse_args_r)

    print("starting worker for", save_subpath, flush=True)
    message = f"{save_subpath}:\t"

    for scale in [int(2**i) for i in range(max_scale + 1)]:

        gc.collect()

        message += f"{scale}..."

        lat = lat_callable(scale)

        if LATTICE_PLOTS:
            plot = LatticePlot(lat.array)
            ghost = GhostPlot(lat.array)

        sim = ScalarField(
            lat,
            (a,),
            1 / scale,
            EvolveLeapfrog(),
            border_types,
            lambda *x: None,
        )
        # initialize pulses
        getattr(sim, "init_cell_" + init_scheme)(pulse_profile_l, direction=1)
        getattr(sim, "init_cell_" + init_scheme)(pulse_profile_r, direction=-1)

        # init plot's first time slice
        if LATTICE_PLOTS:
            plot.update_field_vals(
                sim.t,
                sim.dd_now,
                sim.array[sim.t],
                sim.res_list,
                sim.res_list,
                sim.reg_by_res_now,
                sim.p_now,
            )
            for res in sim.res_list:
                ghost.update_field_vals(sim.t, sim.border_by_res[res * 2])

        # init callbacks
        # clb_data = [clb(sim, None) for clb in loop_callbacks]

        # save starting profile
        d0 = DataNames(
            type=DataNames.DATA_TYPE,
            name=file,
            a=a,
            scale=scale,
            init=init_scheme,
            pulse=pulse_callable,
            params=list((pulse_args_l | pulse_args_r).values()),
            time=sim.t,
        )
        start_vals, start_norm = timeslice_vals(sim.array[sim.t], sim)
        np.savez_compressed(
            f"{save_subpath}/{d0}",
            start_x=x_vals(sim.array[sim.t], sim),
            start_vals=start_vals,
            start_norm=start_norm,
        )

        # time evolution
        for t in sim:
            if LATTICE_PLOTS:
                plot.update_field_vals(
                    sim.t,
                    sim.dd_now,
                    sim.array[t],
                    sim.res_list,
                    sim.res_list,
                    sim.reg_by_res_now,
                    sim.p_now,
                )
                for res in sim.res_list:
                    ghost.update_field_vals(sim.t, sim.border_by_res[res * 2])

            # for clb, data in zip(loop_callbacks, clb_data):
            #     clb(sim, data)

        # save end profile
        # save exact end profile and diff to sim
        x_vals_end = x_vals(sim.array[sim.t], sim)
        # diff values array for each region
        # exact_x, exact_vals, diff = diff_vals(sim, x_vals_end, pulse_profile, DIR)

        diff = []
        exact_x = []
        exact_vals = []
        xvals_idx = 0
        count = dict(zip(sim.res_list, [0] * len(sim.res_list)))
        for reg in sim.array[-1]:
            exact = np.zeros(reg.n)
            x_vals_reg = x_vals_end[xvals_idx : xvals_idx + reg.n]
            # propagate pulse by t steps with speed = 1
            idx_l, data = pulse_profile_l(
                x_vals_reg - (1 * (sim.max_t - reg.a / 2)) * sim.dt
            )
            exact[idx_l] = data
            idx_r, data = pulse_profile_r(
                x_vals_reg - (-1 * (sim.max_t - reg.a / 2)) * sim.dt
            )
            exact[idx_r] += data

            # append values
            exact_x.append(x_vals_reg[idx_l | idx_r])
            exact_vals.append(exact[idx_l | idx_r])

            # append diff vals
            d_now = sim.dd_now[reg.a, count[reg.a]]
            diff.append(exact - d_now.arr[d_now.idx[: reg.n]])

            xvals_idx += reg.n
            count[reg.a] += 1

        d3 = DataNames(
            type=DataNames.DATA_TYPE,
            name=file,
            a=a,
            scale=scale,
            init=init_scheme,
            pulse=pulse_callable,
            params=list((pulse_args_l | pulse_args_r).values()),
            time=sim.t,
        )
        end_vals, end_norm = timeslice_vals(sim.array[sim.t], sim)
        np.savez_compressed(
            f"{save_subpath}/{d3}",
            end_x=x_vals_end,
            end_vals=end_vals,
            end_norm=end_norm,
            end_exact_x=np.concatenate(exact_x),
            end_exact_vals=np.concatenate(exact_vals),
            diff_vals=np.concatenate(diff),
        )

        # # save callback results
        # d6 = DataNames(
        #     type=DataNames.DATA_TYPE,
        #     name=f"{file}_callbacks",
        #     a=a,
        #     scale=scale,
        #     init=init_scheme,
        #     pulse=pulse_callable,
        #     params=list((pulse_args_l | pulse_args_r).values()),
        #     time=sim.t,
        # )
        # np.savez_compressed(
        #     f"{save_subpath}/callbacks/{d6}",
        #     **{clb.__name__: data for clb, data in zip(loop_callbacks, clb_data)},
        # )

        if LATTICE_PLOTS:
            # save plot data
            plot.prep_plot()
            plot.fig.suptitle(f"Values on scale 1/{scale}")
            d1 = DataNames(
                type=DataNames.PLOT_TYPE,
                name=file,
                a=a,
                scale=scale,
                init=init_scheme,
                pulse=pulse_callable,
                params=list((pulse_args_l | pulse_args_r).values()),
                time=sim.t,
            )
            plot.fig.savefig(f"{save_subpath}/{d1}.png", dpi=300)

            plot.fig.clf()
            plt.close(plot.fig)

            # save ghost plot data
            ghost.prep_plot()
            ghost.fig.suptitle(f"Ghost cells on scale 1/{scale}")
            d2 = DataNames(
                type=DataNames.GHOST_TYPE,
                name=file,
                a=a,
                scale=scale,
                init=init_scheme,
                pulse=pulse_callable,
                params=list((pulse_args_l | pulse_args_r).values()),
                time=sim.t,
            )
            ghost.fig.savefig(f"{save_subpath}/{d2}.png", dpi=300)

            ghost.fig.clf()
            plt.close(ghost.fig)

            del plot, ghost

        del sim, lat

    message += " Done"
    return message


if __name__ == "__main__":

    with Pool(PROCESSES, maxtasksperchild=1) as p:
        print(
            *p.starmap(
                worker,
                return_product(
                    SCHEMES,
                    PULSES,
                    LATTICES,
                    (MAX_SCALE,),
                    (A,),
                    BORDERS.items(),
                    (SAVE_PATH,),
                    (FILE,),
                ),
            ),
            sep="\n",
        )
