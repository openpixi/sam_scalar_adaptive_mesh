#! /usr/bin/env python3

"""Script for straight border lattice testing.
Pulses placed at 2|4 border on init.
"""

import os
from multiprocessing import Pool

from mrlattice.lattice.generate import HomogeneousLatticeArray

from sam.solvers.refinement import BORDER_TYPES1, BORDER_TYPES3, BORDER_TYPES5
from sam.init import single_gauss_pulse, single_caret_pulse

from worker import worker, return_product


# # # Parameters

PROCESSES = 8

# path to save output data; NOTE: linux path seperators used!
FILE = os.path.splitext(os.path.basename(__file__))[0]
SAVE_PATH = f"/tmp/{os.path.basename(os.path.dirname(__file__))}/{FILE}/"
if not os.path.exists(SAVE_PATH):
    os.makedirs(SAVE_PATH)

# save sim results as png or pickle lattice plots (huge file size!)
PICKLE_SIM = False

# save lattice plots (requires lots of RAM)
LATTICE_PLOTS = False


# refinement scheme config
BORDERS = {
    "V1": BORDER_TYPES1,
    "V3": BORDER_TYPES3,
    "V5": BORDER_TYPES5,
}

# sim config

DIR = 1  # pulse direction
A = 1.0  # t/x ratio
PPOS = 100  # position of pulse
MAX_SCALE = 5  # 2^x for scaling lattice

# configure initial pulse shapes
PULSES = {
    "gauss_cut_high": (
        single_gauss_pulse,
        {"mu": PPOS, "sigma": 8, "cutoff": 0.005, "amplitude": 1},
    ),
    "gauss_cut_low": (
        single_gauss_pulse,
        {"mu": PPOS, "sigma": 8, "cutoff": 0.001, "amplitude": 1},
    ),
    "caret": (
        single_caret_pulse,
        {"center": PPOS, "width": 16, "amplitude": 0.1},
    ),
}

# init scheme configs:
SCHEMES = {
    "ctr": "centered",
    "avg": "averaged",
}


# lattice configs:
def sctf(scale):
    lat1 = HomogeneousLatticeArray(2, 25 * scale, 10 * scale)
    lat1.generate_array()
    lat2 = HomogeneousLatticeArray(1, 50 * scale, 20 * scale)
    lat2.generate_array()
    lat1.spatial_extend(lat2)
    return lat1


def sftc(scale):
    latt = sctf(scale)
    latt.spatial_mirror()
    return latt


LATTICES = {
    "sctf": sctf,
    "sftc": sftc,
}

if __name__ == "__main__":

    with Pool(PROCESSES, maxtasksperchild=1) as p:
        print(
            *p.starmap(
                worker,
                return_product(
                    SCHEMES,
                    PULSES,
                    LATTICES,
                    (MAX_SCALE,),
                    (A,),
                    (DIR,),
                    BORDERS.items(),
                    (SAVE_PATH,),
                    (FILE,),
                    (LATTICE_PLOTS,),
                    (PICKLE_SIM,),
                ),
            ),
            sep="\n",
        )
