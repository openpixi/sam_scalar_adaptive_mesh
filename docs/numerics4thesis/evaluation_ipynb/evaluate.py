#! /usr/bin/env python3

"""Helper module for working with numeric data

DataNames fields:

`type` is one of "plot", "ghost" or "data".
`name` is the name of the generating file.
`a` is a0/a1 ratio.
`scale` is 1/dt.
`init` is one of "averaged" or "centered".
`pulse` is reference to a pulse initializer from `sam.init`.
`pa-ra-ms` are "-" separated parameters of pulse initializer.
`time` is the integer time step of the sim loop the data is from.
"""

from __future__ import annotations

import os
from dataclasses import dataclass
from typing import Any, Callable, ClassVar, Literal

import numpy as np
import matplotlib.pyplot as plt
from matplotlib import ticker
from matplotlib.figure import Figure
from matplotlib.axes import Axes

from mrlattice.lattice.generate import RegionSlice, RegionTuple

from sam.init import single_caret_pulse, single_gauss_pulse, single_step_pulse
from sam.sim import ScalarField


@dataclass
class DataNames:
    """Container for easy naming convention of data files."""

    type: str
    name: str
    a: float
    scale: int
    init: str
    pulse: Callable
    params: list[float]
    time: int

    DATA_TYPE: ClassVar[str] = "data"
    PLOT_TYPE: ClassVar[str] = "plot"
    GHOST_TYPE: ClassVar[str] = "ghost"

    # translates setup functions to str name
    __str: ClassVar[dict[Any, str]] = {
        # pulse shapes
        single_caret_pulse: "caret",
        single_gauss_pulse: "gauss",
        single_step_pulse: "step",
        # pulse evaluation methods
        "averaged": "a",
        "centered": "c",
    }
    __str_inv: ClassVar[dict[str, Any]] = {v: k for k, v in __str.items()}

    def __str__(self) -> str:
        """Return data file name with stored attributes."""
        return "__".join(
            [
                self.type,
                self.name,
                str(self.a),
                str(self.scale),
                self.__str[self.init],
                self.__str[self.pulse],
                "-".join(str(float(p)) for p in self.params),
                f"t{self.time}",
            ]
        )

    @classmethod
    def parse(cls, path: str) -> DataNames:
        """Returns new `DataNames` parsed from `path`."""
        fname = os.path.splitext(os.path.basename(path))[0]
        dtype, name, *rest = fname.split("__")
        a = float(rest[0])
        scale = int(rest[1])
        init = cls.__str_inv[rest[2]]
        pulse = cls.__str_inv[rest[3]]
        params = [float(p) for p in rest[4].split("-")]
        time = int(rest[5][1:])
        return cls(dtype, name, a, scale, init, pulse, params, time)


def timeslice_norm_delta(scale: float, data: np.ndarray):
    """Plot diff of norm for each time step."""
    fig = plt.figure()
    fig.set_tight_layout({"w_pad": 0.0})
    profile_ax = fig.subplots()
    fig.suptitle(f"Scale: 1/{scale}")

    profile_ax.scatter(np.arange(len(data)) / scale, data, c="tab:blue", marker=".")
    profile_ax.set_ylabel(r"Norm$(t)$", c="tab:blue")
    profile_ax.tick_params(axis="y", colors="tab:blue")
    profile_ax.axhline(data[0], c="k", ls=":")
    profile_ax.set_xlabel(r"$t/$Scale")

    return fig


def timeslice_vals(timeslice: RegionSlice, scalarfield: ScalarField):
    """Return all field vals for `timeslice`."""

    dd_now = scalarfield.dd_now
    res = scalarfield.res_list
    norm: np.float_ = np.float_(0.0)

    vals = []
    r_c = dict(zip(res, [0] * len(res)))
    for region in timeslice:
        # this is a copy of input_vals, because of indexing with ndarray
        d_now = dd_now[region.a, r_c[region.a]]
        data = d_now.arr[d_now.idx[: region.n]]
        vals.append(data)
        norm += np.sum(data * region.a)

        r_c[region.a] += 1

    return np.concatenate(vals), norm


def x_vals(timeslice: RegionSlice, scalarfield: ScalarField) -> np.ndarray:
    """Return all x values for `timeslice`."""

    res = scalarfield.res_list
    pos = scalarfield.p_now

    xvals = []
    r_c = dict(zip(res, [0] * len(res)))
    for region in timeslice:
        # x position and width in finest units
        x_pos = pos[region.a, r_c[region.a]] * region.a / scalarfield.a[0]
        x_width = region.a * region.n / scalarfield.a[0]
        x_step = region.a / scalarfield.a[0]
        # extract values with correct x pos
        centering_offset = region.a / scalarfield.a[0] / 2
        xvals.append(
            np.arange(
                x_pos + centering_offset,
                x_pos + centering_offset + x_width,
                step=x_step,
            )
            * scalarfield.dt
        )

        r_c[region.a] += 1

    return np.concatenate(xvals)


def diff_vals(
    sim: ScalarField,
    x_vals_end: np.ndarray,
    pulse_profile: Callable,
    DIR: Literal[1, -1],
):
    """Return diff vals to analytic solution."""
    diff = []
    exact_x = []
    exact_vals = []
    xvals_idx = 0
    count = dict(zip(sim.res_list, [0] * len(sim.res_list)))
    reg: RegionTuple
    for reg in sim.array[-1]:
        exact = np.zeros(reg.n)
        x_vals_reg = x_vals_end[xvals_idx : xvals_idx + reg.n]
        # propagate pulse by t steps with speed = 1
        idx, data = pulse_profile(x_vals_reg - (DIR * (sim.max_t - reg.a / 2)) * sim.dt)
        exact[idx] = data

        # append values
        exact_x.append(x_vals_reg[idx])
        exact_vals.append(data)

        # append diff vals
        d_now = sim.dd_now[reg.a, count[reg.a]]
        diff.append(exact - d_now.arr[d_now.idx[: reg.n]])

        xvals_idx += reg.n
        count[reg.a] += 1

    return exact_x, exact_vals, diff


def pulse_profile_plot(
    plot_fig: Figure,
    axes: Axes,
    scale: float,
    start_t: int,
    end_t: int,
    start_x: np.ndarray,
    start_vals: np.ndarray,
    end_x: np.ndarray,
    end_vals: np.ndarray,
    end_exact_x: np.ndarray,
    end_exact_vals: np.ndarray,
) -> tuple[Figure, Axes]:
    """Plot the pulse profile."""

    axes.step(
        start_x,
        start_vals,
        where="mid",
        label=f"$\phi_x(t={start_t})$",
    )
    axes.step(
        end_x,
        end_vals,
        where="mid",
        label=f"$\phi_x(t={end_t})$",
    )
    (exline,) = axes.plot(end_exact_x, end_exact_vals, c="Gray", zorder=-1, marker="+")
    exline.set_label(f"$\phi^E(x, t={end_t})$")
    plot_fig.suptitle(f"Scale: 1/{scale}")

    return plot_fig, axes


def cut_axis(ax1, ax2, cut_range_l, cut_range_r, numticks):
    """Cut axis at given limits."""

    ax1.set_xlim(*cut_range_l)
    ax1.set_xticks(
        ticker.LinearLocator(numticks=numticks + 1).tick_values(
            cut_range_l[0], cut_range_l[1]
        )[:-1]
    )
    ax2.set_xlim(*cut_range_r)
    ax2.set_xticks(
        ticker.LinearLocator(numticks=numticks + 1).tick_values(
            cut_range_r[0], cut_range_r[1]
        )[1:]
    )

    # the cut axes
    ax1.spines.right.set_visible(False)
    ax2.spines.left.set_visible(False)
    ax2.yaxis.tick_right()
    kwargs = dict(
        marker=[(-0.5, -0.8), (0.5, 0.8)],
        markersize=12,
        linestyle="none",
        color="k",
        mec="k",
        mew=1,
        clip_on=False,
    )
    ax1.plot([1, 1], [1, 0], transform=ax1.transAxes, **kwargs)
    ax2.plot([0, 0], [0, 1], transform=ax2.transAxes, **kwargs)


def label_common_x(fig, label=r"$x$"):
    """Place centered x axis label."""
    label_ax = fig.add_subplot(111, sharey=fig.gca())
    label_ax.patch.set_visible(False)
    label_ax.yaxis.set_visible(False)
    label_ax.tick_params(bottom=False)
    label_ax.set_frame_on(False)
    label_ax.set_xticks([0.0])
    label_ax.set_xticklabels([" "])
    label_ax.set_xlabel(label)

    return label_ax


def profile_plots_cut_axes(
    cut_range_l: tuple[int, int],
    cut_range_r: tuple[int, int],
    numticks: int,
    scale: float,
    start_t: int,
    end_t: int,
    start_x: np.ndarray,
    start_vals: np.ndarray,
    start_norm: np.float_,
    end_x: np.ndarray,
    end_vals: np.ndarray,
    end_norm: np.float_,
    end_exact_x: np.ndarray,
    end_exact_vals: np.ndarray,
    diff_vals: np.ndarray,
) -> Figure:
    """Prepare profile plots with cut axes."""

    fig = plt.figure()
    fig.set_tight_layout({"w_pad": 0.0})
    ax1, ax2 = fig.subplots(1, 2, sharey=True)

    fig, ax1 = pulse_profile_plot(
        fig,
        ax1,
        scale,
        start_t,
        end_t,
        start_x,
        start_vals,
        end_x,
        end_vals,
        end_exact_x,
        end_exact_vals,
    )
    fig, ax2 = pulse_profile_plot(
        fig,
        ax2,
        scale,
        start_t,
        end_t,
        start_x,
        start_vals,
        end_x,
        end_vals,
        end_exact_x,
        end_exact_vals,
    )

    ax2_xlims = [v if v > 0 else int(np.max(end_x) + v) for v in cut_range_r]
    cut_axis(ax1, ax2, cut_range_l, ax2_xlims, numticks)

    label_ax = label_common_x(fig)
    label_ax.annotate(
        "",
        (0.6, 0.35),
        xytext=(0.4, 0.35),
        xycoords="axes fraction",
        arrowprops={"arrowstyle": "simple", "color": "k"},
    )
    # clear duplicate labels
    for artist in ax2.get_children():
        artist.set_label(None)

    fig.legend(
        loc="upper center",
        bbox_to_anchor=(0.5, 0.95),
        bbox_transform=label_ax.transAxes,
        handlelength=1,
    )
    fig.suptitle(None)

    return fig


def scatter_plots_cut_axes(
    cut_range_l: tuple[int, int],
    cut_range_r: tuple[int, int],
    numticks: int,
    scale: float,
    start_t: int,
    end_t: int,
    start_x: np.ndarray,
    start_vals: np.ndarray,
    start_norm: np.float_,
    end_x: np.ndarray,
    end_vals: np.ndarray,
    end_norm: np.float_,
    end_exact_x: np.ndarray,
    end_exact_vals: np.ndarray,
    diff_vals: np.ndarray,
) -> Figure:
    """Error scatter plot with cut axes."""

    fig = plt.figure()
    fig.set_tight_layout({"w_pad": 0.0})
    ax1, ax2 = fig.subplots(1, 2, sharey=True)

    fig, (ax1, ax1rel) = error_scatter_plot(
        fig, ax1, scale, start_norm, end_x, end_vals, end_norm, diff_vals
    )
    fig, (ax2, ax2rel) = error_scatter_plot(
        fig, ax2, scale, start_norm, end_x, end_vals, end_norm, diff_vals
    )
    ax2_xlims = [v if v > 0 else int(np.max(end_x) + v) for v in cut_range_r]
    cut_axis(ax1, ax2, cut_range_l, ax2_xlims, numticks)

    ax1rel.spines.right.set_visible(False)
    ax1rel.set_ylabel(None)
    ax1rel.set_yticks([])
    ax2rel.spines.left.set_visible(False)
    ax1.yaxis.tick_left()
    ax2.set_ylabel(None)

    label_common_x(fig)

    fig.suptitle(None)
    return fig


def error_scatter_plot(
    scatter_fig: Figure,
    scatter_ax: Axes,
    scale: float,
    start_norm: np.float_,
    end_x: np.ndarray,
    end_vals: np.ndarray,
    end_norm: np.float_,
    diff_vals: np.ndarray,
):
    """Plot the error 'profile' values."""
    n_delta = start_norm - end_norm
    n_delta_rel = n_delta / start_norm
    scatter_fig.suptitle(
        f"Scale: 1/{scale}\n"
        + rf"Norm: $\Delta$={n_delta:.2e}, Rel$\Delta$={n_delta_rel:.2e}"
    )

    scatter_ax.scatter(end_x, diff_vals, c="tab:blue", marker="x")
    scatter_ax.plot(end_x, diff_vals, c="tab:blue", zorder=-5, alpha=0.4)
    scatter_ax.set_ylabel(r"$\phi^E(x, t_N) - \phi_x(t_N)$", c="tab:blue")
    scatter_ax.tick_params(axis="y", colors="tab:blue")
    err_rel = scatter_ax.twinx()
    rel_diff_vals = np.ma.divide(np.ma.masked_values(diff_vals, 0), end_vals)
    err_rel.scatter(end_x, rel_diff_vals, c="tab:green", marker=".")
    err_rel.plot(end_x, rel_diff_vals, c="tab:green", zorder=-5, alpha=0.4)
    err_rel.set_ylabel(
        r"$\Big(\phi^E(x, t_N) - \phi_x(t_N)\Big)/\phi_x(t_N)$", c="tab:green"
    )
    err_rel.tick_params(axis="y", colors="tab:green")

    return scatter_fig, (scatter_ax, err_rel)


def profile_err_plots(
    scale: float,
    start_t: int,
    end_t: int,
    start_x: np.ndarray,
    start_vals: np.ndarray,
    start_norm: np.float_,
    end_x: np.ndarray,
    end_vals: np.ndarray,
    end_norm: np.float_,
    end_exact_x: np.ndarray,
    end_exact_vals: np.ndarray,
    diff_vals: np.ndarray,
) -> Figure:
    """Return profile and scatter error plot figure."""

    profile_fig = plt.figure()
    profile_fig.set_tight_layout({"w_pad": 0.0})
    profile_ax = profile_fig.subplots()
    profile_fig, profile_ax = pulse_profile_plot(
        profile_fig,
        profile_ax,
        scale,
        start_t,
        end_t,
        start_x,
        start_vals,
        end_x,
        end_vals,
        end_exact_x,
        end_exact_vals,
    )
    profile_ax.legend(loc="upper center", handlelength=1, bbox_to_anchor=(0.5, 0.95))
    profile_ax.set_xlabel(r"$x$")
    profile_ax.annotate(
        "",
        (0.6, 0.55),
        xytext=(0.4, 0.55),
        xycoords="axes fraction",
        arrowprops={"arrowstyle": "simple", "color": "k"},
    )

    scatter_fig = plt.figure()
    scatter_fig.set_tight_layout({"w_pad": 0.0})
    scatter_ax = scatter_fig.subplots()
    scatter_fig, _ = error_scatter_plot(
        scatter_fig, scatter_ax, scale, start_norm, end_x, end_vals, end_norm, diff_vals
    )
    scatter_ax.set_xlabel("$x$")

    return profile_fig, scatter_fig


def boxplots_and_errs(axis, scale, vals):
    """Populate axis with boxplots and return norm statistics."""

    # filter out values close to 0.0, they are not `errors`
    # set factor for outliers to 2xIQR; better but not perfect!
    boxplt_vals = axis.boxplot(
        vals[~np.isclose(vals, 0.0)],
        labels=(f"$2^{{{np.log2(1/scale):.0f}}}$",),
        whis=2,
    )
    axis.ticklabel_format(axis="y", style="sci", scilimits=(0, 0))

    # save whiskers' endpoints
    # boxplt_vals = boxplt_vals["caps"]
    # lower_limit, upper_limit = boxplt_vals
    # upper_limit = upper_limit.get_ydata()[0]
    # lower_limit = lower_limit.get_ydata()[0]

    # l2 norm excluding outliers
    # v_idx = np.logical_and(vals <= upper_limit, vals >= lower_limit)
    # l2_norm = np.sqrt(np.sum(np.square(vals[v_idx])) / scale)

    # l2 norm on full v
    l2_norm = np.sqrt(np.sum(np.square(vals)) / scale)

    # lmax norm on full v
    lmax_norm = np.max(np.abs(vals))

    return boxplt_vals, l2_norm, lmax_norm


def l2_lmax_fig(
    l2_norms,
    lmax_norms,
    res_list,
    figure: Figure = None,
    labels=(r"${\lVert e \rVert}_{l^2}$", r"${\lVert e \rVert}_{l^\infty}$"),
    target_line=True,
) -> Figure:
    """Return norm scaling plot figure."""
    # plot l2 norm on log2 scaling and lmax norm
    if not figure:
        fig, axes = plt.subplots(1, 2, sharey=True)
        fig.set_tight_layout(True)
        fig.suptitle("Error Convergence")

        # format axes
        axes[0].set_yscale("log", base=2)
        axes[0].set_xscale("log", base=2)
        axes[0].invert_xaxis()
        axes[0].set_aspect(0.5)
        axes[0].set_anchor("S")
        axes[0].xaxis.set_major_locator(ticker.LogLocator(base=2, numticks=7))

        axes[1].set_yscale("log", base=2)
        axes[1].set_xscale("log", base=2)
        axes[1].invert_xaxis()
        axes[1].set_aspect(0.5)
        axes[1].set_anchor("S")
        axes[1].yaxis.set_tick_params(labelright=True)
        axes[1].yaxis.tick_right()
        axes[1].xaxis.set_major_locator(ticker.LogLocator(base=2, numticks=7))

        # plot target line
        if target_line:
            p_idx = 0  # idx for what point to put line through
            xvals = np.linspace(min(res_list), max(res_list))
            axes[0].plot(
                xvals,
                l2_norms[p_idx] / res_list[p_idx] ** 2 * xvals**2,
                ls=":",
                c="Gray",
                zorder=-5,
                label=r"$\mathcal{O}(a^2)$",
            )
            axes[1].plot(
                xvals,
                lmax_norms[p_idx] / res_list[p_idx] ** 2 * xvals**2,
                ls=":",
                c="Gray",
                zorder=-5,
                label=r"$\mathcal{O}(a^2)$",
            )

        # label scatterplots
        label_ax = fig.add_subplot(111, sharey=axes[0])
        label_ax.patch.set_visible(False)
        label_ax.yaxis.set_visible(False)
        label_ax.tick_params(bottom=False)
        label_ax.set_frame_on(False)
        label_ax.set_xticks([0.0])
        label_ax.set_xticklabels([" "])
        label_ax.set_xlabel(r"Scale $h_i$")

    else:
        fig = figure
        axes = fig.get_axes()

    axes[0].scatter(res_list, l2_norms, marker="x", s=65, label=labels[0])
    axes[1].scatter(res_list, lmax_norms, marker="x", s=65, label=labels[1])

    l0 = axes[0].legend(
        loc="lower left",
        handlelength=1,
        handletextpad=0.6,
        borderpad=0.1,
        labelspacing=0.3,
        borderaxespad=0.05,
    )
    l0._legend_box.align = "left"
    l1 = axes[1].legend(
        loc="lower left",
        handlelength=1,
        handletextpad=0.6,
        borderpad=0.1,
        labelspacing=0.3,
        borderaxespad=0.05,
    )
    l1._legend_box.align = "left"

    return fig


def error_plots(diff_dict: dict[int, np.ndarray]) -> tuple[Figure, Figure]:
    """Return boxplots and norm scaling plot figures."""
    boxplt_fig, boxplt_axes = plt.subplots(1, len(diff_dict), gridspec_kw={"wspace": 1})
    boxplt_fig.set_tight_layout(True)
    boxplt_fig.suptitle("Error Boxplots")
    boxplt_axes[0].set_ylabel(r"$\phi^E(x, t_N) - \phi_x(t_N)$")

    l2_norms = []
    lmax_norms = []
    boxplt_vals = {}

    for i, (k, v) in enumerate(diff_dict.items()):
        boxplt_vals[k], l2, lmax = boxplots_and_errs(boxplt_axes[i], k, v)
        l2_norms.append(l2)
        lmax_norms.append(lmax)

    # label boxplots
    label_ax = boxplt_fig.add_subplot(111, sharey=boxplt_axes[0])
    label_ax.patch.set_visible(False)
    label_ax.yaxis.set_visible(False)
    label_ax.tick_params(bottom=False)
    label_ax.set_frame_on(False)
    label_ax.set_xticks([0.0])
    label_ax.set_xticklabels([" "])
    label_ax.set_xlabel("Scale")

    return boxplt_fig, l2_lmax_fig(l2_norms, lmax_norms, [1 / k for k in boxplt_vals])


def l2_lmax_comp(diff_dict_1, label1, diff_dict_2, label2, target_line=True) -> Figure:
    """Combine error norm scaling of 2 setups."""

    figure = None
    for diff_dict, label in ((diff_dict_1, label1), (diff_dict_2, label2)):
        l2_norms = []
        lmax_norms = []
        for scale, vals in diff_dict.items():
            l2_norms.append(np.sqrt(np.sum(np.square(vals)) / scale))
            lmax_norms.append(np.max(np.abs(vals)))

        figure = l2_lmax_fig(
            l2_norms,
            lmax_norms,
            [1 / k for k in diff_dict],
            figure,
            (label, label),
            target_line,
        )

    figure.get_axes()[0].get_legend().set_title(r"${\lVert e \rVert}_{l^2}$")
    figure.get_axes()[1].get_legend().set_title(r"${\lVert e \rVert}_{l^\infty}$")

    return figure
