"""Module with helper functions to show results in jupyter lab."""

import io
import glob
import pickle

from IPython.display import display
from ipywidgets import Image, HBox, Output, Tab
import matplotlib
import numpy as np
from PIL import Image as PilImage

from evaluate import DataNames, profile_err_plots, error_plots, timeslice_norm_delta

matplotlib.rcParams["figure.facecolor"] = "w"
matplotlib.rcParams["legend.frameon"] = False
matplotlib.rcParams["text.usetex"] = True
matplotlib.rcParams[
    "text.latex.preamble"
] += r"\usepackage{lmodern}\usepackage{amsmath}"


def nb_display(fig: matplotlib.figure.Figure):
    """Call show() on fig if or use display(fig) when inline."""
    if "inline" in matplotlib.get_backend():
        display(fig)
    else:
        fig.show()


def load_figs(folder_name, DATA_PATH):
    """Unpickle saved figures and call show on them."""
    for pf, gf in zip(
        glob.iglob(f"{DATA_PATH}/{folder_name}/plot*.pickle"),
        glob.iglob(f"{DATA_PATH}/{folder_name}/ghost*.pickle"),
    ):
        with open(pf, "rb") as f:
            nb_display(pickle.load(f))
        with open(gf, "rb") as f:
            nb_display(pickle.load(f))


def load_png(folder_name, DATA_PATH):
    """Show saved png figures."""

    pchilds, gchilds = dict(), dict()
    for pf, gf in zip(
        glob.iglob(f"{DATA_PATH}/{folder_name}/plot*.png"),
        glob.iglob(f"{DATA_PATH}/{folder_name}/ghost*.png"),
    ):
        with PilImage.open(pf) as img:
            # scale the imgs
            img = img.resize(
                (450, int(img.size[1] * 450 / img.size[0])),
                PilImage.ANTIALIAS,
            )
            output = io.BytesIO()
            img.save(output, format="PNG")
            pchilds[DataNames.parse(pf).scale] = Image(value=output.getvalue())

        with PilImage.open(gf) as img:
            # scale the imgs
            img = img.resize(
                (600, int(img.size[1] * 600 / img.size[0])),
                PilImage.ANTIALIAS,
            )
            output = io.BytesIO()
            img.save(output, format="PNG")
            gchilds[DataNames.parse(gf).scale] = Image(value=output.getvalue())

    tab = Tab()
    srt_childs = sorted(pchilds)
    for i, k in enumerate(srt_childs):
        tab.set_title(i, f"Scale: 1/{k}")
    tab.children = [
        HBox([pchilds[k], gchilds[k]], layout={"align_items": "flex-start"})
        for k in srt_childs
    ]
    display(tab)


def profiles(folder_name, DATA_PATH):
    """Show profile plots."""
    childs_start, childs_end = dict(), dict()
    for fn in glob.iglob(f"{DATA_PATH}/{folder_name}/*.npz"):
        dn = DataNames.parse(fn)
        with np.load(fn) as data:
            data = dict(data)
            if dn.time > 0:
                data["end_t"] = dn.time
                childs_end[dn.scale] = data
            else:
                data["start_t"] = dn.time
                childs_start[dn.scale] = data

    childs_out = [(Output(), Output()) for _ in childs_start]
    for scale, out in zip(sorted(childs_start), childs_out):
        f1, f2 = profile_err_plots(scale, **childs_start[scale], **childs_end[scale])
        with out[0]:
            f1.canvas.header_visible = False
            nb_display(f1)
        with out[1]:
            f2.canvas.header_visible = False
            nb_display(f2)

    tab = Tab()
    tab.children = [HBox(ch) for ch in childs_out]
    for i, v in enumerate(sorted(childs_start)):
        tab.set_title(i, f"Scale: 1/{v}")
    display(tab)


def errors(folder_name, DATA_PATH):
    """Show error plots."""
    files = [
        x
        for x in map(DataNames.parse, glob.iglob(f"{DATA_PATH}/{folder_name}/*.npz"))
        if x.time > 0
    ]
    files.sort(key=lambda d: d.scale)
    out1, out2 = Output(), Output()
    f1, f2 = error_plots(
        {
            dn.scale: np.load(f"{DATA_PATH}/{folder_name}/{dn}.npz")["diff_vals"]
            for dn in files
        }
    )
    with out1:
        f1.canvas.header_visible = False
        nb_display(f1)
    with out2:
        f2.canvas.header_visible = False
        nb_display(f2)

    display(HBox([out2, out1]))


def callback_plots(folder_name, DATA_PATH, clb_name):
    """Show plots for callback results."""
    childs_end = {}
    for fn in glob.iglob(f"{DATA_PATH}/{folder_name}/*callbacks*.npz"):
        dn = DataNames.parse(fn)
        with np.load(fn) as data:
            data = dict(data)
            data["end_t"] = dn.time
            childs_end[dn.scale] = data[clb_name]

    childs_out = [Output() for _ in childs_end]
    for scale, out in zip(sorted(childs_end), childs_out):

        f1 = timeslice_norm_delta(scale, childs_end[scale])

        with out:
            f1.canvas.header_visible = False
            nb_display(f1)

    tab = Tab()
    tab.children = childs_out
    for i, v in enumerate(sorted(childs_end)):
        tab.set_title(i, f"Scale: 1/{v}")
    display(tab)
