"""A solver for scalar fields on mrlattice adaptive meshes."""

# version as tuple for simple comparisons
VERSION = (0, 0, 1)
# version string created from tuple; used by setup.cfg
__version__ = ".".join([str(x) for x in VERSION])

# FIXME: incomplete module list for star import
# add most frequent sim.XXXX and plotting.XXXX
__all__ = ["solvers", "examples"]
