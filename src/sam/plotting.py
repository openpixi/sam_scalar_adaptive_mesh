"""Plotting functionality.

- Types:
    - LatticePlot
    - LatticeDebugPlot
    - ValuePlot

The types require a lattice array (not the LatticeArray instance) and
provide convenience functions for updating time slices' values and
showing the plots. The type for debugging uses a 5bit mask to encode
different states for the cells. The debugging plots can also be written
to a file (their data that is) and read from said file. This is used
for the validation tests against known-good debug results.
"""
# remove after py 3.11
from __future__ import annotations

import matplotlib
import matplotlib.pyplot as plt
import numpy as np
from matplotlib import gridspec

from mrlattice.lattice.generate import RegionSlice
import mrlattice.plotting as mrlatplt

from sam.containers import NOPOS, NOREGION, ResRegData, ResRegPos, ResRegRegionTuple
from sam.solvers.refinement import BorderBase


class LatticePlot:
    """Convenience for plotting field values on the lattice."""

    def __init__(self, lattice):
        """Return new instance."""

        # pylint: disable=no-member

        self.grid = mrlatplt.generate_plot_lattice(lattice)
        temp_field: np.ma.MaskedArray = np.ma.masked_equal(self.grid, False, copy=True)
        self.plot_mask = ~temp_field.mask
        self.field: np.ma.MaskedArray = np.ma.asarray(temp_field, dtype=np.float_)
        self.fig = None
        self.clb = None

    def update_field_vals(
        self,
        time,
        vals: ResRegData,
        time_slice: RegionSlice,
        res,
        iter_res,
        reg_by_res: ResRegRegionTuple,
        pos_by_res: ResRegPos,
    ):
        """Populate time slice `t=time` of `self.field` with `vals`."""
        # counter to select regions sorted in lists by position
        count = dict(zip(res, [0] * len(res)))
        start = 0
        for reg in time_slice:
            # the "space" occupied in self.field
            stop = start + reg.n * reg.a * 2

            if reg.a in iter_res:

                if (
                    pos_by_res[reg.a, count[reg.a]] is NOPOS
                    and reg_by_res[reg.a, count[reg.a]] is NOREGION
                ):
                    # cusp
                    count[reg.a] += 1

                data = vals[reg.a, count[reg.a]]
                p = pos_by_res[reg.a, count[reg.a]]
                n_reg = reg_by_res[reg.a, count[reg.a]]

                # lattice array and data shape differ, eg. cusp
                if reg.n != n_reg.n:

                    count[reg.a] += 1
                    n_reg_post = reg_by_res[reg.a, count[reg.a]]
                    p_post = pos_by_res[reg.a, count[reg.a]]
                    data_post = vals[reg.a, count[reg.a]]

                    if p + n_reg.n == p_post:
                        assert reg.n == n_reg.n + n_reg_post.n
                        # merge with neighboring next reg possible

                        # create copies of selected data for repeat
                        # explicit use of arr and idx because of
                        # implicit copy when using advanced indexing
                        v = np.repeat(data.arr[data.idx[: n_reg.n]], 2 * reg.a)
                        vv = np.repeat(
                            data_post.arr[data_post.idx[: n_reg_post.n]], 2 * reg.a
                        )
                        v = np.concatenate((v, vv))

                    else:
                        raise Exception(
                            "LatticeArray and simulation data differ!\n"
                            "But regions couldn't be joined, they are not neighbors!"
                        )

                else:
                    # create copy of selected data for repeat
                    # explicit use of arr and idx because of
                    # implicit copy when using advanced indexing
                    v = np.repeat(data.arr[data.idx[: reg.n]], 2 * reg.a)

                self.field[2 * time, start:stop] = v[: stop - start]
                self.field[2 * time + 1, start:stop] = v[: stop - start]

            # advance the "space" and idx count
            start = stop
            count[reg.a] += 1

    def show(self, path=None, **kwargs):
        """Plot the current state of `self.field`."""

        self.prep_plot()

        if path:
            plt.savefig(path, **kwargs)
            plt.close(self.fig)
        else:
            plt.show(**kwargs)

    def prep_plot(self):
        """Prepare plot for showing."""

        # first plot lattice, then cells
        # so that mouse hover correctly shows cells' values, not grid
        self.fig, ax = mrlatplt.plot_mesh(self.grid, alpha_bg=1)
        self.field.mask = self.plot_mask

        im = ax.imshow(self.field, origin="lower", interpolation="none")
        self.clb = self.fig.colorbar(
            im, ax=ax, location="top", orientation="horizontal"
        )

        # remove mask after plotting
        self.field.mask = np.ma.nomask


class LatticeDebugPlot(LatticePlot):
    """Instead of field values, optimize the plot to show debug data."""

    def __init__(self, lattice):
        """`lattice` can be a string pointing to saved plot."""
        if isinstance(lattice, str):
            self.read(lattice)
        else:
            super().__init__(lattice)

    def prep_plot(self):
        """Prepare the current plot of debug data in `self.field`."""

        self.fig, ax = mrlatplt.plot_mesh(self.grid, alpha_bg=1)
        self.field.mask = self.plot_mask

        # plot used info instead of field values
        cmap = matplotlib.cm.get_cmap(name="nipy_spectral")
        bounds = np.arange(-1.5, 32.5)
        norm = matplotlib.colors.BoundaryNorm(bounds, cmap.N)

        mappable = ax.imshow(
            self.field, norm=norm, cmap=cmap, origin="lower", interpolation="none"
        )
        self.field.mask = np.ma.nomask

        # colorbar and labels
        self.clb = self.fig.colorbar(
            mappable,
            orientation="horizontal",
            location="top",
        )
        # description of bitmask values
        self.clb.set_label(
            "B - Boundary,  R - Used Right,  L - Used Left,  P - Used Pre,  "
            "W - Written,  0 - None"
        )

        tickpositions = [
            (bounds[i] + bounds[i + 1]) / 2 for i in range(len(bounds) - 1)
        ]
        self.clb.set_ticks(tickpositions)

        # first value for "black" color is empty
        ticklabels = [""]
        for t in [f"{vb:5b}" for vb in range(32)]:
            s = str()
            for i, v in zip(t, "BRLPW"):
                if i == "1":
                    s += v
                else:
                    s += "0"
            ticklabels.append(s)

        self.clb.ax.set_xticklabels(ticklabels, rotation=90)
        # add corresponding number in each colorbar cell
        for n, p in zip(range(-1, 32), tickpositions):
            self.clb.ax.text(p, 0.25, str(n), ha="center", va="center")

    def save(self, fname: str):
        """Save plot lattice to file `fname`."""
        np.savez(
            fname,
            field=self.field.data,
            field_mask=self.field.mask,
            grid=self.grid,
            plot_mask=self.plot_mask,
        )

    def read(self, fname: str):
        """Read plot lattice from file `fname` containing np array."""
        with np.load(fname + ".npz") as npz:
            self.field = np.ma.MaskedArray(npz["field"], mask=npz["field_mask"])
            self.grid = npz["grid"]
            self.plot_mask = npz["plot_mask"]


class ValuePlot:
    """Plots field values on a lattice without the lattice borders."""

    def __init__(self, lattice):
        """Allocate pixel array for plotting based on `lattice`."""
        self.lat = lattice
        # lattices are always rectangular
        self.field = np.zeros(
            (len(self.lat), sum(reg.a * reg.n for reg in self.lat[0]))
        )
        self.fig = None
        self.clb = None

    def update_field_vals(
        self,
        time,
        vals: ResRegData,
        time_slice: RegionSlice,
        res,
        iter_res,
        reg_by_res: ResRegRegionTuple,
        pos_by_res: ResRegPos,
    ):
        """Populate time slice `t=time` of `self.field` with `vals`."""
        # counter to select regions sorted in lists by position
        count = dict(zip(res, [0] * len(res)))
        start = 0
        for reg in time_slice:
            # the "space" occupied in self.field
            stop = start + reg.n * reg.a

            if reg.a in iter_res:

                if (
                    pos_by_res[reg.a, count[reg.a]] is NOPOS
                    and reg_by_res[reg.a, count[reg.a]] is NOREGION
                ):
                    # cusp
                    count[reg.a] += 1

                data = vals[reg.a, count[reg.a]]
                p = pos_by_res[reg.a, count[reg.a]]
                n_reg = reg_by_res[reg.a, count[reg.a]]

                # lattice array and data shape differ, eg. cusp
                if reg.n != n_reg.n:

                    count[reg.a] += 1
                    n_reg_post = reg_by_res[reg.a, count[reg.a]]
                    p_post = pos_by_res[reg.a, count[reg.a]]
                    data_post = vals[reg.a, count[reg.a]]

                    if p + n_reg.n == p_post:
                        assert reg.n == n_reg.n + n_reg_post.n
                        # merge with neighboring next reg possible

                        # create copies of selected data for repeat
                        # explicit use of arr and idx because of
                        # implicit copy when using advanced indexing
                        v = np.repeat(data.arr[data.idx[: n_reg.n]], reg.a)
                        vv = np.repeat(
                            data_post.arr[data_post.idx[: n_reg_post.n]], reg.a
                        )
                        v = np.concatenate((v, vv))

                    else:
                        raise Exception(
                            "LatticeArray and simulation data differ!\n"
                            "But regions couldn't be joined, they are not neighbors!"
                        )

                else:
                    # create copy of selected data for repeat
                    # explicit use of arr and idx because of
                    # implicit copy when using advanced indexing
                    v = np.repeat(data.arr[data.idx[: reg.n]], reg.a)

                self.field[time, start:stop] = v[: stop - start]

            # advance the "space" and idx count
            start = stop
            count[reg.a] += 1

    def prep_plot(self):
        """Plot the current state of `self.field`."""

        self.fig, ax = plt.subplots()
        ax.set_aspect("equal")
        # remove axes and ticks
        ax.set_xticks([])
        ax.set_yticks([])
        # allow alpha background
        self.fig.patch.set_visible(False)
        ax.set_frame_on(False)

        ax.set_xlabel(r"$x\rightarrow$")
        ax.set_ylabel(r"$t\rightarrow$")

        im = ax.imshow(self.field, origin="lower", interpolation="none")
        self.clb = self.fig.colorbar(
            im, ax=ax, location="top", orientation="horizontal"
        )

    def show(self, path=None, **kwargs):
        """Plot the current state of `self.field`."""

        self.prep_plot()

        if path:
            plt.savefig(path, **kwargs)
            plt.close(self.fig)
        else:
            plt.show(**kwargs)


class GhostPlot(LatticePlot):
    """Plot showing ghost cells."""

    def __init__(self, lattice):
        """Return new instance for ghost plots on `lattice`."""
        super().__init__(lattice)
        self.c_field: np.ma.MaskedArray = np.ma.copy(self.field)
        self.f_field = self.field

    def update_field_vals(self, time, borders):
        """Populate time slice `time` of `self.field` with borders."""
        border: BorderBase
        for border in borders:
            # loop through all cells to fill
            for t_offset, idx, reg, val in border.plot():
                # FIXME: reg will become reg.a in plot() return tuple
                res = reg.a
                t_idx = 2 * (time + t_offset * res)
                # skip out of bounds
                if t_idx + 1 >= len(self.field) or t_idx < 0:
                    continue

                field = self.c_field if res is border.reg.a else self.f_field
                idx *= 2
                idx_slice = idx + res * 2
                field[t_idx, idx:idx_slice] = val
                field[t_idx + 1, idx:idx_slice] = val

    def prep_plot(self):
        # creates ghost cell plots for fine cells
        super().prep_plot()
        other_ax, clb_ax = self.fig.axes[:2]

        # manually add coarse ghost cells' plot
        _, ax = mrlatplt.plot_mesh(self.grid, alpha_bg=1, fig=self.fig)

        # correct colorbar range
        # other_ax hast mappable for grid and cells
        mappable = other_ax.get_images()[1]
        mmin, mmax = mappable.get_clim()
        mmin = min(mmin, np.ma.amin(self.c_field))
        mmax = max(mmax, np.ma.amax(self.c_field))
        mappable.set_clim(vmin=mmin, vmax=mmax)

        # plot c_field
        self.c_field.mask = self.plot_mask
        ax.imshow(
            self.c_field,
            norm=self.clb.norm,
            cmap=self.clb.cmap,
            origin="lower",
            interpolation="none",
        )
        self.c_field.mask = np.ma.nomask

        # re-arrange axes for nice look
        gs = gridspec.GridSpec(3, 1, height_ratios=(0.1, 0.45, 0.45))
        clb_ax.set_position(gs[0, 0].get_position(self.fig))
        other_ax.set_position(gs[1, 0].get_position(self.fig))
        ax.set_position(gs[2, 0].get_position(self.fig))
        clb_ax.set_subplotspec(gs[0, 0])
        other_ax.set_subplotspec(gs[1, 0])
        ax.set_subplotspec(gs[2, 0])
        self.fig.subplots_adjust(bottom=0.075)

        # labels
        other_ax.set_title("Fine cells")
        ax.set_title("Coarse cells")
        self.fig.supxlabel("Ghost cells", size=22)


class GhostDebugPlot(GhostPlot, LatticeDebugPlot):
    """Debug plot showing ghost cells."""

    def __init__(self, lattice):
        """`lattice` can be a string pointing to saved plot."""
        if isinstance(lattice, str):
            self.read(lattice)
            self.field = self.f_field
        else:
            super().__init__(lattice)

    def save(self, fname: str):
        """Save plot lattice values to file `fname`."""
        np.savez(
            fname,
            f_field=self.f_field.data,
            f_mask=self.f_field.mask,
            c_field=self.c_field.data,
            c_mask=self.c_field.mask,
            grid=self.grid,
            plot_mask=self.plot_mask,
        )

    def read(self, fname: str):
        """Read plot lattice from file `fname` containing np array."""
        with np.load(fname + ".npz") as npz:
            self.f_field = np.ma.MaskedArray(npz["f_field"], mask=npz["f_mask"])
            self.c_field = np.ma.MaskedArray(npz["c_field"], mask=npz["c_mask"])
            self.grid = npz["grid"]
            self.plot_mask = npz["plot_mask"]


# TODO:
class GhostValuePlot(GhostPlot, ValuePlot):
    """Plot ghost cell values without the lattice borders."""
