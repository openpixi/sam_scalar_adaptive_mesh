#! /usr/bin/env python3

"""SuperimposePulseLatticeArray with two colliding pulses.

Run this file either as a script:
`python3 /path/so/sam/examples/scripts/collision_pulses.py`
or as a module:
`python3 -m sam.examples.scripts.collision_pulses`
"""

import argparse
from ast import literal_eval
from typing import Callable, TypeVar

import matplotlib

from mrlattice.lattice.configure import LatticeArrayScheme
from mrlattice.lattice.generate import LatticeArray, SuperimposePulseLatticeArray
from sam.init import single_gauss_pulse
from sam.plotting import GhostPlot, LatticePlot, ValuePlot
from sam.sim import ScalarField
from sam.solvers import leapfrog
from sam.solvers.refinement import BORDER_TYPES, cusp_refinement

PrepLatType = TypeVar("PrepLatType")


def cl_args() -> argparse.Namespace:
    """Setup and parse command line (cl) arguments."""

    parser = argparse.ArgumentParser(description="Run a `ScalarField` setup.")
    parser.add_argument(
        "--substeps",
        action="store_const",
        const=True,
        default=False,
        help="Display lattice and ghost plot after each time step.",
        dest="show_steps",
    )
    parser.add_argument(
        "--initplot",
        action="store_const",
        const=True,
        default=False,
        help="Display plots for initial conditions.",
        dest="show_init",
    )
    parser.add_argument(
        "--figsize",
        default=(15, 10),
        type=lambda s: tuple(literal_eval(s)),
        help="Set figure (height,width) in inches (Default (15,10)).",
        metavar="(H,W)",
    )
    parser.add_argument(
        "--savefig",
        default=None,
        type=str,
        help="Save all plots to `PATH` instead of interactive plots.",
        metavar="PATH",
    )
    parser.add_argument(
        "--hidemesh",
        action="store_const",
        const=True,
        default=False,
        help="Show the lattice mesh",
    )

    conf = parser.parse_args()
    conf.savefig_ghost = f"{conf.savefig}_ghost" if conf.savefig else None
    return conf


def prep_lattice(_conf: argparse.Namespace) -> LatticeArray:
    """Generate the simulation lattice."""

    props_dict = {
        "scheme": LatticeArrayScheme.FIXED_NUMBER_SCHEME,
        "coarse_steps": 2,
        "borders_width": 8,
        "buffer": 2,
        "finest_width": 10,
        "repeat": 16,
    }
    lattice = SuperimposePulseLatticeArray(**props_dict)
    lattice.generate_array()
    return lattice


def sim_setup(
    conf: argparse.Namespace, lattice: Callable[[argparse.Namespace], LatticeArray]
):
    """Setup `ScalarField` based on `conf`."""

    matplotlib.rcParams["figure.figsize"] = conf.figsize

    lat = lattice(conf)

    plot = ValuePlot(lat.array) if conf.hidemesh else LatticePlot(lat.array)
    ghost = GhostPlot(lat.array)

    sim = ScalarField(
        lat, (1,), 1, leapfrog.EvolveLeapfrog(), BORDER_TYPES, cusp_refinement
    )

    sim.init_cell_centered(single_gauss_pulse(29, 8, 0.001, 1), direction=1)
    sim.init_cell_centered(single_gauss_pulse(91, 8, 0.001, 1), direction=-1)

    if conf.show_init:
        plot.update_field_vals(
            1,
            sim.dd_now,
            sim.array[0],
            sim.res_list,
            sim.res_list,
            sim.reg_by_res_now,
            sim.p_now,
        )
        plot.update_field_vals(
            0,
            sim.dd_pre,
            sim.array[0],
            sim.res_list,
            sim.res_list,
            sim.reg_by_res_pre,
            sim.p_pre,
        )
        for res in sim.res_list:
            ghost.update_field_vals(res * 2, sim.border_by_res[res * 2])

        ghost.show(conf.savefig_ghost, block=False)
        plot.show(conf.savefig)

    # clear all plot cells' values
    plot.field.fill(0.0)
    ghost.c_field.fill(0.0)
    ghost.f_field.fill(0.0)

    # init plots' first time slice
    plot.update_field_vals(
        sim.t,
        sim.dd_now,
        sim.array[0],
        sim.res_list,
        sim.res_list,
        sim.reg_by_res_now,
        sim.p_now,
    )
    for res in sim.res_list:
        ghost.update_field_vals(0, sim.border_by_res[res * 2])

    return sim, plot, ghost


def sim_loop(
    conf: argparse.Namespace,
    setup: Callable[
        [argparse.Namespace, PrepLatType],
        tuple[ScalarField, LatticePlot, GhostPlot],
    ],
    lattice: PrepLatType,
):
    """Run `sim` and fill `plot` and `ghost` plots."""

    sim, plot, ghost = setup(conf, lattice)

    for t in sim:

        # update plots
        plot.update_field_vals(
            sim.t,
            sim.dd_now,
            sim.array[t],
            sim.res_list,
            sim.res_list,
            sim.reg_by_res_now,
            sim.p_now,
        )
        for res in sim.res_list:
            ghost.update_field_vals(sim.t, sim.border_by_res[res * 2])

        if conf.show_steps:
            ghost.show(conf.savefig_ghost, block=False)
            plot.show(conf.savefig)

    ghost.show(conf.savefig_ghost, block=False)
    plot.show(conf.savefig)


if __name__ == "__main__":
    sim_loop(cl_args(), sim_setup, prep_lattice)
