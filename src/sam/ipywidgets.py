"""Additional widgets for Jupyter notebooks."""

import sys
from dataclasses import asdict, dataclass
from inspect import signature
from typing import Callable, Type

import numpy as np
import numpy.typing as npt
from IPython.core import getipython
from IPython.core.magic import register_line_magic
from IPython.display import display
import ipywidgets as widgets
from packaging import specifiers, version

import mrlattice
import mrlattice.lattice
import mrlattice.lattice.validate
import mrlattice.plotting
from mrlattice.ipywidgets import LatticeArrayPlotWidget
from mrlattice.lattice.generate import LatticeArray

import sam
from sam.init import single_caret_pulse, single_gauss_pulse, single_step_pulse
from sam.plotting import (
    GhostDebugPlot,
    GhostPlot,
    LatticeDebugPlot,
    LatticePlot,
    ValuePlot,
)
from sam.sim import ScalarField
from sam.solvers import DebugResetEvolve, leapfrog, Solver
from sam.solvers.refinement import (
    BorderTypes,
    BORDER_TYPES,
    DEBUG_BORDER_TYPES,
    DEBUG_PASS_BORDER_TYPES,
)
from sam.solvers.refinement.cusps.cusp_legacy import (
    cusp_refinement,
    cusp_refinement_debug,
    cusp_refinement_debug_pass,
)

__all__ = ["SimulationWidget"]


@register_line_magic
def sam_version(line: str):
    """Check if installed sam version matches set version."""

    nb_version_lock = specifiers.SpecifierSet(line)
    installed_version = version.parse(sam.__version__)

    warning_msg = (
        f"The installed version of sam {installed_version}"
        f" does not match the required version {nb_version_lock}!"
        " Please install the correct version."
    )

    if installed_version not in nb_version_lock:
        raise ImportError(warning_msg)


# as per ipython documentation
del sam_version


@dataclass(slots=True)
class SimMode:
    """Namespace for sim_mode."""

    lattice_plot: Type[LatticePlot]
    ghost_plot: Type[GhostPlot]
    solver: Solver
    border_types: BorderTypes
    cusp_refinement: Callable
    dtype: npt.DTypeLike

    def sim_args(self):
        """Return tuple with args for instantiating simulations."""
        return self.solver, self.border_types, self.cusp_refinement, self.dtype


class SimulationWidget(widgets.Box):
    """Widget for controlling simulations."""

    def __init__(self):
        """Prepare new instance."""
        super().__init__()

        # plot output
        self.plot_out = widgets.Output()
        self.sim = None
        self.plot = None
        self.ghost_plot = None

        # widget layouting
        self.w_layout = widgets.Layout(width="max-content", margin="10px 0px 0px 10px")
        self.l_layout = widgets.Layout(width="max-content", margin="10px 0px 0px 20px")
        self.b_layout = widgets.Layout(margin="10px 0px 0px 10px")

        # dropdown widget for involved LA
        self.lattice_dropdown = widgets.Dropdown(layout=self.b_layout)
        self.lattice_dropdown_label = widgets.Label(
            "Lattice Array:", layout=self.l_layout
        )

        # inputs for dt (sim time) and a (lattice spacing)
        self.a_float = widgets.FloatText(
            value=1,
            layout={"width": "70px", "margin": "10px 0px 0px 10px"},
        )
        self.a_label = widgets.Label("$a^0/a^1=$", layout=self.l_layout)
        self.dt_float = widgets.FloatText(
            value=1,
            layout={"width": "70px", "margin": "10px 0px 0px 10px"},
        )
        self.dt_label = widgets.Label("$t/a^0=$", layout=self.l_layout)

        # radio button for init scheme
        self.init_scheme = widgets.RadioButtons(
            options=(
                ("Averaged", "init_cell_averaged"),
                ("Centered", "init_cell_centered"),
            ),
            value="init_cell_centered",
            layout={"width": "max-content", "margin": "10px 10px 10px 10px"},
        )
        self.init_scheme_label = widgets.Label(
            "Initialization Scheme:", layout=self.l_layout
        )

        # radio button for debug selection
        self.sim_mode = widgets.RadioButtons(
            options=[
                [
                    "Values",
                    SimMode(
                        LatticePlot,
                        GhostPlot,
                        leapfrog.EvolveLeapfrog(),
                        BORDER_TYPES,
                        cusp_refinement,
                        np.float_,
                    ),
                ],
                [
                    "Debug",
                    SimMode(
                        LatticeDebugPlot,
                        GhostDebugPlot,
                        DebugResetEvolve(),
                        DEBUG_PASS_BORDER_TYPES,
                        cusp_refinement_debug_pass,
                        np.int8,
                    ),
                ],
            ],
            layout={"width": "max-content", "margin": "10px 10px 10px 10px"},
        )
        self.sim_mode_label = widgets.Label("Simulation Mode:", layout=self.l_layout)
        self.sim_mode.observe(
            self.plot_out.capture()(self._sim_mode_obs), names="value"
        )
        # checkboxes for selecting debug replacements
        self.debug_select_label = widgets.Label(
            "Write debug values to", layout=self.l_layout
        )
        self.region_debug_chckbox = widgets.Checkbox(
            value=False,
            description="regions",
            indent=False,
            layout=self.w_layout,
        )
        self.border_debug_chckbox = widgets.Checkbox(
            value=False,
            description="borders",
            indent=False,
            layout=self.w_layout,
        )
        self.cusp_debug_chckbox = widgets.Checkbox(
            value=False,
            description="cusps",
            indent=False,
            layout=self.w_layout,
        )
        # checkbox for hiding lattice border
        self.hide_lattice_chckbox = widgets.Checkbox(
            value=False,
            description="Hide Lattice Borders",
            indent=False,
            layout=self.w_layout,
        )
        self.hide_lattice_chckbox.observe(
            self.plot_out.capture()(self._hide_lattice_chckbox_obs), names="value"
        )
        # checkbox for plotting ghost cell plots
        self.show_ghost_chckbox = widgets.Checkbox(
            value=False,
            description="Show Ghost Cell Plots",
            indent=False,
            layout=self.l_layout,
        )

        self.pulse_shape = widgets.Dropdown(
            options=[
                ("Please Select", None),
                ("Step Pulse", single_step_pulse),
                ("Gaussian", single_gauss_pulse),
                ("Caret Pulse", single_caret_pulse),
            ],
            layout=self.w_layout,
        )
        self.pulse_shape_label = widgets.Label("Pulse Shape:", layout=self.l_layout)

        # progress bar
        self.progress = widgets.IntProgress(
            value=0,
            min=0,
            max=0,
            bar_style="success",
            style={"bar_color": "green"},
            layout=self.b_layout,
        )
        self.progress_label = widgets.Label(
            "Simulation Progress:", layout=self.l_layout
        )
        self.progress_percent = widgets.Label("0%", layout=self.b_layout)

        # buttons
        self.run_btn = widgets.Button(
            description="Run Simulation", layout=self.b_layout
        )
        self.clear_btn = widgets.Button(
            description="Clear Output", layout=self.b_layout
        )
        self.refresh_btn = widgets.Button(
            description="Refresh Options", layout=self.b_layout
        )
        self.add_pulse_btn = widgets.Button(description="Add", layout=self.b_layout)
        # button callbacks
        self.run_btn.on_click(self.plot_out.capture()(self._run_btn_on_click))
        self.clear_btn.on_click(self.plot_out.capture()(self._clear_btn_on_click))
        self.refresh_btn.on_click(self.plot_out.capture()(self._refresh_btn_on_click))
        self.add_pulse_btn.on_click(
            self.plot_out.capture()(self._add_pulse_btn_on_click)
        )

        # populate list with existing
        self._refresh_btn_on_click()

        # named box for pulse config
        self.pulse_box = widgets.VBox(layout={"width": "100%"})

        # save debug replacement selection for later
        self.debug_checkboxs = widgets.HBox(
            [
                self.debug_select_label,
                self.region_debug_chckbox,
                self.border_debug_chckbox,
                self.cusp_debug_chckbox,
            ]
        )
        # register all widgets with self
        self.children = (
            widgets.VBox(
                [
                    widgets.HBox(
                        [
                            self.sim_mode_label,
                            self.sim_mode,
                            self.init_scheme_label,
                            self.init_scheme,
                        ]
                    ),
                    self.show_ghost_chckbox,
                    widgets.HBox(
                        [
                            self.dt_label,
                            self.dt_float,
                            self.a_label,
                            self.a_float,
                        ]
                    ),
                    widgets.HBox(
                        [
                            widgets.VBox(
                                [
                                    self.lattice_dropdown_label,
                                    self.pulse_shape_label,
                                ],
                            ),
                            widgets.VBox(
                                [
                                    widgets.HBox(
                                        [
                                            self.lattice_dropdown,
                                            self.hide_lattice_chckbox,
                                        ]
                                    ),
                                    widgets.HBox(
                                        [
                                            self.pulse_shape,
                                            self.add_pulse_btn,
                                        ]
                                    ),
                                ]
                            ),
                        ]
                    ),
                    self.pulse_box,
                    widgets.HBox(
                        [
                            self.run_btn,
                            self.clear_btn,
                            self.refresh_btn,
                            self.progress_label,
                            self.progress_percent,
                            self.progress,
                        ]
                    ),
                ],
                layout={"width": "100%"},
            ),
        )

    def _ipython_display_(self, **kwargs):
        """Append to super `display()` call to display `plot_out`."""
        super()._ipython_display_(**kwargs)
        display(self.plot_out)

    def _run_btn_on_click(self, *_):
        self._clear_btn_on_click()

        # prepare lattice array instances
        if isinstance(self.lattice_dropdown.value, LatticeArray):
            output_lattice = self.lattice_dropdown.value
        elif isinstance(self.lattice_dropdown.value, LatticeArrayPlotWidget):
            output_lattice = self.lattice_dropdown.value.lattice
        elif self.lattice_dropdown.value is None:
            raise Exception("Please select a valid lattice")

        # init plot and sim based on sim_mode
        tmp_sim_mode = SimMode(**asdict(self.sim_mode.value))
        self.plot = tmp_sim_mode.lattice_plot(output_lattice.array)
        if self.show_ghost_chckbox.value:
            self.ghost_plot = tmp_sim_mode.ghost_plot(output_lattice.array)

        if self.sim_mode.label == self.sim_mode.options[1][0]:
            # select debug replacements based on selection
            if self.region_debug_chckbox.value is True:
                tmp_sim_mode.solver = leapfrog.DebugEvolveLeapfrog()
            if self.border_debug_chckbox.value is True:
                tmp_sim_mode.border_types = DEBUG_BORDER_TYPES
            if self.cusp_debug_chckbox.value is True:
                tmp_sim_mode.cusp_refinement = cusp_refinement_debug

        self.sim = ScalarField(
            output_lattice,
            (self.a_float.value,),
            self.dt_float.value,
            *tmp_sim_mode.sim_args(),
        )

        # update progress bar max
        self.progress.max = self.sim.max_t - 1

        # manage init conditions, if not Debug
        if not isinstance(self.plot, LatticeDebugPlot):
            # isinstance(self.plot, LatticePlot) is always true
            for hbox in self.pulse_box.children:
                params = [
                    val.value
                    for val in hbox.children
                    if isinstance(val, widgets.FloatText)
                ]

                getattr(self.sim, self.init_scheme.value)(
                    hbox.init_filler(*params), direction=hbox.children[3].value
                )

        # init plot's first time slice
        self.plot.update_field_vals(
            self.sim.t,
            self.sim.dd_now,
            self.sim.array[0],
            self.sim.res_list,
            self.sim.res_list,
            self.sim.reg_by_res_now,
            self.sim.p_now,
        )

        # evolve
        for t in self.sim:
            # update current time slice's vals
            self.plot.update_field_vals(
                self.sim.t,
                self.sim.dd_now,
                self.sim.array[t],
                self.sim.res_list,
                self.sim.res_list,
                self.sim.reg_by_res_now,
                self.sim.p_now,
            )
            # update previous time slice's vals for debug
            if isinstance(self.plot, LatticeDebugPlot):
                self.plot.update_field_vals(
                    self.sim.t - 1,
                    self.sim.dd_pre,
                    self.sim.array[t - 1],
                    self.sim.res_list,
                    self.sim.iter_res,
                    self.sim.reg_by_res_pre,
                    self.sim.p_pre,
                )
            # update ghost plot
            if self.show_ghost_chckbox.value:
                for res in self.sim.res_list:
                    self.ghost_plot.update_field_vals(
                        self.sim.t, self.sim.border_by_res[res * 2]
                    )

            # update progress bar
            self.progress.value = t
            self.progress_percent.value = f"{100*t/(self.sim.max_t-1):.0f}%"

        # plot lattice
        with self.plot_out:
            self.plot.show()
            if self.show_ghost_chckbox.value:
                self.ghost_plot.show()

    def _clear_btn_on_click(self, *_):
        self.plot_out.clear_output()
        if self.plot:
            mrlattice.plotting.plt.close(self.plot.fig)
            self.sim, self.plot = None, None
            self.progress.value = 0
            self.progress_percent.value = "0%"
        if self.ghost_plot:
            mrlattice.plotting.plt.close(self.ghost_plot.fig)

    def _refresh_btn_on_click(self, *_):
        # update available LatticeArrays from globals
        user_ns = getipython.get_ipython().user_ns
        options = [("Please Select", None)] + [
            name
            for name in user_ns.items()
            if isinstance(name[1], (LatticeArray, LatticeArrayPlotWidget))
        ]
        self.lattice_dropdown.options = options
        self.lattice_dropdown.label = "Please Select"

    def _add_pulse_btn_on_click(self, *_):
        # add a new pulse and update widgets with pulse options

        # all widgets
        children = []
        del_btn = widgets.Button(
            description="X", tooltip="remove", layout=self.l_layout
        )
        children.extend(
            [
                del_btn,
                widgets.Label(
                    self.pulse_shape.label + ":",
                    layout={"width": "80px", "margin": "10px 0px 0px 10px"},
                ),
                widgets.Label("Direction:", layout=self.l_layout),
                widgets.RadioButtons(
                    options=[("Right", 1), ("Left", -1)], layout=self.w_layout
                ),
            ]
        )
        try:
            for par in signature(self.pulse_shape.value).parameters.values():
                children.extend(
                    [
                        widgets.Label(par.name + " =", layout=self.l_layout),
                        widgets.FloatText(
                            layout={"width": "90px", "margin": "10px 0px 0px 10px"},
                        ),
                    ]
                )
        except TypeError:
            print(
                f"The selected pulse shape ''{self.pulse_shape.value}'' is invalid.",
                file=sys.stderr,
            )
            return

        # put all into one referenced hbox
        widget = widgets.HBox(children, height="max-content")
        widget.init_filler = self.pulse_shape.value

        def remove_me_on_click(_, *__):
            idx = self.pulse_box.children.index(widget)
            self.pulse_box.children = (
                self.pulse_box.children[:idx] + self.pulse_box.children[idx + 1 :]
            )

        del_btn.on_click(self.plot_out.capture()(remove_me_on_click))

        self.pulse_box.children = self.pulse_box.children + (widget,)

    def _sim_mode_obs(self, change):
        if change["new"] == self.sim_mode.options[0][1]:
            # enable initial pulse placement
            self.hide_lattice_chckbox.disabled = False
            self.pulse_shape.disabled = False
            self.add_pulse_btn.disabled = False
            for w in self.pulse_box.children:
                for ww in w.children:
                    ww.disabled = False
            # hide debug replacement selection
            tmp_childs = list(self.children[0].children)
            tmp_childs.remove(self.debug_checkboxs)
            self.children[0].children = tmp_childs
        elif change["new"] == self.sim_mode.options[1][1]:
            # disable initial pulse placement
            self.hide_lattice_chckbox.disabled = True
            self.pulse_shape.disabled = True
            self.add_pulse_btn.disabled = True
            for w in self.pulse_box.children:
                for ww in w.children:
                    ww.disabled = True
            # show debug replacement selection
            tmp_childs = list(self.children[0].children)
            tmp_childs.insert(1, self.debug_checkboxs)
            self.children[0].children = tmp_childs

    def _hide_lattice_chckbox_obs(self, change):
        # change plotting class for values
        if change["new"] is True:
            self.sim_mode.value.lattice_plot = ValuePlot
        elif change["new"] is False:
            self.sim_mode.value.lattice_plot = LatticePlot
