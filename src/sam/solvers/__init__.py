"""Modules implementing the ScalarField solvers."""

from abc import ABC, abstractmethod

from mrlattice.lattice.generate import RegionTuple

from sam.containers import Data


class Solver(ABC):
    """Interface for solvers."""

    @staticmethod
    @abstractmethod
    def evolve_stencil(
        a2, x, p_ds_pre, p_ds_nxt, d_pre: Data, d_now: Data, d_nxt: Data
    ):
        """Calculate a single value of the next time slice at `x`."""

    def evolve_region(
        self,
        a2,
        xmin,
        xmax,
        p_ds_pre,
        p_ds_nxt,
        reg_now: RegionTuple,
        reg_nxt: RegionTuple,
        d_pre: Data,
        d_now: Data,
        d_nxt: Data,
    ):
        """Calculate all values in the region for the next time slice.

        The range is given by all x in [xmin, reg_now.n + xmax) and is
        not necessarily equal to the full regions' width.
        """
        for x in range(xmin, reg_now.n + xmax):
            self.evolve_stencil(a2, x, p_ds_pre, p_ds_nxt, d_pre, d_now, d_nxt)

    # FIXME: this alias is for backwards compatibility
    def __call__(
        self,
        a2,
        xmin,
        xmax,
        p_ds_pre,
        p_ds_nxt,
        reg_now: RegionTuple,
        reg_nxt: RegionTuple,
        d_pre: Data,
        d_now: Data,
        d_nxt: Data,
    ):
        """Call `self.evolve_region`."""
        self.evolve_region(
            a2, xmin, xmax, p_ds_pre, p_ds_nxt, reg_now, reg_nxt, d_pre, d_now, d_nxt
        )


class DebugResetEvolve(Solver):
    """Stencil that resets nxt slice to zeros."""

    @staticmethod
    def evolve_stencil(_a2, x, _p_ds_pre, p_ds_nxt, _d_pre, _d_now, d_nxt):
        # Literal[0] works as (float) value and debug (MARKER_RESET)
        d_nxt[x + p_ds_nxt] = 0


class DebugPassEvolve(Solver):
    """Passing stencil that does nothing."""

    @staticmethod
    def evolve_stencil(*_):
        """Do nothing."""

    def evolve_region(self, *_):
        """Do nothing."""
