"""Commons for debug border types."""

from itertools import cycle
from unittest.mock import patch

import numpy as np

from mrlattice.lattice.generate import RegionTuple

from sam.solvers import DebugPassEvolve
from sam.solvers.refinement.border_base import BorderBase

# set up the patcher to patch the evolve step methods
patch.TEST_PREFIX = "evolve_"

# bitmask for debug data values:
# 2 **  4  3  2  1  0
#       B  R  L  P  W
MARKER_BOUNDARY = 0b10000  # B : accessed as boundary of region
MARKER_RIGHT = 0b01000  # R : accessed as left of current cell
MARKER_LEFT = 0b00100  # L : accessed as right of current cell
MARKER_PRE = 0b00010  # P : accessed as previous time slice cell
MARKER_WRITTEN = 0b00001  # W : written
MARKER_RESET = 0b00000  # reset cell


def debug_reset(a, a_idx, *_):
    """Resets the value of the first arg cell."""
    a[a_idx] = MARKER_RESET


def debug_copy_to(src, src_idx, dst, dst_idx):
    """Debug values for copy_to."""
    dst[dst_idx] = MARKER_WRITTEN
    dst[dst_idx] |= MARKER_BOUNDARY
    src[src_idx] |= MARKER_PRE


def debug_abg_diag_border(a, a_idx, b, b_idx, A, A_idx, B, B_idx, C, C_idx, D, D_idx):
    """Ref func with alpha, beta, gamma. Fill args with debug values."""
    # a = b + alpha(A-B) + (1-alpha)(C-D)
    a[a_idx] = MARKER_WRITTEN
    a[a_idx] |= MARKER_BOUNDARY
    b[b_idx] |= MARKER_PRE
    A[A_idx] |= MARKER_RIGHT
    B[B_idx] |= MARKER_LEFT
    C[C_idx] |= MARKER_RIGHT
    D[D_idx] |= MARKER_LEFT


def debug_d_diag_border(a, a_idx, b, b_idx, A, A_idx, B, B_idx):
    """Ref func with delta. Fill args with debug values."""
    # a = b + delta/2*(A-B)
    a[a_idx] = MARKER_WRITTEN
    a[a_idx] |= MARKER_BOUNDARY
    b[b_idx] |= MARKER_PRE
    A[A_idx] |= MARKER_RIGHT
    B[B_idx] |= MARKER_LEFT


def debug_average(A, A_idx, a, a_idx, b, b_idx, c, c_idx, d, d_idx):
    """Ref func for avg of 4 finer cells with debug values."""
    # A = alpha(a + b + c + d)
    A[A_idx] = MARKER_WRITTEN
    A[A_idx] |= MARKER_BOUNDARY
    a[a_idx] |= MARKER_LEFT
    b[b_idx] |= MARKER_RIGHT
    c[c_idx] |= MARKER_LEFT
    d[d_idx] |= MARKER_RIGHT


# mapping sam.solvers.refinement.schemes funcs to their debug equiv
SCHEME_EQUIVALENT_FUNC = {
    "alpha_diag_border": debug_abg_diag_border,
    "beta_diag_border": debug_abg_diag_border,
    "gamma_diag_border": debug_abg_diag_border,
    "delta_diag_border": debug_d_diag_border,
    "average": debug_average,
}


# specials for straight border


def debug_straight_v3_special_1(
    e, e_idx, F, F_idx, A, A_idx, H, H_idx, j, j_idx, g, g_idx, f, f_idx
):
    """Ref func for firest step of 'e' in V3 straight border."""
    e[e_idx] = MARKER_WRITTEN
    e[e_idx] |= MARKER_BOUNDARY
    A[A_idx] |= MARKER_RIGHT
    j[j_idx] |= MARKER_RIGHT
    H[H_idx] |= MARKER_LEFT
    g[g_idx] |= MARKER_LEFT
    F[F_idx] |= MARKER_PRE
    f[f_idx] |= MARKER_PRE


def debug_straight_v3_special_2(
    e, e_idx, F, F_idx, a, a_idx, j, j_idx, g, g_idx, f, f_idx
):
    """Ref func for second step of 'e' in V3 straight border."""
    e[e_idx] = MARKER_WRITTEN
    e[e_idx] |= MARKER_BOUNDARY
    a[a_idx] |= MARKER_RIGHT
    j[j_idx] |= MARKER_RIGHT
    f[f_idx] |= MARKER_LEFT
    g[g_idx] |= MARKER_LEFT
    F[F_idx] |= MARKER_PRE


def debug_straight_v5_special(
    G, G_idx, H, H_idx, K, K_idx, r, r_idx, s, s_idx, t, t_idx, q, q_idx
):
    """Ref func for 'G' in V5 with debug values."""
    G[G_idx] = MARKER_WRITTEN
    G[G_idx] |= MARKER_BOUNDARY
    H[H_idx] |= MARKER_LEFT
    K[K_idx] |= MARKER_PRE
    r[r_idx] |= MARKER_LEFT
    s[s_idx] |= MARKER_RIGHT
    t[t_idx] |= MARKER_PRE
    q[q_idx] |= MARKER_RIGHT
    q[q_idx] |= MARKER_LEFT


# mapping sam.solvers.refinement.schemes straight funcs to debug equiv
SCHEME_STRAIGHT_EQUIVALENT_FUNC = {
    "copy_to": debug_copy_to,
    "straight_v3_special_1": debug_straight_v3_special_1,
    "straight_v3_special_2": debug_straight_v3_special_2,
    "straight_v5_special": debug_straight_v5_special,
}


# specials for coarse to fine cusp


def debug_lambda_cusp(d, d_idx, g, g_idx, B, B_idx, D, D_idx, L, L_idx, O, O_idx):
    """Ref func with lambda for CCTF V2 dedicated with debug vals."""
    d[d_idx] = MARKER_WRITTEN
    d[d_idx] |= MARKER_BOUNDARY
    g[g_idx] |= MARKER_PRE
    B[B_idx] |= MARKER_RIGHT
    D[D_idx] |= MARKER_LEFT
    L[L_idx] |= MARKER_RIGHT
    O[O_idx] |= MARKER_LEFT


def debug_kappa_cusp(
    e,
    e_idx,
    f,
    f_idx,
    h,
    h_idx,
    j,
    j_idx,
    C,
    C_idx,
    B,
    B_idx,
    D,
    D_idx,
    L,
    L_idx,
    O,
    O_idx,
):
    """Ref func with kappa for CCTF V2 dedicated with debug vals."""
    e[e_idx] = MARKER_WRITTEN
    e[e_idx] |= MARKER_BOUNDARY
    f[f_idx] |= MARKER_RIGHT
    f[f_idx] |= MARKER_PRE
    h[h_idx] |= MARKER_LEFT
    h[h_idx] |= MARKER_PRE
    j[j_idx] |= MARKER_RIGHT
    j[j_idx] |= MARKER_PRE
    C[C_idx] |= MARKER_RIGHT
    B[B_idx] |= MARKER_RIGHT
    D[D_idx] |= MARKER_LEFT
    L[L_idx] |= MARKER_RIGHT
    O[O_idx] |= MARKER_LEFT


def debug_cctf_v2_b(b, b_idx, e, e_idx, g, g_idx, c, c_idx, f, f_idx, A, A_idx):
    """Ref func for b in CCTF V2 dedicated."""
    b[b_idx] = MARKER_WRITTEN
    b[b_idx] |= MARKER_BOUNDARY
    e[e_idx] |= MARKER_RIGHT
    e[e_idx] |= MARKER_PRE
    g[g_idx] |= MARKER_RIGHT
    g[g_idx] |= MARKER_PRE
    c[c_idx] |= MARKER_RIGHT
    c[c_idx] |= MARKER_PRE
    f[f_idx] |= MARKER_LEFT
    f[f_idx] |= MARKER_PRE
    A[A_idx] |= MARKER_LEFT


# mapping sam.solvers.refinement.cusps funcs to debug equiv
SCHEME_CUSPS_EQUIVALENT_FUNC = {
    "lambda_cusp": debug_lambda_cusp,
    "kappa_cusp": debug_kappa_cusp,
    "cctf_v2_c": debug_d_diag_border,  # same structure
    "cctf_v2_b": debug_cctf_v2_b,
}
SCHEME_SSC_CUSPS_EQUIVALENT_FUNC = {}
SCHEME_SDC_CUSPS_EQUIVALENT_FUNC = {}


class DebugBorderPass(BorderBase):
    """Base class for borders that do nothing."""

    # TODO: This got too tightly coupled to concrete implementations
    # of BorderBase. Figure out a way to decouple again.

    __slots__: set[str] = set()

    def __init__(
        self,
        border_reg: RegionTuple,
        sort_idx,
        arr_idx,
        pos,
        neigh_reg: RegionTuple,
        neighbor_arr_offset,
        neighbor_sort_idx,
        a2,
        _solver,
        border_types,
        *,
        parent=None,
        skip=False,
    ):
        """Return new `BorderBase` instance that passes evolve steps."""
        super().__init__(
            border_reg,
            sort_idx,
            arr_idx,
            pos,
            neigh_reg,
            neighbor_arr_offset,
            neighbor_sort_idx,
            a2,
            DebugPassEvolve(),  # hardcode passing stencil
            border_types,
            parent=parent,
            skip=skip,
        )
        self.d_ghost = np.empty((0,))
        self._cell_names = {}
        self.cell_g = {}
        self._init_evolve = {k: self.solver.evolve_stencil for k in self._init_evolve}
        self._cell_g_init = {}
        self.g_max_dist = {}
        # evolve steps directly call passing stencil
        self.evolve_cycle = cycle((self.solver.evolve_stencil,))

    def init_ghost_cells(self, *_, **__):
        return super().init_ghost_cells(*_, **_)

    def plot(self, *_, **__):
        """Return nothing to plot."""
        return tuple()

    def _copy_parent_data(self, *_, **__):
        """Do nothing."""
