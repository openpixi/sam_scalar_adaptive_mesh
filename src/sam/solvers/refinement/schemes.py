"""Implementations for refinement scheme equations."""


def copy_to(src, src_idx, dst, dst_idx):
    """Copy src to dst."""
    dst[dst_idx] = src[src_idx]


# parameters for refinement equations
# only matter for interacting/non-free leapfrog
ALPHA = 1
BETA = 1
GAMMA = 1
DELTA = 1
LAMBDA = 1
KAPPA = 1


# refinement equations
def alpha_diag_border(a, a_idx, b, b_idx, A, A_idx, B, B_idx, C, C_idx, D, D_idx):
    """Ref func with alpha."""
    # a = b + alpha(A-B) + (1-alpha)(C-D)
    a[a_idx] = (
        b[b_idx] + ALPHA * (A[A_idx] - B[B_idx]) + (1 - ALPHA) * (C[C_idx] - D[D_idx])
    )


def beta_diag_border(a, a_idx, b, b_idx, A, A_idx, B, B_idx, C, C_idx, D, D_idx):
    """Ref func with beta."""
    # a = b + beta(A-B) + (1-beta)(C-D)
    a[a_idx] = (
        b[b_idx] + BETA * (A[A_idx] - B[B_idx]) + (1 - BETA) * (C[C_idx] - D[D_idx])
    )


def gamma_diag_border(a, a_idx, b, b_idx, A, A_idx, B, B_idx, C, C_idx, D, D_idx):
    """Ref func with gamma."""
    # a = b + gamma/2 *(A-B) + (1-gamma)/2 *(C-D)
    a[a_idx] = (
        b[b_idx]
        + GAMMA / 2 * (A[A_idx] - B[B_idx])
        + (1 - GAMMA) / 2 * (C[C_idx] - D[D_idx])
    )


def delta_diag_border(a, a_idx, b, b_idx, A, A_idx, B, B_idx):
    """Ref func with delta."""
    # a = b + delta/2 *(A-B)
    a[a_idx] = b[b_idx] + DELTA / 2 * (A[A_idx] - B[B_idx])


def average(A, A_idx, a, a_idx, b, b_idx, c, c_idx, d, d_idx):
    """Ref func for avg of 4 finer cells."""
    # A = (a + b + c + d) / 4
    A[A_idx] = (a[a_idx] + b[b_idx] + c[c_idx] + d[d_idx]) / 4


# specials for straight border
def straight_v3_special_1(
    e, e_idx, F, F_idx, A, A_idx, H, H_idx, j, j_idx, g, g_idx, f, f_idx
):
    """Ref func for first step of 'e' in V3 straight border."""
    e[e_idx] = (
        2 * F[F_idx] - (A[A_idx] - H[H_idx]) / 4 + (j[j_idx] - g[g_idx]) / 2 - f[f_idx]
    )


def straight_v3_special_2(e, e_idx, F, F_idx, a, a_idx, j, j_idx, g, g_idx, f, f_idx):
    """Ref func for second step of 'e' in V3 straight border."""
    e[e_idx] = 2 * F[F_idx] - (a[a_idx] + f[f_idx] + g[g_idx] - j[j_idx]) / 2


def straight_v35_special(
    e, e_idx, F, F_idx, A, A_idx, H, H_idx, o, o_idx, l, l_idx, f, f_idx  # noqa: E741
):
    """Ref func for 'e' in V35 straight border."""
    # Note: the second gamma part is missing!
    e[e_idx] = (
        2 * F[F_idx] - (A[A_idx] - H[H_idx]) / 4 + (o[o_idx] - l[l_idx]) / 2 - f[f_idx]
    )


def straight_v4_special(a, a_idx, F, F_idx, b, b_idx, e, e_idx, f, f_idx):
    """Ref func for 'a' in V4 straight border."""

    a[a_idx] = 4 * F[F_idx] - b[b_idx] - e[e_idx] - f[f_idx]


def straight_v5_special(
    G, G_idx, H, H_idx, K, K_idx, r, r_idx, s, s_idx, t, t_idx, q, q_idx
):
    """Ref func for 'G' in V5 straight border."""
    G[G_idx] = (
        H[H_idx]
        - K[K_idx]
        + (1 / 2) * (r[r_idx] + s[s_idx])
        + (1 / 4) * (t[t_idx] - q[q_idx])
    )


# specials for coarse to fine cusp
def lambda_cusp(d, d_idx, g, g_idx, B, B_idx, D, D_idx, L, L_idx, O, O_idx):
    """Ref func with lambda for CCTF V2 dedicated."""
    # d = g + lambda/2(B-D) + (1-lambda)/2 *(L-O)
    d[d_idx] = (
        g[g_idx]
        + LAMBDA / 2 * (B[B_idx] - D[D_idx])
        + (1 - LAMBDA) / 2 * (L[L_idx] - O[O_idx])
    )


def kappa_cusp(
    e,
    e_idx,
    f,
    f_idx,
    h,
    h_idx,
    j,
    j_idx,
    C,
    C_idx,
    B,
    B_idx,
    D,
    D_idx,
    L,
    L_idx,
    O,
    O_idx,
):
    """Ref func with kappa for CCTF V2 dedicated."""
    # e = (f − h + j + C) / 2 + kappa(B − D) + (1 − kappa)(L − O)
    e[e_idx] = (
        (f[f_idx] - h[h_idx] + j[j_idx] + C[C_idx]) / 2
        + KAPPA * (B[B_idx] - D[D_idx])
        + (1 - KAPPA) * (L[L_idx] - O[O_idx])
    )


def cctf_v2_c(c, c_idx, h, h_idx, g, g_idx, i, i_idx):
    """Ref func for 'c' & 'a' in CCTF V2 dedicated."""
    # c = h + 2(g − i)
    c[c_idx] = h[h_idx] + 2 * (g[g_idx] - i[i_idx])


def cctf_v2_b(b, b_idx, e, e_idx, g, g_idx, c, c_idx, f, f_idx, A, A_idx):
    """Ref func for b in CCTF V2 dedicated."""
    # b = e + g + (c − f )/2 − A
    b[b_idx] = e[e_idx] + g[g_idx] + (c[c_idx] - f[f_idx]) / 2 - A[A_idx]
