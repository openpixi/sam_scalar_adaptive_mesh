"""Diagonal borders at lightlike separation."""

from itertools import cycle

import numpy as np
from sam.solvers.refinement.border_base import BorderBase, Directional, TempData
from sam.solvers.refinement.schemes import (
    alpha_diag_border,
    beta_diag_border,
    gamma_diag_border,
    delta_diag_border,
    average,
    copy_to,
)


class DiagCoarseToFineR(BorderBase):
    """Lower left to upper right coarse-to-fine border, lightlike."""

    __slots__: set[str] = set()

    def __init__(
        self,
        border_reg,
        sort_idx,
        arr_idx,
        pos,
        neigh_reg,
        neighbor_arr,
        neighbor_sort_idx,
        a2,
        solver,
        border_types,
        *,
        parent=None,
        skip=False,
        d_type=np.float_,
        num_ghost=7,
    ):
        """Return new DiagCoarseToFineR (lightlike)."""
        super().__init__(
            border_reg,
            sort_idx,
            arr_idx,
            pos,
            neigh_reg,
            neighbor_arr,
            neighbor_sort_idx,
            a2,
            solver,
            border_types,
            parent=parent,
            skip=skip,
        )
        self._init_evolve = {
            self.InitEvolve.SLICE_T0_CPY: self.evolve_init_slice_t0_copy_1
        }
        self.evolve_cycle = cycle((self.evolve_t1, self.evolve_t2))
        # 6 ghost cells required, normally TODO: think again
        # but for init step +1 ghost cell for B is required
        self.d_ghost = np.zeros(num_ghost, dtype=d_type)

        self.cell_g = {  # comment names valid after swap
            "J": 0,
            "K": 1,
            "P": 2,  # L
            "M": 3,  # H
            "N": 4,  # I
            "O": 5,
            "B_INIT": 6,  # B used for init
        }
        self._cell_g_init = {"J": -3, "K": -2, "L": -1, "O": -2, "B_INIT": -1}
        self.g_max_dist = {
            self.reg.a: Directional(3, 0),
            self.neighbor.a: Directional(0, 0),
        }
        self._cell_names = {
            "A": 0,
            "B": 0,
            "C": 1,
            "D": 2,
            "E": 0,
            "F": 1,
            "a": -2,
            "b": -1,
            "c": -3,
            "d": -2,
            "e": -1,
            "f": -2,
            "g": -1,
            "h": -3,
            "i": -1,
        }
        self._copy_parent_data()

    def _copy_cusp_parent_data(self):
        # P not required, is overwritten in t1 immediately
        copy_to(
            self.parent.d_ghost, self.parent.cell_g["R"], self.d_ghost, self.cell_g["M"]
        )
        copy_to(
            self.parent.d_ghost, self.parent.cell_g["K"], self.d_ghost, self.cell_g["N"]
        )
        copy_to(
            self.parent.d_ghost, self.parent.cell_g["P"], self.d_ghost, self.cell_g["O"]
        )
        copy_to(
            self.parent.d_ghost, self.parent.cell_g["E"], self.d_ghost, self.cell_g["J"]
        )
        copy_to(
            self.parent.d_ghost, self.parent.cell_g["N"], self.d_ghost, self.cell_g["K"]
        )
        self.parent = None

    def _copy_parent_data(self):
        if isinstance(self.parent, self.BORDER_TYPES["CDCFTC"]):
            self._copy_cusp_parent_data()

    def init_ghost_cells(
        self,
        x_mask_now_c,
        d_now_c,
        x_mask_pre_c,
        d_pre_c,
        x_mask_now_f,
        d_now_f,
        x_mask_pre_f,
        d_pre_f,
    ):
        end_idx = super().init_ghost_cells(
            x_mask_now_c,
            d_now_c,
            x_mask_pre_c,
            d_pre_c,
            x_mask_now_f,
            d_now_f,
            x_mask_pre_f,
            d_pre_f,
        )
        if x_mask_now_c[self._cell_g_init["J"]]:
            self.d_ghost[self.cell_g["J"]] += d_now_c[
                np.count_nonzero(x_mask_now_c[0 : end_idx["J"]])
            ]
        if x_mask_now_c[self._cell_g_init["K"]]:
            self.d_ghost[self.cell_g["K"]] += d_now_c[
                np.count_nonzero(x_mask_now_c[0 : end_idx["K"]])
            ]
        if x_mask_now_c[self._cell_g_init["L"]]:
            self.d_ghost[self.cell_g["P"]] += d_now_c[  # L
                np.count_nonzero(x_mask_now_c[0 : end_idx["L"]])
            ]
        if x_mask_pre_c[self._cell_g_init["O"]]:
            self.d_ghost[self.cell_g["O"]] += d_pre_c[
                np.count_nonzero(x_mask_pre_c[0 : end_idx["O"]])
            ]
        if x_mask_pre_c[self._cell_g_init["B_INIT"]]:
            self.d_ghost[self.cell_g["B_INIT"]] += d_pre_c[
                np.count_nonzero(x_mask_pre_c[0 : end_idx["B_INIT"]])
            ]

        return end_idx

    def border_change_selector(self, border):
        if border is type(self):
            return None

        # currently not implemented, but is valid possibility
        # DiagFineToCoarseL, StraightFineToCoarse
        if border in (self.BORDER_TYPES["DFTCL"], self.BORDER_TYPES["SFTC"]):
            # return border
            raise NotImplementedError(
                f"Change from {type(self).__name__} to {border.__name__} "
                "not implemented."
            )

        raise NotImplementedError(
            f"Change from {type(self).__name__} to {border.__name__} not implemented."
        )

    def evolve_init_slice_t0_copy_1(
        self,
        _reg_pre,
        _n_reg_pre,
        reg_now,
        n_reg_now,
        _reg_nxt,
        n_reg_nxt,
        _d_pre,
        _n_d_pre,
        d_now,
        n_d_now,
        _d_nxt,
        n_d_nxt,
    ):
        """Refinement steps +2:1, +2:2 on initialization."""
        # f has changed idx: (R) -2 -> -4, (L) 1->3
        f_backup = self._cell_names["f"]
        self._cell_names["f"] += int(2 * np.sign(f_backup))
        # H->M
        self.solver.evolve_stencil(
            *self.stencil_args,
            TempData((self.d_ghost, self.cell_g["O"])),
            TempData(
                (self.d_ghost, self.cell_g["K"]),
                (self.d_ghost, self.cell_g["P"]),
                (self.d_ghost, self.cell_g["J"]),
            ),
            TempData((self.d_ghost, self.cell_g["M"])),  # H
        )
        # I->N
        self.solver.evolve_stencil(
            *self.stencil_args,
            TempData((self.d_ghost, self.cell_g["B_INIT"])),
            TempData(
                (self.d_ghost, self.cell_g["P"]),  # L
                (d_now, self.cell_d(reg_now, "A")),
                (self.d_ghost, self.cell_g["K"]),
            ),
            TempData((self.d_ghost, self.cell_g["N"])),  # I
        )
        # a
        # is taken care of by default update stencil
        # b
        gamma_diag_border(
            n_d_nxt,
            self.cell_d(n_reg_nxt, "b"),
            n_d_now,
            self.cell_d(n_reg_now, "d"),
            d_now,
            self.cell_d(reg_now, "A"),
            self.d_ghost,
            self.cell_g["B_INIT"],
            self.d_ghost,
            self.cell_g["N"],  # I
            self.d_ghost,
            self.cell_g["K"],
        )
        # swap cells
        (
            self.cell_g["J"],
            self.cell_g["K"],
            self.cell_g["M"],
            self.cell_g["N"],
            self.cell_g["O"],
            self.cell_g["P"],
        ) = (
            self.cell_g["M"],
            self.cell_g["N"],
            self.cell_g["J"],
            self.cell_g["K"],
            self.cell_g["P"],
            self.cell_g["O"],
        )

        # restore f idx
        self._cell_names["f"] = f_backup
        # leave parent the same and set next init step
        # self.parent = self.InitEvolve.SLICE_T0_CPY
        self._init_evolve[
            self.InitEvolve.SLICE_T0_CPY
        ] = self.evolve_init_slice_t0_copy_2
        return 2

    def evolve_init_slice_t0_copy_2(
        self,
        reg_pre,
        n_reg_pre,
        reg_now,
        n_reg_now,
        _reg_nxt,
        n_reg_nxt,
        d_pre,
        n_d_pre,
        d_now,
        n_d_now,
        _d_nxt,
        n_d_nxt,
    ):
        """Refinement steps +1:1, +1:2 after initialization."""
        # E doesn't exist on lattice yet, use B_INIT
        # F has changed idx: (R) 1 -> 0, (L) -2 -> -1
        F_backup = self._cell_names["F"]
        self._cell_names["F"] += int(-1 * np.sign(F_backup))
        # write L on P
        self.solver.evolve_stencil(
            *self.stencil_args,
            TempData((d_pre, self.cell_d(reg_pre, "F"))),
            TempData(
                (self.d_ghost, self.cell_g["B_INIT"]),
                (d_now, self.cell_d(reg_now, "C")),
                (self.d_ghost, self.cell_g["O"]),
            ),
            TempData((self.d_ghost, self.cell_g["P"])),  # L
        )
        # first e, needs old i
        alpha_diag_border(
            n_d_nxt,
            self.cell_d(n_reg_nxt, "e"),
            n_d_pre,
            self.cell_d(n_reg_pre, "i"),
            self.d_ghost,
            self.cell_g["P"],  # L
            self.d_ghost,
            self.cell_g["O"],
            d_now,
            self.cell_d(reg_now, "C"),
            d_pre,
            self.cell_d(reg_pre, "F"),
        )
        # c replaces i
        alpha_diag_border(
            n_d_nxt,
            self.cell_d(n_reg_nxt, "c"),
            n_d_pre,
            self.cell_d(n_reg_pre, "h"),
            self.d_ghost,
            self.cell_g["K"],
            self.d_ghost,
            self.cell_g["N"],
            d_now,
            self.cell_d(reg_now, "B"),
            self.d_ghost,
            self.cell_g["B_INIT"],
        )
        # d
        delta_diag_border(
            n_d_nxt,
            self.cell_d(n_reg_nxt, "d"),
            n_d_now,
            self.cell_d(n_reg_now, "g"),
            self.d_ghost,
            self.cell_g["P"],  # L
            self.d_ghost,
            self.cell_g["O"],
        )

        # restore F idx
        self._cell_names["F"] = F_backup
        self.parent = None
        # set evolve cycle
        next(self.evolve_cycle)
        return 1

    def evolve_t1(
        self,
        reg_pre,
        n_reg_pre,
        reg_now,
        n_reg_now,
        _reg_nxt,
        n_reg_nxt,
        d_pre,
        n_d_pre,
        d_now,
        n_d_now,
        _d_nxt,
        n_d_nxt,
    ):
        """Refinement steps +1:1, +1:2."""
        # write L on P
        self.solver.evolve_stencil(
            *self.stencil_args,
            TempData((d_pre, self.cell_d(reg_pre, "F"))),
            TempData(
                (d_now, self.cell_d(reg_now, "B")),
                (d_now, self.cell_d(reg_now, "C")),
                (self.d_ghost, self.cell_g["O"]),
            ),
            TempData((self.d_ghost, self.cell_g["P"])),  # L
        )
        # first e, needs old i
        alpha_diag_border(
            n_d_nxt,
            self.cell_d(n_reg_nxt, "e"),
            n_d_pre,
            self.cell_d(n_reg_pre, "i"),
            self.d_ghost,
            self.cell_g["P"],  # L
            self.d_ghost,
            self.cell_g["O"],
            d_now,
            self.cell_d(reg_now, "C"),
            d_pre,
            self.cell_d(reg_pre, "F"),
        )
        # c replaces i
        alpha_diag_border(
            n_d_nxt,
            self.cell_d(n_reg_nxt, "c"),
            n_d_pre,
            self.cell_d(n_reg_pre, "h"),
            self.d_ghost,
            self.cell_g["K"],
            self.d_ghost,
            self.cell_g["N"],
            d_now,
            self.cell_d(reg_now, "B"),
            d_pre,
            self.cell_d(reg_pre, "E"),
        )
        # d
        delta_diag_border(
            n_d_nxt,
            self.cell_d(n_reg_nxt, "d"),
            n_d_now,
            self.cell_d(n_reg_now, "g"),
            self.d_ghost,
            self.cell_g["P"],  # L
            self.d_ghost,
            self.cell_g["O"],
        )
        return 1

    def evolve_t2(
        self,
        reg_pre,
        n_reg_pre,
        reg_now,
        n_reg_now,
        _reg_nxt,
        n_reg_nxt,
        d_pre,
        n_d_pre,
        d_now,
        n_d_now,
        _d_nxt,
        n_d_nxt,
    ):
        """Refinement steps +2:1, +2:2."""
        # H->M, I->N
        self.solver.evolve_stencil(
            *self.stencil_args,
            TempData((self.d_ghost, self.cell_g["O"])),
            TempData(
                (self.d_ghost, self.cell_g["K"]),
                (self.d_ghost, self.cell_g["P"]),
                (self.d_ghost, self.cell_g["J"]),
            ),
            TempData((self.d_ghost, self.cell_g["M"])),  # H
        )
        self.solver.evolve_stencil(
            *self.stencil_args,
            TempData((d_pre, self.cell_d(reg_pre, "B"))),
            TempData(
                (self.d_ghost, self.cell_g["P"]),  # L
                (d_now, self.cell_d(reg_now, "A")),
                (self.d_ghost, self.cell_g["K"]),
            ),
            TempData((self.d_ghost, self.cell_g["N"])),  # I
        )
        # a
        beta_diag_border(
            n_d_nxt,
            self.cell_d(n_reg_nxt, "a"),
            n_d_pre,
            self.cell_d(n_reg_pre, "f"),
            self.d_ghost,
            self.cell_g["P"],  # L
            self.d_ghost,
            self.cell_g["O"],
            self.d_ghost,
            self.cell_g["M"],  # H
            self.d_ghost,
            self.cell_g["J"],
        )
        # b
        gamma_diag_border(
            n_d_nxt,
            self.cell_d(n_reg_nxt, "b"),
            n_d_now,
            self.cell_d(n_reg_now, "d"),
            d_now,
            self.cell_d(reg_now, "A"),
            d_pre,
            self.cell_d(reg_pre, "B"),
            self.d_ghost,
            self.cell_g["N"],  # I
            self.d_ghost,
            self.cell_g["K"],
        )
        # swap cells
        (
            self.cell_g["J"],
            self.cell_g["K"],
            self.cell_g["M"],
            self.cell_g["N"],
            self.cell_g["O"],
            self.cell_g["P"],
        ) = (
            self.cell_g["M"],
            self.cell_g["N"],
            self.cell_g["J"],
            self.cell_g["K"],
            self.cell_g["P"],
            self.cell_g["O"],
        )
        return 2

    def plot(self, p=None, P=None, res=None, nres=None):
        out = []
        p = int(self.p * self.reg.a) if p is None else p
        P = int(self.p * self.reg.a) if P is None else P
        res = self.reg.a if res is None else res
        nres = self.neighbor.a if nres is None else nres
        if self.evolve_step == 1:
            out.append((0, P - res * 3, self.reg, self.d_ghost[self.cell_g["J"]]))
            out.append((0, P - res * 2, self.reg, self.d_ghost[self.cell_g["K"]]))
            out.append((0, P - res, self.reg, self.d_ghost[self.cell_g["P"]]))  # L
            out.append((-1, P - res * 4, self.reg, self.d_ghost[self.cell_g["M"]]))
            out.append((-1, P - res * 3, self.reg, self.d_ghost[self.cell_g["N"]]))
            out.append((-1, P - res * 2, self.reg, self.d_ghost[self.cell_g["O"]]))
            if isinstance(self.parent, self.InitEvolve):
                # for viewing the t=-1 slice after init
                out.append((-1, P - res, self.reg, self.d_ghost[self.cell_g["B_INIT"]]))
        elif self.evolve_step == 2:
            out.append((1, P - res * 2, self.reg, self.d_ghost[self.cell_g["J"]]))  # H
            out.append((1, P - res, self.reg, self.d_ghost[self.cell_g["K"]]))  # I
            out.append((0, P - res * 3, self.reg, self.d_ghost[self.cell_g["M"]]))  # J
            out.append((0, P - res * 2, self.reg, self.d_ghost[self.cell_g["N"]]))  # K
            out.append((0, P - res, self.reg, self.d_ghost[self.cell_g["O"]]))  # L
            out.append((-1, P - res * 2, self.reg, self.d_ghost[self.cell_g["P"]]))  # O

        return out


class DiagCoarseToFineL(DiagCoarseToFineR):
    """Lower right to upper left coarse-to-fine border, lightlike."""

    __slots__: set[str] = set()

    def __init__(
        self,
        border_reg,
        sort_idx,
        arr_idx,
        pos,
        neigh_reg,
        neighbor_arr,
        neighbor_sort_idx,
        a2,
        solver,
        border_types,
        *,
        parent=None,
        skip=False,
        d_type=np.float_,
    ):
        """Return new DiagCoarseToFineL (lightlike)."""
        super().__init__(
            border_reg,
            sort_idx,
            arr_idx,
            pos,
            neigh_reg,
            neighbor_arr,
            neighbor_sort_idx,
            a2,
            solver,
            border_types,
            parent=parent,
            skip=skip,
            d_type=d_type,
        )
        self._cell_g_init = {"J": 2, "K": 1, "L": 0, "O": 1, "B_INIT": 0}
        self.g_max_dist = {
            self.reg.a: Directional(0, 3),
            self.neighbor.a: Directional(0, 0),
        }
        self._cell_names = {
            "A": -1,
            "B": -1,
            "C": -2,
            "D": -3,
            "E": -1,
            "F": -2,
            "a": 1,
            "b": 0,
            "c": 2,
            "d": 1,
            "e": 0,
            "f": 1,
            "g": 0,
            "h": 2,
            "i": 0,
        }

    def _copy_parent_data(self):
        if isinstance(self.parent, self.BORDER_TYPES["CDCCTF"]):
            self._copy_cusp_parent_data()

    def border_change_selector(self, border):
        if border is type(self):
            return None

        # currently not implemented, but is valid possibility
        # DiagFineToCoarseR, StraightCoarseToFine
        if border in (self.BORDER_TYPES["DFTCR"], self.BORDER_TYPES["SCTF"]):
            # return border
            raise NotImplementedError(
                f"Change from {type(self).__name__} to {border.__name__} "
                "not implemented."
            )

        raise NotImplementedError(
            f"Change from {type(self).__name__} to {border.__name__} not implemented."
        )

    def plot(self, p=None, P=None, res=None, nres=None):
        p = (
            int((self.p + self.reg.n) * self.reg.a - self.neighbor.a)
            if p is None
            else p
        )
        P = int((self.p + self.reg.n) * self.reg.a - self.reg.a) if P is None else P
        res = -self.reg.a if res is None else res
        nres = -self.neighbor.a if nres is None else nres
        return super().plot(p, P, res, nres)


class DiagFineToCoarseR(BorderBase):
    """Lower left to upper right fine-to-coarse border, lightlike."""

    __slots__: set[str] = set()

    def __init__(
        self,
        border_reg,
        sort_idx,
        arr_idx,
        pos,
        neigh_reg,
        neighbor_arr,
        neighbor_sort_idx,
        a2,
        solver,
        border_types,
        *,
        parent=None,
        skip=False,
        d_type=np.float_,
        num_ghost=5,
    ):
        """Return new DiagFineToCoarseR (lightlike)."""
        super().__init__(
            border_reg,
            sort_idx,
            arr_idx,
            pos,
            neigh_reg,
            neighbor_arr,
            neighbor_sort_idx,
            a2,
            solver,
            border_types,
            parent=parent,
            skip=skip,
        )
        self._init_evolve = {
            self.InitEvolve.SLICE_T0_CPY: self.evolve_init_slice_t0_copy
        }
        self.evolve_cycle = cycle((self.evolve_t1, self.evolve_t2))
        # 5 ghost cells required
        self.d_ghost = np.zeros(num_ghost, dtype=d_type)

        self.cell_g = {  # comment names valid after swap
            "E": 0,
            "F": 1,
            "G": 2,
            "H": 3,
            "x": 4,
        }
        self._cell_g_init = {"E": -1, "F": 0, "x": -1}
        self.g_max_dist = {
            self.reg.a: Directional(1, 1),
            self.neighbor.a: Directional(1, 0),
        }
        self._cell_names = {
            "A": -2,
            "B": -1,
            "C": -2,
            "D": -1,
            "a": 0,
            "b": 1,
            "y": 0,
            "c": 0,
            "d": 1,
            "e": 2,
            "f": 3,
            "g": 0,
            "h": 1,
            "i": 2,
            "j": 3,
        }

    def init_ghost_cells(
        self,
        x_mask_now_c,
        d_now_c,
        x_mask_pre_c,
        d_pre_c,
        x_mask_now_f,
        d_now_f,
        x_mask_pre_f,
        d_pre_f,
    ):
        end_idx = super().init_ghost_cells(
            x_mask_now_c,
            d_now_c,
            x_mask_pre_c,
            d_pre_c,
            x_mask_now_f,
            d_now_f,
            x_mask_pre_f,
            d_pre_f,
        )
        if x_mask_pre_c[self._cell_g_init["E"]]:
            self.d_ghost[self.cell_g["E"]] += d_pre_c[
                np.count_nonzero(x_mask_pre_c[0 : end_idx["E"]])
            ]
        if x_mask_pre_c[self._cell_g_init["F"]]:
            self.d_ghost[self.cell_g["F"]] += d_pre_c[
                np.count_nonzero(x_mask_pre_c[0 : end_idx["F"]])
            ]
        if x_mask_now_f[self._cell_g_init["x"]]:
            self.d_ghost[self.cell_g["x"]] += d_now_f[
                np.count_nonzero(x_mask_now_f[0 : end_idx["x"]])
            ]

        return end_idx

    def border_change_selector(self, border):
        if border is type(self):
            return None

        # the timelike cusps
        # DiagCoarseToFineL
        if border is self.BORDER_TYPES["DCTFL"]:
            return self.BORDER_TYPES["CSCCTF"]

        # StraightCoarseToFine
        if border is self.BORDER_TYPES["SCTF"]:
            return self.BORDER_TYPES["CDCCTF"]

        raise NotImplementedError(
            f"Change from {type(self).__name__} to {border.__name__} not implemented."
        )

    def evolve_init_slice_t0_copy(
        self,
        _reg_pre,
        n_reg_pre,
        _reg_now,
        n_reg_now,
        _reg_nxt,
        n_reg_nxt,
        _d_pre,
        n_d_pre,
        _d_now,
        n_d_now,
        _d_nxt,
        n_d_nxt,
    ):
        """Refinement step +2:1 on initialization."""
        # a
        # idx of e is shifted by 2 due to change in border position
        e_idx = self.cell_d(n_reg_pre, "e")
        e_idx += int(-2 * np.sign(self._cell_names["e"]))
        self.solver.evolve_stencil(
            *self.stencil_args,
            TempData((n_d_pre, e_idx)),
            TempData(
                (n_d_now, self.cell_d(n_reg_now, "y")),
                (n_d_now, self.cell_d(n_reg_now, "b")),
                (self.d_ghost, self.cell_g["x"]),
            ),
            TempData((n_d_nxt, self.cell_d(n_reg_nxt, "a"))),
        )
        # swap
        self.cell_g["G"], self.cell_g["H"], self.cell_g["E"], self.cell_g["F"] = (
            self.cell_g["E"],
            self.cell_g["F"],
            self.cell_g["G"],
            self.cell_g["H"],
        )
        # reset parent
        self.parent = None
        return 2

    def evolve_t1(
        self,
        _reg_pre,
        n_reg_pre,
        reg_now,
        n_reg_now,
        reg_nxt,
        _n_reg_nxt,
        _d_pre,
        n_d_pre,
        d_now,
        n_d_now,
        d_nxt,
        _n_d_nxt,
    ):
        """Refinement steps +1:1, +1:2."""
        # E
        average(
            self.d_ghost,
            self.cell_g["E"],
            n_d_now,
            self.cell_d(n_reg_now, "c"),
            n_d_now,
            self.cell_d(n_reg_now, "d"),
            n_d_pre,
            self.cell_d(n_reg_pre, "g"),
            n_d_pre,
            self.cell_d(n_reg_pre, "h"),
        )
        # F
        average(
            self.d_ghost,
            self.cell_g["F"],
            n_d_now,
            self.cell_d(n_reg_now, "e"),
            n_d_now,
            self.cell_d(n_reg_now, "f"),
            n_d_pre,
            self.cell_d(n_reg_pre, "i"),
            n_d_pre,
            self.cell_d(n_reg_pre, "j"),
        )
        # A
        self.solver.evolve_stencil(
            *self.stencil_args,
            TempData((self.d_ghost, self.cell_g["G"])),
            TempData(
                (d_now, self.cell_d(reg_now, "D")),
                (self.d_ghost, self.cell_g["E"]),
                (d_now, self.cell_d(reg_now, "C")),
            ),
            TempData((d_nxt, self.cell_d(reg_nxt, "A"))),
        )
        # B
        self.solver.evolve_stencil(
            *self.stencil_args,
            TempData((self.d_ghost, self.cell_g["H"])),
            TempData(
                (self.d_ghost, self.cell_g["E"]),
                (self.d_ghost, self.cell_g["F"]),
                (d_now, self.cell_d(reg_now, "D")),
            ),
            TempData((d_nxt, self.cell_d(reg_nxt, "B"))),
        )
        # x
        self.solver.evolve_stencil(
            *self.stencil_args,
            TempData((n_d_pre, self.cell_d(n_reg_pre, "h"))),
            TempData(
                (n_d_now, self.cell_d(n_reg_now, "d")),
                (n_d_now, self.cell_d(n_reg_now, "e")),
                (n_d_now, self.cell_d(n_reg_now, "c")),
            ),
            TempData((self.d_ghost, self.cell_g["x"])),
        )
        return 1

    def evolve_t2(
        self,
        _reg_pre,
        n_reg_pre,
        _reg_now,
        n_reg_now,
        _reg_nxt,
        n_reg_nxt,
        _d_pre,
        n_d_pre,
        _d_now,
        n_d_now,
        _d_nxt,
        n_d_nxt,
    ):
        """Refinement steps +2:1."""
        # a
        self.solver.evolve_stencil(
            *self.stencil_args,
            TempData((n_d_pre, self.cell_d(n_reg_pre, "e"))),
            TempData(
                (n_d_now, self.cell_d(n_reg_now, "y")),
                (n_d_now, self.cell_d(n_reg_now, "b")),
                (self.d_ghost, self.cell_g["x"]),
            ),
            TempData((n_d_nxt, self.cell_d(n_reg_nxt, "a"))),
        )
        # swap
        self.cell_g["G"], self.cell_g["H"], self.cell_g["E"], self.cell_g["F"] = (
            self.cell_g["E"],
            self.cell_g["F"],
            self.cell_g["G"],
            self.cell_g["H"],
        )
        return 2

    def plot(self, p=None, P=None, res=None, nres=None):
        out = []
        p = int((self.p + self.reg.n) * self.reg.a) if p is None else p
        P = int((self.p + self.reg.n) * self.reg.a) if P is None else P
        res = self.reg.a if res is None else res
        nres = self.neighbor.a if nres is None else nres
        if self.evolve_step == 1:
            out.append((-1, P - res, self.reg, self.d_ghost[self.cell_g["E"]]))
            out.append((-1, P, self.reg, self.d_ghost[self.cell_g["F"]]))
            out.append((-2, P - res * 2, self.reg, self.d_ghost[self.cell_g["G"]]))
            out.append((-2, P - res, self.reg, self.d_ghost[self.cell_g["H"]]))
            out.append((0, p - nres, self.neighbor, self.d_ghost[self.cell_g["x"]]))
        elif self.evolve_step == 2:
            out.append((-1, P - res, self.reg, self.d_ghost[self.cell_g["G"]]))  # E
            out.append((-1, P, self.reg, self.d_ghost[self.cell_g["H"]]))  # F
            out.append((-2, P - res * 2, self.reg, self.d_ghost[self.cell_g["E"]]))  # G
            out.append((-2, P - res, self.reg, self.d_ghost[self.cell_g["F"]]))  # H
            out.append((-1, p - nres, self.neighbor, self.d_ghost[self.cell_g["x"]]))

        return out


class DiagFineToCoarseL(DiagFineToCoarseR):
    """Lower right to upper left fine-to-coarse border, lightlike."""

    __slots__: set[str] = set()

    def __init__(
        self,
        border_reg,
        sort_idx,
        arr_idx,
        pos,
        neigh_reg,
        neighbor_arr,
        neighbor_sort_idx,
        a2,
        solver,
        border_types,
        *,
        parent=None,
        skip=False,
        d_type=np.float_,
    ):
        """Return new DiagFineToCoarseL (lightlike)."""
        super().__init__(
            border_reg,
            sort_idx,
            arr_idx,
            pos,
            neigh_reg,
            neighbor_arr,
            neighbor_sort_idx,
            a2,
            solver,
            border_types,
            parent=parent,
            skip=skip,
            d_type=d_type,
        )
        self._cell_g_init = {"E": 0, "F": -1, "x": 0}
        self.g_max_dist = {
            self.reg.a: Directional(1, 1),
            self.neighbor.a: Directional(0, 1),
        }
        self._cell_names = {
            "A": 1,
            "B": 0,
            "C": 1,
            "D": 0,
            "a": -1,
            "b": -2,
            "y": -1,  # no name on images
            "c": -1,
            "d": -2,
            "e": -3,
            "f": -4,
            "g": -1,
            "h": -2,
            "i": -3,
            "j": -4,
        }

    def border_change_selector(self, border):
        if border is type(self):
            return None

        # the timelike cusps
        # DiagCoarseToFineR
        if border is self.BORDER_TYPES["DCTFR"]:
            return self.BORDER_TYPES["CSCFTC"]

        # StraightFineToCoarse
        if border is self.BORDER_TYPES["SFTC"]:
            return self.BORDER_TYPES["CDCFTC"]

        raise NotImplementedError(
            f"Change from {type(self).__name__} to {border.__name__} not implemented."
        )

    def plot(self, p=None, P=None, res=None, nres=None):
        p = int(self.p * self.reg.a - self.neighbor.a) if p is None else p
        P = int(self.p * self.reg.a - self.reg.a) if P is None else P
        res = -self.reg.a if res is None else res
        nres = -self.neighbor.a if nres is None else nres
        return super().plot(p, P, res, nres)
