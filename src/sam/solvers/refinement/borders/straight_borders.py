"""Straight borders at timelike separation."""

from itertools import cycle

import numpy as np

from sam.solvers.refinement.border_base import BorderBase, Directional, TempData
from sam.solvers.refinement.schemes import (
    copy_to,
    alpha_diag_border,
    gamma_diag_border,
    average,
    straight_v3_special_1,
    straight_v3_special_2,
    straight_v5_special,
)


class StraightCoarseToFineV1(BorderBase):
    """|Coarse || Fine| border at same x position, timelike."""

    # V1 corresponds to pure diagonal border eqs.

    __slots__: set[str] = set()

    def __init__(
        self,
        border_reg,
        sort_idx,
        arr_idx,
        pos,
        neigh_reg,
        neighbor_arr,
        neighbor_sort_idx,
        a2,
        solver,
        border_types,
        *,
        parent=None,
        skip=False,
        d_type=np.float_,
        num_ghost=6,
    ):
        """Return new StraightCoarseToFine (timelike)."""
        super().__init__(
            border_reg,
            sort_idx,
            arr_idx,
            pos,
            neigh_reg,
            neighbor_arr,
            neighbor_sort_idx,
            a2,
            solver,
            border_types,
            parent=parent,
            skip=skip,
        )
        self._init_evolve = {
            self.InitEvolve.SLICE_T0_CPY: self.evolve_init_slice_t0_copy
        }
        self.evolve_cycle = cycle((self.evolve_t1, self.evolve_t2))
        # 6 ghost cells required
        self.d_ghost = np.zeros(num_ghost, dtype=d_type)

        self.cell_g = {  # comment names valid after swap
            "F": 0,
            "H": 1,
            "I": 2,  # G
            "J": 3,  # E
            "c": 4,
            "d": 5,
        }
        self._cell_g_init = {"F": 0, "H": 0}
        self.g_max_dist = {
            self.reg.a: Directional(0, 1),
            self.neighbor.a: Directional(0, 0),
        }
        self._cell_names = {
            "A": -1,
            "B": -2,
            "C": -1,
            "D": -1,
            "a": 0,
            "e": 0,
            "f": 1,
            "g": 2,
            "h": 3,
            "o": 2,
            "k": 2,
            "l": 3,
            "x": 4,
        }

        self._copy_parent_data()

    def _copy_cusp_parent_data(self):
        copy_to(
            self.parent.d_ghost, self.parent.cell_g["E"], self.d_ghost, self.cell_g["J"]
        )
        copy_to(
            self.parent.d_ghost, self.parent.cell_g["N"], self.d_ghost, self.cell_g["I"]
        )
        copy_to(
            self.parent.d_ghost, self.parent.cell_g["O"], self.d_ghost, self.cell_g["H"]
        )
        copy_to(
            self.parent.d_ghost, self.parent.cell_g["K"], self.d_ghost, self.cell_g["F"]
        )
        self.parent = None

    def _copy_parent_data(self):
        if isinstance(self.parent, self.BORDER_TYPES["CDCCTF"]):
            self._copy_cusp_parent_data()

    def init_ghost_cells(
        self,
        x_mask_now_c,
        d_now_c,
        x_mask_pre_c,
        d_pre_c,
        x_mask_now_f,
        d_now_f,
        x_mask_pre_f,
        d_pre_f,
    ):
        end_idx = super().init_ghost_cells(
            x_mask_now_c,
            d_now_c,
            x_mask_pre_c,
            d_pre_c,
            x_mask_now_f,
            d_now_f,
            x_mask_pre_f,
            d_pre_f,
        )
        if x_mask_now_c[self._cell_g_init["F"]]:
            self.d_ghost[self.cell_g["F"]] += d_now_c[
                np.count_nonzero(x_mask_now_c[0 : end_idx["F"]])
            ]
        if x_mask_pre_c[self._cell_g_init["H"]]:
            self.d_ghost[self.cell_g["H"]] += d_pre_c[
                np.count_nonzero(x_mask_pre_c[0 : end_idx["H"]])
            ]
        return end_idx

    def border_change_selector(self, border):
        if border is type(self):
            return None

        # currently not implemented, but is valid possibility
        # DiagCoarseToFineL, DiagFineToCoarseR
        if border in (self.BORDER_TYPES["DCTFL"], self.BORDER_TYPES["DFTCR"]):
            # return border
            raise NotImplementedError(
                f"Change from {type(self).__name__} to {border.__name__} "
                "not implemented."
            )

        raise NotImplementedError(
            f"Change from {type(self).__name__} to {border.__name__} not implemented."
        )

    def evolve_init_slice_t0_copy(
        self,
        _reg_pre,
        n_reg_pre,
        reg_now,
        n_reg_now,
        _reg_nxt,
        n_reg_nxt,
        _d_pre,
        n_d_pre,
        d_now,
        n_d_now,
        _d_nxt,
        n_d_nxt,
    ):
        """Refinement steps +2:1, +2:2, +2:3 on initialization."""
        self.parent = None
        return self.evolve_t2(
            _reg_pre,
            n_reg_pre,
            reg_now,
            n_reg_now,
            _reg_nxt,
            n_reg_nxt,
            _d_pre,
            n_d_pre,
            d_now,
            n_d_now,
            _d_nxt,
            n_d_nxt,
        )

    def evolve_t1(
        self,
        reg_pre,
        n_reg_pre,
        reg_now,
        _n_reg_now,
        reg_nxt,
        n_reg_nxt,
        d_pre,
        n_d_pre,
        d_now,
        _n_d_now,
        d_nxt,
        n_d_nxt,
    ):
        """Refinement step +1:1."""
        # A
        self.solver.evolve_stencil(
            *self.stencil_args,
            TempData((d_pre, self.cell_d(reg_pre, "D"))),
            TempData(
                (d_now, self.cell_d(reg_now, "C")),
                (self.d_ghost, self.cell_g["H"]),
                (d_now, self.cell_d(reg_now, "B")),
            ),
            TempData((d_nxt, self.cell_d(reg_nxt, "A"))),
        )
        # e
        alpha_diag_border(
            n_d_nxt,
            self.cell_d(n_reg_nxt, "e"),
            n_d_pre,
            self.cell_d(n_reg_pre, "o"),
            self.d_ghost,
            self.cell_g["F"],
            self.d_ghost,
            self.cell_g["I"],
            d_now,
            self.cell_d(reg_now, "C"),
            self.d_ghost,
            self.cell_g["J"],
        )
        return 1

    def evolve_t2(
        self,
        _reg_pre,
        n_reg_pre,
        reg_now,
        n_reg_now,
        _reg_nxt,
        n_reg_nxt,
        _d_pre,
        n_d_pre,
        d_now,
        n_d_now,
        _d_nxt,
        n_d_nxt,
    ):
        """Refinement steps +2:1, +2:2, +2:3."""
        # c
        self.solver.evolve_stencil(
            *self.stencil_args,
            TempData((n_d_pre, self.cell_d(n_reg_pre, "k"))),
            TempData(
                (n_d_now, self.cell_d(n_reg_now, "g")),
                (n_d_now, self.cell_d(n_reg_now, "h")),
                (n_d_now, self.cell_d(n_reg_now, "f")),
            ),
            TempData((self.d_ghost, self.cell_g["c"])),
        )
        # d
        self.solver.evolve_stencil(
            *self.stencil_args,
            TempData((n_d_pre, self.cell_d(n_reg_pre, "l"))),
            TempData(
                (n_d_now, self.cell_d(n_reg_now, "h")),
                (n_d_now, self.cell_d(n_reg_now, "x")),
                (n_d_now, self.cell_d(n_reg_now, "g")),
            ),
            TempData((self.d_ghost, self.cell_g["d"])),
        )
        # I -> G
        average(
            self.d_ghost,
            self.cell_g["I"],
            self.d_ghost,
            self.cell_g["c"],
            self.d_ghost,
            self.cell_g["d"],
            n_d_now,
            self.cell_d(n_reg_now, "g"),
            n_d_now,
            self.cell_d(n_reg_now, "h"),
        )
        # J -> E
        self.solver.evolve_stencil(
            *self.stencil_args,
            TempData((self.d_ghost, self.cell_g["H"])),
            TempData(
                (self.d_ghost, self.cell_g["F"]),
                (self.d_ghost, self.cell_g["I"]),
                (d_now, self.cell_d(reg_now, "A")),
            ),
            TempData((self.d_ghost, self.cell_g["J"])),
        )
        # a
        gamma_diag_border(
            n_d_nxt,
            self.cell_d(n_reg_nxt, "a"),
            n_d_now,
            self.cell_d(n_reg_now, "f"),
            d_now,
            self.cell_d(reg_now, "A"),
            self.d_ghost,
            self.cell_g["H"],
            self.d_ghost,
            self.cell_g["J"],
            self.d_ghost,
            self.cell_g["I"],
        )
        self.cell_g["F"], self.cell_g["H"], self.cell_g["J"] = (
            self.cell_g["J"],  # E
            self.cell_g["F"],
            self.cell_g["H"],
        )
        return 2

    def plot(self, p=None, P=None, res=None, nres=None):
        out = []
        p = int((self.p + self.reg.n) * self.reg.a) if p is None else p
        P = p if P is None else P
        res = self.reg.a if res is None else res
        nres = self.neighbor.a if nres is None else nres
        if self.evolve_step == 1:
            out.append((-1, P, self.reg, self.d_ghost[self.cell_g["H"]]))
            out.append((0, P, self.reg, self.d_ghost[self.cell_g["F"]]))
            out.append((-1, P + res, self.reg, self.d_ghost[self.cell_g["I"]]))
            out.append((-2, P, self.reg, self.d_ghost[self.cell_g["J"]]))
        elif self.evolve_step == 2:
            out.append((0, p + nres * 2, self.neighbor, self.d_ghost[self.cell_g["c"]]))
            out.append((0, p + nres * 3, self.neighbor, self.d_ghost[self.cell_g["d"]]))
            out.append((0, P + res, self.reg, self.d_ghost[self.cell_g["I"]]))
            out.append((-1, P, self.reg, self.d_ghost[self.cell_g["J"]]))
            out.append((1, P, self.reg, self.d_ghost[self.cell_g["F"]]))
            out.append((0, P, self.reg, self.d_ghost[self.cell_g["H"]]))

        return out


class StraightFineToCoarseV1(StraightCoarseToFineV1):
    """|Fine || Coarse| border at same x position, timelike."""

    __slots__: set[str] = set()

    def __init__(
        self,
        border_reg,
        sort_idx,
        arr_idx,
        pos,
        neigh_reg,
        neighbor_arr,
        neighbor_sort_idx,
        a2,
        solver,
        border_types,
        *,
        parent=None,
        skip=False,
        d_type=np.float_,
    ):
        """Return new StraightFineToCoarse (timelike)."""
        super().__init__(
            border_reg,
            sort_idx,
            arr_idx,
            pos,
            neigh_reg,
            neighbor_arr,
            neighbor_sort_idx,
            a2,
            solver,
            border_types,
            parent=parent,
            skip=skip,
            d_type=d_type,
        )

        self._cell_g_init = {"F": -1, "H": -1}
        self.g_max_dist = {
            self.reg.a: Directional(1, 0),
            self.neighbor.a: Directional(0, 0),
        }
        self._cell_names = {
            "A": 0,
            "B": 1,
            "C": 0,
            "D": 0,
            "a": -1,
            "e": -1,
            "f": -2,
            "g": -3,
            "h": -4,
            "o": -3,
            "k": -3,
            "l": -4,
            "x": -5,
        }

    def _copy_parent_data(self):
        if isinstance(self.parent, self.BORDER_TYPES["CDCFTC"]):
            self._copy_cusp_parent_data()

    def border_change_selector(self, border):
        if border is type(self):
            return None

        # currently not implemented, but is valid possibility
        # DiagCoarseToFineR, DiagFineToCoarseL
        if border in (self.BORDER_TYPES["DCTFR"], self.BORDER_TYPES["DFTCL"]):
            # return border
            raise NotImplementedError(
                f"Change from {type(self).__name__} to {border.__name__} "
                "not implemented."
            )

        raise NotImplementedError(
            f"Change from {type(self).__name__} to {border.__name__} not implemented."
        )

    def plot(self, p=None, P=None, res=None, nres=None):
        p = int(self.p * self.reg.a - self.neighbor.a) if p is None else p
        P = int(self.p * self.reg.a - self.reg.a) if P is None else P
        res = -self.reg.a if res is None else res
        nres = -self.neighbor.a if nres is None else nres
        return super().plot(p=p, P=P, res=res, nres=nres)


class StraightCoarseToFineV3(BorderBase):
    """|Coarse || Fine| border at same x position, timelike."""

    # V3 uses diag border eqs on a but not e
    # e is corrected with avg for F

    __slots__: set[str] = {"_e_plotval"}

    def __init__(
        self,
        border_reg,
        sort_idx,
        arr_idx,
        pos,
        neigh_reg,
        neighbor_arr,
        neighbor_sort_idx,
        a2,
        solver,
        border_types,
        *,
        parent=None,
        skip=False,
        d_type=np.float_,
        num_ghost=6,
    ):
        """Return new StraightCoarseToFine (timelike)."""
        super().__init__(
            border_reg,
            sort_idx,
            arr_idx,
            pos,
            neigh_reg,
            neighbor_arr,
            neighbor_sort_idx,
            a2,
            solver,
            border_types,
            parent=parent,
            skip=skip,
        )
        self._init_evolve = {
            self.InitEvolve.SLICE_T0_CPY: self.evolve_init_slice_t0_copy
        }
        self.evolve_cycle = cycle((self.evolve_t1, self.evolve_t2))
        # 7 ghost cells required
        self.d_ghost = np.zeros(num_ghost, dtype=d_type)

        self.cell_g = {  # comment names valid after swap
            "F": 0,
            "H": 1,
            "I": 2,  # G
            "J": 3,  # E
            "f": 4,  # c
            "g": 5,  # d
        }
        self._cell_g_init = {"F": 0, "H": 0}
        self.g_max_dist = {
            self.reg.a: Directional(0, 1),
            self.neighbor.a: Directional(0, 0),
        }
        self._cell_names = {
            "A": -1,
            "B": -2,
            "C": -1,
            "D": -1,
            "a": 0,
            "e": 0,
            "f": 1,
            "g": 2,
            "h": 3,
            "i": 0,
            "j": 1,
            "k": 2,
            "l": 3,
            "m": 0,
            "n": 1,
            "o": 2,
            "x": 4,
        }

        # extra for plotting e as ghost on t2 step
        # don't do this, it's bad code
        self._e_plotval = 0.0

        self._copy_parent_data()

    def _copy_cusp_parent_data(self):
        copy_to(
            self.parent.d_ghost, self.parent.cell_g["E"], self.d_ghost, self.cell_g["J"]
        )
        copy_to(
            self.parent.d_ghost, self.parent.cell_g["N"], self.d_ghost, self.cell_g["I"]
        )
        copy_to(
            self.parent.d_ghost, self.parent.cell_g["O"], self.d_ghost, self.cell_g["H"]
        )
        copy_to(
            self.parent.d_ghost, self.parent.cell_g["K"], self.d_ghost, self.cell_g["F"]
        )
        self.parent = None

    def _copy_parent_data(self):
        if isinstance(self.parent, self.BORDER_TYPES["CDCCTF"]):
            self._copy_cusp_parent_data()

    def init_ghost_cells(
        self,
        x_mask_now_c,
        d_now_c,
        x_mask_pre_c,
        d_pre_c,
        x_mask_now_f,
        d_now_f,
        x_mask_pre_f,
        d_pre_f,
    ):
        end_idx = super().init_ghost_cells(
            x_mask_now_c,
            d_now_c,
            x_mask_pre_c,
            d_pre_c,
            x_mask_now_f,
            d_now_f,
            x_mask_pre_f,
            d_pre_f,
        )
        if x_mask_now_c[self._cell_g_init["F"]]:
            self.d_ghost[self.cell_g["F"]] += d_now_c[
                np.count_nonzero(x_mask_now_c[0 : end_idx["F"]])
            ]
        if x_mask_pre_c[self._cell_g_init["H"]]:
            self.d_ghost[self.cell_g["H"]] += d_pre_c[
                np.count_nonzero(x_mask_pre_c[0 : end_idx["H"]])
            ]
        return end_idx

    def border_change_selector(self, border):
        if border is type(self):
            return None

        # currently not implemented, but is valid possibility
        # DiagCoarseToFineL, DiagFineToCoarseR
        if border in (self.BORDER_TYPES["DCTFL"], self.BORDER_TYPES["DFTCR"]):
            # return border
            raise NotImplementedError(
                f"Change from {type(self).__name__} to {border.__name__} "
                "not implemented."
            )

        raise NotImplementedError(
            f"Change from {type(self).__name__} to {border.__name__} not implemented."
        )

    def evolve_init_slice_t0_copy(
        self,
        _reg_pre,
        n_reg_pre,
        reg_now,
        n_reg_now,
        _reg_nxt,
        n_reg_nxt,
        _d_pre,
        n_d_pre,
        d_now,
        n_d_now,
        _d_nxt,
        n_d_nxt,
    ):
        """Refinement steps +2:1, +2:2, +2:3, +2:4 on initialization."""
        self.parent = None
        return self.evolve_t2(
            _reg_pre,
            n_reg_pre,
            reg_now,
            n_reg_now,
            _reg_nxt,
            n_reg_nxt,
            _d_pre,
            n_d_pre,
            d_now,
            n_d_now,
            _d_nxt,
            n_d_nxt,
        )

    def evolve_t1(
        self,
        reg_pre,
        n_reg_pre,
        reg_now,
        n_reg_now,
        reg_nxt,
        n_reg_nxt,
        d_pre,
        n_d_pre,
        d_now,
        n_d_now,
        d_nxt,
        n_d_nxt,
    ):
        """Refinement steps +1:1, +1:2."""
        # f
        self.solver.evolve_stencil(
            *self.stencil_args,
            TempData((n_d_pre, self.cell_d(n_reg_pre, "n"))),
            TempData(
                (n_d_now, self.cell_d(n_reg_now, "j")),
                (n_d_now, self.cell_d(n_reg_now, "k")),
                (n_d_now, self.cell_d(n_reg_now, "i")),
            ),
            TempData((self.d_ghost, self.cell_g["f"])),
        )
        # g
        self.solver.evolve_stencil(
            *self.stencil_args,
            TempData((n_d_pre, self.cell_d(n_reg_pre, "o"))),
            TempData(
                (n_d_now, self.cell_d(n_reg_now, "k")),
                (n_d_now, self.cell_d(n_reg_now, "l")),
                (n_d_now, self.cell_d(n_reg_now, "j")),
            ),
            TempData((self.d_ghost, self.cell_g["g"])),
        )
        # A
        self.solver.evolve_stencil(
            *self.stencil_args,
            TempData((d_pre, self.cell_d(reg_pre, "D"))),
            TempData(
                (d_now, self.cell_d(reg_now, "C")),
                (self.d_ghost, self.cell_g["H"]),
                (d_now, self.cell_d(reg_now, "B")),
            ),
            TempData((d_nxt, self.cell_d(reg_nxt, "A"))),
        )
        # e
        straight_v3_special_1(
            n_d_nxt,
            self.cell_d(n_reg_nxt, "e"),
            self.d_ghost,
            self.cell_g["F"],
            d_nxt,
            self.cell_d(reg_nxt, "A"),
            self.d_ghost,
            self.cell_g["H"],
            n_d_now,
            self.cell_d(n_reg_now, "j"),
            self.d_ghost,
            self.cell_g["g"],
            self.d_ghost,
            self.cell_g["f"],
        )
        return 1

    def evolve_t2(
        self,
        _reg_pre,
        n_reg_pre,
        reg_now,
        n_reg_now,
        _reg_nxt,
        n_reg_nxt,
        _d_pre,
        n_d_pre,
        d_now,
        n_d_now,
        _d_nxt,
        n_d_nxt,
    ):
        """Refinement steps +2:1, +2:2, +2:3, +2:4."""
        # f -> c
        self.solver.evolve_stencil(
            *self.stencil_args,
            TempData((n_d_pre, self.cell_d(n_reg_pre, "k"))),
            TempData(
                (n_d_now, self.cell_d(n_reg_now, "g")),
                (n_d_now, self.cell_d(n_reg_now, "h")),
                (n_d_now, self.cell_d(n_reg_now, "f")),
            ),
            TempData((self.d_ghost, self.cell_g["f"])),
        )
        # g -> d
        self.solver.evolve_stencil(
            *self.stencil_args,
            TempData((n_d_pre, self.cell_d(n_reg_pre, "l"))),
            TempData(
                (n_d_now, self.cell_d(n_reg_now, "h")),
                (n_d_now, self.cell_d(n_reg_now, "x")),
                (n_d_now, self.cell_d(n_reg_now, "g")),
            ),
            TempData((self.d_ghost, self.cell_g["g"])),
        )
        # I -> G
        average(
            self.d_ghost,
            self.cell_g["I"],
            self.d_ghost,
            self.cell_g["f"],  # c
            self.d_ghost,
            self.cell_g["g"],  # g
            n_d_now,
            self.cell_d(n_reg_now, "g"),
            n_d_now,
            self.cell_d(n_reg_now, "h"),
        )
        # J -> E
        self.solver.evolve_stencil(
            *self.stencil_args,
            TempData((self.d_ghost, self.cell_g["H"])),
            TempData(
                (self.d_ghost, self.cell_g["F"]),
                (self.d_ghost, self.cell_g["I"]),
                (d_now, self.cell_d(reg_now, "A")),
            ),
            TempData((self.d_ghost, self.cell_g["J"])),
        )
        # a
        gamma_diag_border(
            n_d_nxt,
            self.cell_d(n_reg_nxt, "a"),
            n_d_now,
            self.cell_d(n_reg_now, "f"),
            d_now,
            self.cell_d(reg_now, "A"),
            self.d_ghost,
            self.cell_g["H"],
            self.d_ghost,
            self.cell_g["J"],
            self.d_ghost,
            self.cell_g["I"],
        )
        # correct e
        straight_v3_special_2(
            n_d_now,
            self.cell_d(n_reg_now, "e"),
            self.d_ghost,
            self.cell_g["F"],
            n_d_nxt,
            self.cell_d(n_reg_nxt, "a"),
            n_d_pre,
            self.cell_d(n_reg_pre, "j"),
            n_d_now,
            self.cell_d(n_reg_now, "g"),
            n_d_now,
            self.cell_d(n_reg_now, "f"),
        )
        # save e for plotting as ghost cell
        self._e_plotval = n_d_now[self.cell_d(n_reg_now, "e")]
        # swap
        self.cell_g["F"], self.cell_g["H"], self.cell_g["J"] = (
            self.cell_g["J"],  # E
            self.cell_g["F"],
            self.cell_g["H"],
        )
        return 2

    def plot(self, p=None, P=None, res=None, nres=None):
        out = []
        p = int((self.p + self.reg.n) * self.reg.a) if p is None else p
        P = p if P is None else P
        res = self.reg.a if res is None else res
        nres = self.neighbor.a if nres is None else nres
        if self.evolve_step == 1:
            out.append((0, p + nres, self.neighbor, self.d_ghost[self.cell_g["f"]]))
            out.append((0, p + 2 * nres, self.neighbor, self.d_ghost[self.cell_g["g"]]))
            out.append((-1, P, self.reg, self.d_ghost[self.cell_g["H"]]))
            out.append((0, P, self.reg, self.d_ghost[self.cell_g["F"]]))
            out.append((-1, P + res, self.reg, self.d_ghost[self.cell_g["I"]]))
            out.append((-2, P, self.reg, self.d_ghost[self.cell_g["J"]]))
        elif self.evolve_step == 2:
            out.append((0, p + nres * 2, self.neighbor, self.d_ghost[self.cell_g["f"]]))
            out.append((0, p + nres * 3, self.neighbor, self.d_ghost[self.cell_g["g"]]))
            out.append((0, P + res, self.reg, self.d_ghost[self.cell_g["I"]]))
            out.append((-1, P, self.reg, self.d_ghost[self.cell_g["J"]]))
            out.append((1, P, self.reg, self.d_ghost[self.cell_g["F"]]))
            out.append((0, P, self.reg, self.d_ghost[self.cell_g["H"]]))
            # e as ghost after correction on pre slice
            out.append((-1, p, self.neighbor, self._e_plotval))

        return out


class StraightFineToCoarseV3(StraightCoarseToFineV3):
    """|Fine || Coarse| border at same x position, timelike."""

    __slots__: set[str] = set()

    def __init__(
        self,
        border_reg,
        sort_idx,
        arr_idx,
        pos,
        neigh_reg,
        neighbor_arr,
        neighbor_sort_idx,
        a2,
        solver,
        border_types,
        *,
        parent=None,
        skip=False,
        d_type=np.float_,
    ):
        """Return new StraightFineToCoarse (timelike)."""
        super().__init__(
            border_reg,
            sort_idx,
            arr_idx,
            pos,
            neigh_reg,
            neighbor_arr,
            neighbor_sort_idx,
            a2,
            solver,
            border_types,
            parent=parent,
            skip=skip,
            d_type=d_type,
        )

        self._cell_g_init = {"F": -1, "H": -1}
        self.g_max_dist = {
            self.reg.a: Directional(1, 0),
            self.neighbor.a: Directional(0, 0),
        }
        self._cell_names = {
            "A": 0,
            "B": 1,
            "C": 0,
            "D": 0,
            "a": -1,
            "e": -1,
            "f": -2,
            "g": -3,
            "h": -4,
            "i": -1,
            "j": -2,
            "k": -3,
            "l": -4,
            "m": -1,
            "n": -2,
            "o": -3,
            "x": -5,
        }

    def _copy_parent_data(self):
        if isinstance(self.parent, self.BORDER_TYPES["CDCFTC"]):
            self._copy_cusp_parent_data()

    def border_change_selector(self, border):
        if border is type(self):
            return None

        # currently not implemented, but is valid possibility
        # DiagCoarseToFineR, DiagFineToCoarseL
        if border in (self.BORDER_TYPES["DCTFR"], self.BORDER_TYPES["DFTCL"]):
            # return border
            raise NotImplementedError(
                f"Change from {type(self).__name__} to {border.__name__} "
                "not implemented."
            )

        raise NotImplementedError(
            f"Change from {type(self).__name__} to {border.__name__} not implemented."
        )

    def plot(self, p=None, P=None, res=None, nres=None):
        p = int(self.p * self.reg.a - self.neighbor.a) if p is None else p
        P = int(self.p * self.reg.a - self.reg.a) if P is None else P
        res = -self.reg.a if res is None else res
        nres = -self.neighbor.a if nres is None else nres
        return super().plot(p=p, P=P, res=res, nres=nres)


class StraightCoarseToFineV5(BorderBase):
    """|Coarse || Fine| border at same x position, timelike."""

    # V5 corresponds to avg substitution

    __slots__: set[str] = set()

    def __init__(
        self,
        border_reg,
        sort_idx,
        arr_idx,
        pos,
        neigh_reg,
        neighbor_arr,
        neighbor_sort_idx,
        a2,
        solver,
        border_types,
        *,
        parent=None,
        skip=False,
        d_type=np.float_,
        num_ghost=6,
    ):
        """Return new StraightCoarseToFine (timelike)."""
        super().__init__(
            border_reg,
            sort_idx,
            arr_idx,
            pos,
            neigh_reg,
            neighbor_arr,
            neighbor_sort_idx,
            a2,
            solver,
            border_types,
            parent=parent,
            skip=skip,
        )
        self._init_evolve = {
            self.InitEvolve.SLICE_T0_CPY: self.evolve_init_slice_t0_copy
        }
        self.evolve_cycle = cycle((self.evolve_t1, self.evolve_t2))
        # 6 ghost cells required
        self.d_ghost = np.zeros(num_ghost, dtype=d_type)

        self.cell_g = {  # comment names valid after swap
            "F": 0,
            "H": 1,
            "I": 2,
            "J": 3,  # E
            "G": 4,
            "t": 5,
        }
        self._cell_g_init = {"F": 0, "G": 1, "H": 0, "I": 1}
        self.g_max_dist = {
            self.reg.a: Directional(0, 2),
            self.neighbor.a: Directional(0, 0),
        }
        self._cell_names = {
            "A": -1,
            "B": -2,
            "C": -1,
            "D": -1,
            "e": 0,
            "o": 2,
            "a": 0,
            "f": 1,
            "q": 4,
            "r": 4,
            "s": 5,
            "t": 4,
        }
        self._copy_parent_data()

    def _copy_cusp_parent_data(self):
        copy_to(
            self.parent.d_ghost, self.parent.cell_g["E"], self.d_ghost, self.cell_g["J"]
        )
        copy_to(
            self.parent.d_ghost, self.parent.cell_g["N"], self.d_ghost, self.cell_g["I"]
        )
        copy_to(
            self.parent.d_ghost, self.parent.cell_g["O"], self.d_ghost, self.cell_g["H"]
        )
        copy_to(
            self.parent.d_ghost, self.parent.cell_g["K"], self.d_ghost, self.cell_g["F"]
        )
        copy_to(
            self.parent.d_ghost, self.parent.cell_g["P"], self.d_ghost, self.cell_g["G"]
        )
        self.parent = None

    def _copy_parent_data(self):
        if isinstance(self.parent, self.BORDER_TYPES["CDCCTF"]):
            self._copy_cusp_parent_data()

    def init_ghost_cells(
        self,
        x_mask_now_c,
        d_now_c,
        x_mask_pre_c,
        d_pre_c,
        x_mask_now_f,
        d_now_f,
        x_mask_pre_f,
        d_pre_f,
    ):
        end_idx = super().init_ghost_cells(
            x_mask_now_c,
            d_now_c,
            x_mask_pre_c,
            d_pre_c,
            x_mask_now_f,
            d_now_f,
            x_mask_pre_f,
            d_pre_f,
        )
        if x_mask_now_c[self._cell_g_init["F"]]:
            self.d_ghost[self.cell_g["F"]] += d_now_c[
                np.count_nonzero(x_mask_now_c[0 : end_idx["F"]])
            ]
        if x_mask_now_c[self._cell_g_init["G"]]:
            self.d_ghost[self.cell_g["G"]] += d_now_c[
                np.count_nonzero(x_mask_now_c[0 : end_idx["G"]])
            ]
        if x_mask_pre_c[self._cell_g_init["H"]]:
            self.d_ghost[self.cell_g["H"]] += d_pre_c[
                np.count_nonzero(x_mask_pre_c[0 : end_idx["H"]])
            ]
        if x_mask_pre_c[self._cell_g_init["I"]]:
            self.d_ghost[self.cell_g["I"]] += d_pre_c[
                np.count_nonzero(x_mask_pre_c[0 : end_idx["I"]])
            ]
        return end_idx

    def border_change_selector(self, border):
        if border is type(self):
            return None

        # CuspDoubleCellCoarseToFine
        if border is self.BORDER_TYPES["DCTFL"]:
            return self.BORDER_TYPES["CDCCTF"]

        raise NotImplementedError(
            f"Change from {type(self).__name__} to {border.__name__} not implemented."
        )

    def evolve_init_slice_t0_copy(
        self,
        _reg_pre,
        _n_reg_pre,
        reg_now,
        n_reg_now,
        _reg_nxt,
        n_reg_nxt,
        _d_pre,
        _n_d_pre,
        d_now,
        n_d_now,
        _d_nxt,
        n_d_nxt,
    ):
        """Refinement steps +2:2, +2:3 on initialization."""
        # same as evolve_t2, but skips G
        # E
        self.solver.evolve_stencil(
            *self.stencil_args,
            TempData((self.d_ghost, self.cell_g["H"])),
            TempData(
                (self.d_ghost, self.cell_g["F"]),
                (self.d_ghost, self.cell_g["G"]),
                (d_now, self.cell_d(reg_now, "A")),
            ),
            TempData((self.d_ghost, self.cell_g["J"])),  # E
        )
        # a
        gamma_diag_border(
            n_d_nxt,
            self.cell_d(n_reg_nxt, "a"),
            n_d_now,
            self.cell_d(n_reg_now, "f"),
            d_now,
            self.cell_d(reg_now, "A"),
            self.d_ghost,
            self.cell_g["H"],
            self.d_ghost,
            self.cell_g["J"],  # E
            self.d_ghost,
            self.cell_g["G"],
        )
        # swap ghost cells, first column
        self.cell_g["F"], self.cell_g["H"], self.cell_g["J"] = (
            self.cell_g["J"],
            self.cell_g["F"],
            self.cell_g["H"],
        )
        # second column
        self.cell_g["G"], self.cell_g["I"] = self.cell_g["I"], self.cell_g["G"]

        self.parent = None
        return 2

    def evolve_t1(
        self,
        reg_pre,
        n_reg_pre,
        reg_now,
        _n_reg_now,
        reg_nxt,
        n_reg_nxt,
        d_pre,
        n_d_pre,
        d_now,
        _n_d_now,
        d_nxt,
        n_d_nxt,
    ):
        """Refinement step +1:1, +1:2."""
        # e
        alpha_diag_border(
            n_d_nxt,
            self.cell_d(n_reg_nxt, "e"),
            n_d_pre,
            self.cell_d(n_reg_pre, "o"),
            self.d_ghost,
            self.cell_g["F"],
            self.d_ghost,
            self.cell_g["I"],
            d_now,
            self.cell_d(reg_now, "C"),
            self.d_ghost,
            self.cell_g["J"],
        )
        # A
        self.solver.evolve_stencil(
            *self.stencil_args,
            TempData((d_pre, self.cell_d(reg_pre, "D"))),
            TempData(
                (d_now, self.cell_d(reg_now, "C")),
                (self.d_ghost, self.cell_g["H"]),
                (d_now, self.cell_d(reg_now, "B")),
            ),
            TempData((d_nxt, self.cell_d(reg_nxt, "A"))),
        )
        # save t for step t2
        copy_to(n_d_pre, self.cell_d(n_reg_pre, "t"), self.d_ghost, self.cell_g["t"])
        return 1

    def evolve_t2(
        self,
        _reg_pre,
        n_reg_pre,
        reg_now,
        n_reg_now,
        _reg_nxt,
        n_reg_nxt,
        _d_pre,
        n_d_pre,
        d_now,
        n_d_now,
        _d_nxt,
        n_d_nxt,
    ):
        """Refinement steps +2:1, +2:2, +2:3."""
        # G
        straight_v5_special(
            self.d_ghost,
            self.cell_g["G"],
            self.d_ghost,
            self.cell_g["H"],
            self.d_ghost,
            self.cell_g["G"],  # K
            n_d_pre,
            self.cell_d(n_reg_pre, "r"),
            n_d_pre,
            self.cell_d(n_reg_pre, "s"),
            self.d_ghost,
            self.cell_g["t"],
            n_d_now,
            self.cell_d(n_reg_now, "q"),
        )
        # E
        self.solver.evolve_stencil(
            *self.stencil_args,
            TempData((self.d_ghost, self.cell_g["H"])),
            TempData(
                (self.d_ghost, self.cell_g["F"]),
                (self.d_ghost, self.cell_g["G"]),
                (d_now, self.cell_d(reg_now, "A")),
            ),
            TempData((self.d_ghost, self.cell_g["J"])),  # E
        )
        # a
        gamma_diag_border(
            n_d_nxt,
            self.cell_d(n_reg_nxt, "a"),
            n_d_now,
            self.cell_d(n_reg_now, "f"),
            d_now,
            self.cell_d(reg_now, "A"),
            self.d_ghost,
            self.cell_g["H"],
            self.d_ghost,
            self.cell_g["J"],  # E
            self.d_ghost,
            self.cell_g["G"],
        )
        # swap ghost cells, first column
        self.cell_g["F"], self.cell_g["H"], self.cell_g["J"] = (
            self.cell_g["J"],
            self.cell_g["F"],
            self.cell_g["H"],
        )
        # second column
        self.cell_g["G"], self.cell_g["I"] = self.cell_g["I"], self.cell_g["G"]
        return 2

    def plot(self, p=None, P=None, res=None, nres=None):
        out = []
        p = int((self.p + self.reg.n) * self.reg.a) if p is None else p
        P = p if P is None else P
        res = self.reg.a if res is None else res
        nres = self.neighbor.a if nres is None else nres
        if self.evolve_step == 1:
            out.append((0, P, self.reg, self.d_ghost[self.cell_g["F"]]))
            out.append((-1, P, self.reg, self.d_ghost[self.cell_g["H"]]))
            out.append((-1, P + res, self.reg, self.d_ghost[self.cell_g["I"]]))
            out.append((-2, P, self.reg, self.d_ghost[self.cell_g["J"]]))
            out.append((-2, P + res, self.reg, self.d_ghost[self.cell_g["G"]]))
            out.append(
                (-2, p + nres * 4, self.neighbor, self.d_ghost[self.cell_g["t"]])
            )
        elif self.evolve_step == 2:
            out.append((0, P + res, self.reg, self.d_ghost[self.cell_g["I"]]))  # G
            out.append((0, P, self.reg, self.d_ghost[self.cell_g["H"]]))  # F
            out.append((-1, P, self.reg, self.d_ghost[self.cell_g["J"]]))  # H
            out.append((-1, P + res, self.reg, self.d_ghost[self.cell_g["G"]]))  # I
            out.append((1, P, self.reg, self.d_ghost[self.cell_g["F"]]))  # E

        return out


class StraightFineToCoarseV5(StraightCoarseToFineV5):
    """|Fine || Coarse| border at same x position, timelike."""

    __slots__: set[str] = set()

    def __init__(
        self,
        border_reg,
        sort_idx,
        arr_idx,
        pos,
        neigh_reg,
        neighbor_arr,
        neighbor_sort_idx,
        a2,
        solver,
        border_types,
        *,
        parent=None,
        skip=False,
        d_type=np.float_,
    ):
        """Return new StraightFineToCoarse (timelike)."""
        super().__init__(
            border_reg,
            sort_idx,
            arr_idx,
            pos,
            neigh_reg,
            neighbor_arr,
            neighbor_sort_idx,
            a2,
            solver,
            border_types,
            parent=parent,
            skip=skip,
            d_type=d_type,
        )

        self._cell_g_init = {"F": -1, "G": -2, "H": -1, "I": -2}
        self.g_max_dist = {
            self.reg.a: Directional(2, 0),
            self.neighbor.a: Directional(0, 0),
        }
        self._cell_names = {
            "A": 0,
            "B": 1,
            "C": 0,
            "D": 0,
            "e": -1,
            "o": -3,
            "a": -1,
            "f": -2,
            "q": -5,
            "r": -5,
            "s": -6,
            "t": -5,
        }

    def _copy_parent_data(self):
        if isinstance(self.parent, self.BORDER_TYPES["CDCFTC"]):
            self._copy_cusp_parent_data()

    def border_change_selector(self, border):
        if border is type(self):
            return None

        # CuspDoubleCellCoarseToFine
        if border is self.BORDER_TYPES["DCTFR"]:
            return self.BORDER_TYPES["CDCFTC"]

        raise NotImplementedError(
            f"Change from {type(self).__name__} to {border.__name__} not implemented."
        )

    def plot(self, p=None, P=None, res=None, nres=None):
        p = int(self.p * self.reg.a - self.neighbor.a) if p is None else p
        P = int(self.p * self.reg.a - self.reg.a) if P is None else P
        res = -self.reg.a if res is None else res
        nres = -self.neighbor.a if nres is None else nres
        return super().plot(p=p, P=P, res=res, nres=nres)
