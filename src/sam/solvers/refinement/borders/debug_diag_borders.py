"""Diagonal borders at lightlike separation for debugging.

The refinement `schemes.[functions]` are patched with their debug
equivalents from `refinement.debug` using `unittest.mock.patch`.
Because all functions, including the leapfrog step used by refinement,
are patched, the debug values are mixed together and often multiply set.

For proper debug validation special setup scripts are provided.
"""

from unittest.mock import patch

import numpy as np

from sam.solvers import leapfrog
from sam.solvers.refinement.borders.diag_borders import (
    DiagCoarseToFineL,
    DiagCoarseToFineR,
    DiagFineToCoarseL,
    DiagFineToCoarseR,
)
from sam.solvers.refinement.debug import DebugBorderPass, SCHEME_EQUIVALENT_FUNC


@patch.multiple(
    "sam.solvers.refinement.borders.diag_borders",
    **SCHEME_EQUIVALENT_FUNC,
)
class DebugDiagCoarseToFineR(DiagCoarseToFineR):
    """DCTFR debug version."""

    __slots__: set[str] = set()

    def __init__(
        self,
        border_reg,
        sort_idx,
        arr_idx,
        pos,
        neigh_reg,
        neighbor_arr,
        neighbor_sort_idx,
        a2,
        _solver,
        border_types,
        *,
        parent=None,
        skip=False,
    ):
        """Return new DebugDiagCoarseToFineR (lightlike)."""
        super().__init__(
            border_reg,
            sort_idx,
            arr_idx,
            pos,
            neigh_reg,
            neighbor_arr,
            neighbor_sort_idx,
            a2,
            leapfrog.DebugEvolveLeapfrog(),  # hardcode debug leapfrog
            border_types,
            parent=parent,
            skip=skip,
            d_type=np.int8,  # change dtype to int
        )


@patch.multiple(
    "sam.solvers.refinement.borders.diag_borders",
    **SCHEME_EQUIVALENT_FUNC,
)
class DebugDiagCoarseToFineL(DiagCoarseToFineL):
    """DCTFL debug version."""

    __slots__: set[str] = set()

    def __init__(
        self,
        border_reg,
        sort_idx,
        arr_idx,
        pos,
        neigh_reg,
        neighbor_arr,
        neighbor_sort_idx,
        a2,
        _solver,
        border_types,
        *,
        parent=None,
        skip=False,
    ):
        """Return new DebugDiagCoarseToFineL (lightlike)."""
        super().__init__(
            border_reg,
            sort_idx,
            arr_idx,
            pos,
            neigh_reg,
            neighbor_arr,
            neighbor_sort_idx,
            a2,
            leapfrog.DebugEvolveLeapfrog(),  # hardcode debug leapfrog
            border_types,
            parent=parent,
            skip=skip,
            d_type=np.int8,  # change dtype to int
        )


@patch.multiple(
    "sam.solvers.refinement.borders.diag_borders",
    **SCHEME_EQUIVALENT_FUNC,
)
class DebugDiagFineToCoarseR(DiagFineToCoarseR):
    """DFTCR debug version."""

    __slots__: set[str] = set()

    def __init__(
        self,
        border_reg,
        sort_idx,
        arr_idx,
        pos,
        neigh_reg,
        neighbor_arr,
        neighbor_sort_idx,
        a2,
        _solver,
        border_types,
        *,
        parent=None,
        skip=False,
    ):
        """Return new DebugDiagFineToCoarseR (lightlike)."""
        super().__init__(
            border_reg,
            sort_idx,
            arr_idx,
            pos,
            neigh_reg,
            neighbor_arr,
            neighbor_sort_idx,
            a2,
            leapfrog.DebugEvolveLeapfrog(),  # hardcode debug leapfrog
            border_types,
            parent=parent,
            skip=skip,
            d_type=np.int8,  # change dtype to int
        )


@patch.multiple(
    "sam.solvers.refinement.borders.diag_borders",
    **SCHEME_EQUIVALENT_FUNC,
)
class DebugDiagFineToCoarseL(DiagFineToCoarseL):
    """DFTCL debug version."""

    __slots__: set[str] = set()

    def __init__(
        self,
        border_reg,
        sort_idx,
        arr_idx,
        pos,
        neigh_reg,
        neighbor_arr,
        neighbor_sort_idx,
        a2,
        _solver,
        border_types,
        *,
        parent=None,
        skip=False,
    ):
        """Return new DebugDiagFineToCoarseL (lightlike)."""
        super().__init__(
            border_reg,
            sort_idx,
            arr_idx,
            pos,
            neigh_reg,
            neighbor_arr,
            neighbor_sort_idx,
            a2,
            leapfrog.DebugEvolveLeapfrog(),  # hardcode debug leapfrog
            border_types,
            parent=parent,
            skip=skip,
            d_type=np.int8,  # change dtype to int
        )


# multiple inheritance needed to get correct `border_change_selector()`.
# the common type is first parent.


class DebugPassDiagCoarseToFineR(DebugBorderPass, DiagCoarseToFineR):
    """DCTFR for debugging that does nothing."""

    __slots__: set[str] = set()


class DebugPassDiagCoarseToFineL(DebugBorderPass, DiagCoarseToFineL):
    """DCTFL for debugging that does nothing."""

    __slots__: set[str] = set()


class DebugPassDiagFineToCoarseR(DebugBorderPass, DiagFineToCoarseR):
    """DFTCR for debugging that does nothing."""

    __slots__: set[str] = set()


class DebugPassDiagFineToCoarseL(DebugBorderPass, DiagFineToCoarseL):
    """DFTCL for debugging that does nothing."""

    __slots__: set[str] = set()
