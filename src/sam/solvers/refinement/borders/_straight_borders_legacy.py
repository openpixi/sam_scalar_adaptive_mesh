"""Straight borders at timelike separation.

Legacy versions that are not maintained.
"""


from itertools import cycle
from sys import stderr
from unittest.mock import patch

import numpy as np

from sam.containers import Directional
from sam.solvers import leapfrog
from sam.solvers.refinement import BORDER_TYPES, BorderTypes
from sam.solvers.refinement.border_base import BorderBase, TempData
from sam.solvers.refinement.borders.debug_straight_borders import (
    STRAIGHT_SCHEME_EQS,
    SCHEME_EQUIVALENT_FUNC,
)
from sam.solvers.refinement.debug import (
    SCHEME_STRAIGHT_EQUIVALENT_FUNC,
    MARKER_BOUNDARY,
    MARKER_RIGHT,
    MARKER_LEFT,
    MARKER_PRE,
    MARKER_WRITTEN,
)
from sam.solvers.refinement.schemes import (
    alpha_diag_border,
    gamma_diag_border,
    average,
    straight_v35_special,
    straight_v4_special,
)


STRAIGHT_SCHEME_EQS = STRAIGHT_SCHEME_EQS.copy()
STRAIGHT_SCHEME_EQS.update(
    {
        "straight_v35_special",
        "straight_v4_special",
    }
)


def debug_straight_v35_special(
    e, e_idx, F, F_idx, A, A_idx, H, H_idx, o, o_idx, l, l_idx, f, f_idx  # noqa: E741
):
    """Ref func for 'e' in V35 straight border."""
    # the second gamma part is missing
    e[e_idx] = MARKER_WRITTEN
    e[e_idx] |= MARKER_BOUNDARY
    A[A_idx] |= MARKER_RIGHT
    o[o_idx] |= MARKER_RIGHT
    H[H_idx] |= MARKER_LEFT
    l[l_idx] |= MARKER_LEFT
    F[F_idx] |= MARKER_PRE
    f[f_idx] |= MARKER_PRE


def debug_straight_v4_special(a, a_idx, F, F_idx, b, b_idx, e, e_idx, f, f_idx):
    """Ref func for 'a' in V4 straight border."""
    a[a_idx] = MARKER_WRITTEN
    a[a_idx] |= MARKER_BOUNDARY
    b[b_idx] |= MARKER_RIGHT
    f[f_idx] |= MARKER_RIGHT
    e[e_idx] |= MARKER_LEFT
    F[F_idx] |= MARKER_PRE


SCHEME_STRAIGHT_EQUIVALENT_FUNC = SCHEME_STRAIGHT_EQUIVALENT_FUNC.copy()
SCHEME_STRAIGHT_EQUIVALENT_FUNC.update(
    {
        "straight_v35_special": debug_straight_v35_special,
        "straight_v4_special": debug_straight_v4_special,
    }
)


class StraightCoarseToFineV2(BorderBase):
    """|Coarse || Fine| border at same x position, timelike."""

    # V2 corresponds to V1 with avg for H before refinement in t1
    # aka 'V1-star'

    __slots__: set[str] = set()

    def init_ghost_cells(self, *_):
        print(f"Using legacy {self.__class__} in newer code!", file=stderr)

    def __init__(
        self,
        border_reg,
        sort_idx,
        arr_idx,
        pos,
        neigh_reg,
        neighbor_arr,
        neighbor_sort_idx,
        a2,
        solver,
        border_types,
        *,
        parent=None,
        skip=False,
        d_type=np.float_,
        num_ghost=6,
    ):
        """Return new StraightCoarseToFine (timelike)."""
        super().__init__(
            border_reg,
            sort_idx,
            arr_idx,
            pos,
            neigh_reg,
            neighbor_arr,
            neighbor_sort_idx,
            a2,
            solver,
            border_types,
            parent=parent,
            skip=skip,
        )
        self._init_evolve = {k: lambda *x: None for k in self.InitEvolve}
        self.parent = None
        self.evolve_cycle = cycle((self.evolve_t1, self.evolve_t2))
        self.evolve_step = 2
        # 6 ghost cells required
        self.d_ghost = np.zeros(num_ghost, dtype=d_type)
        self.g_max_dist = {
            self.reg.a: Directional(0, 0),
            self.neighbor.a: Directional(0, 0),
        }
        self.cell_g = {  # comment names valid after swap
            "F": 0,
            "H": 1,
            "I": 2,  # G
            "J": 3,  # E
            "c": 4,
            "d": 5,
        }
        self._cell_names = {
            "A": -1,
            "B": -2,
            "C": -1,
            "D": -1,
            "a": 0,
            "e": 0,
            "f": 1,
            "g": 2,
            "h": 3,
            "i": 0,
            "j": 1,
            "o": 2,
            "k": 2,
            "l": 3,
            "m": 0,
            "n": 1,
            "x": 4,
        }

    def border_change_selector(self, border):
        if border is type(self):
            return None

        # currently not implemented, but is valid possibility
        # DiagCoarseToFineL, DiagFineToCoarseR
        if border in (self.BORDER_TYPES["DCTFL"], self.BORDER_TYPES["DFTCR"]):
            # return border
            raise NotImplementedError(
                f"Change from {type(self).__name__} to {border.__name__} "
                "not implemented."
            )

        raise NotImplementedError(
            f"Change from {type(self).__name__} to {border.__name__} not implemented."
        )

    def evolve_t1(
        self,
        reg_pre,
        n_reg_pre,
        reg_now,
        n_reg_now,
        reg_nxt,
        n_reg_nxt,
        d_pre,
        n_d_pre,
        d_now,
        n_d_now,
        d_nxt,
        n_d_nxt,
    ):
        """Refinement step +1:1."""
        # recalculate H as avg of fine
        average(
            self.d_ghost,
            self.cell_g["H"],
            n_d_now,
            self.cell_d(n_reg_now, "i"),
            n_d_now,
            self.cell_d(n_reg_now, "j"),
            n_d_pre,
            self.cell_d(n_reg_pre, "m"),
            n_d_pre,
            self.cell_d(n_reg_pre, "n"),
        )
        # A
        self.solver.evolve_stencil(
            *self.stencil_args,
            TempData((d_pre, self.cell_d(reg_pre, "D"))),
            TempData(
                (d_now, self.cell_d(reg_now, "C")),
                (self.d_ghost, self.cell_g["H"]),
                (d_now, self.cell_d(reg_now, "B")),
            ),
            TempData((d_nxt, self.cell_d(reg_nxt, "A"))),
        )
        # e
        alpha_diag_border(
            n_d_nxt,
            self.cell_d(n_reg_nxt, "e"),
            n_d_pre,
            self.cell_d(n_reg_pre, "o"),
            self.d_ghost,
            self.cell_g["F"],
            self.d_ghost,
            self.cell_g["I"],
            d_now,
            self.cell_d(reg_now, "C"),
            self.d_ghost,
            self.cell_g["J"],
        )
        return 1

    def evolve_t2(
        self,
        _reg_pre,
        n_reg_pre,
        reg_now,
        n_reg_now,
        _reg_nxt,
        n_reg_nxt,
        _d_pre,
        n_d_pre,
        d_now,
        n_d_now,
        _d_nxt,
        n_d_nxt,
    ):
        """Refinement steps +2:1, +2:2, +2:3."""
        # c
        self.solver.evolve_stencil(
            *self.stencil_args,
            TempData((n_d_pre, self.cell_d(n_reg_pre, "k"))),
            TempData(
                (n_d_now, self.cell_d(n_reg_now, "g")),
                (n_d_now, self.cell_d(n_reg_now, "h")),
                (n_d_now, self.cell_d(n_reg_now, "f")),
            ),
            TempData((self.d_ghost, self.cell_g["c"])),
        )
        # d
        self.solver.evolve_stencil(
            *self.stencil_args,
            TempData((n_d_pre, self.cell_d(n_reg_pre, "l"))),
            TempData(
                (n_d_now, self.cell_d(n_reg_now, "h")),
                (n_d_now, self.cell_d(n_reg_now, "x")),
                (n_d_now, self.cell_d(n_reg_now, "g")),
            ),
            TempData((self.d_ghost, self.cell_g["d"])),
        )
        # I -> G
        average(
            self.d_ghost,
            self.cell_g["I"],
            self.d_ghost,
            self.cell_g["c"],
            self.d_ghost,
            self.cell_g["d"],
            n_d_now,
            self.cell_d(n_reg_now, "g"),
            n_d_now,
            self.cell_d(n_reg_now, "h"),
        )
        # J -> E
        self.solver.evolve_stencil(
            *self.stencil_args,
            TempData((self.d_ghost, self.cell_g["H"])),
            TempData(
                (self.d_ghost, self.cell_g["F"]),
                (self.d_ghost, self.cell_g["I"]),
                (d_now, self.cell_d(reg_now, "A")),
            ),
            TempData((self.d_ghost, self.cell_g["J"])),
        )
        # a
        gamma_diag_border(
            n_d_nxt,
            self.cell_d(n_reg_nxt, "a"),
            n_d_now,
            self.cell_d(n_reg_now, "f"),
            d_now,
            self.cell_d(reg_now, "A"),
            self.d_ghost,
            self.cell_g["H"],
            self.d_ghost,
            self.cell_g["J"],
            self.d_ghost,
            self.cell_g["I"],
        )
        self.cell_g["F"], self.cell_g["H"], self.cell_g["J"] = (
            self.cell_g["J"],  # E
            self.cell_g["F"],
            self.cell_g["H"],
        )
        return 2

    def plot(self, p=None, P=None, res=None, nres=None):
        out = []
        p = int((self.p + self.reg.n) * self.reg.a) if p is None else p
        P = p if P is None else P
        res = self.reg.a if res is None else res
        nres = self.neighbor.a if nres is None else nres
        if self.evolve_step == 1:
            out.append((-1, P, self.reg, self.d_ghost[self.cell_g["H"]]))
            out.append((0, P, self.reg, self.d_ghost[self.cell_g["F"]]))
            out.append((-1, P + res, self.reg, self.d_ghost[self.cell_g["I"]]))
            out.append((-2, P, self.reg, self.d_ghost[self.cell_g["J"]]))
        elif self.evolve_step == 2:
            out.append((0, p + nres * 2, self.neighbor, self.d_ghost[self.cell_g["c"]]))
            out.append((0, p + nres * 3, self.neighbor, self.d_ghost[self.cell_g["d"]]))
            out.append((0, P + res, self.reg, self.d_ghost[self.cell_g["I"]]))
            out.append((-1, P, self.reg, self.d_ghost[self.cell_g["J"]]))
            out.append((1, P, self.reg, self.d_ghost[self.cell_g["F"]]))
            out.append((0, P, self.reg, self.d_ghost[self.cell_g["H"]]))

        return out


class StraightFineToCoarseV2(StraightCoarseToFineV2):
    """|Fine || Coarse| border at same x position, timelike."""

    __slots__: set[str] = set()

    def __init__(
        self,
        border_reg,
        sort_idx,
        arr_idx,
        pos,
        neigh_reg,
        neighbor_arr,
        neighbor_sort_idx,
        a2,
        solver,
        border_types,
        *,
        parent=None,
        skip=False,
        d_type=np.float_,
    ):
        """Return new StraightFineToCoarse (timelike)."""
        super().__init__(
            border_reg,
            sort_idx,
            arr_idx,
            pos,
            neigh_reg,
            neighbor_arr,
            neighbor_sort_idx,
            a2,
            solver,
            border_types,
            parent=parent,
            skip=skip,
            d_type=d_type,
        )

        self._cell_names = {
            "A": 0,
            "B": 1,
            "C": 0,
            "D": 0,
            "a": -1,
            "e": -1,
            "f": -2,
            "g": -3,
            "h": -4,
            "i": -1,
            "j": -2,
            "o": -3,
            "k": -3,
            "l": -4,
            "m": -1,
            "n": -2,
            "x": -5,
        }

    def border_change_selector(self, border):
        if border is type(self):
            return None

        # currently not implemented, but is valid possibility
        # DiagCoarseToFineR, DiagFineToCoarseL
        if border in (self.BORDER_TYPES["DCTFR"], self.BORDER_TYPES["DFTCL"]):
            # return border
            raise NotImplementedError(
                f"Change from {type(self).__name__} to {border.__name__} "
                "not implemented."
            )

        raise NotImplementedError(
            f"Change from {type(self).__name__} to {border.__name__} not implemented."
        )

    def plot(self, p=None, P=None, res=None, nres=None):
        p = int(self.p * self.reg.a - self.neighbor.a) if p is None else p
        P = int(self.p * self.reg.a - self.reg.a) if P is None else P
        res = -self.reg.a if res is None else res
        nres = -self.neighbor.a if nres is None else nres
        return super().plot(p=p, P=P, res=res, nres=nres)


class StraightCoarseToFineV35(BorderBase):
    """|Coarse || Fine| border at same x position, timelike."""

    # V3 uses diag border eqs on a but not e
    # old variant that ignores (E-G) term

    __slots__: set[str] = set()

    def init_ghost_cells(self, *_):
        print(f"Using legacy {self.__class__} in newer code!", file=stderr)

    def __init__(
        self,
        border_reg,
        sort_idx,
        arr_idx,
        pos,
        neigh_reg,
        neighbor_arr,
        neighbor_sort_idx,
        a2,
        solver,
        border_types,
        *,
        parent=None,
        skip=False,
        d_type=np.float_,
        num_ghost=7,
    ):
        """Return new StraightCoarseToFine (timelike)."""
        super().__init__(
            border_reg,
            sort_idx,
            arr_idx,
            pos,
            neigh_reg,
            neighbor_arr,
            neighbor_sort_idx,
            a2,
            solver,
            border_types,
            parent=parent,
            skip=skip,
        )
        self._init_evolve = {k: lambda *x: None for k in self.InitEvolve}
        self.parent = None
        self.evolve_cycle = cycle((self.evolve_t1, self.evolve_t2))
        # 7 ghost cells required
        self.d_ghost = np.zeros(num_ghost, dtype=d_type)
        self.g_max_dist = {
            self.reg.a: Directional(0, 0),
            self.neighbor.a: Directional(0, 0),
        }
        self.cell_g = {  # comment names valid after swap
            "F": 0,
            "H": 1,
            "I": 2,  # G
            "J": 3,  # E
            "c": 4,
            "d": 5,
            "f": 6,
        }
        self._cell_names = {
            "A": -1,
            "B": -2,
            "C": -1,
            "D": -1,
            "a": 0,
            "e": 0,
            "f": 1,
            "g": 2,
            "h": 3,
            "i": 0,
            "j": 1,
            "k": 2,
            "l": 3,
            "m": 0,
            "n": 1,
            "o": 2,
            "x": 4,
        }

    def border_change_selector(self, border):
        if border is type(self):
            return None

        # currently not implemented, but is valid possibility
        # DiagCoarseToFineL, DiagFineToCoarseR
        if border in (self.BORDER_TYPES["DCTFL"], self.BORDER_TYPES["DFTCR"]):
            # return border
            raise NotImplementedError(
                f"Change from {type(self).__name__} to {border.__name__} "
                "not implemented."
            )

        raise NotImplementedError(
            f"Change from {type(self).__name__} to {border.__name__} not implemented."
        )

    def evolve_t1(
        self,
        reg_pre,
        n_reg_pre,
        reg_now,
        n_reg_now,
        reg_nxt,
        n_reg_nxt,
        d_pre,
        n_d_pre,
        d_now,
        n_d_now,
        d_nxt,
        n_d_nxt,
    ):
        """Refinement step +1."""
        # f
        self.solver.evolve_stencil(
            *self.stencil_args,
            TempData((n_d_pre, self.cell_d(n_reg_pre, "n"))),
            TempData(
                (n_d_now, self.cell_d(n_reg_now, "j")),
                (n_d_now, self.cell_d(n_reg_now, "k")),
                (n_d_now, self.cell_d(n_reg_now, "i")),
            ),
            TempData((self.d_ghost, self.cell_g["f"])),
        )
        # A
        self.solver.evolve_stencil(
            *self.stencil_args,
            TempData((d_pre, self.cell_d(reg_pre, "D"))),
            TempData(
                (d_now, self.cell_d(reg_now, "C")),
                (self.d_ghost, self.cell_g["H"]),
                (d_now, self.cell_d(reg_now, "B")),
            ),
            TempData((d_nxt, self.cell_d(reg_nxt, "A"))),
        )
        # e
        straight_v35_special(
            n_d_nxt,
            self.cell_d(n_reg_nxt, "e"),
            self.d_ghost,
            self.cell_g["F"],
            d_nxt,
            self.cell_d(reg_nxt, "A"),
            self.d_ghost,
            self.cell_g["H"],
            n_d_pre,
            self.cell_d(n_reg_pre, "o"),
            n_d_now,
            self.cell_d(n_reg_now, "l"),
            self.d_ghost,
            self.cell_g["f"],
        )
        return 1

    def evolve_t2(
        self,
        _reg_pre,
        n_reg_pre,
        reg_now,
        n_reg_now,
        _reg_nxt,
        n_reg_nxt,
        _d_pre,
        n_d_pre,
        d_now,
        n_d_now,
        _d_nxt,
        n_d_nxt,
    ):
        """Refinement steps +2:1, +2:2, +2:3."""
        # c
        self.solver.evolve_stencil(
            *self.stencil_args,
            TempData((n_d_pre, self.cell_d(n_reg_pre, "k"))),
            TempData(
                (n_d_now, self.cell_d(n_reg_now, "g")),
                (n_d_now, self.cell_d(n_reg_now, "h")),
                (n_d_now, self.cell_d(n_reg_now, "f")),
            ),
            TempData((self.d_ghost, self.cell_g["c"])),
        )
        # d
        self.solver.evolve_stencil(
            *self.stencil_args,
            TempData((n_d_pre, self.cell_d(n_reg_pre, "l"))),
            TempData(
                (n_d_now, self.cell_d(n_reg_now, "h")),
                (n_d_now, self.cell_d(n_reg_now, "x")),
                (n_d_now, self.cell_d(n_reg_now, "g")),
            ),
            TempData((self.d_ghost, self.cell_g["d"])),
        )
        # I -> G
        average(
            self.d_ghost,
            self.cell_g["I"],
            self.d_ghost,
            self.cell_g["c"],
            self.d_ghost,
            self.cell_g["d"],
            n_d_now,
            self.cell_d(n_reg_now, "g"),
            n_d_now,
            self.cell_d(n_reg_now, "h"),
        )
        # J -> E
        self.solver.evolve_stencil(
            *self.stencil_args,
            TempData((self.d_ghost, self.cell_g["H"])),
            TempData(
                (self.d_ghost, self.cell_g["F"]),
                (self.d_ghost, self.cell_g["I"]),
                (d_now, self.cell_d(reg_now, "A")),
            ),
            TempData((self.d_ghost, self.cell_g["J"])),
        )
        # a
        gamma_diag_border(
            n_d_nxt,
            self.cell_d(n_reg_nxt, "a"),
            n_d_now,
            self.cell_d(n_reg_now, "f"),
            d_now,
            self.cell_d(reg_now, "A"),
            self.d_ghost,
            self.cell_g["H"],
            self.d_ghost,
            self.cell_g["J"],
            self.d_ghost,
            self.cell_g["I"],
        )
        self.cell_g["F"], self.cell_g["H"], self.cell_g["J"] = (
            self.cell_g["J"],  # E
            self.cell_g["F"],
            self.cell_g["H"],
        )
        return 2

    def plot(self, p=None, P=None, res=None, nres=None):
        out = []
        p = int((self.p + self.reg.n) * self.reg.a) if p is None else p
        P = p if P is None else P
        res = self.reg.a if res is None else res
        nres = self.neighbor.a if nres is None else nres
        if self.evolve_step == 1:
            out.append((-1, P, self.reg, self.d_ghost[self.cell_g["H"]]))
            out.append((0, P, self.reg, self.d_ghost[self.cell_g["F"]]))
            out.append((-1, P + res, self.reg, self.d_ghost[self.cell_g["I"]]))
            out.append((-2, P, self.reg, self.d_ghost[self.cell_g["J"]]))
            out.append((0, p + nres, self.neighbor, self.d_ghost[self.cell_g["f"]]))
        elif self.evolve_step == 2:
            out.append((0, p + nres * 2, self.neighbor, self.d_ghost[self.cell_g["c"]]))
            out.append((0, p + nres * 3, self.neighbor, self.d_ghost[self.cell_g["d"]]))
            out.append((0, P + res, self.reg, self.d_ghost[self.cell_g["I"]]))
            out.append((-1, P, self.reg, self.d_ghost[self.cell_g["J"]]))
            out.append((1, P, self.reg, self.d_ghost[self.cell_g["F"]]))
            out.append((0, P, self.reg, self.d_ghost[self.cell_g["H"]]))

        return out


class StraightFineToCoarseV35(StraightCoarseToFineV35):
    """|Fine || Coarse| border at same x position, timelike."""

    __slots__: set[str] = set()

    def __init__(
        self,
        border_reg,
        sort_idx,
        arr_idx,
        pos,
        neigh_reg,
        neighbor_arr,
        neighbor_sort_idx,
        a2,
        solver,
        border_types,
        *,
        parent=None,
        skip=False,
        d_type=np.float_,
    ):
        """Return new StraightFineToCoarse (timelike)."""
        super().__init__(
            border_reg,
            sort_idx,
            arr_idx,
            pos,
            neigh_reg,
            neighbor_arr,
            neighbor_sort_idx,
            a2,
            solver,
            border_types,
            parent=parent,
            skip=skip,
            d_type=d_type,
        )

        self._cell_names = {
            "A": 0,
            "B": 1,
            "C": 0,
            "D": 0,
            "a": -1,
            "e": -1,
            "f": -2,
            "g": -3,
            "h": -4,
            "i": -1,
            "j": -2,
            "k": -3,
            "l": -4,
            "m": -1,
            "n": -2,
            "o": -3,
            "x": -5,
        }

    def border_change_selector(self, border):
        if border is type(self):
            return None

        # currently not implemented, but is valid possibility
        # DiagCoarseToFineR, DiagFineToCoarseL
        if border in (self.BORDER_TYPES["DCTFR"], self.BORDER_TYPES["DFTCL"]):
            # return border
            raise NotImplementedError(
                f"Change from {type(self).__name__} to {border.__name__} "
                "not implemented."
            )

        raise NotImplementedError(
            f"Change from {type(self).__name__} to {border.__name__} not implemented."
        )

    def plot(self, p=None, P=None, res=None, nres=None):
        p = int(self.p * self.reg.a - self.neighbor.a) if p is None else p
        P = int(self.p * self.reg.a - self.reg.a) if P is None else P
        res = -self.reg.a if res is None else res
        nres = -self.neighbor.a if nres is None else nres
        return super().plot(p=p, P=P, res=res, nres=nres)


class StraightCoarseToFineV4(BorderBase):
    """|Coarse || Fine| border at same x position, timelike."""

    # uses diag border eqs on e but not a

    __slots__: set[str] = set()

    def init_ghost_cells(self, *_):
        print(f"Using legacy {self.__class__} in newer code!", file=stderr)

    def __init__(
        self,
        border_reg,
        sort_idx,
        arr_idx,
        pos,
        neigh_reg,
        neighbor_arr,
        neighbor_sort_idx,
        a2,
        solver,
        border_types,
        *,
        parent=None,
        skip=False,
        d_type=np.float_,
        num_ghost=5,
    ):
        """Return new StraightCoarseToFine (timelike)."""
        super().__init__(
            border_reg,
            sort_idx,
            arr_idx,
            pos,
            neigh_reg,
            neighbor_arr,
            neighbor_sort_idx,
            a2,
            solver,
            border_types,
            parent=parent,
            skip=skip,
        )
        self._init_evolve = {k: lambda *x: None for k in self.InitEvolve}
        self.parent = None
        self.evolve_cycle = cycle((self.evolve_t1, self.evolve_t2))
        # 5 ghost cells required
        self.d_ghost = np.zeros(num_ghost, dtype=d_type)
        self.g_max_dist = {
            self.reg.a: Directional(0, 0),
            self.neighbor.a: Directional(0, 0),
        }
        self.cell_g = {  # comment names valid after swap
            "F": 0,
            "H": 1,
            "I": 2,
            "J": 3,
            "b": 4,
        }
        self._cell_names = {
            "A": -1,
            "B": -2,
            "C": -1,
            "D": -1,
            "a": 0,
            "e": 0,
            "f": 1,
            "g": 2,
            "i": 0,
            "j": 1,
            "k": 2,
            "l": 3,
            "m": 0,
            "n": 1,
            "o": 2,
            "p": 3,
        }

    def border_change_selector(self, border):
        if border is type(self):
            return None

        # currently not implemented, but is valid possibility
        # DiagCoarseToFineL, DiagFineToCoarseR
        if border in (self.BORDER_TYPES["DCTFL"], self.BORDER_TYPES["DFTCR"]):
            # return border
            raise NotImplementedError(
                f"Change from {type(self).__name__} to {border.__name__} "
                "not implemented."
            )

        raise NotImplementedError(
            f"Change from {type(self).__name__} to {border.__name__} not implemented."
        )

    def evolve_t1(
        self,
        reg_pre,
        n_reg_pre,
        reg_now,
        n_reg_now,
        reg_nxt,
        n_reg_nxt,
        d_pre,
        n_d_pre,
        d_now,
        n_d_now,
        d_nxt,
        n_d_nxt,
    ):
        """Refinement steps +1:1, +1:2."""
        # I
        average(
            self.d_ghost,
            self.cell_g["I"],
            n_d_now,
            self.cell_d(n_reg_now, "k"),
            n_d_now,
            self.cell_d(n_reg_now, "l"),
            n_d_pre,
            self.cell_d(n_reg_pre, "o"),
            n_d_pre,
            self.cell_d(n_reg_pre, "p"),
        )
        # F
        self.solver.evolve_stencil(
            *self.stencil_args,
            TempData((self.d_ghost, self.cell_g["J"])),
            TempData(
                (self.d_ghost, self.cell_g["H"]),
                (self.d_ghost, self.cell_g["I"]),
                (d_now, self.cell_d(reg_now, "C")),
            ),
            TempData((self.d_ghost, self.cell_g["F"])),
        )
        # e
        alpha_diag_border(
            n_d_nxt,
            self.cell_d(n_reg_nxt, "e"),
            n_d_pre,
            self.cell_d(n_reg_pre, "o"),
            self.d_ghost,
            self.cell_g["F"],
            self.d_ghost,
            self.cell_g["I"],
            d_now,
            self.cell_d(reg_now, "C"),
            self.d_ghost,
            self.cell_g["J"],
        )
        # A
        self.solver.evolve_stencil(
            *self.stencil_args,
            TempData((d_pre, self.cell_d(reg_pre, "D"))),
            TempData(
                (d_now, self.cell_d(reg_now, "C")),
                (self.d_ghost, self.cell_g["H"]),
                (d_now, self.cell_d(reg_now, "B")),
            ),
            TempData((d_nxt, self.cell_d(reg_nxt, "A"))),
        )
        return 1

    def evolve_t2(
        self,
        _reg_pre,
        n_reg_pre,
        _reg_now,
        n_reg_now,
        _reg_nxt,
        n_reg_nxt,
        _d_pre,
        n_d_pre,
        _d_now,
        n_d_now,
        _d_nxt,
        n_d_nxt,
    ):
        """Refinement steps +2:1, +2:2, +2:3."""
        # b
        self.solver.evolve_stencil(
            *self.stencil_args,
            TempData((n_d_pre, self.cell_d(n_reg_pre, "j"))),
            TempData(
                (n_d_now, self.cell_d(n_reg_now, "f")),
                (n_d_now, self.cell_d(n_reg_now, "g")),
                (n_d_now, self.cell_d(n_reg_now, "e")),
            ),
            TempData((self.d_ghost, self.cell_g["b"])),
        )
        # a
        straight_v4_special(
            n_d_nxt,
            self.cell_d(n_reg_nxt, "a"),
            self.d_ghost,
            self.cell_g["F"],
            self.d_ghost,
            self.cell_g["b"],
            n_d_now,
            self.cell_d(n_reg_now, "e"),
            n_d_now,
            self.cell_d(n_reg_now, "f"),
        )
        self.cell_g["H"], self.cell_g["J"], self.cell_g["F"] = (
            self.cell_g["F"],
            self.cell_g["H"],
            self.cell_g["J"],
        )
        return 2

    def plot(self, p=None, P=None, res=None, nres=None):
        out = []
        p = int((self.p + self.reg.n) * self.reg.a) if p is None else p
        P = p if P is None else P
        res = self.reg.a if res is None else res
        nres = self.neighbor.a if nres is None else nres
        if self.evolve_step == 1:
            out.append((-1, P, self.reg, self.d_ghost[self.cell_g["H"]]))
            out.append((0, P, self.reg, self.d_ghost[self.cell_g["F"]]))
            out.append((-1, P + res, self.reg, self.d_ghost[self.cell_g["I"]]))
            out.append((-2, P, self.reg, self.d_ghost[self.cell_g["J"]]))
        elif self.evolve_step == 2:
            out.append((0, p + nres, self.neighbor, self.d_ghost[self.cell_g["b"]]))
            out.append((-1, P + res, self.reg, self.d_ghost[self.cell_g["I"]]))
            out.append((0, P, self.reg, self.d_ghost[self.cell_g["H"]]))
            out.append((-1, P, self.reg, self.d_ghost[self.cell_g["J"]]))
            out.append((-2, P, self.reg, self.d_ghost[self.cell_g["F"]]))

        return out


class StraightFineToCoarseV4(StraightCoarseToFineV4):
    """|Fine || Coarse| border at same x position, timelike."""

    __slots__: set[str] = set()

    def __init__(
        self,
        border_reg,
        sort_idx,
        arr_idx,
        pos,
        neigh_reg,
        neighbor_arr,
        neighbor_sort_idx,
        a2,
        evolve_stencil,
        solver,
        *,
        parent=None,
        skip=False,
        d_type=np.float_,
    ):
        """Return new StraightFineToCoarse (timelike)."""
        super().__init__(
            border_reg,
            sort_idx,
            arr_idx,
            pos,
            neigh_reg,
            neighbor_arr,
            neighbor_sort_idx,
            a2,
            evolve_stencil,
            solver,
            parent=parent,
            skip=skip,
            d_type=d_type,
        )
        self.g_max_dist = {
            self.reg.a: Directional(0, 0),
            self.neighbor.a: Directional(0, 0),
        }
        self._cell_names = {
            "A": 0,
            "B": 1,
            "C": 0,
            "D": 0,
            "a": -1,
            "e": -1,
            "f": -2,
            "g": -3,
            "i": -1,
            "j": -2,
            "k": -3,
            "l": -4,
            "m": -1,
            "n": -2,
            "o": -3,
            "p": -4,
        }

    def border_change_selector(self, border):
        if border is type(self):
            return None

        # currently not implemented, but is valid possibility
        # DiagCoarseToFineR, DiagFineToCoarseL
        if border in (self.BORDER_TYPES["DCTFR"], self.BORDER_TYPES["DFTCL"]):
            # return border
            raise NotImplementedError(
                f"Change from {type(self).__name__} to {border.__name__} "
                "not implemented."
            )

        raise NotImplementedError(
            f"Change from {type(self).__name__} to {border.__name__} not implemented."
        )

    def plot(self, p=None, P=None, res=None, nres=None):
        p = int(self.p * self.reg.a - self.neighbor.a) if p is None else p
        P = int(self.p * self.reg.a - self.reg.a) if P is None else P
        res = -self.reg.a if res is None else res
        nres = -self.neighbor.a if nres is None else nres
        return super().plot(p=p, P=P, res=res, nres=nres)


@patch.multiple(
    "sam.solvers.refinement.borders.straight_borders",
    **SCHEME_EQUIVALENT_FUNC,
)
class DebugStraightCoarseToFineV2(StraightCoarseToFineV2):
    """SCTF V2 debug version."""

    __slots__: set[str] = set()

    def __init__(
        self,
        border_reg,
        sort_idx,
        arr_idx,
        pos,
        neigh_reg,
        neighbor_arr,
        neighbor_sort_idx,
        a2,
        _solver,
        border_types,
        *,
        parent=None,
        skip=False,
    ):
        """Return new DebugStraightCoarseToFine (timelike)."""
        super().__init__(
            border_reg,
            sort_idx,
            arr_idx,
            pos,
            neigh_reg,
            neighbor_arr,
            neighbor_sort_idx,
            a2,
            leapfrog.DebugEvolveLeapfrog(),  # hardcode debug leapfrog
            border_types,
            parent=parent,
            skip=skip,
            d_type=np.int8,  # change dtype to int
        )


@patch.multiple(
    "sam.solvers.refinement.borders.straight_borders",
    **SCHEME_EQUIVALENT_FUNC,
)
class DebugStraightFineToCoarseV2(StraightFineToCoarseV2):
    """SFTC V2 debug version."""

    __slots__: set[str] = set()

    def __init__(
        self,
        border_reg,
        sort_idx,
        arr_idx,
        pos,
        neigh_reg,
        neighbor_arr,
        neighbor_sort_idx,
        a2,
        _solver,
        border_types,
        *,
        parent=None,
        skip=False,
    ):
        """Return new DebugStraightFineToCoarse (timelike)."""
        super().__init__(
            border_reg,
            sort_idx,
            arr_idx,
            pos,
            neigh_reg,
            neighbor_arr,
            neighbor_sort_idx,
            a2,
            leapfrog.DebugEvolveLeapfrog(),  # hardcode debug leapfrog
            border_types,
            parent=parent,
            skip=skip,
            d_type=np.int8,  # change dtype to int
        )


@patch.multiple(
    "sam.solvers.refinement.borders.straight_borders",
    **SCHEME_EQUIVALENT_FUNC,
)
class DebugStraightCoarseToFineV35(StraightCoarseToFineV35):
    """SCTF V35 debug version."""

    __slots__: set[str] = set()

    def __init__(
        self,
        border_reg,
        sort_idx,
        arr_idx,
        pos,
        neigh_reg,
        neighbor_arr,
        neighbor_sort_idx,
        a2,
        _solver,
        border_types,
        *,
        parent=None,
        skip=False,
    ):
        """Return new DebugStraightCoarseToFine (timelike)."""
        super().__init__(
            border_reg,
            sort_idx,
            arr_idx,
            pos,
            neigh_reg,
            neighbor_arr,
            neighbor_sort_idx,
            a2,
            leapfrog.DebugEvolveLeapfrog(),  # hardcode debug leapfrog
            border_types,
            parent=parent,
            skip=skip,
            d_type=np.int8,  # change dtype to int
        )


@patch.multiple(
    "sam.solvers.refinement.borders.straight_borders",
    **SCHEME_EQUIVALENT_FUNC,
)
class DebugStraightFineToCoarseV35(StraightFineToCoarseV35):
    """SFTC V35 debug version."""

    __slots__: set[str] = set()

    def __init__(
        self,
        border_reg,
        sort_idx,
        arr_idx,
        pos,
        neigh_reg,
        neighbor_arr,
        neighbor_sort_idx,
        a2,
        _solver,
        border_types,
        *,
        parent=None,
        skip=False,
    ):
        """Return new DebugStraightFineToCoarse (timelike)."""
        super().__init__(
            border_reg,
            sort_idx,
            arr_idx,
            pos,
            neigh_reg,
            neighbor_arr,
            neighbor_sort_idx,
            a2,
            leapfrog.DebugEvolveLeapfrog(),  # hardcode debug leapfrog
            border_types,
            parent=parent,
            skip=skip,
            d_type=np.int8,  # change dtype to int
        )


@patch.multiple(
    "sam.solvers.refinement.borders.straight_borders",
    **SCHEME_EQUIVALENT_FUNC,
)
class DebugStraightCoarseToFineV4(StraightCoarseToFineV4):
    """SCTF V4 debug version."""

    __slots__: set[str] = set()

    def __init__(
        self,
        border_reg,
        sort_idx,
        arr_idx,
        pos,
        neigh_reg,
        neighbor_arr,
        neighbor_sort_idx,
        a2,
        _solver,
        border_types,
        *,
        parent=None,
        skip=False,
    ):
        """Return new DebugStraightCoarseToFine (timelike)."""
        super().__init__(
            border_reg,
            sort_idx,
            arr_idx,
            pos,
            neigh_reg,
            neighbor_arr,
            neighbor_sort_idx,
            a2,
            leapfrog.DebugEvolveLeapfrog(),  # hardcode debug leapfrog
            border_types,
            parent=parent,
            skip=skip,
            d_type=np.int8,  # change dtype to int
        )


@patch.multiple(
    "sam.solvers.refinement.borders.straight_borders",
    **SCHEME_EQUIVALENT_FUNC,
)
class DebugStraightFineToCoarseV4(StraightFineToCoarseV4):
    """SFTC V4 debug version."""

    __slots__: set[str] = set()

    def __init__(
        self,
        border_reg,
        sort_idx,
        arr_idx,
        pos,
        neigh_reg,
        neighbor_arr,
        neighbor_sort_idx,
        a2,
        _solver,
        border_types,
        *,
        parent=None,
        skip=False,
    ):
        """Return new DebugStraightFineToCoarse (timelike)."""
        super().__init__(
            border_reg,
            sort_idx,
            arr_idx,
            pos,
            neigh_reg,
            neighbor_arr,
            neighbor_sort_idx,
            a2,
            leapfrog.DebugEvolveLeapfrog(),  # hardcode debug leapfrog
            border_types,
            parent=parent,
            skip=skip,
            d_type=np.int8,  # change dtype to int
        )


BORDER_TYPES2 = BorderTypes(
    BORDER_TYPES, SCTF=StraightCoarseToFineV2, SFTC=StraightFineToCoarseV2
)
BORDER_TYPES35 = BorderTypes(
    BORDER_TYPES, SCTF=StraightCoarseToFineV35, SFTC=StraightFineToCoarseV35
)
BORDER_TYPES4 = BorderTypes(
    BORDER_TYPES, SCTF=StraightCoarseToFineV4, SFTC=StraightFineToCoarseV4
)
