"""Straight borders at timelike separation for debugging.

The refinement `schemes.[functions]` are patched with their debug
equivalents from `refinement.debug` using `unittest.mock.patch`.
Because all functions, including the leapfrog step used by refinement,
are patched, the debug values are mixed together and often multiply set.

For proper debug validation special setup scripts are provided.
"""

from unittest.mock import patch

import numpy as np

from sam.solvers import leapfrog
from sam.solvers.refinement.borders.straight_borders import (
    StraightCoarseToFineV1,
    StraightFineToCoarseV1,
    StraightCoarseToFineV3,
    StraightFineToCoarseV3,
    StraightCoarseToFineV5,
    StraightFineToCoarseV5,
)
from sam.solvers.refinement.debug import (
    DebugBorderPass,
    SCHEME_EQUIVALENT_FUNC as SCHEME_DIAG_EQUIVALENT_FUNC,
    SCHEME_STRAIGHT_EQUIVALENT_FUNC,
)

# scheme eqs
STRAIGHT_SCHEME_EQS = {
    "copy_to",
    "alpha_diag_border",
    "gamma_diag_border",
    "average",
    "straight_v3_special_1",
    "straight_v3_special_2",
    "straight_v5_special",
}
SCHEME_EQUIVALENT_FUNC = {
    k: v for k, v in SCHEME_DIAG_EQUIVALENT_FUNC.items() if k in STRAIGHT_SCHEME_EQS
}
SCHEME_EQUIVALENT_FUNC.update(SCHEME_STRAIGHT_EQUIVALENT_FUNC)


@patch.multiple(
    "sam.solvers.refinement.borders.straight_borders",
    **SCHEME_EQUIVALENT_FUNC,
)
class DebugStraightCoarseToFineV1(StraightCoarseToFineV1):
    """SCTF V1 debug version."""

    __slots__: set[str] = set()

    def __init__(
        self,
        border_reg,
        sort_idx,
        arr_idx,
        pos,
        neigh_reg,
        neighbor_arr,
        neighbor_sort_idx,
        a2,
        _solver,
        border_types,
        *,
        parent=None,
        skip=False,
    ):
        """Return new DebugStraightCoarseToFine (timelike)."""
        super().__init__(
            border_reg,
            sort_idx,
            arr_idx,
            pos,
            neigh_reg,
            neighbor_arr,
            neighbor_sort_idx,
            a2,
            leapfrog.DebugEvolveLeapfrog(),  # hardcode debug leapfrog
            border_types,
            parent=parent,
            skip=skip,
            d_type=np.int8,  # change dtype to int
        )


@patch.multiple(
    "sam.solvers.refinement.borders.straight_borders",
    **SCHEME_EQUIVALENT_FUNC,
)
class DebugStraightFineToCoarseV1(StraightFineToCoarseV1):
    """SFTC V1 debug version."""

    __slots__: set[str] = set()

    def __init__(
        self,
        border_reg,
        sort_idx,
        arr_idx,
        pos,
        neigh_reg,
        neighbor_arr,
        neighbor_sort_idx,
        a2,
        _solver,
        border_types,
        *,
        parent=None,
        skip=False,
    ):
        """Return new DebugStraightFineToCoarse (timelike)."""
        super().__init__(
            border_reg,
            sort_idx,
            arr_idx,
            pos,
            neigh_reg,
            neighbor_arr,
            neighbor_sort_idx,
            a2,
            leapfrog.DebugEvolveLeapfrog(),  # hardcode debug leapfrog
            border_types,
            parent=parent,
            skip=skip,
            d_type=np.int8,  # change dtype to int
        )


@patch.multiple(
    "sam.solvers.refinement.borders.straight_borders",
    **SCHEME_EQUIVALENT_FUNC,
)
class DebugStraightCoarseToFineV3(StraightCoarseToFineV3):
    """SCTF V3 debug version."""

    __slots__: set[str] = set()

    def __init__(
        self,
        border_reg,
        sort_idx,
        arr_idx,
        pos,
        neigh_reg,
        neighbor_arr,
        neighbor_sort_idx,
        a2,
        _solver,
        border_types,
        *,
        parent=None,
        skip=False,
    ):
        """Return new DebugStraightCoarseToFine (timelike)."""
        super().__init__(
            border_reg,
            sort_idx,
            arr_idx,
            pos,
            neigh_reg,
            neighbor_arr,
            neighbor_sort_idx,
            a2,
            leapfrog.DebugEvolveLeapfrog(),  # hardcode debug leapfrog
            border_types,
            parent=parent,
            skip=skip,
            d_type=np.int8,  # change dtype to int
        )


@patch.multiple(
    "sam.solvers.refinement.borders.straight_borders",
    **SCHEME_EQUIVALENT_FUNC,
)
class DebugStraightFineToCoarseV3(StraightFineToCoarseV3):
    """SFTC V3 debug version."""

    __slots__: set[str] = set()

    def __init__(
        self,
        border_reg,
        sort_idx,
        arr_idx,
        pos,
        neigh_reg,
        neighbor_arr,
        neighbor_sort_idx,
        a2,
        _solver,
        border_types,
        *,
        parent=None,
        skip=False,
    ):
        """Return new DebugStraightFineToCoarse (timelike)."""
        super().__init__(
            border_reg,
            sort_idx,
            arr_idx,
            pos,
            neigh_reg,
            neighbor_arr,
            neighbor_sort_idx,
            a2,
            leapfrog.DebugEvolveLeapfrog(),  # hardcode debug leapfrog
            border_types,
            parent=parent,
            skip=skip,
            d_type=np.int8,  # change dtype to int
        )


@patch.multiple(
    "sam.solvers.refinement.borders.straight_borders",
    **SCHEME_EQUIVALENT_FUNC,
)
class DebugStraightCoarseToFineV5(StraightCoarseToFineV5):
    """SCTF V5 debug version."""

    __slots__: set[str] = set()

    def __init__(
        self,
        border_reg,
        sort_idx,
        arr_idx,
        pos,
        neigh_reg,
        neighbor_arr,
        neighbor_sort_idx,
        a2,
        _solver,
        border_types,
        *,
        parent=None,
        skip=False,
    ):
        """Return new DebugStraightCoarseToFine (timelike)."""
        super().__init__(
            border_reg,
            sort_idx,
            arr_idx,
            pos,
            neigh_reg,
            neighbor_arr,
            neighbor_sort_idx,
            a2,
            leapfrog.DebugEvolveLeapfrog(),  # hardcode debug leapfrog
            border_types,
            parent=parent,
            skip=skip,
            d_type=np.int8,  # change dtype to int
        )


@patch.multiple(
    "sam.solvers.refinement.borders.straight_borders",
    **SCHEME_EQUIVALENT_FUNC,
)
class DebugStraightFineToCoarseV5(StraightFineToCoarseV5):
    """SFTC V5 debug version."""

    __slots__: set[str] = set()

    def __init__(
        self,
        border_reg,
        sort_idx,
        arr_idx,
        pos,
        neigh_reg,
        neighbor_arr,
        neighbor_sort_idx,
        a2,
        _solver,
        border_types,
        *,
        parent=None,
        skip=False,
    ):
        """Return new DebugStraightFineToCoarse (timelike)."""
        super().__init__(
            border_reg,
            sort_idx,
            arr_idx,
            pos,
            neigh_reg,
            neighbor_arr,
            neighbor_sort_idx,
            a2,
            leapfrog.DebugEvolveLeapfrog(),  # hardcode debug leapfrog
            border_types,
            parent=parent,
            skip=skip,
            d_type=np.int8,  # change dtype to int
        )


class DebugPassStraightCoarseToFine(DebugBorderPass):
    """SCTF for debugging that does nothing."""

    __slots__: set[str] = set()

    def border_change_selector(self, border):
        if border is type(self):
            return None

        # currently not implemented, but is valid possibility
        # DiagCoarseToFineL, DiagFineToCoarseR
        if border in (self.BORDER_TYPES["DCTFL"], self.BORDER_TYPES["DFTCR"]):
            # return border
            raise NotImplementedError(
                f"Change from {type(self).__name__} to {border.__name__} "
                "not implemented."
            )

        raise NotImplementedError(
            f"Change from {type(self).__name__} to {border.__name__} not implemented."
        )


class DebugPassStraightFineToCoarse(DebugBorderPass):
    """SFTC for debugging that does nothing."""

    def border_change_selector(self, border):
        if border is type(self):
            return None

        # currently not implemented, but is valid possibility
        # DiagCoarseToFineR, DiagFineToCoarseL
        if border in (self.BORDER_TYPES["DCTFR"], self.BORDER_TYPES["DFTCL"]):
            # return border
            raise NotImplementedError(
                f"Change from {type(self).__name__} to {border.__name__} "
                "not implemented."
            )

        raise NotImplementedError(
            f"Change from {type(self).__name__} to {border.__name__} not implemented."
        )

    __slots__: set[str] = set()
