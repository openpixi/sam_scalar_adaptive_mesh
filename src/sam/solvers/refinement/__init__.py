"""Library code for border and cusp refinement."""

# FIXME: remove after py 3.11
from __future__ import annotations

from collections import abc
from typing import Iterable, Iterator, Type, TYPE_CHECKING, overload

if TYPE_CHECKING:
    from _typeshed import SupportsKeysAndGetItem

# after typing imports the imports from sam are in wrong order
# pylint: disable=wrong-import-position

from sam.solvers.refinement.border_base import BorderBase
from sam.solvers.refinement.borders.diag_borders import (
    DiagCoarseToFineL,
    DiagCoarseToFineR,
    DiagFineToCoarseL,
    DiagFineToCoarseR,
)
from sam.solvers.refinement.borders.straight_borders import (
    StraightCoarseToFineV1,
    StraightFineToCoarseV1,
    StraightCoarseToFineV3,
    StraightFineToCoarseV3,
    StraightCoarseToFineV5,
    StraightFineToCoarseV5,
)
from sam.solvers.refinement.borders.debug_diag_borders import (
    DebugDiagCoarseToFineL,
    DebugDiagCoarseToFineR,
    DebugDiagFineToCoarseL,
    DebugDiagFineToCoarseR,
    DebugPassDiagCoarseToFineL,
    DebugPassDiagCoarseToFineR,
    DebugPassDiagFineToCoarseL,
    DebugPassDiagFineToCoarseR,
)

from sam.solvers.refinement.borders.debug_straight_borders import (
    DebugStraightCoarseToFineV1,
    DebugStraightFineToCoarseV1,
    DebugStraightCoarseToFineV3,
    DebugStraightFineToCoarseV3,
    DebugStraightCoarseToFineV5,
    DebugStraightFineToCoarseV5,
    DebugPassStraightCoarseToFine,
    DebugPassStraightFineToCoarse,
)

# alias default straight border
StraightCoarseToFine = StraightCoarseToFineV5
StraightFineToCoarse = StraightFineToCoarseV5
DebugStraightCoarseToFine = DebugStraightCoarseToFineV5
DebugStraightFineToCoarse = DebugStraightFineToCoarseV5

from sam.solvers.refinement.cusps.cusps import (
    CuspCoarseToFineV1,
    CuspCoarseToFineV2,
    CuspFineToCoarse,
)
from sam.solvers.refinement.cusps.ssc_cusps import (
    CuspSingleCellCoarseToFine,
    CuspSingleCellFineToCoarse,
)
from sam.solvers.refinement.cusps.sdc_cusps import (
    CuspDoubleCellCoarseToFineV1V5,
    CuspDoubleCellFineToCoarseV1V5,
)
from sam.solvers.refinement.cusps.debug_cusps import (
    DebugCuspCoarseToFineV1,
    DebugCuspCoarseToFineV2,
    DebugCuspFineToCoarse,
    DebugPassCuspCoarseToFine,
    DebugPassCuspFineToCoarse,
)
from sam.solvers.refinement.cusps.debug_ssc_cusps import (
    DebugCuspSingleCellCoarseToFine,
    DebugCuspSingleCellFineToCoarse,
    DebugPassCuspSingleCellCoarseToFine,
    DebugPassCuspSingleCellFineToCoarse,
)
from sam.solvers.refinement.cusps.debug_sdc_cusps import (
    DebugCuspDoubleCellCoarseToFineV1V5,
    DebugCuspDoubleCellFineToCoarseV1V5,
    DebugPassCuspDoubleCellCoarseToFine,
    DebugPassCuspDoubleCellFineToCoarse,
)

# alias default cusps
CuspCoarseToFine = CuspCoarseToFineV1
CuspDoubleCellCoarseToFine = CuspDoubleCellCoarseToFineV1V5
CuspDoubleCellFineToCoarse = CuspDoubleCellFineToCoarseV1V5
DebugCuspCoarseToFine = DebugCuspCoarseToFineV1
DebugCuspDoubleCellCoarseToFine = DebugCuspDoubleCellCoarseToFineV1V5
DebugCuspDoubleCellFineToCoarse = DebugCuspDoubleCellFineToCoarseV1V5

# FIXME: remove after cusp implementation
from sam.solvers.refinement.cusps.cusp_legacy import (
    cusp_refinement,
    cusp_refinement_debug_pass,
)


class BorderTypes(abc.Mapping):
    """Dictionary mapping border acronyms to `BorderBase` subtypes.

    Keys are 'str' acronyms and values are 'BorderBase' subtypes.
    This is a 'frozen dict' and only knows `KEYS` for each of the
    possible border and cusp types.
    """

    KEYS = {
        "SCTF",
        "SFTC",
        "DCTFR",
        "DFTCR",
        "DCTFL",
        "DFTCL",
        "CDCCTF",
        "CDCFTC",
        "CSCCTF",
        "CSCFTC",
        "CCTF",
        "CFTC",
    }

    __slots__: set[str] = {"_dict"}

    @overload
    def __init__(self, **kwargs: Type[BorderBase]) -> None:
        ...

    @overload
    def __init__(
        self,
        mapping: SupportsKeysAndGetItem[str, Type[BorderBase]],
        **kwargs: Type[BorderBase],
    ) -> None:
        ...

    @overload
    def __init__(
        self,
        iterable: Iterable[tuple[str, Type[BorderBase]]],
        **kwargs: Type[BorderBase],
    ) -> None:
        ...

    def __init__(self, *args, **kwargs):
        self._dict = dict(*args, **kwargs)
        if missing := (self.KEYS - set(self.keys())):
            raise KeyError(f"Supplied values are missing the keys: {missing}.")
        if invalid := (set(self.keys()) - self.KEYS):
            raise KeyError(f"Supplied key `{invalid}` is invalid.")

    @classmethod
    def fromkeys(
        cls, __iterable: Iterable[str], __value: Type[BorderBase]
    ) -> BorderTypes:
        # pylint: disable=C0116
        return cls((k, __value) for k in __iterable)

    def __getitem__(self, __k: str) -> Type[BorderBase]:
        return self._dict[__k]

    def __iter__(self) -> Iterator[Type[BorderBase]]:
        return iter(self._dict)

    def __len__(self) -> int:
        return len(self._dict)

    def copy(self) -> BorderTypes:
        """Return shallow copy of self."""
        return type(self)(**self)


# TODO: separate border and cusp types into separate BorderTypes dicts
#       also, the ScalarField will take 2 BorderTypes args
#       one for cusps, one for borders. makes combining easier

BORDER_TYPES = BorderTypes(
    {
        "SCTF": StraightCoarseToFine,
        "SFTC": StraightFineToCoarse,
        "DCTFR": DiagCoarseToFineR,
        "DFTCR": DiagFineToCoarseR,
        "DCTFL": DiagCoarseToFineL,
        "DFTCL": DiagFineToCoarseL,
        "CDCCTF": CuspDoubleCellCoarseToFine,
        "CDCFTC": CuspDoubleCellFineToCoarse,
        "CSCCTF": CuspSingleCellCoarseToFine,
        "CSCFTC": CuspSingleCellFineToCoarse,
        "CCTF": CuspCoarseToFine,
        "CFTC": CuspFineToCoarse,
    }
)
# variants for straight borders
BORDER_TYPES1 = BorderTypes(
    BORDER_TYPES,
    SCTF=StraightCoarseToFineV1,
    SFTC=StraightFineToCoarseV1,
    CCTF=CuspCoarseToFineV1,
    CDCCTF=CuspDoubleCellCoarseToFineV1V5,
    CDCFTC=CuspDoubleCellFineToCoarseV1V5,
)
BORDER_TYPES3 = BorderTypes(
    BORDER_TYPES, SCTF=StraightCoarseToFineV3, SFTC=StraightFineToCoarseV3
)
BORDER_TYPES5 = BorderTypes(
    BORDER_TYPES,
    SCTF=StraightCoarseToFineV5,
    SFTC=StraightFineToCoarseV5,
    CDCCTF=CuspDoubleCellCoarseToFineV1V5,
    CDCFTC=CuspDoubleCellFineToCoarseV1V5,
)
CUSP_TYPES2 = BorderTypes(BORDER_TYPES, CCTF=CuspCoarseToFineV2)

# variants for debugging
DEBUG_BORDER_TYPES = BorderTypes(
    {
        "SCTF": DebugStraightCoarseToFine,
        "SFTC": DebugStraightFineToCoarse,
        "DCTFR": DebugDiagCoarseToFineR,
        "DFTCR": DebugDiagFineToCoarseR,
        "DCTFL": DebugDiagCoarseToFineL,
        "DFTCL": DebugDiagFineToCoarseL,
        "CDCCTF": DebugCuspDoubleCellCoarseToFine,
        "CDCFTC": DebugCuspDoubleCellFineToCoarse,
        "CSCCTF": DebugCuspSingleCellCoarseToFine,
        "CSCFTC": DebugCuspSingleCellFineToCoarse,
        "CCTF": DebugCuspCoarseToFine,
        "CFTC": DebugCuspFineToCoarse,
    }
)
DEBUG_BORDER_TYPES1 = BorderTypes(
    DEBUG_BORDER_TYPES,
    SCTF=DebugStraightCoarseToFineV1,
    SFTC=DebugStraightFineToCoarseV1,
    CCTF=DebugCuspCoarseToFineV1,
    CDCCTF=DebugCuspDoubleCellCoarseToFineV1V5,
    CDCFTC=DebugCuspDoubleCellFineToCoarseV1V5,
)
DEBUG_BORDER_TYPES3 = BorderTypes(
    DEBUG_BORDER_TYPES,
    SCTF=DebugStraightCoarseToFineV3,
    SFTC=DebugStraightFineToCoarseV3,
)
DEBUG_BORDER_TYPES5 = BorderTypes(
    DEBUG_BORDER_TYPES,
    SCTF=DebugStraightCoarseToFineV5,
    SFTC=DebugStraightFineToCoarseV5,
    CDCCTF=DebugCuspDoubleCellCoarseToFineV1V5,
    CDCFTC=DebugCuspDoubleCellFineToCoarseV1V5,
)
DEBUG_CUSP_TYPES2 = BorderTypes(DEBUG_BORDER_TYPES, CCTF=DebugCuspCoarseToFineV2)

DEBUG_PASS_BORDER_TYPES = BorderTypes(
    {
        "SCTF": DebugPassStraightCoarseToFine,
        "SFTC": DebugPassStraightFineToCoarse,
        "DCTFR": DebugPassDiagCoarseToFineR,
        "DFTCR": DebugPassDiagFineToCoarseR,
        "DCTFL": DebugPassDiagCoarseToFineL,
        "DFTCL": DebugPassDiagFineToCoarseL,
        "CDCCTF": DebugPassCuspDoubleCellCoarseToFine,
        "CDCFTC": DebugPassCuspDoubleCellFineToCoarse,
        "CSCCTF": DebugPassCuspSingleCellCoarseToFine,
        "CSCFTC": DebugPassCuspSingleCellFineToCoarse,
        "CCTF": DebugPassCuspCoarseToFine,
        "CFTC": DebugPassCuspFineToCoarse,
    }
)

__all__ = ["BORDER_TYPES", "DEBUG_BORDER_TYPES", "DEBUG_PASS_BORDER_TYPES"]
