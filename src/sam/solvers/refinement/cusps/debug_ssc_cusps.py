"""Straight double cell cusps at timelike separation for debugging."""

from itertools import cycle

import numpy as np

from sam.solvers import leapfrog
from sam.solvers.refinement import debug
from sam.solvers.refinement.cusps.ssc_cusps import (
    CuspSingleCellCoarseToFine,
    CuspSingleCellFineToCoarse,
)
from sam.solvers.refinement.debug import DebugBorderPass


class DebugCuspSingleCellCoarseToFine(CuspSingleCellCoarseToFine):
    """Debug version."""

    __slots__: set[str] = set()

    def __init__(
        self,
        border_reg,
        sort_idx,
        arr_idx,
        pos,
        neigh_reg,
        neighbor_arr,
        neighbor_sort_idx,
        a2,
        solver,
        border_types,
        *,
        parent=None,
        skip=False,
    ):
        """Return new DebugCuspSingleCellCoarseToFine, timelike cusp."""
        super().__init__(
            border_reg,
            sort_idx,
            arr_idx,
            pos,
            neigh_reg,
            neighbor_arr,
            neighbor_sort_idx,
            a2,
            leapfrog.DebugEvolveLeapfrog(),  # hardcode debug leapfrog
            border_types,
            parent=parent,
            skip=skip,
            d_type=np.int8,
        )
        self.evolve_cycle = cycle((self.evolve_t1, self.evolve_t2))

    def evolve_t1(
        self,
        reg_pre,
        n_reg_pre,
        reg_now,
        n_reg_now,
        reg_nxt,
        n_reg_nxt,
        d_pre,
        n_d_pre,
        d_now,
        n_d_now,
        d_nxt,
        n_d_nxt,
    ):
        n_d_nxt[0:3] = debug.MARKER_BOUNDARY

    def evolve_t2(
        self,
        reg_pre,
        n_reg_pre,
        reg_now,
        n_reg_now,
        reg_nxt,
        n_reg_nxt,
        d_pre,
        n_d_pre,
        d_now,
        n_d_now,
        d_nxt,
        n_d_nxt,
    ):
        n_d_nxt[0:2] = debug.MARKER_BOUNDARY


class DebugCuspSingleCellFineToCoarse(CuspSingleCellFineToCoarse):
    """Debug version."""

    __slots__: set[str] = set()

    def __init__(
        self,
        border_reg,
        sort_idx,
        arr_idx,
        pos,
        neigh_reg,
        neighbor_arr,
        neighbor_sort_idx,
        a2,
        solver,
        border_types,
        *,
        parent=None,
        skip=False,
    ):
        """Return new DebugCuspSingleCellFineToCoarse, timelike cusp."""
        super().__init__(
            border_reg,
            sort_idx,
            arr_idx,
            pos,
            neigh_reg,
            neighbor_arr,
            neighbor_sort_idx,
            a2,
            leapfrog.DebugEvolveLeapfrog(),  # hardcode debug leapfrog
            border_types,
            parent=parent,
            skip=skip,
            d_type=np.int8,
        )
        self.evolve_cycle = cycle((self.evolve_t1, self.evolve_t2))

    def evolve_t1(
        self,
        reg_pre,
        n_reg_pre,
        reg_now,
        n_reg_now,
        reg_nxt,
        n_reg_nxt,
        d_pre,
        n_d_pre,
        d_now,
        n_d_now,
        d_nxt,
        n_d_nxt,
    ):
        n_d_nxt[n_reg_nxt.n - 3 : n_reg_nxt.n] = debug.MARKER_BOUNDARY

    def evolve_t2(
        self,
        reg_pre,
        n_reg_pre,
        reg_now,
        n_reg_now,
        reg_nxt,
        n_reg_nxt,
        d_pre,
        n_d_pre,
        d_now,
        n_d_now,
        d_nxt,
        n_d_nxt,
    ):
        n_d_nxt[n_reg_nxt.n - 2 : n_reg_nxt.n] = debug.MARKER_BOUNDARY


class DebugPassCuspSingleCellCoarseToFine(DebugBorderPass, CuspSingleCellCoarseToFine):
    """CSCCTF for debugging that does nothing."""

    __slots__: set[str] = set()


class DebugPassCuspSingleCellFineToCoarse(DebugBorderPass, CuspSingleCellFineToCoarse):
    """CSCFTC for debugging that does nothing."""

    __slots__: set[str] = set()
