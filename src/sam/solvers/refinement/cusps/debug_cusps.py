"""Double cell cusps at spacelike separation for debugging."""

from unittest.mock import patch

import numpy as np

from sam.solvers import leapfrog
from sam.solvers.refinement.cusps.cusps import (
    CuspCoarseToFineV1,
    CuspCoarseToFineV2,
    CuspFineToCoarse,
)
from sam.solvers.refinement.debug import (
    DebugBorderPass,
    SCHEME_EQUIVALENT_FUNC as SCHEME_DIAG_EQUIVALENT_FUNC,
    SCHEME_STRAIGHT_EQUIVALENT_FUNC,
    SCHEME_CUSPS_EQUIVALENT_FUNC,
)

# scheme eqs
CUSP_SCHEME_EQS = {"average", "copy_to"}
SCHEME_EQUIVALENT_FUNC = {
    k: v for k, v in SCHEME_DIAG_EQUIVALENT_FUNC.items() if k in CUSP_SCHEME_EQS
}
SCHEME_EQUIVALENT_FUNC.update(
    {k: v for k, v in SCHEME_STRAIGHT_EQUIVALENT_FUNC.items() if k in CUSP_SCHEME_EQS}
)
SCHEME_EQUIVALENT_FUNC.update(SCHEME_CUSPS_EQUIVALENT_FUNC)


@patch.multiple(
    "sam.solvers.refinement.cusps.cusps",
    **SCHEME_EQUIVALENT_FUNC,
)
class DebugCuspCoarseToFineV1(CuspCoarseToFineV1):
    """CCTF debug version."""

    __slots__: set[str] = set()

    def __init__(
        self,
        border_n_reg,
        sort_idx,
        arr_idx,
        pos,
        neigh_reg,
        neighbor_arr_offset,
        neighbor_sort_idx,
        a2,
        _solver,
        border_types,
        *,
        parent,
        skip=False,
    ):
        """Return new DebugCuspCoarseToFine (spacelike)."""
        super().__init__(
            border_n_reg,
            sort_idx,
            arr_idx,
            pos,
            neigh_reg,
            neighbor_arr_offset,
            neighbor_sort_idx,
            a2,
            leapfrog.DebugEvolveLeapfrog(),  # hardcode debug leapfrog
            border_types,
            parent=parent,
            skip=skip,
            d_type=np.int8,  # change dtype to int
        )


@patch.multiple(
    "sam.solvers.refinement.cusps.cusps",
    **SCHEME_EQUIVALENT_FUNC,
)
class DebugCuspCoarseToFineV2(CuspCoarseToFineV2):
    """CCTF debug version with dedicated eqs."""

    __slots__: set[str] = set()

    def __init__(
        self,
        border_n_reg,
        sort_idx,
        arr_idx,
        pos,
        neigh_reg,
        neighbor_arr_offset,
        neighbor_sort_idx,
        a2,
        _solver,
        border_types,
        *,
        parent,
        skip=False,
    ):
        """Return new DebugCuspCoarseToFine (spacelike)."""
        super().__init__(
            border_n_reg,
            sort_idx,
            arr_idx,
            pos,
            neigh_reg,
            neighbor_arr_offset,
            neighbor_sort_idx,
            a2,
            leapfrog.DebugEvolveLeapfrog(),  # hardcode debug leapfrog
            border_types,
            parent=parent,
            skip=skip,
            d_type=np.int8,  # change dtype to int
        )


@patch.multiple(
    "sam.solvers.refinement.cusps.cusps",
    **SCHEME_EQUIVALENT_FUNC,
)
class DebugCuspFineToCoarse(CuspFineToCoarse):
    """CFTC debug version."""

    __slots__: set[str] = set()

    def __init__(
        self,
        border_reg,
        sort_idx,
        arr_idx,
        pos,
        neigh_reg,
        neighbor_arr_offset,
        neighbor_sort_idx,
        a2,
        solver,
        border_types,
        *,
        parent,
        skip=False,
    ):
        """Return new DebugCuspFineToCoarse (spacelike)."""
        super().__init__(
            border_reg,
            sort_idx,
            arr_idx,
            pos,
            neigh_reg,
            neighbor_arr_offset,
            neighbor_sort_idx,
            a2,
            leapfrog.DebugEvolveLeapfrog(),  # hardcode debug leapfrog
            border_types,
            parent=parent,
            skip=skip,
            d_type=np.int8,  # change dtype to int
        )


class DebugPassCuspCoarseToFine(DebugBorderPass, CuspCoarseToFineV1):
    """CCTF for debugging that does nothing."""

    # inherits from CuspCoarseToFineV1 to get all interfaces,
    # ultimately, that doesn't matter

    __slots__: set[str] = set()


class DebugPassCuspFineToCoarse(DebugBorderPass, CuspFineToCoarse):
    """CFTC for debugging that does nothing."""

    __slots__: set[str] = set()
