"""Double cell cusps at spacelike separation."""

# FIXME: remove after py 3.11
from __future__ import annotations

import typing

import numpy as np

from mrlattice.lattice.generate import RegionTuple

from sam.containers import NONDARR
from sam.solvers import Solver
from sam.solvers.refinement.border_base import BorderBase, TempData
from sam.solvers.refinement.schemes import (
    average,
    copy_to,
    cctf_v2_b,
    cctf_v2_c,
    kappa_cusp,
    lambda_cusp,
)


# prevent circular import
if typing.TYPE_CHECKING:
    from sam.solvers.refinement import BorderTypes


class CuspCoarseToFineV1(BorderBase):
    """Disappearing double cell spacelike cusp with diagonal borders."""

    # reuses diagonal border eqs.

    # takes over 2 diagonal borders and saves them as parents.
    # triggers 2 times for the border evolve step, once for each parent.
    # therefore needs to match evolve conditions for both parents.
    # for this, in evolve_tXxxx self morphs between the parents,
    # by changing neighbor data accordingly.
    # a new attribute is required: border_n_reg
    # to save the old RegionTuples of both borders.
    # the old data is required, if border res not in iter_res,
    # else the self.border_update will change border_n_reg to the
    # RegionTuples of the parents, the updated ones.

    __slots__: set[str] = {"border_n_reg"}

    parent: tuple[BorderBase, BorderBase]

    def __init__(
        self,
        border_n_reg: tuple[RegionTuple, RegionTuple],
        sort_idx,
        arr_idx,
        pos,
        neigh_reg: tuple[RegionTuple, RegionTuple],
        neighbor_arr_offset: tuple[int, int],
        neighbor_sort_idx: tuple[int, int],
        a2,
        solver: Solver,
        border_types: BorderTypes,
        *,
        parent: tuple[BorderBase, BorderBase],
        skip=False,
        d_type=np.float_,
    ):
        """Return new CuspCoarseToFine (spacelike)."""
        # init self with old data from one parent
        # cusp border change will update, if necessary
        super().__init__(
            parent[0].reg,
            sort_idx,
            arr_idx,
            parent[0].p,
            border_n_reg[0],  # use reg now
            parent[0].neigh_arr_offset,
            parent[0].neigh_by_res_idx,
            a2,
            solver,
            border_types,
            parent=parent,
            skip=skip,
        )
        self._init_evolve = {}
        self.evolve_cycle = iter(
            (self.evolve_t1_b1, self.evolve_t1_b2, self.evolve_t2_b1, self.evolve_t2_b2)
        )
        self.d_ghost = NONDARR(d_type)
        self.cell_g = {}
        self._cell_g_init = {}
        self.g_max_dist = {}
        self._cell_names = {}
        self.border_p = pos
        self.border_n_reg = border_n_reg

        # update parents with border_change and new region
        # parents won't trigger during self.border_change()
        self._parent_border_change(RegionTuple(self.reg.a, 0), self.border_p, neigh_reg)

    def init_ghost_cells(self, *_):
        """This type has no ghost cells to init."""
        raise NotImplementedError

    def border_change_selector(self, *_):
        raise NotImplementedError

    def plot(self, *_):
        return self.parent[0].plot() + self.parent[1].plot()

    def _parent_border_change(
        self, reg_nxt: RegionTuple, p_nxt, neigh: tuple[RegionTuple, RegionTuple]
    ):

        for b, n in zip(self.parent, neigh):
            change = b.border_change(reg_nxt, p_nxt, n)
            assert not change

    def border_change(self, _reg_nxt: RegionTuple, p_nxt, neigh: RegionTuple):
        self.reg = self.parent[0].reg  # both parent have same reg
        self.p = p_nxt
        self.neighbor = neigh
        self.border_n_reg = (self.parent[0].neighbor, self.parent[1].neighbor)

    def evolve_t1_b1(
        self,
        reg_pre,
        n_reg_pre,
        reg_now,
        n_reg_now,
        reg_nxt,
        n_reg_nxt,
        d_pre,
        n_d_pre,
        d_now,
        n_d_now,
        d_nxt,
        n_d_nxt,
    ):
        """Refinement steps +1:1, +1:2."""

        self.parent[0].evolve(
            reg_pre,
            n_reg_pre,
            reg_now,
            n_reg_now,
            reg_nxt,
            n_reg_nxt,
            d_pre,
            n_d_pre,
            d_now,
            n_d_now,
            d_nxt,
            n_d_nxt,
        )
        # prepare for other border
        self.neighbor = self.border_n_reg[1]
        self.neigh_by_res_idx = self.parent[1].neigh_by_res_idx

        return -1

    def evolve_t1_b2(
        self,
        reg_pre,
        n_reg_pre,
        reg_now,
        n_reg_now,
        reg_nxt,
        n_reg_nxt,
        d_pre,
        n_d_pre,
        d_now,
        n_d_now,
        d_nxt,
        n_d_nxt,
    ):
        """Refinement steps +1:1, +1:2."""

        self.parent[1].evolve(
            reg_pre,
            n_reg_pre,
            reg_now,
            n_reg_now,
            reg_nxt,
            n_reg_nxt,
            d_pre,
            n_d_pre,
            d_now,
            n_d_now,
            d_nxt,
            n_d_nxt,
        )
        # prepare for other border
        self.neighbor = self.parent[0].neighbor
        self.neigh_by_res_idx = self.parent[0].neigh_by_res_idx

        return 1

    def evolve_t2_b1(
        self,
        reg_pre,
        n_reg_pre,
        reg_now,
        n_reg_now,
        reg_nxt,
        n_reg_nxt,
        d_pre,
        n_d_pre,
        d_now,
        n_d_now,
        d_nxt,
        n_d_nxt,
    ):
        """Refinement steps +2:1, +2:2."""

        # use other parents ghost array for L (A in diag scheme)
        reg_now = RegionTuple(self.parent[0].reg.a, 1)
        d_now = TempData((self.parent[1].d_ghost, self.parent[1].cell_g["P"]))  # L

        self.parent[0].evolve(
            reg_pre,
            n_reg_pre,
            reg_now,
            n_reg_now,
            reg_nxt,
            n_reg_nxt,
            d_pre,
            n_d_pre,
            d_now,
            n_d_now,
            d_nxt,
            n_d_nxt,
        )
        # prepare for other border
        self.neighbor = self.parent[1].neighbor
        self.neigh_by_res_idx = self.parent[1].neigh_by_res_idx

        return -2

    def evolve_t2_b2(
        self,
        reg_pre,
        n_reg_pre,
        reg_now,
        n_reg_now,
        reg_nxt,
        n_reg_nxt,
        d_pre,
        n_d_pre,
        d_now,
        n_d_now,
        d_nxt,
        n_d_nxt,
    ):
        """Refinement steps +2:1, +2:2."""

        # use other parents ghost array for L (A in diag scheme)
        # this is after b1 so ghost cells of b1 are swapped
        # therefore: A == L -> O
        reg_now = RegionTuple(self.parent[1].reg.a, 1)
        d_now = TempData((self.parent[0].d_ghost, self.parent[0].cell_g["O"]))  # L

        self.parent[1].evolve(
            reg_pre,
            n_reg_pre,
            reg_now,
            n_reg_now,
            reg_nxt,
            n_reg_nxt,
            d_pre,
            n_d_pre,
            d_now,
            n_d_now,
            d_nxt,
            n_d_nxt,
        )

        return 2


class CuspCoarseToFineV2(BorderBase):
    """Disappearing double cell spacelike cusp with diagonal borders."""

    # uses dedicated refinement eqs.

    __slots__: set[str] = {"border_n_reg"}

    parent: tuple[BorderBase, BorderBase]

    def __init__(
        self,
        border_n_reg: tuple[RegionTuple, RegionTuple],
        sort_idx,
        arr_idx,
        pos,
        neigh_reg: tuple[RegionTuple, RegionTuple],
        neighbor_arr_offset: tuple[int, int],
        neighbor_sort_idx: tuple[int, int],
        a2,
        solver: Solver,
        border_types: BorderTypes,
        *,
        parent: tuple[BorderBase, BorderBase],
        skip=False,
        d_type=np.float_,
    ):
        """Return new CuspCoarseToFine (spacelike)."""
        # init self with old data from one parent
        # cusp border change will update, if necessary
        super().__init__(
            parent[0].reg,
            sort_idx,
            arr_idx,
            parent[0].p,
            border_n_reg[0],  # use reg now
            parent[0].neigh_arr_offset,
            parent[0].neigh_by_res_idx,
            a2,
            solver,
            border_types,
            parent=parent,
            skip=skip,
        )
        assert isinstance(parent[0], self.BORDER_TYPES["DCTFR"])
        assert isinstance(parent[1], self.BORDER_TYPES["DCTFL"])
        self._init_evolve = {}
        self.evolve_cycle = iter(
            (self.evolve_t1_b1, self.evolve_t1_b2, self.evolve_t2_b1, self.evolve_t2_b2)
        )
        self.d_ghost = NONDARR(d_type)
        self.cell_g = {}
        self._cell_g_init = {}
        self.g_max_dist = {}
        self._cell_names = {
            "A": 0,
            "B": 1,
            "C": 0,
            "D": 1,
            "E": 2,
            "F": 3,
            "a": -2,
            "b": -1,
            "c": -3,
            "d": -2,
            "e": -1,
            "f": -2,
            "g": -1,
            "h": -3,
            "i": -2,
            "j": -1,
            "aa": 1,
            "bb": 0,
            "cc": 2,
            "dd": 1,
            "ee": 0,
            "ff": 1,
            "gg": 0,
            "hh": 2,
            "ii": 1,
            "jj": 0,
        }
        self.border_p = pos
        self.border_n_reg = border_n_reg

        # order for evolve ghost cell recycling
        # t1 b1: L[b1.P], e, e->X[b2.K], d, c
        # t1 b2: L'[b2.P], e', e'->Y[b1.K], d', c'
        # t2 b1: a, b
        # t2 b2: a', b'

        # update parents with border_change and new region
        # parents won't trigger during self.border_change()
        self._parent_border_change(RegionTuple(self.reg.a, 0), self.border_p, neigh_reg)

    def _parent_border_change(
        self, reg_nxt: RegionTuple, p_nxt, neigh: tuple[RegionTuple, RegionTuple]
    ):

        for b, n in zip(self.parent, neigh):
            change = b.border_change(reg_nxt, p_nxt, n)
            assert not change

    def border_change(self, _reg_nxt: RegionTuple, p_nxt, neigh: RegionTuple):
        self.reg = self.parent[0].reg  # both parent have same reg
        self.p = p_nxt
        self.neighbor = neigh
        self.border_n_reg = (self.parent[0].neighbor, self.parent[1].neighbor)

    def init_ghost_cells(self, *_):
        """This type has no ghost cells to init."""
        raise NotImplementedError

    def border_change_selector(self, *_):
        raise NotImplementedError

    def plot(self, p=None, P=None, res=None, nres=None):
        out = []
        border_l = self.parent[0]
        border_r = self.parent[1]
        # pos is for left border
        p = int(border_l.p * border_l.reg.a) if p is None else p
        P = int(border_l.p * border_l.reg.a) if P is None else P
        res = border_l.reg.a if res is None else res
        nres = border_l.neighbor.a if nres is None else nres

        # L[P] from left, L'[p] from right
        out.append((0, P - res, self.reg, border_l.d_ghost[border_l.cell_g["P"]]))
        out.append((0, P, self.reg, border_r.d_ghost[border_r.cell_g["P"]]))
        # O from left, O' from right
        out.append((-1, P - res * 2, self.reg, border_l.d_ghost[border_l.cell_g["O"]]))
        out.append((-1, P + res, self.reg, border_r.d_ghost[border_r.cell_g["O"]]))

        if self.evolve_step == 2:
            out.append((-1, p, self.neighbor, border_l.d_ghost[border_l.cell_g["K"]]))
            out.append(
                (-1, p - nres, self.neighbor, border_r.d_ghost[border_r.cell_g["K"]])
            )

        return out

    def evolve_t1_b1(
        self,
        reg_pre,
        n_reg_pre,
        reg_now,
        n_reg_now,
        _reg_nxt,
        n_reg_nxt,
        d_pre,
        n_d_pre,
        d_now,
        n_d_now,
        _d_nxt,
        n_d_nxt,
    ):
        """Refinement steps +1:1, +1:2 for left border."""
        border = self.parent[0]
        # write L on P using border
        self.solver.evolve_stencil(
            *self.stencil_args,
            TempData((d_pre, border.cell_d(reg_pre, "F"))),
            TempData(
                (d_now, border.cell_d(reg_now, "B")),
                (d_now, border.cell_d(reg_now, "C")),
                (border.d_ghost, border.cell_g["O"]),
            ),
            TempData((border.d_ghost, border.cell_g["P"])),  # L
        )
        # e
        kappa_cusp(
            n_d_nxt,
            self.cell_d(n_reg_nxt, "e"),
            n_d_now,
            self.cell_d(n_reg_now, "f"),
            n_d_pre,
            self.cell_d(n_reg_pre, "h"),
            n_d_pre,
            self.cell_d(n_reg_pre, "j"),
            d_pre,
            self.cell_d(reg_pre, "C"),
            d_now,
            self.cell_d(reg_now, "B"),
            d_pre,
            self.cell_d(reg_pre, "D"),
            border.d_ghost,
            border.cell_g["P"],  # L
            border.d_ghost,
            border.cell_g["O"],
        )
        # save e to right border ghost cell
        copy_to(
            n_d_nxt,
            self.cell_d(n_reg_nxt, "e"),
            self.parent[1].d_ghost,
            self.parent[1].cell_g["K"],
        )
        # d
        lambda_cusp(
            n_d_nxt,
            self.cell_d(n_reg_nxt, "d"),
            n_d_now,
            self.cell_d(n_reg_now, "g"),
            d_now,
            self.cell_d(reg_now, "B"),
            d_pre,
            self.cell_d(reg_pre, "D"),
            border.d_ghost,
            border.cell_g["P"],  # L
            border.d_ghost,
            border.cell_g["O"],
        )
        # c
        cctf_v2_c(
            n_d_nxt,
            self.cell_d(n_reg_nxt, "c"),
            n_d_pre,
            self.cell_d(n_reg_pre, "h"),
            n_d_now,
            self.cell_d(n_reg_now, "g"),
            n_d_pre,
            self.cell_d(n_reg_pre, "i"),
        )

        # prepare for other border
        self.neighbor = self.border_n_reg[1]
        self.neigh_by_res_idx = self.parent[1].neigh_by_res_idx

        return -1

    def evolve_t1_b2(
        self,
        reg_pre,
        n_reg_pre,
        reg_now,
        n_reg_now,
        _reg_nxt,
        n_reg_nxt,
        d_pre,
        n_d_pre,
        d_now,
        n_d_now,
        _d_nxt,
        n_d_nxt,
    ):
        """Refinement steps +1:1, +1:2 for right border."""
        border = self.parent[1]
        # write L' on P using border
        self.solver.evolve_stencil(
            *self.stencil_args,
            TempData((d_pre, border.cell_d(reg_pre, "F"))),
            TempData(
                (d_now, border.cell_d(reg_now, "B")),
                (d_now, border.cell_d(reg_now, "C")),
                (border.d_ghost, border.cell_g["O"]),
            ),
            TempData((border.d_ghost, border.cell_g["P"])),  # L
        )
        # e'
        kappa_cusp(
            n_d_nxt,
            self.cell_d(n_reg_nxt, "ee"),
            n_d_now,
            self.cell_d(n_reg_now, "ff"),
            n_d_pre,
            self.cell_d(n_reg_pre, "hh"),
            n_d_pre,
            self.cell_d(n_reg_pre, "jj"),
            d_pre,
            self.cell_d(reg_pre, "F"),
            d_now,
            self.cell_d(reg_now, "A"),
            d_pre,
            self.cell_d(reg_pre, "E"),
            border.d_ghost,
            border.cell_g["P"],  # L'
            border.d_ghost,
            border.cell_g["O"],
        )
        # save e to left border ghost cell
        copy_to(
            n_d_nxt,
            self.cell_d(n_reg_nxt, "ee"),
            self.parent[0].d_ghost,
            self.parent[0].cell_g["K"],
        )
        # d'
        lambda_cusp(
            n_d_nxt,
            self.cell_d(n_reg_nxt, "dd"),
            n_d_now,
            self.cell_d(n_reg_now, "gg"),
            d_now,
            self.cell_d(reg_now, "A"),
            d_pre,
            self.cell_d(reg_pre, "E"),
            border.d_ghost,
            border.cell_g["P"],  # L'
            border.d_ghost,
            border.cell_g["O"],
        )
        # c'
        cctf_v2_c(
            n_d_nxt,
            self.cell_d(n_reg_nxt, "cc"),
            n_d_pre,
            self.cell_d(n_reg_pre, "hh"),
            n_d_now,
            self.cell_d(n_reg_now, "gg"),
            n_d_pre,
            self.cell_d(n_reg_pre, "ii"),
        )

        # prepare for other border
        self.neighbor = self.parent[0].neighbor
        self.neigh_by_res_idx = self.parent[0].neigh_by_res_idx

        return 1

    def evolve_t2_b1(
        self,
        reg_pre,
        n_reg_pre,
        _reg_now,
        n_reg_now,
        _reg_nxt,
        n_reg_nxt,
        d_pre,
        n_d_pre,
        _d_now,
        n_d_now,
        _d_nxt,
        n_d_nxt,
    ):
        """Refinement step +2:1 for left border."""
        # b
        cctf_v2_b(
            n_d_nxt,
            self.cell_d(n_reg_nxt, "b"),
            self.parent[0].d_ghost,
            self.parent[0].cell_g["K"],  # ee
            n_d_pre,
            self.cell_d(n_reg_pre, "g"),
            n_d_now,
            self.cell_d(n_reg_now, "c"),
            n_d_pre,
            self.cell_d(n_reg_pre, "f"),
            d_pre,
            self.cell_d(reg_pre, "A"),
        )
        # a
        cctf_v2_c(
            n_d_nxt,
            self.cell_d(n_reg_nxt, "a"),
            n_d_pre,
            self.cell_d(n_reg_pre, "f"),
            n_d_now,
            self.cell_d(n_reg_now, "d"),
            n_d_pre,
            self.cell_d(n_reg_pre, "g"),
        )

        # prepare for other border
        self.neighbor = self.parent[1].neighbor
        self.neigh_by_res_idx = self.parent[1].neigh_by_res_idx

        return -2

    def evolve_t2_b2(
        self,
        reg_pre,
        n_reg_pre,
        _reg_now,
        n_reg_now,
        _reg_nxt,
        n_reg_nxt,
        d_pre,
        n_d_pre,
        _d_now,
        n_d_now,
        _d_nxt,
        n_d_nxt,
    ):
        """Refinement step +2:1 for right border."""
        # b'
        cctf_v2_b(
            n_d_nxt,
            self.cell_d(n_reg_nxt, "bb"),
            self.parent[1].d_ghost,
            self.parent[1].cell_g["K"],  # ee
            n_d_pre,
            self.cell_d(n_reg_pre, "gg"),
            n_d_now,
            self.cell_d(n_reg_now, "cc"),
            n_d_pre,
            self.cell_d(n_reg_pre, "ff"),
            d_pre,
            self.cell_d(reg_pre, "B"),
        )
        # a'
        cctf_v2_c(
            n_d_nxt,
            self.cell_d(n_reg_nxt, "aa"),
            n_d_pre,
            self.cell_d(n_reg_pre, "ff"),
            n_d_now,
            self.cell_d(n_reg_now, "dd"),
            n_d_pre,
            self.cell_d(n_reg_pre, "gg"),
        )

        return 2


class CuspFineToCoarse(BorderBase):
    """Emerging double cell spacelike cusp."""

    # exists in addition to the two borders at the cusp
    # those two borders are stored as parents
    # calculates cusp refinement and activates the cusp borders

    __slots__: set[str] = set()

    parent: tuple[BorderBase, BorderBase]

    def __init__(
        self,
        border_reg: RegionTuple,
        sort_idx,
        arr_idx,
        pos,
        neigh_reg: RegionTuple,
        neighbor_arr_offset,
        neighbor_sort_idx,
        a2,
        solver: Solver,
        border_types: BorderTypes,
        *,
        parent: tuple[BorderBase, BorderBase],
        skip=False,
        d_type=np.float_,
        num_ghost=6,
    ):
        """Return new CuspFineToCoarse (spacelike)."""
        # init works like borders, but register with left neigh region
        super().__init__(
            border_reg,
            sort_idx,
            arr_idx,
            pos,
            neigh_reg,
            neighbor_arr_offset,
            neighbor_sort_idx,
            a2,
            solver,
            border_types,
            parent=parent,
            skip=skip,
        )
        assert isinstance(parent[0], self.BORDER_TYPES["DFTCL"])
        assert isinstance(parent[1], self.BORDER_TYPES["DFTCR"])
        self._init_evolve = {}
        self.evolve_cycle = iter(
            (self.evolve_t1_b1, self.evolve_t1_b2, self.evolve_disarm)
        )
        # 6 ghost cells for fine cells, rest from parents
        self.d_ghost = np.zeros(num_ghost, dtype=d_type)
        self.cell_g = {
            "ww": 0,  # w', c'
            "xx": 1,  # x', v'
            "yy": 2,  # y', g'
            "w": 3,
            "x": 4,  # v
            "y": 5,  # d'
        }
        # z' is x of left border
        # z is x of right border
        # order for evolve ghost cell recycling is:
        # b1: v'->x', z', d', c', g'
        # b2: y'->g', x', y->d', w'->c', z, v->x, w, x
        self._cell_g_init = {}
        self.g_max_dist = {}
        self._cell_names = {
            "A": 0,
            "B": 1,
            "ff": -4,
            "ee": -3,
            "dd": -2,
            "cc": -1,
            "jj": -4,
            "ii": -3,
            "hh": -2,
            "gg": -1,
            "c": 0,
            "d": 1,
            "e": 2,
            "f": 3,
            "g": 0,
            "h": 1,
            "i": 2,
            "j": 3,
        }

    def init_ghost_cells(self, *_):
        """This type has no ghost cells to init."""
        raise NotImplementedError

    def border_change_selector(self, *_):
        raise NotImplementedError

    def evolve_t1_b1(
        self,
        _reg_pre,
        n_reg_pre,
        _reg_now,
        n_reg_now,
        _reg_nxt,
        _n_reg_nxt,
        _d_pre,
        n_d_pre,
        _d_now,
        n_d_now,
        _d_nxt,
        _n_d_nxt,
    ):
        """Refinement steps +1:1, +1:2, +1:3 for left side."""
        # the left border
        border = self.parent[0]
        # C is F in left parent border
        average(
            border.d_ghost,
            border.cell_g["F"],
            n_d_pre,
            self.cell_d(n_reg_pre, "jj"),
            n_d_pre,
            self.cell_d(n_reg_pre, "ii"),
            n_d_now,
            self.cell_d(n_reg_now, "ff"),
            n_d_now,
            self.cell_d(n_reg_now, "ee"),
        )
        # D is E in left parent border
        average(
            border.d_ghost,
            border.cell_g["E"],
            n_d_pre,
            self.cell_d(n_reg_pre, "hh"),
            n_d_pre,
            self.cell_d(n_reg_pre, "gg"),
            n_d_now,
            self.cell_d(n_reg_now, "dd"),
            n_d_now,
            self.cell_d(n_reg_now, "cc"),
        )
        # v' -> x'
        self.solver.evolve_stencil(
            *self.stencil_args,
            TempData((n_d_pre, self.cell_d(n_reg_pre, "ii"))),
            TempData(
                (n_d_now, self.cell_d(n_reg_now, "ee")),
                (n_d_now, self.cell_d(n_reg_now, "dd")),
                (n_d_now, self.cell_d(n_reg_now, "ff")),
            ),
            TempData((self.d_ghost, self.cell_g["xx"])),
        )
        # z' is x of left border
        self.solver.evolve_stencil(
            *self.stencil_args,
            TempData((n_d_pre, self.cell_d(n_reg_pre, "hh"))),
            TempData(
                (n_d_now, self.cell_d(n_reg_now, "dd")),
                (n_d_now, self.cell_d(n_reg_now, "cc")),
                (n_d_now, self.cell_d(n_reg_now, "ee")),
            ),
            TempData((border.d_ghost, border.cell_g["x"])),
        )
        # store d', c', g' for next evolve step
        copy_to(n_d_pre, self.cell_d(n_reg_pre, "gg"), self.d_ghost, self.cell_g["yy"])
        copy_to(n_d_now, self.cell_d(n_reg_now, "dd"), self.d_ghost, self.cell_g["y"])
        copy_to(n_d_now, self.cell_d(n_reg_now, "cc"), self.d_ghost, self.cell_g["ww"])

        # set self neigh to other border neigh
        self.neighbor = self.parent[1].neighbor
        self.neigh_by_res_idx = self.parent[1].neigh_by_res_idx
        return -1

    def evolve_t1_b2(
        self,
        _reg_pre,
        n_reg_pre,
        _reg_now,
        n_reg_now,
        reg_nxt,
        _n_reg_nxt,
        _d_pre,
        n_d_pre,
        _d_now,
        n_d_now,
        d_nxt,
        _n_d_nxt,
    ):
        """Refinement steps +1:1, +1:2, +1:3, +1:4 for both sides."""
        # the right border
        border = self.parent[1]
        # F is F in left parent border
        average(
            border.d_ghost,
            border.cell_g["F"],
            n_d_pre,
            self.cell_d(n_reg_pre, "j"),
            n_d_pre,
            self.cell_d(n_reg_pre, "i"),
            n_d_now,
            self.cell_d(n_reg_now, "f"),
            n_d_now,
            self.cell_d(n_reg_now, "e"),
        )
        # E is E in left parent border
        average(
            border.d_ghost,
            border.cell_g["E"],
            n_d_pre,
            self.cell_d(n_reg_pre, "h"),
            n_d_pre,
            self.cell_d(n_reg_pre, "g"),
            n_d_now,
            self.cell_d(n_reg_now, "d"),
            n_d_now,
            self.cell_d(n_reg_now, "c"),
        )
        # y'
        self.solver.evolve_stencil(
            *self.stencil_args,
            TempData((self.d_ghost, self.cell_g["yy"])),
            TempData(
                (self.d_ghost, self.cell_g["ww"]),
                (n_d_now, self.cell_d(n_reg_now, "c")),
                (self.d_ghost, self.cell_g["y"]),
            ),
            TempData((self.d_ghost, self.cell_g["yy"])),
        )
        # x'
        self.solver.evolve_stencil(
            *self.stencil_args,
            TempData((self.d_ghost, self.cell_g["y"])),
            TempData(
                (self.parent[0].d_ghost, self.parent[0].cell_g["x"]),  # left border. z'
                (self.d_ghost, self.cell_g["yy"]),
                (self.d_ghost, self.cell_g["xx"]),
            ),
            TempData((self.d_ghost, self.cell_g["xx"])),
        )
        # y
        self.solver.evolve_stencil(
            *self.stencil_args,
            TempData((n_d_pre, self.cell_d(n_reg_pre, "g"))),
            TempData(
                (n_d_now, self.cell_d(n_reg_now, "c")),
                (n_d_now, self.cell_d(n_reg_now, "d")),
                (self.d_ghost, self.cell_g["ww"]),
            ),
            TempData((self.d_ghost, self.cell_g["y"])),
        )
        # w'
        self.solver.evolve_stencil(
            *self.stencil_args,
            TempData((self.d_ghost, self.cell_g["ww"])),
            TempData(
                (self.d_ghost, self.cell_g["yy"]),
                (self.d_ghost, self.cell_g["y"]),
                (self.parent[0].d_ghost, self.parent[0].cell_g["x"]),  # left border, z'
            ),
            TempData((self.d_ghost, self.cell_g["ww"])),
        )
        # z
        self.solver.evolve_stencil(
            *self.stencil_args,
            TempData((n_d_pre, self.cell_d(n_reg_pre, "h"))),
            TempData(
                (n_d_now, self.cell_d(n_reg_now, "d")),
                (n_d_now, self.cell_d(n_reg_now, "e")),
                (n_d_now, self.cell_d(n_reg_now, "c")),
            ),
            TempData((border.d_ghost, border.cell_g["x"])),  # right border, z
        )
        # v -> x
        self.solver.evolve_stencil(
            *self.stencil_args,
            TempData((n_d_pre, self.cell_d(n_reg_pre, "i"))),
            TempData(
                (n_d_now, self.cell_d(n_reg_now, "e")),
                (n_d_now, self.cell_d(n_reg_now, "f")),
                (n_d_now, self.cell_d(n_reg_now, "d")),
            ),
            TempData((self.d_ghost, self.cell_g["x"])),
        )
        # w
        self.solver.evolve_stencil(
            *self.stencil_args,
            TempData((n_d_now, self.cell_d(n_reg_now, "c"))),
            TempData(
                (self.d_ghost, self.cell_g["y"]),
                (border.d_ghost, border.cell_g["x"]),  # right border, z
                (self.d_ghost, self.cell_g["yy"]),
            ),
            TempData((self.d_ghost, self.cell_g["w"])),
        )
        # x
        self.solver.evolve_stencil(
            *self.stencil_args,
            TempData((n_d_now, self.cell_d(n_reg_now, "d"))),
            TempData(
                (border.d_ghost, border.cell_g["x"]),  # right border, z
                (self.d_ghost, self.cell_g["x"]),
                (self.d_ghost, self.cell_g["y"]),
            ),
            TempData((self.d_ghost, self.cell_g["x"])),
        )

        # A
        average(
            d_nxt,
            self.cell_d(reg_nxt, "A"),
            self.parent[0].d_ghost,
            self.parent[0].cell_g["x"],  # left border, z'
            self.d_ghost,
            self.cell_g["yy"],
            self.d_ghost,
            self.cell_g["xx"],
            self.d_ghost,
            self.cell_g["ww"],
        )
        # B
        average(
            d_nxt,
            self.cell_d(reg_nxt, "B"),
            self.d_ghost,
            self.cell_g["y"],
            border.d_ghost,
            border.cell_g["x"],  # right border, z
            self.d_ghost,
            self.cell_g["w"],
            self.d_ghost,
            self.cell_g["x"],
        )

        # set self neigh to other border neigh
        self.neighbor = self.parent[0].neighbor
        self.neigh_by_res_idx = self.parent[0].neigh_by_res_idx
        return 1

    def evolve_disarm(self, *_):
        """Disarm self evolve at second +2:X step."""
        # disable self
        self.skip = True
        return 2

    def plot(self, p=None, P=None, res=None, nres=None):
        out = []
        # position counted from cusp center
        # is is left border
        p = int((self.p + 1) * self.reg.a) if p is None else p
        P = int((self.p + 1) * self.reg.a) if P is None else P
        res = self.reg.a if res is None else res
        nres = self.neighbor.a if nres is None else nres

        # trigger 2 times to fill res>2 fine ghost cells
        if self.evolve_step == 1:
            out.append(
                (1, p - 2 * nres, self.neighbor, self.d_ghost[self.cell_g["xx"]])
            )
            out.append((1, p - nres, self.neighbor, self.d_ghost[self.cell_g["ww"]]))
            out.append((1, p, self.neighbor, self.d_ghost[self.cell_g["w"]]))
            out.append((1, p + nres, self.neighbor, self.d_ghost[self.cell_g["x"]]))
            out.append((0, p - nres, self.neighbor, self.d_ghost[self.cell_g["yy"]]))
            out.append((0, p, self.neighbor, self.d_ghost[self.cell_g["y"]]))

        return out
