"""Straight double-cell cusps at timelike separation."""

import numpy as np

from sam.containers import Directional
from sam.solvers.refinement.schemes import (
    alpha_diag_border,
    beta_diag_border,
    gamma_diag_border,
    delta_diag_border,
    average,
    copy_to,
)
from sam.solvers.refinement.border_base import BorderBase, TempData


class CuspDoubleCellCoarseToFineV1V5(BorderBase):
    """Double cell timelike coarse-to-fine cusp."""

    __slots__: set[str] = set()

    def __init__(
        self,
        border_reg,
        sort_idx,
        arr_idx,
        pos,
        neigh_reg,
        neighbor_arr,
        neighbor_sort_idx,
        a2,
        solver,
        border_types,
        *,
        parent,
        skip=False,
        d_type=np.float_,
        num_ghost=9,
    ):
        """Return new CuspDoubleCellCoarseToFine (timelike cusp)."""
        super().__init__(
            border_reg,
            sort_idx,
            arr_idx,
            pos,
            neigh_reg,
            neighbor_arr,
            neighbor_sort_idx,
            a2,
            solver,
            border_types,
            parent=parent,
            skip=skip,
        )
        self._init_evolve = {}
        self._cell_g_init = {}
        # only 4 steps
        self.evolve_cycle = iter(
            (
                self.evolve_t1,
                self.evolve_t2,
                self.evolve_t3,
                self.evolve_t4,
            )
        )

        self.d_ghost = np.zeros(num_ghost, dtype=d_type)
        self.g_max_dist = {
            self.reg.a: Directional(0, 3),
            self.neighbor.a: Directional(0, 4),
        }
        self.cell_g = {  # comment names valid after t2
            "Q": 0,  # M
            "R": 1,  # J
            "E": 2,  # H
            "P": 3,  # L
            "O": 4,
            "N": 6,  # I
            "K": 5,
            "n": 7,
            "o": 8,
        }
        self._cell_names = {
            "G": -2,
            "F": -1,
            "C": -2,
            "B": -1,
            "A": -1,
            "a": 1,
            "b": 0,
            "c": 2,
            "d": 1,
            "e": 0,
            "f": 1,
            "g": 0,
            "h": 2,
            "i": 0,
            "p": 4,
            "q": 5,
            "r": 1,
            "s": 3,
            "t": 4,
            "u": 5,
            "v": 0,  # used for now and pre slice
            "w": 1,  # used for now and pre slice
            "x": 2,  # used for now and pre slice
            "y": 3,  # used for now and pre slice
        }

        self._copy_parent_data()

    def _copy_diag_parent_data(self):
        copy_to(
            self.parent.d_ghost,
            self.parent.cell_g["G"],
            self.d_ghost,
            self.cell_g["Q"],
        )
        copy_to(
            self.parent.d_ghost,
            self.parent.cell_g["H"],
            self.d_ghost,
            self.cell_g["R"],
        )
        self.parent = None

    def _copy_straight_parent_data(self):
        # start at t3
        next(self.evolve_cycle)
        next(self.evolve_cycle)

        # NOTE: requires straight V5, else key error on G (=K)
        copy_to(
            self.parent.d_ghost,
            self.parent.cell_g["F"],
            self.d_ghost,
            self.cell_g["K"],
        )
        copy_to(
            self.parent.d_ghost,
            self.parent.cell_g["H"],
            self.d_ghost,
            self.cell_g["O"],
        )
        copy_to(
            self.parent.d_ghost,
            self.parent.cell_g["I"],
            self.d_ghost,
            self.cell_g["N"],
        )
        copy_to(
            self.parent.d_ghost,
            self.parent.cell_g["J"],
            self.d_ghost,
            self.cell_g["E"],
        )
        copy_to(
            self.parent.d_ghost,
            self.parent.cell_g["G"],  # K
            self.d_ghost,
            self.cell_g["P"],
        )
        self.parent = None

    def _copy_parent_data(self):
        if isinstance(self.parent, self.BORDER_TYPES["DFTCR"]):
            self._copy_diag_parent_data()

        elif isinstance(self.parent, self.BORDER_TYPES["SCTF"]):
            self._copy_straight_parent_data()

    def init_ghost_cells(self, *_):
        raise NotImplementedError

    def border_change_selector(self, border):
        # StraightCoarseToFine
        if border is self.BORDER_TYPES["SCTF"]:
            return border

        # DiagCoarseToFineL: double cell cusp
        if border is self.BORDER_TYPES["DCTFL"]:
            # one time
            if self.parent is self:
                return border

            return None

        raise NotImplementedError(
            f"Change from {type(self).__name__} to {border.__name__} not implemented."
        )

    def evolve_t1(
        self,
        _reg_pre,
        n_reg_pre,
        reg_now,
        n_reg_now,
        reg_nxt,
        n_reg_nxt,
        _d_pre,
        n_d_pre,
        d_now,
        n_d_now,
        d_nxt,
        n_d_nxt,
    ):
        """Refinement step +1:1, +1:2."""
        # E
        average(
            self.d_ghost,
            self.cell_g["E"],
            n_d_now,
            self.cell_d(n_reg_now, "v"),
            n_d_now,
            self.cell_d(n_reg_now, "w"),
            n_d_pre,
            self.cell_d(n_reg_pre, "v"),  # cell in past of v
            n_d_pre,
            self.cell_d(n_reg_pre, "w"),  # cell in past of W
        )
        # P
        average(
            self.d_ghost,
            self.cell_g["P"],
            n_d_now,
            self.cell_d(n_reg_now, "x"),
            n_d_now,
            self.cell_d(n_reg_now, "y"),
            n_d_pre,
            self.cell_d(n_reg_pre, "x"),  # cell in past of x
            n_d_pre,
            self.cell_d(n_reg_pre, "y"),  # cell in past of y
        )
        # O
        self.solver.evolve_stencil(
            *self.stencil_args,
            TempData((self.d_ghost, self.cell_g["R"])),
            TempData(
                (self.d_ghost, self.cell_g["E"]),
                (self.d_ghost, self.cell_g["P"]),
                (d_now, self.cell_d(reg_now, "F")),
            ),
            TempData((self.d_ghost, self.cell_g["O"])),
        )
        # B
        self.solver.evolve_stencil(
            *self.stencil_args,
            TempData((self.d_ghost, self.cell_g["Q"])),
            TempData(
                (d_now, self.cell_d(reg_now, "F")),
                (self.d_ghost, self.cell_g["E"]),
                (d_now, self.cell_d(reg_now, "G")),
            ),
            TempData((d_nxt, self.cell_d(reg_nxt, "B"))),
        )
        # i
        alpha_diag_border(
            n_d_nxt,
            self.cell_d(n_reg_nxt, "i"),
            n_d_pre,
            self.cell_d(n_reg_pre, "x"),
            self.d_ghost,
            self.cell_g["O"],
            self.d_ghost,
            self.cell_g["P"],
            d_now,
            self.cell_d(reg_now, "F"),
            self.d_ghost,
            self.cell_g["R"],
        )
        return 1

    def evolve_t2(
        self,
        _reg_pre,
        n_reg_pre,
        reg_now,
        n_reg_now,
        _reg_nxt,
        n_reg_nxt,
        _d_pre,
        n_d_pre,
        d_now,
        n_d_now,
        _d_nxt,
        n_d_nxt,
    ):
        """Refinement steps +2:1, +2:2, +2:3."""
        # n
        self.solver.evolve_stencil(
            *self.stencil_args,
            TempData((n_d_pre, self.cell_d(n_reg_pre, "x"))),
            TempData(
                (n_d_now, self.cell_d(n_reg_now, "h")),
                (n_d_now, self.cell_d(n_reg_now, "s")),
                (n_d_now, self.cell_d(n_reg_now, "r")),
            ),
            TempData((self.d_ghost, self.cell_g["n"])),
        )
        # o
        self.solver.evolve_stencil(
            *self.stencil_args,
            TempData((n_d_pre, self.cell_d(n_reg_pre, "y"))),
            TempData(
                (n_d_now, self.cell_d(n_reg_now, "s")),
                (n_d_now, self.cell_d(n_reg_now, "t")),
                (n_d_now, self.cell_d(n_reg_now, "h")),
            ),
            TempData((self.d_ghost, self.cell_g["o"])),
        )
        # N
        average(
            self.d_ghost,
            self.cell_g["N"],
            self.d_ghost,
            self.cell_g["n"],
            self.d_ghost,
            self.cell_g["o"],
            n_d_now,
            self.cell_d(n_reg_now, "h"),
            n_d_now,
            self.cell_d(n_reg_now, "s"),
        )
        # K
        self.solver.evolve_stencil(
            *self.stencil_args,
            TempData((self.d_ghost, self.cell_g["E"])),
            TempData(
                (self.d_ghost, self.cell_g["O"]),
                (self.d_ghost, self.cell_g["N"]),
                (d_now, self.cell_d(reg_now, "B")),
            ),
            TempData((self.d_ghost, self.cell_g["K"])),
        )
        # g
        gamma_diag_border(
            n_d_nxt,
            self.cell_d(n_reg_nxt, "g"),
            n_d_now,
            self.cell_d(n_reg_now, "r"),
            d_now,
            self.cell_d(reg_now, "B"),
            self.d_ghost,
            self.cell_g["E"],
            self.d_ghost,
            self.cell_g["K"],
            self.d_ghost,
            self.cell_g["N"],
        )
        return 2

    def evolve_t3(
        self,
        reg_pre,
        n_reg_pre,
        reg_now,
        n_reg_now,
        _reg_nxt,
        n_reg_nxt,
        d_pre,
        n_d_pre,
        d_now,
        n_d_now,
        _d_nxt,
        n_d_nxt,
    ):
        """Refinement steps +3:1, +3:2."""
        # Q -> M
        average(
            self.d_ghost,
            self.cell_g["Q"],  # M
            n_d_pre,
            self.cell_d(n_reg_pre, "t"),
            n_d_pre,
            self.cell_d(n_reg_pre, "u"),
            n_d_now,
            self.cell_d(n_reg_now, "p"),
            n_d_now,
            self.cell_d(n_reg_now, "q"),
        )
        # R -> J
        self.solver.evolve_stencil(
            *self.stencil_args,
            TempData((self.d_ghost, self.cell_g["P"])),
            TempData(
                (self.d_ghost, self.cell_g["N"]),
                (self.d_ghost, self.cell_g["Q"]),  # M
                (self.d_ghost, self.cell_g["O"]),
            ),
            TempData((self.d_ghost, self.cell_g["R"])),  # J
        )
        # L -> P
        self.solver.evolve_stencil(
            *self.stencil_args,
            TempData((d_pre, self.cell_d(reg_pre, "F"))),
            TempData(
                (d_now, self.cell_d(reg_now, "B")),
                (self.d_ghost, self.cell_g["O"]),
                (d_now, self.cell_d(reg_now, "C")),
            ),
            TempData((self.d_ghost, self.cell_g["P"])),  # L
        )
        # first e, needs old i
        alpha_diag_border(
            n_d_nxt,
            self.cell_d(n_reg_nxt, "e"),
            n_d_pre,
            self.cell_d(n_reg_pre, "i"),
            self.d_ghost,
            self.cell_g["P"],  # L
            self.d_ghost,
            self.cell_g["O"],
            d_now,
            self.cell_d(reg_now, "C"),
            d_pre,
            self.cell_d(reg_pre, "F"),
        )
        # c replaces i
        alpha_diag_border(
            n_d_nxt,
            self.cell_d(n_reg_nxt, "c"),
            n_d_pre,
            self.cell_d(n_reg_pre, "h"),
            self.d_ghost,
            self.cell_g["K"],
            self.d_ghost,
            self.cell_g["N"],
            d_now,
            self.cell_d(reg_now, "B"),
            self.d_ghost,
            self.cell_g["E"],
        )
        # d
        delta_diag_border(
            n_d_nxt,
            self.cell_d(n_reg_nxt, "d"),
            n_d_now,
            self.cell_d(n_reg_now, "g"),
            self.d_ghost,
            self.cell_g["P"],  # L
            self.d_ghost,
            self.cell_g["O"],
        )
        return 3

    def evolve_t4(
        self,
        reg_pre,
        n_reg_pre,
        reg_now,
        n_reg_now,
        _reg_nxt,
        n_reg_nxt,
        d_pre,
        n_d_pre,
        d_now,
        n_d_now,
        _d_nxt,
        n_d_nxt,
    ):
        """Refinement steps +4:1, +4:2."""
        # H -> E
        self.solver.evolve_stencil(
            *self.stencil_args,
            TempData((self.d_ghost, self.cell_g["O"])),
            TempData(
                (self.d_ghost, self.cell_g["K"]),
                (self.d_ghost, self.cell_g["R"]),  # J
                (self.d_ghost, self.cell_g["P"]),  # L
            ),
            TempData((self.d_ghost, self.cell_g["E"])),  # H
        )
        # I -> N
        self.solver.evolve_stencil(
            *self.stencil_args,
            TempData((d_pre, self.cell_d(reg_pre, "B"))),
            TempData(
                (self.d_ghost, self.cell_g["P"]),  # L
                (self.d_ghost, self.cell_g["K"]),
                (d_now, self.cell_d(reg_now, "A")),
            ),
            TempData((self.d_ghost, self.cell_g["N"])),  # I
        )
        # a
        beta_diag_border(
            n_d_nxt,
            self.cell_d(n_reg_nxt, "a"),
            n_d_pre,
            self.cell_d(n_reg_pre, "f"),
            self.d_ghost,
            self.cell_g["P"],  # L
            self.d_ghost,
            self.cell_g["O"],
            self.d_ghost,
            self.cell_g["E"],  # H
            self.d_ghost,
            self.cell_g["R"],  # J
        )
        # b
        gamma_diag_border(
            n_d_nxt,
            self.cell_d(n_reg_nxt, "b"),
            n_d_now,
            self.cell_d(n_reg_now, "d"),
            d_now,
            self.cell_d(reg_now, "A"),
            d_pre,
            self.cell_d(reg_pre, "B"),
            self.d_ghost,
            self.cell_g["N"],  # I
            self.d_ghost,
            self.cell_g["K"],
        )
        self.parent = self
        return 4

    def plot(self, p=None, P=None, res=None, nres=None):
        out = []
        p = int((self.p + self.reg.n) * self.reg.a) if p is None else p
        P = p if P is None else P
        res = self.reg.a if res is None else res
        nres = self.neighbor.a if nres is None else nres
        if self.evolve_step == 1:
            out.append((-1, P, self.reg, self.d_ghost[self.cell_g["E"]]))
            out.append((-1, P + res, self.reg, self.d_ghost[self.cell_g["P"]]))
            out.append((-2, P - res, self.reg, self.d_ghost[self.cell_g["Q"]]))
            out.append((-2, P, self.reg, self.d_ghost[self.cell_g["R"]]))
            out.append((0, P, self.reg, self.d_ghost[self.cell_g["O"]]))
        elif self.evolve_step == 2:
            out.append((-1, P, self.reg, self.d_ghost[self.cell_g["E"]]))
            out.append((-1, P + res, self.reg, self.d_ghost[self.cell_g["P"]]))
            out.append((-2, P - res, self.reg, self.d_ghost[self.cell_g["Q"]]))
            out.append((-2, P, self.reg, self.d_ghost[self.cell_g["R"]]))
            out.append((0, p + nres * 2, self.neighbor, self.d_ghost[self.cell_g["n"]]))
            out.append((0, p + nres * 3, self.neighbor, self.d_ghost[self.cell_g["o"]]))
            out.append((0, P, self.reg, self.d_ghost[self.cell_g["O"]]))
            out.append((1, P, self.reg, self.d_ghost[self.cell_g["K"]]))
        elif self.evolve_step == 3:
            out.append((0, P + res * 2, self.reg, self.d_ghost[self.cell_g["R"]]))  # J
            out.append((0, P + res, self.reg, self.d_ghost[self.cell_g["K"]]))
            out.append((0, P, self.reg, self.d_ghost[self.cell_g["P"]]))  # L
            out.append((-1, P + res * 3, self.reg, self.d_ghost[self.cell_g["Q"]]))  # M
            out.append((-1, P + res * 2, self.reg, self.d_ghost[self.cell_g["N"]]))
            out.append((-1, P + res, self.reg, self.d_ghost[self.cell_g["O"]]))
            out.append((-2, P + res, self.reg, self.d_ghost[self.cell_g["E"]]))
        elif self.evolve_step == 4:
            out.append((1, P + res, self.reg, self.d_ghost[self.cell_g["E"]]))  # H
            out.append((1, P, self.reg, self.d_ghost[self.cell_g["N"]]))  # I
            out.append((0, P + res * 2, self.reg, self.d_ghost[self.cell_g["R"]]))  # J
            out.append((0, P + res, self.reg, self.d_ghost[self.cell_g["K"]]))
            out.append((0, P, self.reg, self.d_ghost[self.cell_g["P"]]))  # L
            out.append((-1, P + res, self.reg, self.d_ghost[self.cell_g["O"]]))

        return out


class CuspDoubleCellFineToCoarseV1V5(CuspDoubleCellCoarseToFineV1V5):
    """Double cell timelike fine-to-coarse cusp."""

    __slots__: set[str] = set()

    def __init__(
        self,
        border_reg,
        sort_idx,
        arr_idx,
        pos,
        neigh_reg,
        neighbor_arr,
        neighbor_sort_idx,
        a2,
        solver,
        border_types,
        *,
        parent,
        skip=False,
        d_type=np.float_,
    ):
        """Return new CuspDoubleCellFineToCoarse (timelike cusp)."""
        super().__init__(
            border_reg,
            sort_idx,
            arr_idx,
            pos,
            neigh_reg,
            neighbor_arr,
            neighbor_sort_idx,
            a2,
            solver,
            border_types,
            parent=parent,
            skip=skip,
            d_type=d_type,
        )
        self.g_max_dist = {
            self.reg.a: Directional(3, 0),
            self.neighbor.a: Directional(4, 0),
        }
        self._cell_names = {
            "G": 1,
            "F": 0,
            "C": 1,
            "B": 0,
            "A": 0,
            "a": -2,
            "b": -1,
            "c": -3,
            "d": -2,
            "e": -1,
            "f": -2,
            "g": -1,
            "h": -3,
            "i": -1,
            "p": -5,
            "q": -6,
            "r": -2,
            "s": -4,
            "t": -5,
            "u": -6,
            "v": -1,  # used for now and pre slice
            "w": -2,  # used for now and pre slice
            "x": -3,  # used for now and pre slice
            "y": -4,  # used for now and pre slice
        }

    def _copy_parent_data(self):
        if isinstance(self.parent, self.BORDER_TYPES["DFTCL"]):
            self._copy_diag_parent_data()

        elif isinstance(self.parent, self.BORDER_TYPES["SFTC"]):
            self._copy_straight_parent_data()

    def border_change_selector(self, border):
        # StraightFineToCoarse
        if border is self.BORDER_TYPES["SFTC"]:
            return border

        # DiagCoarseToFineR: double cell cusp
        if border is self.BORDER_TYPES["DCTFR"]:
            # one time
            if self.parent is self:
                return border

            return None

        raise NotImplementedError(
            f"Change from {type(self).__name__} to {border.__name__} not implemented."
        )

    def plot(self, p=None, P=None, res=None, nres=None):
        p = int(self.p * self.reg.a - self.neighbor.a) if p is None else p
        P = int(self.p * self.reg.a - self.reg.a) if P is None else P
        res = -self.reg.a if res is None else res
        nres = -self.neighbor.a if nres is None else nres
        return super().plot(p, P, res, nres)
