"""Straight single cell cusps at timelike separation."""

import numpy as np

from sam.solvers.refinement.border_base import BorderBase


class CuspSingleCellCoarseToFine(BorderBase):
    """Single cell timelike cusp, coarse - fine."""

    __slots__: set[str] = set()

    def __init__(
        self,
        border_reg,
        sort_idx,
        arr_idx,
        pos,
        neigh_reg,
        neighbor_arr,
        neighbor_sort_idx,
        a2,
        solver,
        border_types,
        *,
        parent=None,
        skip=False,
        d_type=np.float_,
    ):
        """Return new CuspSingleCellCoarseToFine (timelike cusp)."""
        super().__init__(
            border_reg,
            sort_idx,
            arr_idx,
            pos,
            neigh_reg,
            neighbor_arr,
            neighbor_sort_idx,
            a2,
            solver,
            border_types,
            parent=parent,
            skip=skip,
        )

    def border_change_selector(self, border):
        # this cusp changes to DiagCoarseToFineL per definition
        if border is self.BORDER_TYPES["DCTFL"]:
            return border

        raise NotImplementedError(
            f"Change from {type(self).__name__} to {border.__name__} not implemented."
        )


class CuspSingleCellFineToCoarse(CuspSingleCellCoarseToFine):
    """Single cell timelike cusp, fine - coarse."""

    __slots__: set[str] = set()

    def __init__(
        self,
        border_reg,
        sort_idx,
        arr_idx,
        pos,
        neigh_reg,
        neighbor_arr,
        neighbor_sort_idx,
        a2,
        solver,
        border_types,
        *,
        parent=None,
        skip=False,
        d_type=np.float_,
    ):
        """Return new CuspSingleCellFineToCoarse (timelike cusp)."""
        super().__init__(
            border_reg,
            sort_idx,
            arr_idx,
            pos,
            neigh_reg,
            neighbor_arr,
            neighbor_sort_idx,
            a2,
            solver,
            border_types,
            parent=parent,
            skip=skip,
        )

    def border_change_selector(self, border):
        # this cusp changes to DiagCoarseToFineR per definition
        if border is self.BORDER_TYPES["DCTFR"]:
            return border

        raise NotImplementedError(
            f"Change from {type(self).__name__} to {border.__name__} not implemented."
        )
