"""Straight double cell cusps at timelike separation for debugging."""

from unittest.mock import patch

import numpy as np

from sam.solvers import leapfrog
from sam.solvers.refinement.cusps.sdc_cusps import (
    CuspDoubleCellCoarseToFineV1V5,
    CuspDoubleCellFineToCoarseV1V5,
)
from sam.solvers.refinement.debug import (
    DebugBorderPass,
    SCHEME_EQUIVALENT_FUNC as SCHEME_DIAG_EQUIVALENT_FUNC,
    SCHEME_SDC_CUSPS_EQUIVALENT_FUNC,
)

# scheme eqs
CUSP_SCHEME_EQS = {
    "alpha_diag_border",
    "average",
    "beta_diag_border",
    "copy_to",
    "delta_diag_border",
    "gamma_diag_border",
}
SCHEME_EQUIVALENT_FUNC = {
    k: v for k, v in SCHEME_DIAG_EQUIVALENT_FUNC.items() if k in CUSP_SCHEME_EQS
}
SCHEME_EQUIVALENT_FUNC.update(SCHEME_SDC_CUSPS_EQUIVALENT_FUNC)


@patch.multiple(
    "sam.solvers.refinement.cusps.sdc_cusps",
    **SCHEME_EQUIVALENT_FUNC,
)
class DebugCuspDoubleCellCoarseToFineV1V5(CuspDoubleCellCoarseToFineV1V5):
    """Debug version double cell timelike coarse-to-fine cusp."""

    __slots__: set[str] = set()

    def __init__(
        self,
        border_reg,
        sort_idx,
        arr_idx,
        pos,
        neigh_reg,
        neighbor_arr,
        neighbor_sort_idx,
        a2,
        _solver,
        border_types,
        *,
        parent,
        skip=False,
    ):
        """Return new DebugCuspDoubleCellCoarseToFine, timelike cusp."""
        super().__init__(
            border_reg,
            sort_idx,
            arr_idx,
            pos,
            neigh_reg,
            neighbor_arr,
            neighbor_sort_idx,
            a2,
            leapfrog.DebugEvolveLeapfrog(),  # hardcode debug leapfrog
            border_types,
            parent=parent,
            skip=skip,
            d_type=np.int8,  # change dtype to int
        )


@patch.multiple(
    "sam.solvers.refinement.cusps.sdc_cusps",
    **SCHEME_EQUIVALENT_FUNC,
)
class DebugCuspDoubleCellFineToCoarseV1V5(CuspDoubleCellFineToCoarseV1V5):
    """Debug version double cell timelike fine-to-coarse cusp."""

    __slots__: set[str] = set()

    def __init__(
        self,
        border_reg,
        sort_idx,
        arr_idx,
        pos,
        neigh_reg,
        neighbor_arr,
        neighbor_sort_idx,
        a2,
        _solver,
        border_types,
        *,
        parent,
        skip=False,
    ):
        """Return new DebugCuspDoubleCellCoarseToFine, timelike cusp."""
        super().__init__(
            border_reg,
            sort_idx,
            arr_idx,
            pos,
            neigh_reg,
            neighbor_arr,
            neighbor_sort_idx,
            a2,
            leapfrog.DebugEvolveLeapfrog(),  # hardcode debug leapfrog
            border_types,
            parent=parent,
            skip=skip,
            d_type=np.int8,  # change dtype to int
        )


class DebugPassCuspDoubleCellCoarseToFine(
    DebugBorderPass, CuspDoubleCellCoarseToFineV1V5
):
    """CDCCTF for debugging that does nothing."""

    __slots__: set[str] = set()


class DebugPassCuspDoubleCellFineToCoarse(
    DebugBorderPass, CuspDoubleCellFineToCoarseV1V5
):
    """CDCFTC for debugging that does nothing."""

    __slots__: set[str] = set()
