"""Legacy code for phasing out old interfaces."""

import numpy as np

from sam.containers import NOPOS
from sam.solvers.refinement import debug


def cusp_refinement(
    reg_pre,
    reg_now,
    reg_nxt,
    p_pre,
    p_now,
    p_nxt,
    d_pre,
    d_now,
    d_nxt,
):
    """Cusp refinement."""
    # TODO: remove in favor of border treatment
    raise NotImplementedError


def cusp_refinement_debug(
    reg_pre,
    reg_now,
    reg_nxt,
    p_pre,
    p_now,
    p_nxt,
    d_pre,
    d_now,
    d_nxt,
):
    """Cusp refinement."""
    # TODO: remove in favor of border treatment
    raise NotImplementedError


def cusp_refinement_debug_pass(
    reg_pre,
    reg_now,
    reg_nxt,
    p_pre,
    p_now,
    p_nxt,
    d_pre,
    d_now,
    d_nxt,
):
    """Cusp refinement with debug values."""
    # TODO: remove in favor of border treatment

    # set all cells involved with new cusp to boundary
    # prevents value recycling

    # FIXME: also test against region sentinel

    # emerging cusp, flag=3->2
    if p_now is NOPOS:
        d_nxt[: reg_nxt.n] = debug.MARKER_BOUNDARY

    # emerging cusp, flag=2->1
    elif p_pre is NOPOS:
        d_nxt[: reg_nxt.n] = debug.MARKER_BOUNDARY

    # disappearing cusp, flag=-2
    elif p_nxt is NOPOS:
        # in-place bitwise or: '|='
        np.bitwise_or.at(d_now.arr, d_now.idx[: reg_now.n], debug.MARKER_BOUNDARY)
