"""Interface for border and cusp refinement."""

# FIXME: remove after py 3.11
from __future__ import annotations

import enum
import typing
from abc import ABC, abstractmethod
from collections.abc import MutableSequence
from typing import Callable, Iterable, Iterator, Type

import numpy as np
import numpy.typing as npt

from mrlattice.lattice.generate import RegionTuple

from sam.containers import NOREGION, Data, Directional
from sam.lattice import position_diff
from sam.solvers import Solver

# prevent circular import
if typing.TYPE_CHECKING:
    from sam.solvers.refinement import BorderTypes


class TempData(MutableSequence):
    """Temporary data array slice used for `BorderBase` types.

    All item access returns a view on the data.
    """

    # TODO: investigate performance issues because of frequent inits?
    # borders make use of 3 new instances each time they use the
    # evolve stencil. is this a perf issue? optimizations?
    # TODO: this is a data container and should move to containers.py

    __slots__ = {"_slice"}

    def __init__(
        self,
        *arr_key: tuple[np.ndarray | Data, int],
        values: Iterable[tuple[np.ndarray | Data, int]] = None,
    ):
        """Return new instance."""
        self._slice = list(arr_key)
        if values is not None:
            self._slice.extend(values)

    def __getitem__(self, key):
        """Return value at position(s) `key`."""
        if isinstance(key, slice):
            pair_slice = self._slice[key]
            return tuple(arr[idx] for arr, idx in pair_slice)
        if isinstance(key, int):
            arr, idx = self._slice[key]
            return arr[idx]

        raise TypeError(f"Key {key!r} is not supported.")

    def __setitem__(self, key, value):
        """Assign `value` at position `key`."""
        if isinstance(key, int):
            arr, idx = self._slice[key]
            arr[idx] = value
            return

        raise TypeError(f"Key {key!r} not supported.")

    def __delitem__(self, key):
        """Remove item at position `key`."""
        self._slice.__delitem__(key)

    def __len__(self):
        """Return len of self."""
        return len(self._slice)

    def insert(self, index: int, value: tuple[np.ndarray, int]):
        self._slice.insert(index, value)


class BorderBase(ABC):
    """Base class for all border types."""

    # TODO: use a dict to assign slots, key is name, val is docstring
    __slots__ = {
        "reg",
        "by_res_idx",
        "arr_idx",
        "p",
        "border_p",
        "neighbor",
        "neigh_arr_offset",
        "neigh_by_res_idx",
        "BORDER_TYPES",
        "parent",
        "skip",
        "d_ghost",
        "_cell_names",
        "cell_g",
        "_cell_g_init",
        "evolve_cycle",
        "g_max_dist",
        "evolve_step",
        "_init_evolve",
        "stencil_args",
        "solver",
    }

    # instance attributes required to be implemented
    # array for ghost cells
    d_ghost: np.ndarray
    # mapping of cell names to idx counted from border
    # any name must use uppercase letters for reg (coarse)
    # and lowercase letters for neighbor (fine)
    _cell_names: dict[str, int]
    # mapping of ghost cell names to idx in `d_ghost`
    cell_g: dict[str, int]
    # mapping of ghost cell names to idx counted from border
    # only used in init_ghost_cells
    _cell_g_init: dict[str, int]
    # max distance of ghost cells to border [counted in cells of region]
    g_max_dist: dict[int, Directional[int]]
    # cycle of callables for each evolve substep
    evolve_cycle: Iterator[Callable[..., int]]
    # dict that maps initial evolve steps to strategies
    _init_evolve: dict[InitEvolve, Callable[..., int]]

    # first evolve step selection
    @enum.unique
    class InitEvolve(enum.Enum):
        """Initial evolve step options."""

        # first real lattice time slice is duplicated to t=-1 for init
        SLICE_T0_CPY = enum.auto()

    @abstractmethod
    def __init__(
        self,
        border_reg: RegionTuple,
        sort_idx,
        arr_idx,
        pos,
        neigh_reg: RegionTuple,
        neighbor_arr_offset,
        neighbor_sort_idx,
        a2,
        solver: Solver,
        border_types: BorderTypes,
        *,
        parent: None | BorderBase | tuple[BorderBase, BorderBase] | InitEvolve = None,
        skip=False,
    ):
        """Return new border instance."""
        # TODO: change to iterator like evolve_cycle
        self.evolve_step = 1
        self.reg = border_reg
        self.by_res_idx = sort_idx
        self.arr_idx = arr_idx
        self.p = pos  # FIXME:  pos of reg ever needed or just border_p
        self.neighbor = neigh_reg
        self.neigh_arr_offset = neighbor_arr_offset
        self.neigh_by_res_idx = neighbor_sort_idx
        self.border_p = pos if self.neigh_arr_offset < 0 else pos + self.reg.n
        self.BORDER_TYPES = border_types
        self.parent = parent
        self.skip = skip
        # default args for evolve stencil on a single cell
        self.stencil_args = (a2, 0, 0, 0)
        self.solver = solver

    def cell_d(self, reg: RegionTuple, cell: str) -> int:
        """Return positive idx of cell corrected by reg.n."""
        # FIXME: non-standard, cause cell_g is accessed via getitem []
        # and this is a function call
        return (
            self._cell_names[cell]
            if self._cell_names[cell] >= 0
            else reg.n + self._cell_names[cell]
        )

    def __str__(self) -> str:
        """Return human readable string representation."""
        return f"<{type(self).__name__} at {hex(id(self))}>"

    def __repr__(self):
        """Return string representation."""
        # very hacky way of printing all slots from all base classes
        # doesn't work if __slots__ is a single string
        # TODO: make elem of subtraction set an instance attr
        return "".join(
            (
                f"<{type(self).__name__}(",
                ", ".join(
                    f"{attr}={getattr(self, attr)}"
                    for c in type(self).__mro__[:-2]
                    for attr in set(getattr(c, "__slots__"))
                    - {
                        "BORDER_TYPES",
                        "d_ghost",
                        "stencil_args",
                        "solver",
                        "evolve_cycle",
                        "cell_g",
                    }
                ),
                f"), d_ghost at {hex(id(self.d_ghost))}",
                f" -- at {hex(id(self))}>",
            )
        )

    @abstractmethod
    def init_ghost_cells(
        self,
        x_mask_now_c: npt.NDArray[np.bool_],
        d_now_c,
        x_mask_pre_c: npt.NDArray[np.bool_],
        d_pre_c,
        x_mask_now_f: npt.NDArray[np.bool_],
        d_now_f,
        x_mask_pre_f: npt.NDArray[np.bool_],
        d_pre_f,
    ):
        """Initialize ghost cells with values from initial condition.

        Return index position for placement of ghost cells.
        """
        # use placement from _cell_g_init and correct by number of
        # cells from g_max_dist to have idx position in array of len
        # g_max_dist[].sum that is also the shape of x_mask

        return {
            k: v
            if v >= 0
            else (
                v + self.g_max_dist[self.reg.a].sum
                if k.isupper()
                else v + self.g_max_dist[self.neighbor.a].sum
            )
            for k, v in self._cell_g_init.items()
        }

    def evolve(
        self,
        reg_pre: RegionTuple,
        n_reg_pre: RegionTuple,
        reg_now: RegionTuple,
        n_reg_now: RegionTuple,
        reg_nxt: RegionTuple,
        n_reg_nxt: RegionTuple,
        d_pre: Data,
        n_d_pre: Data,
        d_now: Data,
        n_d_now: Data,
        d_nxt: Data,
        n_d_nxt: Data,
    ):
        """Perform the next refinement step for finer region."""
        # reg_nxt = self.reg,
        # but n_reg_nxt=/=self.neighbor if reg.a not in iter_res

        if self.skip:
            return

        if isinstance(self.parent, self.InitEvolve):
            self.evolve_step = self._init_evolve[self.parent](
                reg_pre,
                n_reg_pre,
                reg_now,
                n_reg_now,
                reg_nxt,
                n_reg_nxt,
                d_pre,
                n_d_pre,
                d_now,
                n_d_now,
                d_nxt,
                n_d_nxt,
            )
            return

        # fetch next step and execute
        self.evolve_step = next(self.evolve_cycle)(
            reg_pre,
            n_reg_pre,
            reg_now,
            n_reg_now,
            reg_nxt,
            n_reg_nxt,
            d_pre,
            n_d_pre,
            d_now,
            n_d_now,
            d_nxt,
            n_d_nxt,
        )

    # TODO: proper interface
    # @abstractmethod
    def plot(
        self, p: int = None, P: int = None, res: int = None, nres: int = None
    ) -> Iterable[tuple[int, int, RegionTuple, int]]:
        """Return data for plotting with `GhostPlot`.

        Returns (t_offset, idx, reg, val) tuple for each ghost cell.
        """
        # how it works now:
        # t_offset is calculated as 0 == the cells with upper border
        # touching the red line for that evolve step. positive counts up
        # and negative counts backwards in units of the given reg cell.
        # coarser cells are modified in chunks of finest width height.
        # idx is in finest units the position of cell counted from the
        # start of the reg of the border (which is the coarser reg).
        # so for mirrored types this requires adding the reg.N count.
        # reg is the region tuple and val the value to set.
        # passing the reg itself is not necessary, only the res is.
        # it is used to differentiate between fine and coarse

    @staticmethod
    def select_border(
        border_types: BorderTypes, neighbor: int, p_delta: int
    ) -> Type[BorderBase]:
        """Return border type."""

        # FIXME: candidate for match statement with 3.10
        if p_delta > 0 and neighbor == +1:
            name = "DCTFL"
        elif p_delta > 0 and neighbor == -1:
            name = "DFTCL"
        elif p_delta < 0 and neighbor == +1:
            name = "DFTCR"
        elif p_delta < 0 and neighbor == -1:
            name = "DCTFR"
        else:
            if neighbor == -1:
                name = "SFTC"
            elif neighbor == +1:
                name = "SCTF"

        return border_types[name]

    @classmethod
    def get_border_type(
        cls,
        border_types,
        reg_now: RegionTuple,
        reg_nxt: RegionTuple,
        p_now,
        p_nxt,
        neigh_p: RegionTuple,
        neigh_n: RegionTuple,
    ) -> Iterable[tuple[Type[BorderBase], int]]:
        """Identify the types of the left and right borders of reg."""

        p_ds, p_de = position_diff(p_now, p_nxt, reg_now, reg_nxt)
        # neigh tells next or pre reg (or both) share border
        border, neigh = [], []

        if reg_nxt.a > neigh_p.a:
            neigh.append(-1)
            border.append(cls.select_border(border_types, -1, p_ds))

        if reg_nxt.a > neigh_n.a:
            neigh.append(+1)
            border.append(cls.select_border(border_types, +1, p_de))

        return zip(border, neigh)

    @abstractmethod
    def border_change_selector(self, border) -> None | Type[BorderBase]:
        """Return changed border type if changed else `None`.

        Raises:
            NotImplementedError: This change is not implemented.
        """

    def border_change(self, reg_nxt: RegionTuple, p_nxt, neigh: RegionTuple):
        """Detect change of border and update or return new border."""

        neighbors = (
            RegionTuple(reg_nxt.a + 1, -1),
            neigh,
        )

        # this loop only iterates once with set `neighbors`
        for b, a in self.get_border_type(
            self.BORDER_TYPES,
            self.reg,
            reg_nxt,
            self.p,
            p_nxt,
            *neighbors[:: self.neigh_arr_offset],
        ):
            assert a == self.neigh_arr_offset

            if (border := self.border_change_selector(b)) is None:
                # same type, update attrs
                self.reg = reg_nxt
                self.p = p_nxt
                self.neighbor = neigh
                return None

            # fetch args for init
            args = (
                reg_nxt,
                self.by_res_idx,
                self.arr_idx,
                p_nxt,
                neigh,
                self.neigh_arr_offset,
                self.neigh_by_res_idx,
                self.stencil_args[0],  # a2
                self.solver,
                self.BORDER_TYPES,
            )
            # save source border type (parent) for refinement
            kwargs = {"parent": self}

        # if UnboundLocalError, then get_border_type returned empty,
        # it's an error, as a border can never "vanish", only change
        return border, args, kwargs

    @staticmethod
    def get_ins_idx(cusp_idx, borders: list[BorderBase]):
        """Return index for `borders` to insert new cusp borders."""
        idx = -1
        for idx, b_reg in enumerate(borders):
            if cusp_idx == b_reg.by_res_idx:
                break
        else:
            idx += 1

        return idx

    @staticmethod
    def get_del_idx(cusp_reg, cusp_idx, borders: list[BorderBase]):
        """Return index for `borders` to delete cusp borders."""
        # TODO: this seems pointless, cusp_idx == idx always?
        for idx, border in enumerate(borders):
            if border.reg == cusp_reg and border.by_res_idx == cusp_idx:
                break
        else:
            # if never break, trigger error
            idx = len(borders)

        return idx

    @staticmethod
    def border_ins_update(
        res, n_res, arr_idx, border_by_res: Iterable[list[BorderBase]]
    ):
        """Update all borders after arr_idx for insertion."""
        for res_borders in border_by_res:
            for b in res_borders:
                # only change regs to the right of cusp
                if b.arr_idx >= arr_idx:
                    b.arr_idx += 2
                    if b.neighbor.a in [res, n_res]:
                        b.neigh_by_res_idx += 1
                    if b.reg.a in [res, n_res]:
                        b.by_res_idx += 1
                elif b.arr_idx == arr_idx - 1 and b.neigh_arr_offset == 1:
                    # if embedding region at arr_idx-1 previously had a
                    # border to its right, that border is now attached
                    # to the embedding region after cusp: idx+=2
                    b.arr_idx += 2
                    b.by_res_idx += 1
                    if b.neighbor.a == res:
                        b.neigh_by_res_idx += 1

    @staticmethod
    def border_del_update(
        res, n_res, arr_idx, border_by_res: Iterable[list[BorderBase]]
    ):
        """Update all borders after arr_idx for deleted border."""
        for res_borders in border_by_res:
            for b in res_borders:
                # only change regs to the right of cusp
                if b.arr_idx > arr_idx:
                    b.arr_idx -= 2
                    if b.neighbor.a in [res, n_res]:
                        b.neigh_by_res_idx -= 1
                    if b.reg.a in [res, n_res]:
                        b.by_res_idx -= 1

    @staticmethod
    def activate_borders(cusp_reg, cusp_idx, borders: list[BorderBase]):
        """Activate borders after cusp insertion."""
        # TODO: obsolete method
        raise NotImplementedError
        for b in borders:
            if b.reg == cusp_reg and b.by_res_idx == cusp_idx:
                assert b.skip is True
                b.skip = False
