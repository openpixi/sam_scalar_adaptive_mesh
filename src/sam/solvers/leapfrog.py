"""Implementation of `solvers.Solver` with leapfrog stencil."""

from mrlattice.lattice.generate import RegionTuple

from sam.solvers import Solver
from sam.solvers.refinement import debug


class EvolveLeapfrog(Solver):
    """Simple leapfrog solver for non-interacting scalar field."""

    @staticmethod
    def evolve_stencil(a2, x, p_ds_pre, p_ds_nxt, d_pre, d_now, d_nxt):
        """Evaluate the leapfrog stencil on a single cell."""
        # free field propagation
        a2 = a2[0]
        d_nxt[x + p_ds_nxt] = (
            a2 * (d_now[x - 1] + d_now[x + 1])
            - d_pre[x + p_ds_pre]
            + d_now[x] * 2 * (1 - a2)
        )


class DebugEvolveLeapfrog(Solver):
    """Write debug values to next time slice."""

    @staticmethod
    def evolve_stencil(a2, x, p_ds_pre, p_ds_nxt, d_pre, d_now, d_nxt):
        """Evaluate debug leapfrog stencil on a single cell."""
        # assign values based on usage
        d_nxt[x + p_ds_nxt] = debug.MARKER_WRITTEN
        d_pre[x + p_ds_pre] |= debug.MARKER_PRE
        d_now[x - 1] |= debug.MARKER_LEFT
        d_now[x + 1] |= debug.MARKER_RIGHT

    def evolve_region(
        self,
        a2,
        xmin,
        xmax,
        p_ds_pre,
        p_ds_nxt,
        reg_now: RegionTuple,
        reg_nxt: RegionTuple,
        d_pre,
        d_now,
        d_nxt,
    ):
        """Write debug values to next time slice."""

        super().evolve_region(
            a2, xmin, xmax, p_ds_pre, p_ds_nxt, reg_now, reg_nxt, d_pre, d_now, d_nxt
        )

        # mark the boundary cells that are not used above
        d_nxt[: xmin + p_ds_nxt] = debug.MARKER_BOUNDARY
        d_nxt[reg_now.n + xmax + p_ds_nxt : reg_nxt.n] = debug.MARKER_BOUNDARY
