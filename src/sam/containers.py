"""Module for data containers."""

from __future__ import annotations

from collections import abc
from dataclasses import dataclass, field
from functools import cache
from typing import (
    Any,
    Callable,
    Generic,
    Iterable,
    Sequence,
    TYPE_CHECKING,
    TypeAlias,
    TypeVar,
    overload,
)

import numpy as np
import numpy.typing as npt

from mrlattice.lattice.generate import RegionSlice, RegionTuple

if TYPE_CHECKING:
    from _typeshed import SupportsKeysAndGetItem


_ScalarT = TypeVar("_ScalarT", bound=np.generic)
_T = TypeVar("_T")


@dataclass(slots=True)
class Directional(Generic[_T]):
    """Container for (l, r) directional data."""

    l: _T
    r: _T
    sum: _T = field(init=False)

    def __post_init__(self):
        """Set the sum field."""
        self.sum = self.l + self.r


class ResMap(abc.MutableMapping[int, _T]):
    """Mapping of regions' resolutions to data.

    Provides 1 dim indexing: `[res]`.
    """

    __slots__ = {"_dict"}

    @overload
    def __init__(self, mapping: SupportsKeysAndGetItem[int, _T], /):
        ...

    @overload
    def __init__(self, iterable: Iterable[tuple[int, _T]], /):
        ...

    def __init__(self, *args):
        """Works like dict, but no kwargs allowed."""
        self._dict: dict[int, _T] = dict(*args)

    @classmethod
    def fromkeys(cls, iterable: Iterable[int], value: _T) -> ResMap[_T]:
        """Fill with the same `value` and keys from `iterable`."""
        return cls((k, value) for k in iterable)

    def __getitem__(self, idx: int) -> _T:
        # noqa: D105
        return self._dict[idx]

    def __setitem__(self, idx: int, val: _T) -> None:
        # noqa: D105
        self._dict[idx] = val

    def __delitem__(self, idx: int) -> None:
        # noqa: D105
        del self._dict[idx]

    def __iter__(self):
        """Iterator over the keys."""
        return iter(self._dict)

    def __len__(self):
        """Number of elements."""
        return len(self._dict)


class ResRegMap(ResMap[list[_T]]):
    """Mapping of regions' resolutions to lists with data.

    Provides 2 dim indexing: `[res, reg_idx]`.
    The second index can be a slice and a new list with references to
    the same elements will be returned (like python lists).
    """

    __slots__: set[str] = set()

    @overload
    def __getitem__(self, idx_slice: int) -> list[_T]:
        ...

    @overload
    def __getitem__(self, idx_slice: tuple[int, int]) -> _T:
        ...

    @overload
    def __getitem__(self, idx_slice: tuple[int, slice]) -> list[_T]:
        ...

    def __getitem__(self, idx_slice):
        # noqa: D105
        if isinstance(idx_slice, tuple):
            res, reg = idx_slice
            return self._dict[res][reg]

        return super().__getitem__(idx_slice)

    @overload
    def __setitem__(self, idx_slice: int, val: list[_T]) -> None:
        ...

    @overload
    def __setitem__(self, idx_slice: tuple[int, int], val: _T) -> None:
        ...

    @overload
    def __setitem__(self, idx_slice: tuple[int, slice], val: Sequence[_T]) -> None:
        ...

    def __setitem__(self, idx_slice, val):
        # noqa: D105
        if isinstance(idx_slice, tuple):
            res, reg = idx_slice
            self._dict[res][reg] = val
            return

        super().__setitem__(idx_slice, val)

    @overload
    def __delitem__(self, idx_slice: int) -> None:
        ...

    @overload
    def __delitem__(self, idx_slice: tuple[int, int]) -> None:
        ...

    @overload
    def __delitem__(self, idx_slice: tuple[int, slice]) -> None:
        ...

    def __delitem__(self, idx_slice):
        # noqa: D105
        if isinstance(idx_slice, tuple):
            res, reg = idx_slice
            del self._dict[res][reg]
            return

        super().__delitem__(idx_slice)


class ResRegRegionTuple(ResRegMap[RegionTuple]):
    """Mapping of regions' res'es to sorted lists of RegionTuples.

    Keys are int resolutions. Values are lists of RegionTuples of the
    same resolution that are sorted by their x positions on the lattice
    (growing x). The lists can be empty.
    """

    @classmethod
    def from_region_slice(
        cls, res_list: Iterable[int], time_slice: RegionSlice
    ) -> ResRegRegionTuple:
        """Fill keys from `res_list` and vals from `time_slice`."""

        sorted_regs = cls((k, []) for k in res_list)
        for reg in time_slice:
            sorted_regs[reg.a].append(reg)

        return sorted_regs


class Data(Generic[_ScalarT]):
    """Basic ndarray container with included index and empty arrays.

    The 'empty' array shall have dtype=`np.bool_` and tell apart used
    elements in the 'data' array. The 'index' array shall have dtype=
    `np.intp`. The dtype of the 'data' array can be any valid dtype.
    All indexing operations on `Data` are first applied to the 'index'
    array and then the result is used to index the 'data' array via
    numpy's advanced indexing. The 'empty' and 'index' arrays are
    exposed via `Data.empty` and `Data.idx`. The data array is exposed
    via `Data.arr` if needed.
    """

    __slots__ = {"arr", "empty", "idx"}

    def __init__(
        self,
        data: npt.NDArray[_ScalarT],
        empty: npt.NDArray[np.bool_],
        index: npt.NDArray[np.intp],
    ) -> None:
        """Initialize instance with references to given ndarrays."""
        self.arr = data
        self.empty = empty
        self.idx = index

    @overload
    def __getitem__(self, idx_slice: int) -> _ScalarT:
        ...

    @overload
    def __getitem__(self, idx_slice: slice) -> npt.NDArray[_ScalarT]:
        ...

    @overload
    def __getitem__(self, idx_slice: Any) -> Any:
        ...
        # advanced numpy indexing

    def __getitem__(self, idx_slice):
        # noqa: D105
        if not isinstance(idx_slice, int):
            raise NotImplementedError(
                "Indexing with slices returns a copy of the data."
            )

        return self.arr[self.idx[idx_slice]]

    @overload
    def __setitem__(self, idx_slice: int, val: _ScalarT) -> None:
        ...

    @overload
    def __setitem__(self, idx_slice: slice, val: npt.NDArray[_ScalarT]) -> None:
        ...

    @overload
    def __setitem__(self, idx_slice: Any, val: npt.NDArray[_ScalarT]) -> None:
        ...
        # advanced numpy indexing

    def __setitem__(self, idx_slice, val):
        # noqa: D105
        self.arr[self.idx[idx_slice]] = val


class RegData(abc.MutableSequence[Data[_ScalarT]]):
    """Sorted container of regions' `Data`.

    Provides 2 dim indexing: `[reg_idx, arr_idx]`.
    The first index cannot be a slice if both are given.
    """

    # implementation detail:
    # all Data elements of the sequence share the same ndarray for
    # the `_data` and `empty` attributes

    __slots__ = {"_data"}

    def __init__(self, elements: Iterable[Data[_ScalarT]]) -> None:
        """Initialize with `Data` instances."""
        self._data = list(elements)

    @overload
    def __getitem__(self, idx_slice: int) -> Data[_ScalarT]:
        ...

    @overload
    def __getitem__(self, idx_slice: slice) -> RegData[_ScalarT]:
        ...

    @overload
    def __getitem__(self, idx_slice: tuple[int, int]) -> _ScalarT:
        ...

    @overload
    def __getitem__(self, idx_slice: tuple[int, slice]) -> npt.NDArray[_ScalarT]:
        ...

    @overload
    def __getitem__(self, idx_slice: tuple[int, Any]) -> Any:
        ...
        # advanced numpy indexing

    def __getitem__(self, idx_slice):
        # noqa: D105
        if isinstance(idx_slice, tuple):
            reg_idx, arr_idx = idx_slice

            if not isinstance(reg_idx, int):
                raise TypeError(
                    f"First element of index tuple has wrong type: {type(reg_idx)}."
                    f" Expected: {int}"
                )

            return self._data[reg_idx][arr_idx]

        if isinstance(idx_slice, slice):
            return RegData(self._data[idx_slice])

        return self._data[idx_slice]

    @overload
    def __setitem__(self, idx_slice: int, val: Data[_ScalarT]) -> None:
        ...

    @overload
    def __setitem__(self, idx_slice: slice, val: Iterable[Data[_ScalarT]]) -> None:
        ...

    @overload
    def __setitem__(self, idx_slice: tuple[int, int], val: _ScalarT) -> None:
        ...

    @overload
    def __setitem__(
        self, idx_slice: tuple[int, slice], val: _ScalarT | npt.NDArray[_ScalarT]
    ) -> None:
        ...

    @overload
    def __setitem__(
        self, idx_slice: tuple[int, Any], val: _ScalarT | npt.NDArray[_ScalarT]
    ) -> None:
        ...
        # advanced numpy indexing

    def __setitem__(self, idx_slice, val):
        # noqa: D105
        if isinstance(idx_slice, tuple):
            reg_idx, arr_idx = idx_slice

            if not isinstance(reg_idx, int):
                raise TypeError(
                    f"First element of index tuple has wrong type: {type(reg_idx)}."
                    f" Expected: {int}"
                )

            self._data[reg_idx][arr_idx] = val
            return

        self._data[idx_slice] = val

    def __delitem__(self, idx: int | slice) -> None:
        # noqa: D105
        del self._data[idx]

    def __len__(self):
        """Number of elements."""
        return len(self._data)

    def insert(self, index: int, value: Data[_ScalarT]) -> None:
        """Insert new `val` at `idx`."""
        self._data.insert(index, value)


class ResRegData(abc.MutableMapping[int, RegData[_ScalarT]]):
    """Mapping of resolutions to regions' `RegData`.

    Provides 3 dim indexing: `[res, reg_idx, arr_idx]`.
    The first index cannot be a slice in any case. The second index
    cannot be a slice only if the third index is given as well.
    """

    __slots__ = {"_dict"}

    def __init__(
        self,
        data: ResMap[npt.NDArray[_ScalarT]],
        empty: ResMap[npt.NDArray[np.bool_]],
        index: ResRegMap[npt.NDArray[np.intp]],
    ) -> None:
        """Inits `RegData` for each res in `index` if not empty."""
        self._dict: dict[int, RegData[_ScalarT]] = {
            res: RegData(Data(data[res], empty[res], idx) for idx in idx_lst)
            for res, idx_lst in index.items()
            if idx_lst
        }
        # FIXME: are these required?
        # self._data = data
        # self._empty = empty
        # self._idx = index

    @overload
    def __getitem__(self, idx_slice: int) -> RegData[_ScalarT]:
        ...

    @overload
    def __getitem__(self, idx_slice: tuple[int, int]) -> Data[_ScalarT]:
        ...

    @overload
    def __getitem__(self, idx_slice: tuple[int, slice]) -> RegData[_ScalarT]:
        ...

    @overload
    def __getitem__(self, idx_slice: tuple[int, int, int]) -> _ScalarT:
        ...

    @overload
    def __getitem__(self, idx_slice: tuple[int, int, slice]) -> npt.NDArray[_ScalarT]:
        ...

    @overload
    def __getitem__(self, idx_slice: tuple[int, int, Any]) -> Any:
        ...
        # advanced numpy indexing

    def __getitem__(self, idx_slice):
        # noqa: D105
        if isinstance(idx_slice, tuple):

            if (size := len(idx_slice)) == 2:
                res, reg_idx = idx_slice
                return self._dict[res][reg_idx]

            if size == 3:
                res, reg_idx, arr_idx = idx_slice

                if not isinstance(reg_idx, int):
                    raise TypeError(
                        "Second element of index tuple has wrong type: "
                        f"{type(reg_idx)}."
                        f" Expected: {int}"
                    )

                return self._dict[res][reg_idx][arr_idx]

            if size > 3:
                raise ValueError(f"Index tuple has len {size} > 3.")

            idx_slice = idx_slice[0]

        return self._dict[idx_slice]

    @overload
    def __setitem__(self, idx_slice: int, val: RegData[_ScalarT]) -> None:
        ...

    @overload
    def __setitem__(self, idx_slice: tuple[int, int], val: Data[_ScalarT]) -> None:
        ...

    @overload
    def __setitem__(
        self, idx_slice: tuple[int, slice], val: Iterable[Data[_ScalarT]]
    ) -> None:
        ...

    @overload
    def __setitem__(self, idx_slice: tuple[int, int, int], val: _ScalarT) -> None:
        ...

    @overload
    def __setitem__(
        self, idx_slice: tuple[int, int, slice], val: _ScalarT | npt.NDArray[_ScalarT]
    ) -> None:
        ...

    @overload
    def __setitem__(
        self,
        idx_slice: tuple[int, int, Any],
        val: _ScalarT | npt.NDArray[_ScalarT],
    ) -> None:
        ...
        # advanced numpy indexing

    def __setitem__(self, idx_slice, val):
        # noqa: D105
        if isinstance(idx_slice, tuple):

            if (size := len(idx_slice)) == 2:
                res, reg_idx = idx_slice
                self._dict[res][reg_idx] = val
                return

            if size == 3:
                res, reg_idx, arr_idx = idx_slice

                if not isinstance(reg_idx, int):
                    raise TypeError(
                        "Second element of index tuple has wrong type: "
                        f"{type(reg_idx)}."
                        f" Expected: {int}"
                    )

                self._dict[res][reg_idx][arr_idx] = val
                return

            if size > 3:
                raise ValueError(f"Index tuple has len {size} > 3.")

            idx_slice = idx_slice[0]

        self._dict[idx_slice] = val

    @overload
    def __delitem__(self, idx_slice: int) -> None:
        ...

    @overload
    def __delitem__(self, idx_slice: tuple[int, int | slice]) -> None:
        ...

    def __delitem__(self, idx_slice):
        # noqa: D105
        if isinstance(idx_slice, tuple):

            if (size := len(idx_slice)) != 2:
                raise ValueError(
                    f"Index tuple for deletion has wrong len: {size}. Expected: 2"
                )

            res, reg_idx = idx_slice
            del self._dict[res][reg_idx]
            if not self._dict[res]:
                del self._dict[res]
            return

        del self._dict[idx_slice]

    def __iter__(self):
        """Iterator over keys."""
        return iter(self._dict)

    def __len__(self):
        """Number of elements."""
        return len(self._dict)


# FIXME: needs a home class
# and could be nice to split responsibilities
def prep_nxt_ts(
    time: int, lat_array, dd_pre: ResRegData, res_list: list[int]
) -> tuple[ResRegRegionTuple, ResRegData, ResRegPos]:
    """Prepare data for next time slice."""

    # regions of next time slice
    ts_nxt: RegionSlice = lat_array[time + 1]
    reg_by_res_nxt = ResRegRegionTuple.from_region_slice(res_list, ts_nxt)

    # TODO: this should be a method of ResRegData.idx_copy() -> RRData
    # create data for next region
    # explicit copy of index arrays to avoid interference with pre data
    # result is a deep copy for index arrays, shallow for data and empty
    dd_nxt = ResRegData(ResMap({}), ResMap({}), ResRegMap({}))
    for res, reg_data in dd_pre.items():
        dd_nxt[res] = RegData([])
        for data in reg_data:
            # copy of Data instance with deep copy of index array
            dd_nxt[res].append(Data(data.arr, data.empty, data.idx.copy()))

    # prepare region x positions
    p_nxt_dict = ResRegPos((k, []) for k in res_list)
    start = 0
    for reg in ts_nxt:
        p_nxt_dict[reg.a].append(start / reg.a)
        start += reg.a * reg.n

    return reg_by_res_nxt, dd_nxt, p_nxt_dict


# define alias types and sentinels

ResRegPos: TypeAlias = ResRegMap[float]

# sentinels as invalid element placeholders
NOREGION = RegionTuple(None, None)  # type: ignore[arg-type]
NOPOS = float("nan")


def nondarr(dtype: type[_ScalarT]) -> npt.NDArray[_ScalarT]:
    """Returns empty ndarray sentinels of `dtype`."""
    return np.empty((0,), dtype=dtype)


# use functools.cache to have singleton sentinels
# explicit type override to fix type
NONDARR: Callable[[type[_ScalarT]], npt.NDArray[_ScalarT]] = cache(
    nondarr
)  # type: ignore
