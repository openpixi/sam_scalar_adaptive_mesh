"""Collection of different initial conditions for sam.

Each function's signature is:
    calc_init(*args) -> Callable[
        [ x_array: np.ndarray ],
        [ idx: np.ndarray, data: np.ndarray ]
        ]

The returned callable is to be used for `ScalarField.init_[..]` methods.
`x_array` sets the x values to evaluate the exact fill functions at.
The returned callable returns a tuple of `idx`, `data`, where `idx` is a
bool array with exactly as many True values as data has total values.
It is the mask that tells at which positions of `x_array` there are
values (and which are left untouched by fill). The values of `data`
correspond to the x values in `x_array` where `idx` is True.
`fill(x_array[idx]) == (True, data)` holds.
"""

import numpy as np
from scipy.stats import norm

# FIXME: rename to remove the single prefix.
#        this is legacy naming, now all init.funcs are chainable if
#        more than one init pulse is needed.
#        the old naming is still used in legacy testing/eval code


def single_step_pulse(center, width, amplitude=1.0):
    """Place a single step shaped pulse centered at `center`."""

    start = center - width / 2
    end = start + width

    def filler(x_array: np.ndarray) -> tuple[np.ndarray, np.ndarray]:

        x_limited = np.logical_and(x_array >= start, x_array <= end)
        data = np.full(np.count_nonzero(x_limited), amplitude)

        return x_limited, data

    return filler


def single_gauss_pulse(mu, sigma, cutoff, amplitude):
    """Place a single gauss shaped pulse at `mu`."""

    gaussian = norm(loc=mu, scale=sigma)

    def filler(x_array: np.ndarray) -> tuple[np.ndarray, np.ndarray]:

        values = gaussian.pdf(x_array) * amplitude / gaussian.pdf(mu)
        x_limited = values >= cutoff

        return x_limited, values[x_limited]

    return filler


def single_caret_pulse(center, width, amplitude):
    """Place a caret (^) shaped pulse at `mu`."""

    def filler(x_array: np.ndarray) -> tuple[np.ndarray, np.ndarray]:

        # - width/2 * amplitude * || center - x || + amplitude
        x_limited = np.abs(center - x_array) <= width / 2
        values = (
            np.abs(center - x_array[x_limited]) * (-2) * amplitude / width + amplitude
        )

        return x_limited, values

    return filler
