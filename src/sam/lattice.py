"""Interface to the adaptive lattice."""

import numpy as np

from mrlattice.lattice.generate import RegionTuple

from sam.containers import Data, RegData


def update_nxt(d_nxt: Data, reg_pre: RegionTuple, reg_nxt: RegionTuple, p_pre, p_nxt):
    """Prepare nxt data for next time slice and return free idxs."""

    # Modifies also pre.empty, because nxt and pre share empty array.

    free: list[int] = []

    p_ds_nxt, p_de_nxt = position_diff(p_pre, p_nxt, reg_pre, reg_nxt)

    # FIXME: instead of loops over p_ds_nxt, use
    # argmax = np.nonzero(u_empty[res])[:width]
    # assert argmax not empty
    # u_empty[res][argmax] = False
    # x_new[:width] = argmax

    empty = d_nxt.empty
    idx = d_nxt.idx

    if p_ds_nxt != 0:
        # starting pos varies, roll idx
        idx = np.roll(idx, p_ds_nxt)
        if p_ds_nxt > 0:
            # add in front
            for i in range(p_ds_nxt):
                assert True in empty
                argmax = empty.argmax()
                empty[argmax] = False
                idx[i] = argmax
        elif p_ds_nxt < 0:
            # remove from front
            free.extend(range(p_ds_nxt, 0))

    if p_de_nxt != 0:
        # end position varies, no roll necessary as at end
        # conditions are reversed!
        if p_de_nxt < 0:
            # append
            for i in range(-p_de_nxt):
                assert True in empty
                argmax = empty.argmax()
                empty[argmax] = False
                # use reg_pre + p_ds_nxt as endpoint
                # idx is copy of pre, so has "old" shape
                idx[reg_pre.n + p_ds_nxt + i] = argmax
        elif p_de_nxt > 0:
            # remove from end
            free.extend(reg_pre.n + p_ds_nxt - i for i in range(p_de_nxt, 0, -1))

    # save new modified values for index array
    d_nxt.idx = idx

    return free


def free_nxt(dreg_nxt: RegData, free: list[list[int]]):
    """Free data positions in free for each reg."""
    for d_nxt, f in zip(dreg_nxt, free):
        d_nxt.empty[d_nxt.idx[f]] = True
        d_nxt.idx[f] = 0


def position_diff(p, p_other, reg: RegionTuple, reg_other: RegionTuple):
    """Return reg bounds differences of `p` and `p_other`."""
    p_ds = p - p_other
    assert p_ds == int(p_ds)
    p_ds = int(p_ds)

    p_de = p_ds + reg.n - reg_other.n
    assert p_de == int(p_de)
    p_de = int(p_de)

    return p_ds, p_de


def get_insertion_idx(p_list, reg: RegionTuple, pos):
    """Return idx and p of cusp."""
    idx = 0
    p: float = 0.0
    for idx, p in enumerate(p_list):
        if (reg.n + pos) / 2 == p:
            break
    return idx, p


def calc_reg_change(p_now, p_nxt, reg_now: RegionTuple, reg_nxt: RegionTuple, offset=0):
    """Calculate new reg data for cusps.

    Offset determines the number of cells to remove.
    """
    p_ds, _ = position_diff(p_now, p_nxt, reg_now, reg_nxt)
    new_now_width = reg_nxt.n - p_ds + offset
    assert new_now_width == int(new_now_width)
    new_now_width = int(new_now_width)
    reg_now_post = RegionTuple(
        reg_now.a,
        reg_now.n - new_now_width,
    )
    reg_now = RegionTuple(reg_now.a, new_now_width)
    p_now_post = p_now + new_now_width

    return reg_now, reg_now_post, p_now_post
