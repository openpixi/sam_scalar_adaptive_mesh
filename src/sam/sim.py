"""Scalar field in 1+1D on an adaptive mesh."""

from collections import Counter
from itertools import cycle
from typing import Callable, Iterator, Literal, overload

import numpy as np
import numpy.typing as npt

from mrlattice.lattice.generate import LatticeArray, RegionSlice, RegionTuple

from sam import containers as ct
from sam import lattice
from sam.solvers import Solver
from sam.solvers.refinement import BorderBase, BorderTypes


class ScalarField:
    """Basic, non-interacting scalar field."""

    # TODO: figure out good way to deal with debugging print log
    #       __debug__ with -OO interpreter flag is temporary solution

    def __init__(
        self,
        lat_array: LatticeArray,
        a: tuple[float, ...],
        dt: float,
        evolve_region: Solver,
        borders: BorderTypes,
        cusps: Callable,  # FIXME: change to BorderTypes
        d_type: npt.DTypeLike = np.float_,
    ):
        """Set up simulation environment.

        Args:
            lat_array: instance of `LatticeArray` for this sim.
            a: scaling factors t/x for each dimension x.
            dt: scaling factor for: real time = dt * sim time.
            evolve_region: implementation of Solver for one evolve step
                for a given region.
            borders: Mapping of keys to BorderBase subtypes to use.
            cusps: Callable that implements cusp refinement.
            d_type: numpy data type to use for the field values.
        """

        # the LatticeArray instance and actual lattice array
        self.lat_array = lat_array
        self.array = lat_array.array

        # all properties that LatticeArray should provide
        self.max_num_cells = ct.ResMap(self.lat_array.max_num_cells)
        # self.max_num_regs = self.lat_array.coarse_steps.value + 1
        self.res_list = self.lat_array.resolution_list
        self.max_res = self.lat_array.resolution_list[-1]
        # max power of 2, int typecast doesn't strip
        self.max_pow = int(np.log2(self.max_res))

        # dimensions of lattice
        self.max_t, self.dims = self.lat_array.tx_dims
        self.nx = self.dims[0]
        self.a = a
        self.a2 = tuple(aa * aa for aa in self.a)

        # simulation ticks
        self.t = int(0)
        # time step per tick
        self.dt = dt
        # conversion factor to real time
        # real simulation time = t * dt

        # selection of exact evolve procedures
        self.evolve_region = evolve_region
        self.borders = borders
        self.cusps = cusps

        if __debug__:
            print(
                "Configuration:",
                self.lat_array,
                self.evolve_region,
                self.borders,
                self.cusps,
                "",
                "#" * 80,
                "",
                sep="\n",
            )

        # prepare the cycle of resolutions to evolve for each time step
        tmp_sequence = np.zeros(self.max_res, dtype=np.uint16)
        for r in range(1, self.max_pow + 1):
            s = 2**r
            tmp_sequence[s - 1 :: s] = r

        self.res_evolve_cycle = [
            [2**s for s in range(0, next_res + 1)] for next_res in tmp_sequence
        ]
        self.next_iter_res: Iterator[list[int]] = cycle(self.res_evolve_cycle)
        self.iter_res_nofilter: list[int] = []
        self.iter_res: list[int] = []

        # cusp specific extra data in dicts by res.
        # one cusp has multiple entries for different res keys
        # flag to track state of cusp detection
        self.cusp_flag = ct.ResMap((k, int()) for k in self.res_list)
        # starting position of cusp
        self.cusp_p = ct.ResMap((k, float()) for k in self.res_list)
        # index of cusp in by res sorted containers
        self.cusp_idx = ct.ResMap((k, int()) for k in self.res_list)
        # reg tuple of cusp
        self.cusp_reg = ct.ResMap((k, ct.NOREGION) for k in self.res_list)
        # flag to trigger cusp swap when evolve swap not called
        self.cusp_swap = ct.ResMap((k, False) for k in self.res_list)

        # track each regions starting x position
        # in units of its resolution
        # list sorted from left to right (growing x)
        self.p_now = ct.ResRegPos((k, []) for k in self.res_list)
        self.p_pre = ct.ResRegPos((k, []) for k in self.res_list)

        # self.p = {
        #   1: [4, 10, ...]
        #   2: [10.5, 44, ...]
        #   4: ...
        # }

        # containers for sorted (by res) RegionTuples
        # pre and now are forced equal for first time step
        self.reg_by_res_pre = ct.ResRegRegionTuple.from_region_slice(
            self.res_list, self.array[0]
        )
        self.reg_by_res_now = ct.ResRegRegionTuple.from_region_slice(
            self.res_list, self.array[0]
        )

        # self.reg_by_res = {
        #   1: [(4,4,1), (10,10,1), ...]
        #   2: [(6,6,2), (4,4,2), ...]
        #   4: ...
        # }

        # set selected np dtype for data arrays
        self.d_type = d_type

        # TODO: use of buffer and its value needs fundamental proof
        # extra cells necessary for computation
        # 4 is set using two pulses next to each other
        self.buffer = 20
        # arrays for field values
        d_now = ct.ResMap(
            (k, np.zeros(num + self.buffer, dtype=self.d_type))
            for k, num in self.max_num_cells.items()
        )
        d_pre = ct.ResMap(
            (k, np.zeros(num + self.buffer, dtype=self.d_type))
            for k, num in self.max_num_cells.items()
        )

        # d = {
        #   1: np.ndarray(num^, dtype),
        #   2: np.ndarray(num^, dtype),
        #   4: ...
        # }

        # arrays for empty spots in d
        empty_now = ct.ResMap(
            (k, np.ones(d_now[k].shape[0], dtype=np.bool_)) for k in self.max_num_cells
        )
        empty_pre = ct.ResMap(
            (k, np.ones(d_pre[k].shape[0], dtype=np.bool_)) for k in self.max_num_cells
        )

        # empty = {
        #   1: np.ones(num^, dtype=np.bool_)
        #   2: np.ones(num^, dtype=np.bool_)
        #   4: ...
        # }

        # number of regions per res in first time slice
        reg_n = Counter(r.a for r in self.array[0])

        # arrays for bookkeeping u array indices
        idx_now = ct.ResRegMap(
            (k, [np.zeros(d_now[k].shape[0], dtype=np.intp) for _ in range(n)])
            for k, n in reg_n.items()
        )
        idx_pre = ct.ResRegMap(
            (k, [np.zeros(d_pre[k].shape[0], dtype=np.intp) for _ in range(n)])
            for k, n in reg_n.items()
        )

        # idx = {
        #   1: [np.ndarray(max_num, dtype=np.intp), ...],
        #   2: [np.ndarray(max_num, dtype=np.intp), ...],
        #   4: ...
        # }

        # data containers
        self.dd_now = ct.ResRegData(d_now, empty_now, idx_now)
        self.dd_pre = ct.ResRegData(d_pre, empty_pre, idx_pre)

        # generate indices for index arrays
        self._init_data_containers()

        # save refs to res's data that don't appear on array[0]
        unused_res = set(self.res_list) - set(reg_n)
        self.unused_now = ct.ResMap(
            (k, ct.Data(d_now[k], empty_now[k], ct.NONDARR(np.intp)))
            for k in unused_res
        )
        self.unused_nxt = ct.ResMap(
            (k, ct.Data(d_pre[k], empty_pre[k], ct.NONDARR(np.intp)))
            for k in unused_res
        )

        # border management
        border_res_list = self.res_list + [self.res_list[-1] * 2]
        self.border_by_res = ct.ResRegMap[BorderBase]((k, []) for k in border_res_list)
        # borders created by emerging cusps and queued for addition
        # [ (cusp_res, ins_idx, border_instance) ]
        self.cusp_ins_borders: list[tuple[int, int, BorderBase]] = []

        # res of coarser partner is key
        # self.border_by_res = {
        #   1: []       <- always empty, sentinel
        #   2: [<BorderBase>, <BorderBase>, ...]
        #   4: ...
        # max: []       <- always empty, sentinel
        # }

        # needs to be placed after other inits
        self._init_borders()

    def _init_data_containers(self):
        """Save u indices in idx arrays for slice t=0,-1."""

        start = ct.ResMap((k, int()) for k in self.res_list)
        count = ct.ResMap((k, int()) for k in self.res_list)
        reg: RegionTuple
        for reg in self.array[0]:

            # region offsets to left boundary
            offset = sum(k * v for k, v in start.items()) / reg.a
            self.p_now[reg.a].append(offset)
            self.p_pre[reg.a].append(offset)

            cnt = count[reg.a]
            count[reg.a] += 1
            st = start[reg.a]
            start[reg.a] += reg.n
            ed = start[reg.a]

            # generate indices
            self.dd_now[reg.a, cnt].idx[: reg.n] = np.arange(st, ed)
            self.dd_pre[reg.a, cnt].idx[: reg.n] = np.arange(st, ed)
            # flag array values
            self.dd_now[reg.a, cnt].empty[st:ed] = False
            self.dd_pre[reg.a, cnt].empty[st:ed] = False

    def _init_borders(self):
        """Prepare borders and fill `border_by_res`."""

        # figure out type of border for first time slice by "peeking"
        # at next time slice's borders and using resulting border types

        # skip if first time slice has no borders
        if len(self.array[0]) == 1:
            return

        # first region
        reg: RegionTuple = self.array[0][0]
        # next slice with change of reg is reg.a time steps away
        slice_nxt: RegionSlice = self.array[reg.a]
        # only has right (+1) neighbor => pass in "fake" left neighbor
        borders = BorderBase.get_border_type(
            self.borders,
            reg,
            slice_nxt[0],
            0,
            0,
            RegionTuple(reg.a + 1, -1),
            slice_nxt[1],
        )
        self.border_by_res[reg.a].extend(
            b(
                reg,
                0,
                0,
                0.0,
                self.array[0][1],
                na,
                0,
                self.a2,
                self.evolve_region,
                self.borders,
                parent=BorderBase.InitEvolve.SLICE_T0_CPY,
            )
            for b, na in borders
        )

        # prepare loop counter for next borders
        count = ct.ResMap((k, int()) for k in self.res_list)
        # account for first region
        count[reg.a] += 1

        # loop through rest of regions except for last
        for cnt, reg in enumerate(self.array[0][1:-1], 1):
            slice_nxt = self.array[reg.a]
            p_now = self.p_now[reg.a, count[reg.a]]
            # calc new p in slice_nxt
            p_nxt = sum(r.a * r.n for r in slice_nxt[:cnt]) / reg.a

            borders = BorderBase.get_border_type(
                self.borders,
                reg,
                slice_nxt[cnt],
                p_now,
                p_nxt,
                slice_nxt[cnt - 1],
                slice_nxt[cnt + 1],
            )
            # save border with data from first time slice
            self.border_by_res[reg.a].extend(
                b(
                    reg,
                    count[reg.a],
                    cnt,
                    p_now,
                    self.array[0][cnt + na],
                    na,
                    # correct count for neighbor
                    count[self.array[0][cnt + 1].a]
                    if na == 1
                    else count[self.array[0][cnt - 1].a]
                    if count[self.array[0][cnt - 1].a] == 0
                    else count[self.array[0][cnt - 1].a] - 1,
                    self.a2,
                    self.evolve_region,
                    self.borders,
                    parent=BorderBase.InitEvolve.SLICE_T0_CPY,
                )
                for b, na in borders
            )

            count[reg.a] += 1

        # last only has left (-1) neighbor => pass in "fake" right
        reg = self.array[0][-1]
        slice_nxt = self.array[reg.a]
        p_now = self.p_now[reg.a, count[reg.a]]
        p_nxt = sum(r.a * r.n for r in slice_nxt[:-1]) / reg.a
        borders = BorderBase.get_border_type(
            self.borders,
            reg,
            slice_nxt[-1],
            p_now,
            p_nxt,
            slice_nxt[-2],
            RegionTuple(reg.a + 1, -1),
        )
        neigh_reg: RegionTuple = self.array[0][-2]
        self.border_by_res[reg.a].extend(
            b(
                reg,
                count[reg.a],
                len(self.array[0]) - 1,
                p_now,
                neigh_reg,
                na,
                # correct count for neighbor
                count[neigh_reg.a]
                if count[neigh_reg.a] == 0
                else count[neigh_reg.a] - 1,
                self.a2,
                self.evolve_region,
                self.borders,
                parent=BorderBase.InitEvolve.SLICE_T0_CPY,
            )
            for b, na in borders
        )

        # no border should be entered for res == max*2
        assert not self.border_by_res[self.res_list[-1] * 2]
        # no border should be entered for res == 1
        assert 1 not in self.border_by_res or not self.border_by_res[1]

        if __debug__:
            for k, v in self.border_by_res.items():
                for i, vv in enumerate(v):
                    print(f"{i}: res={k}", vv)

            print("\n", "#" * 80, "\n", sep="")

    def __iter__(self):
        """Return self as iterator."""
        return self

    def __next__(self):
        """Implements iterator functionality as evolve steps."""
        if self.t >= self.max_t - 1:
            raise StopIteration

        # fetch resolutions for current ts
        self.iter_res_nofilter = next(self.next_iter_res)
        ts_res_list = self.lat_array.ts_resolution_list(self.t + 1)
        self.iter_res = [r for r in self.iter_res_nofilter if r in ts_res_list]
        # evolve one tick
        self.evolve()
        self.t += 1

        return self.t

    @overload
    def __getitem__(self, idx_slice: slice) -> Iterator[int]:
        ...

    @overload
    def __getitem__(self, idx_slice: int) -> int:
        ...

    def __getitem__(self, idx_slice):
        """List-like indexing of instances, returning steps."""
        if isinstance(idx_slice, int):
            if idx_slice < self.t or idx_slice >= self.max_t:
                raise IndexError(
                    f"The requested idx {idx_slice} is out of bounds. "
                    f"Use values {self.t} <= idx < {self.max_t-1}."
                )

            for _ in range(idx_slice - self.t):
                self.__next__()

            return self.t

        if isinstance(idx_slice, slice):
            start = idx_slice.start if idx_slice.start is not None else self.t + 1

            step = idx_slice.step
            if step is None:
                step = 1
            elif step < 1:
                raise IndexError("Negative stepping is not allowed.")

            stop = idx_slice.stop
            if stop is None:
                stop = self.max_t
            elif stop > self.max_t or stop <= self.t:
                raise IndexError(
                    f"The requested stop idx {stop} is out of bounds. "
                    f"Use values {self.t} <= idx < {self.max_t}."
                )

            def __gen():
                for i in range(start, stop, step):
                    yield self[i]

            return __gen()

        raise TypeError(f"Cannot index {type(self)} with {type(idx_slice)}.")

    def init_cell_averaged(
        self,
        fill: Callable[[np.ndarray], tuple[np.ndarray, np.ndarray]],
        direction: Literal[-1, 1] = 1,
    ):
        """Init field with `fill` averaging finest cells for coarser.

        Repeated use superimposes with existing values.
        """
        # factor in physical scale
        # f(x-direction*t) : t = dt * simt, x = dt / a * simx
        # based on finest region

        # eval at centers for whole x range (using correct x,t values)
        # finest res is hardcoded to 1
        x_width = self.nx / self.a[0]
        x_step = 1 / self.a[0]
        centering_offset = (1 / self.a[0] - direction) / 2
        x_arr = np.arange(centering_offset, centering_offset + x_width, step=x_step)

        # get data in finest reg res for all pre and now time slices
        idx_data = {}
        max_res = max(self.lat_array.ts_resolution_list(0))
        for t in range(-max_res, max_res, 1):
            idx_data[t] = fill((x_arr - direction * t) * self.dt)

        # average data in chunks of reg res
        count = ct.ResMap(zip(self.res_list, [0] * len(self.res_list)))
        reg: RegionTuple
        for reg in self.array[0]:

            # x position and width in lattice units
            # don't factor in sim.a as values are interpreted as indices
            x_pos = int(self.p_now[reg.a, count[reg.a]] * reg.a)
            x_width = reg.a * reg.n
            # idx_slice to limit to this regions values
            idx_slice = slice(x_pos, x_pos + x_width)

            # tmp array for this region
            avg_arr_now, avg_arr_pre = np.zeros(x_width), np.zeros(x_width)

            # avg reg.a many time slices values
            for t in range(-reg.a, reg.a, 1):

                idx, data = idx_data[t]

                # it could happend that idx returns no True values
                # get number of places to skip in data
                data_start = np.count_nonzero(idx[: idx_slice.start])
                # number of places to write
                data_num = data_start + np.count_nonzero(idx[idx_slice])
                # write to pre or now arrays
                if t < 0:
                    avg_arr = avg_arr_pre
                else:
                    avg_arr = avg_arr_now

                avg_arr[idx[idx_slice]] += data[data_start:data_num]

            # reshape avg arrays
            avg_arr_now.shape = (reg.n, reg.a)
            avg_arr_pre.shape = (reg.n, reg.a)
            # reduced slices, location of final value
            avg_arr_now_reduced = avg_arr_now[:, 0]
            avg_arr_pre_reduced = avg_arr_pre[:, 0]
            # reduce (sum) reg.a elements (the columns) in chunks
            np.add.reduce(avg_arr_now, axis=1, out=avg_arr_now_reduced)
            np.add.reduce(avg_arr_pre, axis=1, out=avg_arr_pre_reduced)
            # avg over reg.a
            avg_arr_now_reduced /= reg.a * reg.a
            avg_arr_pre_reduced /= reg.a * reg.a

            # save to sim via unbuffered in-place addition
            d_now = self.dd_now[reg.a, count[reg.a]]
            np.add.at(d_now.arr, d_now.idx[: reg.n], avg_arr_now_reduced)
            # assume same lattice shape for all t < 0 slices
            # the time slice for t == 0 is duplicated
            d_pre = self.dd_pre[reg.a, count[reg.a]]
            np.add.at(d_pre.arr, d_pre.idx[: reg.n], avg_arr_pre_reduced)

            count[reg.a] += 1

        # init border ghost cells, reuse idx_data collection
        for res_b in self.border_by_res.values():
            for b in res_b:
                # don't factor in sim.a but use as idxs for idx_data[t]
                # x_pos of border counted in finest cells
                x_pos = int(b.border_p * b.reg.a)
                x_width_c = ct.Directional(
                    b.reg.a * b.g_max_dist[b.reg.a].l,
                    b.reg.a * b.g_max_dist[b.reg.a].r,
                )
                x_width_f = ct.Directional(
                    b.neighbor.a * b.g_max_dist[b.neighbor.a].l,
                    b.neighbor.a * b.g_max_dist[b.neighbor.a].r,
                )
                idx_slice_c = slice(x_pos - x_width_c.l, x_pos + x_width_c.r)
                idx_slice_f = slice(x_pos - x_width_f.l, x_pos + x_width_f.r)
                # tmp arrays for avgs
                avg_arr_now_c = np.zeros(x_width_c.sum)
                avg_arr_pre_c = np.zeros(x_width_c.sum)
                avg_arr_now_f = np.zeros(x_width_f.sum)
                avg_arr_pre_f = np.zeros(x_width_f.sum)

                # collect data for all time slices of the ghost cells
                for t in range(0, b.reg.a, 1):
                    idx, data = idx_data[t]
                    data_start = np.count_nonzero(idx[: idx_slice_c.start])
                    data_num = data_start + np.count_nonzero(idx[idx_slice_c])
                    avg_arr_now_c[idx[idx_slice_c]] += data[data_start:data_num]

                for t in range(-b.reg.a, 0, 1):
                    idx, data = idx_data[t]
                    data_start = np.count_nonzero(idx[: idx_slice_c.start])
                    data_num = data_start + np.count_nonzero(idx[idx_slice_c])
                    avg_arr_pre_c[idx[idx_slice_c]] += data[data_start:data_num]

                for t in range(0, b.neighbor.a, 1):
                    idx, data = idx_data[t]
                    data_start = np.count_nonzero(idx[: idx_slice_f.start])
                    data_num = data_start + np.count_nonzero(idx[idx_slice_f])
                    avg_arr_now_f[idx[idx_slice_f]] += data[data_start:data_num]

                for t in range(-b.neighbor.a, 0, 1):
                    idx, data = idx_data[t]
                    data_start = np.count_nonzero(idx[: idx_slice_f.start])
                    data_num = data_start + np.count_nonzero(idx[idx_slice_f])
                    avg_arr_pre_f[idx[idx_slice_f]] += data[data_start:data_num]

                # reshape for easier avg
                avg_arr_now_c.shape = (b.g_max_dist[b.reg.a].sum, b.reg.a)
                avg_arr_pre_c.shape = (b.g_max_dist[b.reg.a].sum, b.reg.a)
                avg_arr_now_f.shape = (b.g_max_dist[b.neighbor.a].sum, b.neighbor.a)
                avg_arr_pre_f.shape = (b.g_max_dist[b.neighbor.a].sum, b.neighbor.a)
                # prepare location to store avg result
                avg_arr_now_reduced_c = avg_arr_now_c[:, 0]
                avg_arr_pre_reduced_c = avg_arr_pre_c[:, 0]
                avg_arr_now_reduced_f = avg_arr_now_f[:, 0]
                avg_arr_pre_reduced_f = avg_arr_pre_f[:, 0]
                # do avg calc
                np.add.reduce(avg_arr_now_c, axis=1, out=avg_arr_now_reduced_c)
                np.add.reduce(avg_arr_pre_c, axis=1, out=avg_arr_pre_reduced_c)
                np.add.reduce(avg_arr_now_f, axis=1, out=avg_arr_now_reduced_f)
                np.add.reduce(avg_arr_pre_f, axis=1, out=avg_arr_pre_reduced_f)
                avg_arr_now_reduced_c /= b.reg.a * b.reg.a
                avg_arr_pre_reduced_c /= b.reg.a * b.reg.a
                avg_arr_now_reduced_f /= b.neighbor.a * b.neighbor.a
                avg_arr_pre_reduced_f /= b.neighbor.a * b.neighbor.a
                # roll arrays to match order expected by border
                avg_arr_now_reduced_c = np.roll(
                    avg_arr_now_reduced_c, -b.g_max_dist[b.reg.a].l
                )
                avg_arr_pre_reduced_c = np.roll(
                    avg_arr_pre_reduced_c, -b.g_max_dist[b.reg.a].l
                )
                avg_arr_now_reduced_f = np.roll(
                    avg_arr_now_reduced_f, -b.g_max_dist[b.neighbor.a].l
                )
                avg_arr_pre_reduced_f = np.roll(
                    avg_arr_pre_reduced_f, -b.g_max_dist[b.neighbor.a].l
                )
                # masks where arr is non-zero
                x_mask_now_c = np.asarray(avg_arr_now_reduced_c, dtype=np.bool_)
                x_mask_pre_c = np.asarray(avg_arr_pre_reduced_c, dtype=np.bool_)
                x_mask_now_f = np.asarray(avg_arr_now_reduced_f, dtype=np.bool_)
                x_mask_pre_f = np.asarray(avg_arr_pre_reduced_f, dtype=np.bool_)

                # populate ghost cells, only pass data where nonzero
                b.init_ghost_cells(
                    x_mask_now_c,
                    avg_arr_now_reduced_c[x_mask_now_c],
                    x_mask_pre_c,
                    avg_arr_pre_reduced_c[x_mask_pre_c],
                    x_mask_now_f,
                    avg_arr_now_reduced_f[x_mask_now_f],
                    x_mask_pre_f,
                    avg_arr_pre_reduced_f[x_mask_pre_f],
                )

    def init_cell_centered(
        self,
        fill: Callable[[np.ndarray], tuple[np.ndarray, np.ndarray]],
        direction: Literal[-1, 1] = 1,
    ):
        """Init field with `fill` evaluated always at cell centers.

        Repeated use superimposes with existing values.
        """
        # factor in physical scale
        # f(x-direction*t) : t = dt * simt, x = dt / a * simx
        # based on finest region

        count = ct.ResMap(zip(self.res_list, [0] * len(self.res_list)))
        reg: RegionTuple
        for reg in self.array[0]:

            # x pos (of lower left corner) and width in finest units
            x_pos = self.p_now[reg.a, count[reg.a]] * reg.a / self.a[0]
            x_width = reg.a * reg.n / self.a[0]
            x_step = reg.a / self.a[0]

            # eval at centers for each cell (using correct x,t values)
            centering_offset = reg.a * (1 / self.a[0] - direction) / 2
            x_arr = np.arange(
                x_pos + centering_offset,
                x_pos + centering_offset + x_width,
                step=x_step,
            )

            # time slice t = 0
            # save to sim via unbuffered in-place addition
            f_idx, data = fill(x_arr * self.dt)
            d_now = self.dd_now[reg.a, count[reg.a]]
            d_now_idx = d_now.idx[: reg.n][f_idx]
            np.add.at(d_now.arr, d_now_idx, data)

            # time slice t = - reg.a
            # assume same lattice shape for all t < 0 slices
            # the time slice for t == 0 is duplicated
            f_idx, data = fill((x_arr + direction * reg.a) * self.dt)
            d_pre = self.dd_pre[reg.a, count[reg.a]]
            d_pre_idx = d_pre.idx[: reg.n][f_idx]
            np.add.at(d_pre.arr, d_pre_idx, data)

            count[reg.a] += 1

        # init border ghost cells
        for res_b in self.border_by_res.values():
            for b in res_b:

                # x pos of border and width for coarse and fine cells
                x_pos = b.border_p * b.reg.a / self.a[0]
                x_width_c = ct.Directional(
                    b.reg.a * b.g_max_dist[b.reg.a].l / self.a[0],
                    b.reg.a * b.g_max_dist[b.reg.a].r / self.a[0],
                )
                x_width_f = ct.Directional(
                    b.neighbor.a * b.g_max_dist[b.neighbor.a].l / self.a[0],
                    b.neighbor.a * b.g_max_dist[b.neighbor.a].r / self.a[0],
                )
                x_step_c = b.reg.a / self.a[0]
                x_step_f = b.neighbor.a / self.a[0]

                # generate x array at cell centers
                # 0 -> max_dist count right from border
                # -1 -> -max_dist count left from border
                centering_offset_c = b.reg.a * (1 / self.a[0] - direction) / 2
                centering_offset_f = b.neighbor.a * (1 / self.a[0] - direction) / 2
                x_arr_c = np.arange(
                    x_pos + centering_offset_c - x_width_c.l,
                    x_pos + centering_offset_c + x_width_c.r,
                    step=x_step_c,
                )
                x_arr_c = np.roll(x_arr_c, -b.g_max_dist[b.reg.a].l)
                x_arr_f = np.arange(
                    x_pos + centering_offset_f - x_width_f.l,
                    x_pos + centering_offset_f + x_width_f.r,
                    step=x_step_f,
                )
                x_arr_f = np.roll(x_arr_f, -b.g_max_dist[b.neighbor.a].l)

                # evaluate filler
                d_now_c = fill(x_arr_c * self.dt)
                d_now_f = fill(x_arr_f * self.dt)
                d_pre_c = fill((x_arr_c + direction * b.reg.a) * self.dt)
                d_pre_f = fill((x_arr_f + direction * b.neighbor.a) * self.dt)

                # populate ghost cells
                b.init_ghost_cells(*d_now_c, *d_pre_c, *d_now_f, *d_pre_f)

    @staticmethod
    def split_idx(idx: npt.NDArray[np.intp], reg: RegionTuple):
        """Split `xidx` matching to `reg` and return rest."""

        idx_post = idx.copy()
        idx[reg.n :] = 0
        idx_post[: reg.n] = 0
        idx_post = np.roll(idx_post, -reg.n)
        return idx_post

    def fetch_idx(self, data: ct.Data, shape, width):
        """Return idx for new `width` number of cells with `res`."""

        # this is replacement for sentinel index array
        assert np.array_equal(data.idx, ct.NONDARR(np.intp))

        x_new = np.zeros(shape, dtype=np.intp)
        # FIXME: make method of Data
        # FIXME: instead of loop, use
        # argmax = np.nonzero(u_empty[res])[:width]
        # assert argmax not empty
        # u_empty[res][argmax] = False
        # x_new[:width] = argmax
        for i in range(width):
            assert True in data.empty
            # fetch new space in u array for cusp
            argmax = data.empty.argmax()
            data.empty[argmax] = False
            x_new[i] = argmax

        return x_new

    def _add_emerging_cusp(
        self,
        res: int,
        num,
        p_pre,
        p_now,
        p_nxt,
        reg_pre: RegionTuple,
        reg_now: RegionTuple,
        reg_nxt: RegionTuple,
        dd_nxt: ct.ResRegData,
        idx_pre: npt.NDArray[np.intp],
        idx_now: npt.NDArray[np.intp],
        idx_nxt: npt.NDArray[np.intp],
        p_nxt_dict: ct.ResRegPos,
        reg_by_res_nxt: ct.ResRegRegionTuple,
    ):
        # new width and p of now
        reg_now, reg_now_post, p_now_post = lattice.calc_reg_change(
            p_now, p_nxt, reg_now, reg_nxt, offset=2
        )
        self.reg_by_res_now[res].insert(num + 1, reg_now_post)
        self.reg_by_res_now[res, num] = reg_now
        self.p_now[res].insert(num + 1, p_now_post)

        # new width and p of pre
        reg_pre, reg_pre_post, p_pre_post = lattice.calc_reg_change(
            p_pre, p_now, reg_pre, reg_now, offset=0
        )
        self.reg_by_res_pre[res].insert(num + 1, reg_pre_post)
        self.reg_by_res_pre[res, num] = reg_pre
        self.p_pre[res].insert(num + 1, p_pre_post)

        # split idx_now
        idx_now_post = self.split_idx(idx_now, reg_now)
        d_n_now = self.dd_now[res, num]
        self.dd_now[res].insert(
            num + 1, ct.Data(d_n_now.arr, d_n_now.empty, idx_now_post)
        )
        # split idx pre
        x_pre_post = self.split_idx(idx_pre, reg_pre)
        d_n_pre = self.dd_pre[res, num]
        self.dd_pre[res].insert(
            num + 1, ct.Data(d_n_pre.arr, d_n_pre.empty, x_pre_post)
        )
        # split idx nxt
        # use reg_pre, because idx_nxt is copy of pre and
        # index adjustments take place in evolve step
        idx_nxt_post = self.split_idx(idx_nxt, reg_pre)
        d_n_nxt = dd_nxt[res, num]
        dd_nxt[res].insert(num + 1, ct.Data(d_n_nxt.arr, d_n_nxt.empty, idx_nxt_post))

        # save cusp data
        cusp_res = 2 * res
        idx, p = lattice.get_insertion_idx(p_nxt_dict[cusp_res], reg_nxt, p_nxt)
        self.cusp_idx[cusp_res] = idx
        self.cusp_p[cusp_res] = p
        self.cusp_reg[cusp_res] = reg_by_res_nxt[cusp_res, idx]

        # insert cusp's data at idx
        try:
            self.reg_by_res_pre[cusp_res].insert(idx, ct.NOREGION)
            self.reg_by_res_now[cusp_res].insert(idx, ct.NOREGION)
            self.p_pre[cusp_res].insert(idx, ct.NOPOS)
            self.p_now[cusp_res].insert(idx, ct.NOPOS)

            # NOTE:
            # very implementation specific!
            # get any data of same res for references to arr, empty
            # using idx-1 to account for idx == len(dd[cusp_res])

            n_d_nxt = dd_nxt[cusp_res, idx - 1]
            d_nxt = ct.Data(n_d_nxt.arr, n_d_nxt.empty, ct.NONDARR(np.intp))
            d_nxt.idx = self.fetch_idx(
                d_nxt, self.max_num_cells[cusp_res] + self.buffer, 2
            )
            dd_nxt[cusp_res].insert(idx, d_nxt)

            n_d_now = self.dd_now[cusp_res, idx - 1]
            self.dd_now[cusp_res].insert(
                idx, ct.Data(n_d_now.arr, n_d_now.empty, ct.NONDARR(np.intp))
            )

            n_d_pre = self.dd_pre[cusp_res, idx - 1]
            self.dd_pre[cusp_res].insert(
                idx, ct.Data(n_d_pre.arr, n_d_pre.empty, ct.NONDARR(np.intp))
            )

        except KeyError:
            # this is the first data for this res
            self.reg_by_res_pre[cusp_res] = [ct.NOREGION]
            self.reg_by_res_now[cusp_res] = [ct.NOREGION]
            self.p_pre[cusp_res] = [ct.NOPOS]
            self.p_now[cusp_res] = [ct.NOPOS]

            d_nxt = self.unused_nxt.pop(cusp_res)
            d_nxt.idx = self.fetch_idx(
                d_nxt, self.max_num_cells[cusp_res] + self.buffer, 2
            )
            dd_nxt[cusp_res] = ct.RegData([d_nxt])
            self.dd_now[cusp_res] = ct.RegData([self.unused_now.pop(cusp_res)])
            # new Data instance with same array references as nxt
            self.dd_pre[cusp_res] = ct.RegData(
                [ct.Data(d_nxt.arr, d_nxt.empty, ct.NONDARR(np.intp))]
            )

        # arm cusp flag
        self.cusp_flag[res] = 1

    def _emerging_cusp_borders(
        self, res, p_nxt: ct.ResRegPos, reg_by_res_nxt: ct.ResRegRegionTuple
    ):
        # attach new borders to cusp

        cusp_res = 2 * res
        cusp_idx = self.cusp_idx[cusp_res]
        cusp_reg = self.cusp_reg[cusp_res]
        assert cusp_reg == reg_by_res_nxt[cusp_res, cusp_idx]

        # count for sort_idx
        count = ct.ResMap((k, 0) for k in self.res_list)
        reg: RegionTuple
        for arr_idx, reg in enumerate(self.array[self.t + 1]):
            # find the exact region of the cusp
            if reg == cusp_reg and count[reg.a] == cusp_idx:

                if arr_idx == 0:
                    raise NotImplementedError
                    # cusp is first region
                    BorderBase.border_ins_update(
                        cusp_res, res, arr_idx, self.border_by_res.values()
                    )
                    # TODO: cusp instance missing, proper init args
                    neigh_reg: RegionTuple = self.array[self.t + 1][1]
                    neigh_sort_idx = 0
                    self.cusp_ins_borders = [
                        (
                            cusp_res,
                            0,
                            # only right moving border possible
                            self.borders["DFTCR"](
                                cusp_reg,
                                cusp_idx,
                                arr_idx,
                                p_nxt[cusp_res, cusp_idx],
                                neigh_reg,
                                +1,
                                neigh_sort_idx,
                                self.a2,
                                self.evolve_region,
                                self.borders,
                                skip=True,
                            ),
                        )
                    ]
                    return

                if arr_idx == len(self.array[self.t + 1]) - 1:
                    raise NotImplementedError
                    # cusp is last region, no border updates necessary
                    neigh_reg = self.array[self.t + 1][-2]
                    neigh_sort_idx = (
                        count[neigh_reg.a]
                        if count[neigh_reg.a] == 0
                        else count[neigh_reg.a] - 1
                    )
                    # TODO: cusp instance missing & proper init args
                    self.cusp_ins_borders = [
                        (
                            cusp_res,
                            len(self.border_by_res[cusp_res]),
                            # only left moving border possible
                            self.borders["DFTCL"](
                                cusp_reg,
                                cusp_idx,
                                arr_idx,
                                p_nxt[cusp_res, cusp_idx],
                                neigh_reg,
                                -1,
                                neigh_sort_idx,
                                self.a2,
                                self.evolve_region,
                                self.borders,
                                skip=True,
                            ),
                        )
                    ]
                    return

                # cusp is somewhere in the middle of the time slice
                # get insertion index
                ins = BorderBase.get_ins_idx(cusp_idx, self.border_by_res[cusp_res])
                assert ins == cusp_idx
                # then update existing borders
                BorderBase.border_ins_update(
                    cusp_res, res, arr_idx, self.border_by_res.values()
                )

                # fetch l(eft) and r(ight) neighbor regions
                neigh_l_reg: RegionTuple = self.array[self.t + 1][arr_idx - 1]
                neigh_l_sort_idx = (
                    count[neigh_l_reg.a]
                    if count[neigh_l_reg.a] == 0
                    else count[neigh_l_reg.a] - 1
                )

                neigh_r_reg: RegionTuple = self.array[self.t + 1][arr_idx + 1]
                neigh_r_sort_idx = count[neigh_r_reg.a]

                # emerging cusp is always insertion of coarser region
                # thus, each cusp has two borders on each side
                # the border instances
                border_l = self.borders["DFTCL"](
                    cusp_reg,
                    cusp_idx,
                    arr_idx,
                    p_nxt[cusp_res, cusp_idx],
                    neigh_l_reg,
                    -1,
                    neigh_l_sort_idx,
                    self.a2,
                    self.evolve_region,
                    self.borders,
                    skip=True,
                )
                border_r = self.borders["DFTCR"](
                    cusp_reg,
                    cusp_idx,
                    arr_idx,
                    p_nxt[cusp_res, cusp_idx],
                    neigh_r_reg,
                    +1,
                    neigh_r_sort_idx,
                    self.a2,
                    self.evolve_region,
                    self.borders,
                    skip=True,
                )
                cusp = self.borders["CFTC"](
                    cusp_reg,
                    cusp_idx,
                    arr_idx,
                    p_nxt[cusp_res, cusp_idx],
                    neigh_l_reg,
                    -1,
                    neigh_l_sort_idx,
                    self.a2,
                    self.evolve_region,
                    self.borders,
                    parent=(border_l, border_r),
                )

                # first the cusp, then the borders
                # so that ins idx refers to cusp instance
                self.cusp_ins_borders.extend(
                    [
                        (cusp_res, ins, cusp),
                        (cusp_res, ins + 1, border_l),
                        (cusp_res, ins + 2, border_r),
                    ]
                )
                return

            count[reg.a] += 1

    def _dissolve_cusp(self, res, dd_nxt, reg_by_res_nxt):
        cusp_res = 2 * res
        cusp_idx = self.cusp_idx[cusp_res]
        cusp_reg = self.reg_by_res_now[cusp_res, cusp_idx]

        # emerging cusp
        idx = self.cusp_idx[cusp_res]
        # TBD: if new cusp is inserted at next time step,
        # (somewhere left of this cusp) then cusp_idx is wrong
        # it's either 2 or 4 right now
        cell_num = min(reg_by_res_nxt[cusp_res, idx].n, 4)
        # replace sentinel NONDARR entry
        dd_nxt[cusp_res, idx].idx = self.fetch_idx(
            dd_nxt[cusp_res, idx],
            self.max_num_cells[cusp_res] + self.buffer,
            cell_num,
        )

        b_idx = BorderBase.get_del_idx(cusp_reg, cusp_idx, self.border_by_res[cusp_res])
        assert b_idx == cusp_idx
        # delete cusp
        if __debug__:
            print(
                f"\tDL >> {b_idx}: res={cusp_res} {self.border_by_res[cusp_res, b_idx]}"
            )

        del self.border_by_res[cusp_res, self.cusp_idx[cusp_res]]

    @staticmethod
    def _insert_sentinels(
        reg_by_res_nxt: ct.ResRegRegionTuple, p_nxt_dict: ct.ResRegPos, res, idx
    ):
        try:
            reg_by_res_nxt[res].insert(idx, ct.NOREGION)
            p_nxt_dict[res].insert(idx, ct.NOPOS)
        except KeyError:
            # this is the first entry with res in nxt
            reg_by_res_nxt[res] = [ct.NOREGION]
            p_nxt_dict[res] = [ct.NOPOS]

    def _split_disappearing_cusp(
        self,
        res,
        num,
        p_now,
        p_nxt,
        reg_now: RegionTuple,
        reg_nxt: RegionTuple,
        p_nxt_dict: ct.ResRegPos,
        reg_by_res_nxt: ct.ResRegRegionTuple,
    ):
        cusp_res = 2 * res
        idx, p = lattice.get_insertion_idx(self.p_now[cusp_res], reg_now, p_now)
        self.cusp_idx[res] = num
        self.cusp_idx[cusp_res] = idx
        self.cusp_p[cusp_res] = p
        self.cusp_reg[cusp_res] = self.reg_by_res_pre[cusp_res, idx]
        self.cusp_swap[cusp_res] = cusp_res not in self.iter_res

        # insert sentinels into nxt
        self._insert_sentinels(reg_by_res_nxt, p_nxt_dict, cusp_res, idx)

        # split nxt for merging region
        reg_nxt, reg_nxt_post, p_nxt_post = lattice.calc_reg_change(
            p_nxt, p_now, reg_nxt, reg_now, offset=2
        )
        reg_by_res_nxt[res].insert(num + 1, reg_nxt_post)
        reg_by_res_nxt[res, num] = reg_nxt
        p_nxt_dict[res].insert(num + 1, p_nxt_post)

        # arm cusp flag
        self.cusp_flag[res] = -2

        return (
            p_nxt_dict[res, num + 1] / 2,  # in res of cusp
            tuple(reg_by_res_nxt[res, num : num + 2]),
            (-1, 1),
            (num, num + 1),
        )

    def find_cusps(
        self,
        reg_by_res_nxt: ct.ResRegRegionTuple,
        dd_nxt: ct.ResRegData,
        p_nxt_dict: ct.ResRegPos,
    ):
        """Detect cusps for next time slices and do array magic."""
        # NOTE:
        # This logic only works if there is a single cusp with double
        # cell tip in a specific time slice and its neighboring next
        # and previous 2 time slices don't have changing number of
        # regions per time slice (slices measured in embedded res)
        # TODO: Detect cusps at border of lattice
        # for asymmetric border-cusp a change of 1 in regs per timeslice
        # for symmetric border-cusp (on both ends of the lattice) it's
        # a change of 2 in regs per timeslice, just like cups!
        # so requires different logic to detect

        # change in numbers of regs per time slice
        if abs(len(self.array[self.t]) - len(self.array[self.t + 1])) == 2:
            # iteration has to be ascending, or cusps trigger errors
            for res in self.iter_res:
                # num counts index in sorted containers
                for num, (reg_nxt, reg_now) in enumerate(
                    zip(reg_by_res_nxt[res], self.reg_by_res_now[res])
                ):
                    p_nxt = p_nxt_dict[res, num]
                    p_now = self.p_now[res, num]
                    p_pre = self.p_pre[res, num]

                    reg_pre = self.reg_by_res_pre[res, num]

                    idx_nxt = dd_nxt[res, num].idx
                    idx_now = self.dd_now[res, num].idx
                    idx_pre = self.dd_pre[res, num].idx

                    # 4 is width of cusp in embedded region
                    # correct for offset in region start position
                    # reg end cannot be corrected, as cusp is in the way
                    cusp_width = p_now - p_nxt

                    # emerging cusp
                    if (diff := reg_nxt.n - reg_now.n) <= -4 + cusp_width:
                        self._add_emerging_cusp(
                            res,
                            num,
                            p_pre,
                            p_now,
                            p_nxt,
                            reg_pre,
                            reg_now,
                            reg_nxt,
                            dd_nxt,
                            idx_pre,
                            idx_now,
                            idx_nxt,
                            p_nxt_dict,
                            reg_by_res_nxt,
                        )
                        self._emerging_cusp_borders(res, p_nxt_dict, reg_by_res_nxt)
                        return

                    # disappearing cusp
                    if diff >= 4 + cusp_width:
                        self._disappearing_cusp_borders(
                            res,
                            *self._split_disappearing_cusp(
                                res,
                                num,
                                p_now,
                                p_nxt,
                                reg_now,
                                reg_nxt,
                                p_nxt_dict,
                                reg_by_res_nxt,
                            ),
                        )
                        return

    def _merge_after_cusp(
        self,
        res,
        num,
        reg_pre: RegionTuple,
        reg_now: RegionTuple,
        reg_pre_post: RegionTuple,
        reg_now_post: RegionTuple,
        dd_nxt: ct.ResRegData,
    ):
        # the p
        del self.p_pre[res, num + 1]
        del self.p_now[res, num + 1]

        # the regs
        new_pre_width = reg_pre.n + reg_pre_post.n
        self.reg_by_res_pre[res, num] = RegionTuple(reg_pre.a, new_pre_width)
        new_now_width = reg_now.n + reg_now_post.n
        self.reg_by_res_now[res, num] = RegionTuple(reg_now.a, new_now_width)
        del self.reg_by_res_pre[res, num + 1]
        del self.reg_by_res_now[res, num + 1]

        # merge index and data
        idx_pre = self.dd_pre[res, num].idx
        idx_pre_post = self.dd_pre[res, num + 1].idx
        idx_now = self.dd_now[res, num].idx
        idx_now_post = self.dd_now[res, num + 1].idx
        idx_nxt = dd_nxt[res, num].idx
        idx_nxt_post = dd_nxt[res, num + 1].idx

        np.copyto(idx_pre[reg_pre.n : new_pre_width], idx_pre_post[0 : reg_pre_post.n])
        del self.dd_pre[res, num + 1]
        np.copyto(idx_now[reg_now.n : new_now_width], idx_now_post[0 : reg_now_post.n])
        del self.dd_now[res, num + 1]
        # at this point nxt is still copy of pre
        np.copyto(idx_nxt[reg_pre.n : new_pre_width], idx_nxt_post[0 : reg_pre_post.n])
        del dd_nxt[res, num + 1]

    def _del_cusp(self, res, dd_nxt: ct.ResRegData):
        # remove cusp at res*2
        cusp_res = 2 * res
        # this 2*res is forced swapped!
        idx = self.cusp_idx[cusp_res]
        # free data
        # nxt is copy of pre:
        #   nxt==pre->now, now->pre, nxt == pre
        # for u: pre<->now
        del dd_nxt[cusp_res, idx]
        np.put(
            self.dd_now[cusp_res, idx].empty,
            self.dd_now[cusp_res, idx].idx[: self.cusp_reg[cusp_res].n],
            True,
        )
        del self.dd_now[cusp_res, idx]
        np.put(
            self.dd_pre[cusp_res, idx].empty,
            self.dd_pre[cusp_res, idx].idx[: self.reg_by_res_pre[cusp_res, idx].n],
            True,
        )
        del self.dd_pre[cusp_res, idx]
        # del from all other containers
        del self.reg_by_res_pre[cusp_res, idx]
        del self.reg_by_res_now[cusp_res, idx]
        del self.p_pre[cusp_res, idx]
        del self.p_now[cusp_res, idx]

    def _disappearing_cusp_borders(
        self, res, p_nxt_cusp, n_regs, n_arr_offsets, n_sort_idxs
    ):
        cusp_res = 2 * res
        cusp_idx = self.cusp_idx[cusp_res]
        cusp_reg = self.reg_by_res_now[cusp_res, cusp_idx]

        b_idx = BorderBase.get_del_idx(cusp_reg, cusp_idx, self.border_by_res[cusp_res])
        assert b_idx == cusp_idx
        arr_idx = self.border_by_res[cusp_res, b_idx].arr_idx

        if __debug__:
            print(
                f"\tDL >> {b_idx}: res={cusp_res} {self.border_by_res[cusp_res, b_idx]}"
            )
            print(
                f"\tDL >> {b_idx+1}: res={cusp_res} {self.border_by_res[cusp_res, b_idx+1]}"
            )

        # there are always 2 neighboring borders for a cusp
        b1 = self.border_by_res[cusp_res].pop(b_idx)
        # b_idx correct, len was reduced by 1
        b2 = self.border_by_res[cusp_res].pop(b_idx)

        border_n_regs = (
            self.reg_by_res_now[res, b1.neigh_by_res_idx],
            self.reg_by_res_now[res, b2.neigh_by_res_idx],
        )
        # make new cusp border
        self.border_by_res[cusp_res].insert(
            cusp_idx,
            self.borders["CCTF"](
                border_n_regs,
                cusp_idx,
                arr_idx,
                p_nxt_cusp,
                n_regs,
                n_arr_offsets,
                n_sort_idxs,
                self.a2,
                self.evolve_region,
                self.borders,
                parent=(b1, b2),
            ),
        )

        if __debug__:
            print(
                f"\tAD >> {b_idx}: res={cusp_res} {self.border_by_res[cusp_res, cusp_idx]}"
            )

    def cusp_cleanup(
        self,
        reg_by_res_nxt: ct.ResRegRegionTuple,
        dd_nxt: ct.ResRegData,
        p_nxt_dict: ct.ResRegPos,
    ):
        """Perform leftover steps for cusp detection and cleanup."""

        # only if res is in current iter_res
        for res, flag in (
            kv for kv in self.cusp_flag.items() if kv[0] in self.iter_res
        ):
            if flag == 1:
                # emerging cusp, first step
                cusp_res = 2 * res
                cusp = self.border_by_res[cusp_res, self.cusp_idx[cusp_res]]
                # arm borders to step +2:1
                for b in cusp.parent:
                    b.skip = False
                    next(b.evolve_cycle)

                # update flag
                self.cusp_flag[res] = 2

            elif flag == 2:
                # emerging cusp, last step
                self._dissolve_cusp(res, dd_nxt, reg_by_res_nxt)
                # update flag
                self.cusp_flag[res] = 0

            elif flag == -1:
                # disappearing cusp, last step
                num = self.cusp_idx[res]
                reg_pre = self.reg_by_res_pre[res, num]
                reg_pre_post = self.reg_by_res_pre[res, num + 1]
                reg_now = self.reg_by_res_now[res, num]
                reg_now_post = self.reg_by_res_now[res, num + 1]

                self._merge_after_cusp(
                    res, num, reg_pre, reg_now, reg_pre_post, reg_now_post, dd_nxt
                )
                self._del_cusp(res, dd_nxt)

                # update other borders' lattice data
                cusp_res = 2 * res
                arr_idx = self.border_by_res[cusp_res, self.cusp_idx[cusp_res]].arr_idx
                BorderBase.border_del_update(
                    cusp_res, res, arr_idx, self.border_by_res.values()
                )

                # remove cusp from borders
                del self.border_by_res[cusp_res, self.cusp_idx[cusp_res]]

                # update flag
                self.cusp_flag[res] = 0

            elif flag == -2:
                # disappearing cusp, first step
                cusp_res = 2 * res
                # re-insert sentinels into nxt slice
                # this swap is delayed, new nxt data is already here
                # and has to be corrected again
                self._insert_sentinels(
                    reg_by_res_nxt, p_nxt_dict, cusp_res, self.cusp_idx[cusp_res]
                )

                # force a swap if 2*res not in iter_res
                if self.cusp_swap[cusp_res]:
                    self.swap(cusp_res, reg_by_res_nxt, dd_nxt, p_nxt_dict)

                # disappearing cusp, first step
                num = self.cusp_idx[res]
                p_nxt = p_nxt_dict[res, num]
                p_now = self.p_now[res, num]
                reg_nxt = reg_by_res_nxt[res, num]
                reg_now = self.reg_by_res_now[res, num]

                # new region
                reg_nxt, reg_nxt_post, p_nxt_post = lattice.calc_reg_change(
                    p_nxt, p_now, reg_nxt, reg_now, offset=0
                )
                reg_by_res_nxt[res].insert(num + 1, reg_nxt_post)
                reg_by_res_nxt[res, num] = reg_nxt
                p_nxt_dict[res].insert(num + 1, p_nxt_post)

                # update flag
                self.cusp_flag[res] = -1

    def border_update(
        self, reg_by_res_nxt: ct.ResRegRegionTuple, p_nxt_dict: ct.ResRegPos
    ):
        """Identify border types for next evolve step."""
        for res in self.iter_res:
            for b_idx, border in enumerate(self.border_by_res[res]):
                idx = border.by_res_idx
                n_idx = border.neigh_by_res_idx
                n = border.neighbor
                # update borders or replace on change
                if borders := border.border_change(
                    reg_by_res_nxt[res, idx],
                    p_nxt_dict[res, idx],
                    reg_by_res_nxt[n.a, n_idx],
                ):
                    self.border_by_res[res, b_idx] = borders[0](
                        *borders[1], **borders[2]
                    )
                    if __debug__:
                        print(
                            f"\tCH >> {b_idx}: res={res} {self.border_by_res[res, b_idx]}"
                        )
                else:
                    if __debug__:
                        print(f"\t   >> {b_idx}: res={res} {border}")

        # add new emerging cusp
        for addition in self.cusp_ins_borders:
            self.border_by_res[addition[0]].insert(*addition[1:])
            if __debug__:
                print(f"\tAD >> {addition[1]}: {addition[2]}")

        self.cusp_ins_borders.clear()

    def swap(
        self,
        res: int,
        reg_by_res_nxt: ct.ResRegRegionTuple,
        dd_nxt: ct.ResRegData,
        p_nxt_dict: ct.ResRegPos,
    ):
        """Swap data pointers for `res`."""

        self.dd_pre[res], self.dd_now[res] = self.dd_now[res], dd_nxt[res]
        self.p_now[res], self.p_pre[res] = p_nxt_dict[res], self.p_now[res]
        self.reg_by_res_now[res], self.reg_by_res_pre[res] = (
            reg_by_res_nxt[res],
            self.reg_by_res_now[res],
        )

    def evolve(self):
        """Evolve one time step."""

        if __debug__:
            print(f"EVOLVE t={self.t}")

        # prepare values for next time slice
        # dd_nxt at this time just copy of dd_pre
        reg_by_res_nxt, dd_nxt, p_nxt_dict = ct.prep_nxt_ts(
            self.t, self.array, self.dd_pre, self.res_list
        )

        # side effect: modifies dd_nxt.empty, args and self.cusp{**}
        self.cusp_cleanup(reg_by_res_nxt, dd_nxt, p_nxt_dict)

        # side effect: modifies self data and args in place
        self.find_cusps(reg_by_res_nxt, dd_nxt, p_nxt_dict)

        # side effect: modifies border_by_res
        self.border_update(reg_by_res_nxt, p_nxt_dict)

        # loop for border and cusp refinement and dd_nxt prep
        # reverse order required for border refinement
        for res in reversed(self.iter_res):

            # FIXME: remove this one
            # index correction counter
            idx_corr = int()

            # data arr positions to free
            free: list[list[int]] = []

            for (
                reg_pre,
                reg_now,
                reg_nxt,
                p_pre,
                p_now,
                p_nxt,
                d_pre,
                d_now,
                d_nxt,
            ) in zip(
                self.reg_by_res_pre[res],
                self.reg_by_res_now[res],
                reg_by_res_nxt[res],
                self.p_pre[res],
                self.p_now[res],
                p_nxt_dict[res],
                self.dd_pre[res],
                self.dd_now[res],
                dd_nxt[res],
            ):
                # cusp regions
                # FIXME: test or assert also against reg_zzz
                if ct.NOPOS in [p_pre, p_now, p_nxt]:
                    # dd_nxt was already repaired by find_cusps
                    idx_corr += 1
                    free.append([])
                    continue

                # prepare index for next time slice
                # changes d_nxt.idx in place
                free.append(lattice.update_nxt(d_nxt, reg_pre, reg_nxt, p_pre, p_nxt))
                idx_corr += 1

                # border refinement needs new dd_nxt values
                # therefore, iter_res needs to be reversed
                # so that reg and neighbor have updated values
                b_res = res * 2
                n_idx = idx_corr - 1  # each reg appends one
                # use reg_nxt if border has been already updated
                n_reg = reg_nxt if b_res in self.iter_res else reg_now
                # all borders whose neighbor is current reg
                res_borders = [
                    b
                    for b in self.border_by_res[b_res]
                    if b.neighbor == n_reg and b.neigh_by_res_idx == n_idx
                ]
                for border in res_borders:
                    if __debug__:
                        print(
                            f"\tEV >> {border.by_res_idx}: res={border.reg.a} {border}"
                        )

                    border.evolve(
                        self.reg_by_res_pre[b_res, border.by_res_idx],
                        reg_pre,
                        self.reg_by_res_now[b_res, border.by_res_idx],
                        reg_now,
                        border.reg,  # has been updated
                        reg_nxt,
                        self.dd_pre[b_res, border.by_res_idx],
                        d_pre,
                        self.dd_now[b_res, border.by_res_idx],
                        d_now,
                        dd_nxt[b_res, border.by_res_idx],
                        d_nxt,
                    )

            # free locations after all same res regions' data is ready
            # after all alloc and idx shifting is done
            lattice.free_nxt(dd_nxt[res], free)

        # loop for standard evolve regions
        for res in self.iter_res:

            for (
                reg_pre,
                reg_now,
                reg_nxt,
                p_pre,
                p_now,
                p_nxt,
                d_pre,
                d_now,
                d_nxt,
            ) in zip(
                self.reg_by_res_pre[res],
                self.reg_by_res_now[res],
                reg_by_res_nxt[res],
                self.p_pre[res],
                self.p_now[res],
                p_nxt_dict[res],
                self.dd_pre[res],
                self.dd_now[res],
                dd_nxt[res],
            ):
                # skip on cusp regions
                # FIXME: test or assert also against reg_zzz
                if ct.NOPOS in [p_pre, p_now, p_nxt]:
                    continue

                # get position differences for start and end of reg
                p_ds_nxt, p_de_nxt = lattice.position_diff(
                    p_now, p_nxt, reg_now, reg_nxt
                )
                p_ds_pre, p_de_pre = lattice.position_diff(
                    p_now, p_pre, reg_now, reg_pre
                )

                # limits for simple sweep
                # TODO: other update stencils might require more
                #       neighbor cells. make this variable!
                xmin = max(1, -p_ds_nxt, -p_ds_pre)
                xmax = min(-1, -p_de_nxt, -p_de_pre)

                # calculate new field values: EVOLVE step
                self.evolve_region(
                    self.a2,
                    xmin,
                    xmax,
                    p_ds_pre,
                    p_ds_nxt,
                    reg_now,
                    reg_nxt,
                    d_pre,
                    d_now,
                    d_nxt,
                )

            # pointer swap for current res (only done in evolve loop)
            self.swap(res, reg_by_res_nxt, dd_nxt, p_nxt_dict)
